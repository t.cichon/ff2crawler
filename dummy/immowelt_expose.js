module.exports.data = `

<!DOCTYPE html>

<html class="desktop">

<head>
    <title>Ansehen: frisch modernisiertes Wohngl&#252;ck Etagenwohnung M&#252;nster (2LE9G4H)</title>
    <meta name="description" content="Etagenwohnung in M&#252;nster (Mauritz) zur Miete mit 2 Zimmer und 49,19 m&#178; Wohnfl&#228;che. Ausstattung: Elektro, Gas, Zentralheizung, Kelleranteil." />
    <meta name="theme-color" content="#ffc133" />

            <meta property="og:image" content="https://media-pics1.immowelt.org/1/D/E/6/2DBEFC28AA624CA1A0A33D6F3BFF6ED1.jpg" />
            <meta property="og:image" content="https://media-pics1.immowelt.org/1/F/F/3/7B45144E59974EEBBEF4BAAAD1123FF1.jpg" />
            <meta property="og:image" content="https://media-pics2.immowelt.org/8/5/7/A/7B890CD47212413E9A134487071AA758.jpg" />
            <meta property="og:image" content="https://media-pics1.immowelt.org/4/5/5/A/E0E2953F48694BC597548C1D6F60A554.jpg" />
            <meta property="og:image" content="https://media-pics2.immowelt.org/F/5/3/F/ECCB1A7E228E4E9F9463FC5FAD53F35F.jpg" />

    
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="copyright" content="Immowelt AG" />
    <meta name="publisher" content="Immowelt AG" />
    <meta name="company" content="Immowelt AG" />
    <meta name="fb:page_id" property="fb:page_id" content="30229182118" />
    
    <meta name="fragment" content="!">

    

    
    <meta name="format-detection" content="telephone=no" />
    <meta name="twitter:card" content="summary">
    <meta property="og:title" content="Ansehen: frisch modernisiertes Wohngl&#252;ck Etagenwohnung M&#252;nster (2LE9G4H) ">
    <meta property="og:description" content="Etagenwohnung in M&#252;nster (Mauritz) zur Miete mit 2 Zimmer und 49,19 m&#178; Wohnfl&#228;che. Ausstattung: Elektro, Gas, Zentralheizung, Kelleranteil.">


        <link rel="Alternate" hreflang="de-de" href="https://www.immowelt.de/expose/2LE9G4H" />
        <link rel="Alternate" hreflang="de-at" href="https://www.immowelt.at/expose/2LE9G4H" />
        <link rel="Alternate" hreflang="de-ch" href="https://www.immowelt.ch/expose/2LE9G4H" />
        <link rel="Alternate" hreflang="x-default" href="https://www.immowelt.de/expose/2LE9G4H" />


    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link type="text/css" rel="stylesheet" href="//media-static.immowelt.org/app_themes/global_rwd/stylesheet/opensans.css?636662108473026290">
    <link type="text/css" rel="stylesheet" media="screen" href="//media-static.immowelt.org/app_themes/global_rwd/stylesheet/base.css?636662108473026290" />
    <link type="text/css" rel="stylesheet" media="print" href="//media-static.immowelt.org/app_themes/global_rwd/stylesheet/print.css?636662108473026290" />
    <link type="text/css" rel="stylesheet" media="all" href="//media-static.immowelt.org/app_themes/global_rwd/plugin/fontello/fontello.css?636662108473026290" />
    <link type="text/css" rel="stylesheet" href="//media-static.immowelt.org/app_themes/global_rwd/plugin/jquery-ui/jquery-ui.css?636662108473026290" />
    <link type="text/css" rel="stylesheet" media="screen" href="//media-static.immowelt.org/app_themes/global_rwd/plugin/offcanvas_mmenu/jquery.mmenu.css?636662108473026290" />

    <link rel="stylesheet" type="text/css" href="//media-static.immowelt.org/app_themes/global_rwd/plugin/remodal/remodal.css?636662108473026290" media="screen" />
    <link rel="stylesheet" type="text/css" href="//media-static.immowelt.org/app_themes/global_rwd/plugin/remodal/remodal-default-theme.css?636662108473026290" media="screen" />

    
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700' rel='stylesheet' type='text/css' />
    <link rel="stylesheet" type="text/css" href="//media-static.immowelt.org/app_themes/global_rwd/stylesheet/expose.css?636662108473026290" media="screen" />
    <link rel="stylesheet" type="text/css" href="//media-static.immowelt.org/app_themes/global_rwd/stylesheet/gewerbeexpose.css?636662108473026290" media="screen" />
    <link rel="stylesheet" type="text/css" href="//media-static.immowelt.org/app_themes/global_rwd/stylesheet/expose_livestream.css?636662108473026290" media="screen" />
    <link rel="stylesheet" type="text/css" href="//media-static.immowelt.org/app_themes/global_rwd/plugin/fontello/fontello.css?636662108473026290" media="screen" />
    <link rel="stylesheet" type="text/css" href="//media-static.immowelt.org/app_themes/global_rwd/plugin/vivify/vivify.min.css?636662108473026290" media="screen" />

    <link rel="stylesheet" type="text/css" href="//media-static.immowelt.org/app_themes/global_rwd/stylesheet/map.css?636662108473026290" />

            <link rel="stylesheet" media="all" href="//media-static.immowelt.org/app_themes/global_rwd/plugin/owl-carousel/owl.carousel.css?636662108473026290" />
            <link rel="stylesheet" media="all" href="//media-static.immowelt.org/app_themes/mid_0_rwd/plugin/owl-carousel/owl.theme.iw.de.css?636662108473026290" />
        <link rel="stylesheet" href="//media-static.immowelt.org/app_themes/global_rwd/plugin/pannellum/pannellum.css?636662108473026290">



    <!--[if lte IE 8]>
        <link type="text/css" rel="stylesheet" media="screen" href="//media-static.immowelt.org/app_themes/global_rwd/stylesheet/ie8.css?636662108473026290" />
        
        <script type="text/javascript" src="/_scripts/mvc/common/respond.js?636662108473026290"></script>
        <link href="//media-static.immowelt.org/app_themes/global_rwd/ie8/respond_proxy.html" id="respond-proxy" rel="respond-proxy" />
        <link href="/app_themes/global_rwd/ie8/respond.proxy.gif" id="respond-redirect" rel="respond-redirect" />
        <script src="/_scripts/mvc/common/respond.proxy.js?636662108473026290"></script>
        
        <script type="text/javascript" src="//media-static.immowelt.org/_scripts/mvc/common/html5shiv.pack.js?636662108473026290"></script>
        
        
    <![endif]-->

<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script type="text/javascript">
    
    window.jQuery || document.write('<script type="text/javascript" src="//media-static.immowelt.org/_scripts/jquery/jquery.min.js?636662108473026290"><\\/script>');
</script>


<script type="text/javascript" src="/mvcroot/home/pagevariables?area=Expose"></script>
<script type="text/javascript" src="//media-static.immowelt.org/_ts/errorlogging.pack.js?636662108473026290"></script>
<script type="text/javascript" src="//media-static.immowelt.org/_scripts/mvc/_plugin/jquery-ui.pack.js?636662108473026290"></script>


<script type="text/javascript" src="//media-static.immowelt.org/_scripts/mvc/bundles/masterheadDesktop.pack.js?be43a5b67021dbefa3fbca2dba819a0c" crossorigin ></script>





<script type="text/javascript" src="//tags.tiqcdn.com/utag/axelspringer/immowelt-immowelt.de/prod/utag.sync.js"></script>

<!-- Yieldlab / Yieldprobe Tracking -->
<script type="text/javascript" src="https://ad.yieldlab.net/yp/62716,68993,128090,68991,209960?ts=636695092617560886"></script><!-- Yieldlove -->
<script type='text/javascript'>
    var yieldlove_site_id = null;
    var patterns =
    [
        {re:/expose/i, site_id:"immowelt.de_expose"},
        {re:/liste/i, site_id:"immowelt.de_srp"},
        {re:/ratgeber.immowelt.de/i, site_id:"immowelt.de_ratgebersearch"},
        //{re://i, site_id:"immowelt.de_pricesearch"},
        //{re://i, site_id:"immowelt.de_search"},
    ];


    for(i=0;i<patterns.length;i++) {
        matches = location.toString().match(patterns[i].re);
        if(matches != null) {
            yieldlove_site_id = patterns[i].site_id;
            break;
        }
    }

    if(yieldlove_site_id == null)
        window.yieldlove_prevent_autoload = true;

</script>
<script type='text/javascript' src='//cdn-a.yieldlove.com/yieldlove-bidder.js?immowelt.de'></script>



</head>
<!--[if IE 8]><body class="ie_old ie_8"><![endif]-->
<!--[if IE 9]><body class="ie_old ie_9"><![endif]-->
<!--[if !IE]><!-->
<body class="modern_browser ">


<script type="text/javascript">
    //<![CDATA[
    var utag_data = {"customer_status":"nicht_eingeloggt","customer_type":"-","app_medienid":0,"app_server":"W85","app_time":1533912461,"app_version":"25.07.2018 15:02:59","customer_cookie_accepted":false,"customer_is_domestic":"0","object_gok":["98081e39-c331-47b3-a8b3-5587fd86d480"],"object_id":52934893,"broker_id":6971775,"object_marketingtype":["MIETE_PACHT"],"object_objektart":["wohnung"],"object_objektart_sub":"ETAGE","object_city":["Münster , Westf"],"object_district":["Mauritz"],"object_federalstate":["Nordrhein-Westfalen"],"object_state":["Deutschland"],"object_price":430.1600,"object_currency":"EUR","object_rooms":2.0,"object_area":49.19,"object_features":["ELEKTRO","GAS","Zentralheizung","Kelleranteil"],"object_zip":"48155","object_count_photos":5,"object_count_documents":0,"object_address_is_visible":true,"object_geoid":"10805515000016","page_type":"productdetailpage","product_id":["500"],"product_name":["kontakt"],"product_variant":["kontakt"],"product_quantity":[1],"product_price":[0.0],"product_price_total":0.0,"product_period":["0"],"product_payment":[""],"product_category":["Nordrhein-Westfalen/Münster , Westf/Mauritz/wohnung/MIETE_PACHT"],"enh_promo_id":["dkk","schufa","goldgas","huk24","goldgas"],"enh_promo_name":["dkk","schufa","goldgas","huk24_hausratversicherung","goldgas-text"],"enh_promo_creative":["expose_teaser","expose_teaser","expose_teaser","expose_teaser","expose_teaser"],"enh_promo_position":["expose","expose","expose","expose","expose"],"exposeaufruf":1};
    //]]>
</script>


<script type="text/javascript">
    (function(b){
        var a="//tags.tiqcdn.com/utag/axelspringer/immowelt-immowelt.de/prod/utag.js",c="script",d=b.createElement(c);
        d.src=a;d.type="text/java"+c;d.async=true;
        a=b.getElementsByTagName(c)[0];a.parentNode.insertBefore(d,a);
    })(document);
</script>

    <!--<![endif]-->
    <div id="js_innerBody">

    

<div id="cookiebanner" class="cookie_banner">
    <div class="content_wrapper padding_top_10 padding_bottom_10 clear">
        <div class="iw_content">
            <div class="iw_left margin_right_40">
                <span class="margin_right_10">
                    Um die Webseite optimal gestalten und fortlaufend verbessern zu können, verwenden wir Cookies.
                    Durch die weitere Nutzung der Webseite stimmen Sie der Verwendung von Cookies zu.
                    Weitere Informationen erhalten Sie in der <a id="HplDatenschutz" href="/immoweltag/datenschutz" target="_blank">Datenschutzerklärung</a>.
                </span>
            </div>
            <a id="acceptCookiePolicy" class="js-acceptCookiePolicy icon-close iw_right" title="Schließen"></a>
        </div>
    </div>
</div>

        <div id="basecontainer" class="base">
            <noscript>
                <div class="message_info">
                    <h3>Bitte aktivieren Sie JavaScript</h3>
                    <p class="padding_none">Der Funktionsumfang und die Benutzerfreundlichkeit dieser Seite ist im Moment stark eingeschr&auml;nkt, da JavaScript nicht aktiviert ist.</p>
                </div>
            </noscript>

            



<div class="header">
    <div class="content_wrapper clear">
        <div class="iw_content">

<a href="/" title="immowelt.de" class="logo" id="logoNoAd">
    <picture>
        <!--[if IE 9]><video style="display: none;"><![endif]-->
        <source srcset='//media-static.immowelt.org/app_themes/global_rwd/image/logo/logo_immowelt_de_blank.svg' media="(max-width: 500px)" alt="Immobilien, Wohnungen und Häuser" title="Immobilien, Wohnungen und Häuser" />
        <source srcset='//media-static.immowelt.org/app_themes/global_rwd/image/logo/logo_immowelt_de_blank.svg' media="(max-width: 640px)" alt="Immobilien, Wohnungen und Häuser" title="Immobilien, Wohnungen und Häuser" />
        <source srcset='//media-static.immowelt.org/app_themes/global_rwd/image/logo/logo_immowelt_de.svg' media="(min-width: 641px) and (max-width: 980px)" alt="Immobilien, Wohnungen und Häuser" title="Immobilien, Wohnungen und Häuser" />
        <source srcset='//media-static.immowelt.org/app_themes/global_rwd/image/logo/logo_immowelt_de.svg' media="(min-width: 1271px)" />

        <!--[if IE 9]></video><![endif]-->
        <!--[if IE 8]><video style="display: none;"><![endif]-->
        <source srcset='//media-static.immowelt.org/app_themes/global_rwd/image/logo/logo_immowelt_de_blank.png' media="(max-width: 500px)" alt="Immobilien, Wohnungen und Häuser" title="Immobilien, Wohnungen und Häuser" />
        <source srcset='//media-static.immowelt.org/app_themes/global_rwd/image/logo/logo_immowelt_de_blank.png' media="(max-width: 640px)" alt="Immobilien, Wohnungen und Häuser" title="Immobilien, Wohnungen und Häuser" />
        <source srcset='//media-static.immowelt.org/app_themes/global_rwd/image/logo/logo_immowelt_de.png' media="(min-width: 641px) and (max-width: 980px)" alt="Immobilien, Wohnungen und Häuser" title="Immobilien, Wohnungen und Häuser" />
        <source srcset='//media-static.immowelt.org/app_themes/global_rwd/image/logo/logo_immowelt_de.png' media="(min-width: 1271px)" />
        <!--[if IE 8]></video><![endif]-->
        <!--[IF !IE]> -->
        <img src='//media-static.immowelt.org/app_themes/global_rwd/image/logo/logo_immowelt_de.svg' alt="Immobilien, Wohnungen und Häuser" title="Immobilien, Wohnungen und Häuser">
        <!-- <![ENDIF]-->
        <!--[if IE]><img src='//media-static.immowelt.org/app_themes/global_rwd/image/logo/logo_immowelt_de.svg' alt='Immobilien, Wohnungen und Häuser' title='Immobilien, Wohnungen und Häuser'><![endif]-->
    </picture>
</a>



<a href="/" title="immowelt.de" class="logo" id="logoAndAd" style="display:none">
    <picture>
        <!--[if IE 9]><video style="display: none;"><![endif]-->
        <source srcset='//media-static.immowelt.org/app_themes/global_rwd/image/logo/logo_immowelt_de_blank.svg' media="(max-width: 500px)" alt="Immobilien, Wohnungen und Häuser" title="Immobilien, Wohnungen und Häuser" />
        <source srcset='//media-static.immowelt.org/app_themes/global_rwd/image/logo/logo_immowelt_de_blank.svg' media="(max-width: 640px)" alt="Immobilien, Wohnungen und Häuser" title="Immobilien, Wohnungen und Häuser" />
        <source srcset='//media-static.immowelt.org/app_themes/global_rwd/image/logo/logo_immowelt_de.svg' media="(min-width: 641px) and (max-width: 980px)" alt="Immobilien, Wohnungen und Häuser" title="Immobilien, Wohnungen und Häuser" />
        <source srcset='//media-static.immowelt.org/app_themes/global_rwd/image/logo/logo_immowelt_de_blank.png' media="(min-width: 981px) and (max-width: 1270px)" alt="Immobilien, Wohnungen und Häuser" title="Immobilien, Wohnungen und Häuser" />
        <source srcset='//media-static.immowelt.org/app_themes/global_rwd/image/logo/logo_immowelt_de.svg' media="(min-width: 1271px)" />

        <!--[if IE 9]></video><![endif]-->
        <!--[if IE 8]><video style="display: none;"><![endif]-->
        <source srcset='//media-static.immowelt.org/app_themes/global_rwd/image/logo/logo_immowelt_de_blank.png' media="(max-width: 500px)" alt="Immobilien, Wohnungen und Häuser" title="Immobilien, Wohnungen und Häuser" />
        <source srcset='//media-static.immowelt.org/app_themes/global_rwd/image/logo/logo_immowelt_de_blank.png' media="(max-width: 640px)" alt="Immobilien, Wohnungen und Häuser" title="Immobilien, Wohnungen und Häuser" />
        <source srcset='//media-static.immowelt.org/app_themes/global_rwd/image/logo/logo_immowelt_de.png' media="(min-width: 641px) and (max-width: 980px)" alt="Immobilien, Wohnungen und Häuser" title="Immobilien, Wohnungen und Häuser" />
        <source srcset='//media-static.immowelt.org/app_themes/global_rwd/image/logo/logo_immowelt_de_blank.png' media="(min-width: 981px) and (max-width: 1270px)" alt="Immobilien, Wohnungen und Häuser" title="Immobilien, Wohnungen und Häuser" />
        <source srcset='//media-static.immowelt.org/app_themes/global_rwd/image/logo/logo_immowelt_de.png' media="(min-width: 1271px)" />
        <!--[if IE 8]></video><![endif]-->
        <!--[IF !IE]> -->
        <img src='//media-static.immowelt.org/app_themes/global_rwd/image/logo/logo_immowelt_de.svg' alt="Immobilien, Wohnungen und Häuser" title="Immobilien, Wohnungen und Häuser"><!-- <![ENDIF]-->
        <!--[if IE]>
            <img src='//media-static.immowelt.org/app_themes/global_rwd/image/logo/logo_immowelt_de.svg' alt='Immobilien, Wohnungen und Häuser' title='Immobilien, Wohnungen und Häuser'>
        <![endif]-->
    </picture>
</a>


        </div>
    </div>

    <div class="nav" id="js-menu-nav">
        <div class="content_wrapper">
            <div class="iw_content">
                <div class="iw_left relative">
                    <a class="menu_button" href="#menu"><span class="icon-bars"></span><span class="menu_text no_s">Menü</span></a>
                    <nav id="menu">
                        <ul class="nav-menu js_ui-first_level" data-highlight-main="Wohnen" data-highlight-sub="Default">
                            <li class="show-offcanvas-nav clear close_wrapper"><a class="icon-close iw_right" href="#" onclick="IwAG.$('nav#menu').trigger('close.mm'); return false;"></a></li>

                            
                            <li class="js_ui-has_dropdown current mm-opened">
                                <a href="/" class="js-channelLink js-trackingLink" data-channel="Wohnen" data-trackvalue="wohnen">Wohnen</a>
                            </li>

                            
                            <li class="js_ui-has_dropdown ">
                                <a href="/gewerbeimmobilien" class="js-channelLink js-trackingLink" data-channel="Gewerbe" data-trackvalue="gewerbe">Gewerbe</a>
                            </li>

                            
                            <li class="js_ui-has_dropdown ">
                                <a href="/auslandsimmobilien" class="js-channelLink js-trackingLink" data-channel="Ausland" data-trackvalue="ausland">Ausland</a>
                            </li>

                            
                            <li class="js_ui-has_dropdown ">
                                <a href="/anbieten/immobilien" class="js-channelLink js-trackingLink" data-channel="Anbieten" data-trackvalue="anbieten">Anbieten</a>
                            </li>

                            
                            <li class="js_ui-has_dropdown ">
                                <a href="https://ratgeber.immowelt.de/baufinanzierung.html" class="js-channelLink js-trackingLink" data-channel="Baufinanzierung" data-trackvalue="baufi" onclick="IwAG.Base.route(event, 'ratgeber');">Baufinanzierung</a>
                            </li>

                            
                            <li class="js_ui-has_dropdown ">
                                <a href="/immobilienpreise/index.aspx" class="js-channelLink js-trackingLink" data-channel="MarktPreise" data-trackvalue="preise">Markt & Preise</a>
                            </li>

                            
                            <li class="js_ui-has_dropdown">
                                <a href="https://ratgeber.immowelt.de/umzug.html" class="js-channelLink js-trackingLink" data-channel="Umzug" data-trackvalue="ratgeber" onclick="IwAG.Base.route(event, '');">Umzug</a>
                            </li>
                        </ul>
                    </nav>
                </div>

                
                <div class="iw_right relative">
                    <a class="menu_button" href="#menu_myiw">
                        <span class="menu_text no_s">Meine Immowelt</span><span class="icon-user"></span>
                    </a>

                    <nav id="menu_myiw">
                        <ul class="nav-menu js_ui-first_level">
                            <li class="show-offcanvas-nav clear close_wrapper"><a href="#" onclick="IwAG.$('nav#menu_myiw').trigger('close.mm'); return false;" class="icon-close iw_left"></a></li>
                            <!-- MyIW //    responsive //   NICHT eingelogged-->
                                <li class="show-offcanvas-nav">
                                    <a data-remodal-target="dev-modal-auth~login" href="#" class="js-trackingLink" data-trackvalue="myiw_anmelden">
                                        <span class="btn_01 ci_color w_100p">Jetzt anmelden</span>
                                    </a>
                                </li>
                                <li class="margin_top_20 show-offcanvas-nav">
                                    <a href="#" data-remodal-target="dev-modal-auth~register" class="js-trackingLink touch_link" data-trackvalue="myiw_register">Kostenlos registrieren</a>
                                </li>
                            <li class="js_ui-has_dropdown myiw mm-opened ">
                                <a href="/myiw/meineimmowelt/index.aspx" class="js-trackingLink" data-trackvalue="myiw">
                                    Meine Immowelt

                                </a>
                                <div class="nav-dropdown w_280">
                                    <ul class="second_level dropdown_box clear">

                                        <li class="show-offcanvas-nav">
                                            <a href="/myiw/meineimmowelt/index.aspx" class="js-trackingLink" data-trackvalue="myiw_uebersicht">Übersicht</a>
                                        </li>

                                        <!-- MyIW //    responsive //   NICHT eingelogged-->
                                            <li class="hide-offcanvas-nav">
                                                <a data-remodal-target="dev-modal-auth~login" href="#" class="js-trackingLink" data-trackvalue="myiw_anmelden">
                                                    <span class="btn_01 ci_color w_200">Jetzt anmelden</span>
                                                </a>
                                            </li>
                                            <li class="margin_top_20 hide-offcanvas-nav">
                                                <span>Neu hier?</span>
                                                <a data-remodal-target="dev-modal-auth~register" href="#" class="link_01 touch_link js-trackingLink" data-trackvalue="myiw_register">kostenlos registrieren</a>
                                            </li>
                                            <li class="margin_top_20">
                                                <a href="javascript:void(0);" class="touch_link js-trackingLink" data-trackvalue="myiw_merkzettel" onclick="return IwAG.Base.goToInternalLink(['immobilien', 'immomerker'], 'aspx');">
                                                    Merkzettel <span class="HeaderMerkerLinkCount"></span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="javascript:void(0);" class="touch_link js-trackingLink" data-trackvalue="myiw_suchauftraege" onclick="return IwAG.Base.goToInternalLink(['suchauftrag', 'anlegen']);">Suchaufträge</a>
                                            </li>
                                            <li>
                                                <a href="javascript:void(0);" class="touch_link js-trackingLink" data-trackvalue="myiw_suchverlauf" onclick="return IwAG.Base.goToInternalLink(['letzteobjekte', 'index'], 'aspx');">Suchverlauf</a>
                                            </li>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>


    <div class="remodal-bg "></div>

<div class="remodal double_width" data-remodal-id="dev-modal-auth" data-remodal-options="hashTracking: false">
    <div class="modal-content ">
            <form>
        <div class="modal-header js-modal-header">
            <button data-remodal-action="close" class="btn_close icon-close iw_right"></button>
            <h3 class="modal_headline js-login">Anmelden <span class="sub"></span></h3>
            <h3 class="modal_headline js-registration">Registrieren <span class="sub">Jetzt registrieren und viele Vorteile nutzen!</span></h3>
        </div>
        <div class="modal-body">
            <div class="grid_row js-login">
                <div class="modal_column1 grid_05o12 grid_05o12_m grid_12o12_s padding_bottom_none">
                    <label class="w_100p">
                        E-Mail oder Benutzername
                        <input class="js-usrmail-input margin_bottom_10 w_100p" data-val="true" data-val-required="Bitte geben Sie Ihre E-Mail-Adresse ein!" id="Name" name="Name" type="text" value="" />
                        <span class="field-validation-valid js-validation-usrmail" data-valmsg-for="Name" data-valmsg-replace="true"></span>
                    </label>

                    <label class="w_100p">
                        Passwort<span class="iw_right"><a class="link_02 padding_none js-submit-jump" href="https://secure.immowelt.de/autmvc/LostPassword/start">vergessen?</a></span>
                        <input class="js-pwd-input margin_bottom_10 w_100p" data-val="true" data-val-required="Bitte geben Sie Ihr Passwort ein!" id="Password" name="Password" type="password" />
                        <span class="field-validation-valid js-validation-pwd" data-valmsg-for="Password" data-valmsg-replace="true"></span>
                    </label>

                        <label class="w_100p radiobutton">
                            angemeldet bleiben?
                            <input type="checkbox" class="js-always-loggedin-input" />
                        </label>
                    <div class="js-error-capslock message_info margin_top_20" style="display:none;">
                        Ihre Feststelltaste ist aktiv!
                    </div>
                    <div class="js-error-message message_error margin_top_20" style="display:none;"></div>

                    <button class="btn_01 ci_color iw_right w_100p margin_top_10 margin_bottom_10 js-submit-login">Anmelden</button>

                    <a class="link_02 margin_top_20 padding_none js-login js-auth-change-link margin_bottom_10" data-remodal-target="dev-modal-auth~both-register" href="#">Neu hier? Registrieren</a>


                    <input type="hidden" class="js-endlink-input" value="https://www.immowelt.de/expose/2LE9G4H" />
                    <input type="hidden" class="js-mediaid-input" value="0" />
                    <input type="hidden" class="js-additionaldata-input" value="portal=www.immowelt.de&amp;referrer=https%253a%252f%252fwww.immowelt.de%252fexpose%252f2LE9G4H" />
                    <input type="hidden" class="js-urltologin-input" value="https://secure.immowelt.de/autmvc/Authentication/Login" />
                    <input type="hidden" class="js-urltocreatejumpurl-input" value="https://secure.immowelt.de/autmvc/Authentication/JumpTo" />
                    <input type="hidden" class="js-eventtype-input" value="common" />
                </div>
                <div class="grid_07o12 grid_07o12_m grid_12o12_s padding_bottom_none">
                    <strong class="w_100p"><span class="icon-lock"></span> Sicherheitshinweis: Geben Sie Ihre Zugangsdaten niemals an Dritte weiter</strong>
                    <p class="padding_top_10">Falls Sie verdächtige Nachrichten erhalten, die nach Ihren Zugangsdaten fragen oder Sie zum Besuch einer fremden Webseite auffordern, wenden Sie sich bitte direkt an uns.</p>
                    <a class="link_02" onclick='IwAG.Authentication.getInstance().goToSchutz(0)' href="javascript:void(0);">aktuelle Sicherheitsinformationen lesen</a>

                </div>
            </div>
            <div class="js-registration">
                <div class="grid_row">
                    <div class="modal_column1 grid_05o12 grid_05o12_m grid_12o12_s padding_bottom_none">
                        <div class="no_l no_m no_s">
    <label>
        Geben Sie eine gültige Email-Adresse an.
        <input type="text" id="ValidatationForEmail726" name="ValidatationForEmail" autocomplete="off" />
    </label>
    <input type="hidden" id="objectVal225" name="objectVal" value="ol70qUDhW3Dq063BSWNdLIMoIYamEn2mQBay3kTiQhbKEBngk9mGN4+YGN3Fn7+EfmgkAls01rA0keadBrFd6Q==" />
</div>
                        <label class="w_100p">
                            E-Mail
                            <input class="js-usrmail-input margin_bottom_10 w_100p" data-val="true" data-val-required="Bitte geben Sie Ihre E-Mail-Adresse ein!" id="Email" name="Email" type="text" value="" />
                            <span class="field-validation-valid js-validation-usrmail" data-valmsg-for="Email" data-valmsg-replace="true"></span>
                        </label>
                        Mit der Registrierung nehmen Sie den Immowelt-Service in Anspruch. Sie erhalten anhand der von Ihnen eingegebenen Daten, genutzten Services und auf Grundlage unseres Geschäftszwecks auf Ihr Anliegen ausgerichtete Informationen. Diesem Service können Sie jederzeit unter <a href="mailto:datenschutz@immowelt.de">datenschutz@immowelt.de</a> widersprechen. Weitere Informationen finden Sie in der <a target="_blank" href="https://www.immowelt.de/immoweltag/datenschutz">Datenschutzerklärung</a>.

                        <button class="btn_01 ci_color iw_right w_100p margin_top_10 margin_bottom_20 js-submit-register">Registrieren</button>

                        <a class="link_02 margin_top_20 padding_none js-registration js-auth-change-link margin_bottom_10" data-remodal-target="dev-modal-auth~both-login" href="#">mit bestehendem Konto anmelden</a>

                        <input type="hidden" class="js-endlink-input" value="https://www.immowelt.de/expose/2LE9G4H" />
                        <input type="hidden" class="js-mediaid-input" value="0" />
                        <input type="hidden" class="js-additionaldata-input" value="portal=www.immowelt.de&amp;referrer=https%253a%252f%252fwww.immowelt.de%252fexpose%252f2LE9G4H" />
                        <input type="hidden" class="js-urltoregister-input" value="https://secure.immowelt.de/autmvc/Register/Register" />
                        <input type="hidden" class="js-eventtype-input" value="common" />

                    </div>

                    <div class="grid_07o12 grid_07o12_m grid_12o12_s padding_bottom_none">
                        <ul class="textlist_icon_02 no_s">
                            <li>Suchaufträge, Suchanzeigen und Merkzettel helfen bei Ihrer Suche</li>
                            <li>Ihr Suchfortschritt bleibt erhalten - auch mobil</li>
                            <li>erstellen und verwalten Sie Immobilienanzeigen</li>
                        </ul>
                        <strong class="w_100p margin_top_20"><span class="icon-lock"></span> Ihre persönlichen Daten werden geschützt</strong>
                        <p class="padding_top_10">Wir gewährleisten den größtmöglichen Schutz Ihrer persönlichen Daten und geben sie nicht an Dritte weiter.</p>
                    </div>
                </div>
                <div class="js-error-message message_error margin_top_10 clear" style="display:none;"></div>


            </div>
            <div class="js-registration-success" style="display:none;">
                <div class="message_success">
                    <h3>Bitte bestätigen Sie Ihre Registrierung.</h3>
                    <p>
                        In Kürze erhalten Sie eine E-Mail mit einem Bestätigungslink. Klicken Sie bitte auf diesen Link, um Ihre Registrierung abzuschließen.
                        Hinweis: Sollten Sie die E-Mail nicht erhalten, prüfen Sie bitte auch Ihren Spam-Ordner.
                    </p>
                </div>
            </div>

        </div>
        <div class="modal-footer">
        </div>
    </form>

    </div>
</div>




<div class="remodal double_width" data-remodal-id="dev-modal-custom-auth" data-remodal-options="hashTracking: false">
    <div class="modal-content ">
            <form>
        <div class="modal-header js-modal-header">
            <button data-remodal-action="close" class="btn_close icon-close iw_right"></button>
            <h3 class="modal_headline js-remember">Merkzettel speichern für Ihren nächsten Besuch</h3>
            <h3 class="modal_headline js-annotation">Ihre Notizen: Mit Benutzerkonto überall verfügbar</h3>
            <h3 class="modal_headline js-fadeout">Speichern Sie die Fortschritte Ihrer Immobiliensuche</h3>
        </div>
        <div class="modal-body">
            <div class="grid_row">
                <div class="modal_column1 grid_12o12 js-login">
                    <div class="grid_row">
                        <div class="modal_column1 grid_05o12 grid_05o12_m grid_12o12_s padding_bottom_none">
                            <label class="w_100p">
                                E-Mail oder Benutzername
                                <input class="js-usrmail-input margin_bottom_10 w_100p" data-val="true" data-val-required="Bitte geben Sie Ihre E-Mail-Adresse ein!" id="Name2" name="Name2" type="text" value="" />
                                <span class="field-validation-valid js-validation-usrmail" data-valmsg-for="Name2" data-valmsg-replace="true"></span>
                            </label>

                            <label class="w_100p">
                                Passwort<span class="iw_right"><a class="link_02 padding_none js-submit-jump" href="https://secure.immowelt.de/autmvc/LostPassword/start">vergessen?</a></span>
                                <input class="js-pwd-input margin_bottom_10 w_100p" data-val="true" data-val-required="Bitte geben Sie Ihr Passwort ein!" id="Password2" name="Password2" type="password" />
                                <span class="field-validation-valid js-validation-pwd" data-valmsg-for="Password2" data-valmsg-replace="true"></span>
                            </label>

                                <label class="w_100p radiobutton">
                                    angemeldet bleiben?
                                    <input type="checkbox" class="js-always-loggedin-input" />
                                </label>
                            <div class="js-error-capslock message_info margin_top_20" style="display:none;">
                                Ihre Feststelltaste ist aktiv!
                            </div>
                            <div class="js-error-message message_error margin_top_20" style="display:none;"></div>

                            <button class=" btn_01 ci_color iw_right w_100p margin_top_10 js-submit-login">Anmelden</button>

                            <a class="link_02 margin_top_20 padding_none js-login js-auth-change-link margin_bottom_10" data-remodal-target="dev-modal-auth~both-register" href="#">Neu hier? Registrieren</a>

                            <input type="hidden" class="js-endlink-input" value="https://www.immowelt.de/expose/2LE9G4H" />
                            <input type="hidden" class="js-mediaid-input" value="0" />
                            <input type="hidden" class="js-additionaldata-input" value="portal=www.immowelt.de&amp;referrer=https%253a%252f%252fwww.immowelt.de%252fexpose%252f2LE9G4H" />
                            <input type="hidden" class="js-urltologin-input" value="https://secure.immowelt.de/autmvc/Authentication/Login" />
                            <input type="hidden" class="js-urltocreatejumpurl-input" value="https://secure.immowelt.de/autmvc/Authentication/JumpTo" />
                            <input type="hidden" class="js-eventtype-input" value="common" />
                        </div>
                        <div class="grid_07o12 grid_07o12_m grid_12o12_s padding_bottom_none">
                            <ul class="textlist_icon_02 no_s js-remember">
                                <li>behalten Sie interessante Angebote immer im Blick - auch mobil!</li>
                                <li>zusätzlich helfen Suchaufträge und Notizen bei Ihrer Suche</li>
                                <li>mit einem Benutzerkonto bleibt Ihr Suchfortschritt erhalten</li>
                                <li><strong>kostenlos und einfach: Ihre E-Mail und ein Passwort genügen</strong></li>
                            </ul>
                            <ul class="textlist_icon_02 no_s js-annotation">
                                <li>halten Sie Fragen, Infos oder Termine zu Immobilien schnell fest</li>
                                <li>zusätzlich helfen Suchaufträge und Merkzettel bei Ihrer Suche</li>
                                <li>mit einem Benutzerkonto bleibt Ihr Suchfortschritt erhalten</li>
                                <li><strong>kostenlos und einfach: Ihre E-Mail und ein Passwort genügen</strong></li>
                            </ul>
                            <ul class="textlist_icon_02 no_s js-fadeout">
                                <li>beliebig viele <strong>Anzeigen ausblenden</strong></li>
                                <li><strong>Suchauftr&auml;ge</strong> liefern passende Angebote per E-Mail</li>
                                <li><strong>Merkzettel</strong> und <strong>Notizen</strong> verbessern Ihren &Uuml;berblick</li>
                                <li><strong>Checklisten</strong> unterst&uuml;tzen Sie z.B. bei Ihrem Umzug</li>
                                <li><strong>Jederzeit</strong> unterwegs und auf verschiedenen Ger&auml;ten abrufbar</li>
                                <li><strong>kostenlos und einfach: Ihre E-Mail und ein Passwort gen&uuml;gen</strong></li>
                            </ul>
                            <strong class="w_100p margin_top_20"><span class="icon-lock"></span> Ihre persönlichen Daten werden geschützt</strong>
                            <p class="padding_top_10">Wir gewährleisten den größtmöglichen Schutz Ihrer persönlichen Daten und geben sie nicht an Dritte weiter.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="js-registration">
                <div class="grid_row">
                    <div class="modal_column1 grid_05o12 grid_05o12_m grid_12o12_s">
                        <div class="no_l no_m no_s">
    <label>
        Geben Sie eine gültige Email-Adresse an.
        <input type="text" id="ValidatationForEmail414" name="ValidatationForEmail" autocomplete="off" />
    </label>
    <input type="hidden" id="objectVal41" name="objectVal" value="ol70qUDhW3Dq063BSWNdLIMoIYamEn2mQBay3kTiQhbKEBngk9mGN4+YGN3Fn7+EfmgkAls01rA0keadBrFd6Q==" />
</div>
                        <label class="w_100p">
                            E-Mail
                            <input class="js-usrmail-input margin_bottom_10 w_100p" data-val="true" data-val-required="Bitte geben Sie Ihre E-Mail-Adresse ein!" id="Email2" name="Email2" type="text" value="" />
                            <span class="field-validation-valid js-validation-usrmail" data-valmsg-for="Email2" data-valmsg-replace="true"></span>
                        </label>
                        Mit der Registrierung nehmen Sie den Immowelt-Service in Anspruch. Sie erhalten anhand der von Ihnen eingegebenen Daten, genutzten Services und auf Grundlage unseres Geschäftszwecks auf Ihr Anliegen ausgerichtete Informationen. Diesem Service können Sie jederzeit unter <a href="mailto:datenschutz@immowelt.de">datenschutz@immowelt.de</a> widersprechen. Weitere Informationen finden Sie in der <a target="_blank" href="https://www.immowelt.de/immoweltag/datenschutz">Datenschutzerklärung</a>.
                        <div class="h_010"></div>

                        <button class="btn_01 ci_color iw_right w_100p margin_top_20 margin_bottom_20 js-submit-register">Registrieren</button>

                        <a class="link_02 margin_top_20 padding_none js-registration js-auth-change-link margin_bottom_10" data-remodal-target="dev-modal-auth~both-login" href="#">mit bestehendem Konto anmelden</a>

                        <input type="hidden" class="js-endlink-input" value="https://www.immowelt.de/expose/2LE9G4H" />
                        <input type="hidden" class="js-mediaid-input" value="0" />
                        <input type="hidden" class="js-additionaldata-input" value="portal=www.immowelt.de&amp;referrer=https%253a%252f%252fwww.immowelt.de%252fexpose%252f2LE9G4H" />
                        <input type="hidden" class="js-urltoregister-input" value="https://secure.immowelt.de/autmvc/Register/Register" />
                        <input type="hidden" class="js-eventtype-input" value="" />
                    </div>

                    <div class="grid_07o12 grid_07o12_m grid_12o12_s padding_bottom_none">
                        <ul class="textlist_icon_02 no_s js-remember">
                            <li>behalten Sie interessante Angebote immer im Blick - auch mobil!</li>
                            <li>zusätzlich helfen Suchaufträge und Notizen bei Ihrer Suche</li>
                            <li>mit einem Benutzerkonto bleibt Ihr Suchfortschritt erhalten</li>
                            <li><strong>kostenlos und einfach: Ihre E-Mail und ein Passwort genügen</strong></li>
                        </ul>
                        <ul class="textlist_icon_02 no_s js-annotation">
                            <li>halten Sie Fragen, Infos oder Termine zu Immobilien schnell fest</li>
                            <li>zusätzlich helfen Suchaufträge und Merkzettel bei Ihrer Suche</li>
                            <li>mit einem Benutzerkonto bleibt Ihr Suchfortschritt erhalten</li>
                            <li><strong>kostenlos und einfach: Ihre E-Mail und ein Passwort genügen</strong></li>
                        </ul>
                        <ul class="textlist_icon_02 no_s js-fadeout">
                            <li>beliebig viele <strong>Anzeigen ausblenden</strong></li>
                            <li><strong>Suchauftr&auml;ge</strong> liefern passende Angebote per E-Mail</li>
                            <li><strong>Merkzettel</strong> und <strong>Notizen</strong> verbessern Ihren &Uuml;berblick</li>
                            <li><strong>Checklisten</strong> unterst&uuml;tzen Sie z.B. bei Ihrem Umzug</li>
                            <li><strong>Jederzeit</strong> unterwegs und auf verschiedenen Ger&auml;ten abrufbar</li>
                            <li><strong>kostenlos und einfach: Ihre E-Mail und ein Passwort gen&uuml;gen</strong></li>
                        </ul>
                        <strong class="w_100p margin_top_20"><span class="icon-lock"></span> Ihre persönlichen Daten werden geschützt</strong>
                        <p class="padding_top_10">Wir gewährleisten den größtmöglichen Schutz Ihrer persönlichen Daten und geben sie nicht an Dritte weiter.</p>
                    </div>
                </div>
                <div class="js-error-message message_error margin_top_10" style="display:none;"></div>
            </div>
            <div class="grid_12o12 js-registration-success" style="display:none;">
                <div class="message_success">
                    <h3>Bitte bestätigen Sie Ihre Registrierung.</h3>
                    In Kürze erhalten Sie eine E-Mail mit einem Bestätigungslink. Klicken Sie bitte auf diesen Link, um Ihre Registrierung abzuschließen.
                    Hinweis: Sollten Sie die E-Mail nicht erhalten, prüfen Sie bitte auch Ihren Spam-Ordner.
                </div>
            </div>
        </div>
        <div class="modal-footer">

        </div>
    </form>

    </div>
</div>


    

    
    <div class="content_wrapper">
        <div class="iw_content">
            <div class="breadcrumb">
                Sie sind hier:
                        <span itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
                            <a href="/" itemprop="url">
                                <span itemprop="title">Immobilien</span>
                            </a>
                        </span>
                        &gt;
                        <span itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
                            <a href="/suche/wohnungen" itemprop="url">
                                <span itemprop="title">Wohnungssuche</span>
                            </a>
                        </span>
                        &gt;
                        <span itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
                            <a href="/suche/wohnungen/mieten" itemprop="url">
                                <span itemprop="title">mieten</span>
                            </a>
                        </span>
                        &gt;
                        <span itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
                            <a href="/suche/muenster-mauritz/wohnungen/mieten" itemprop="url">
                                <span itemprop="title">M&#252;nster (Mauritz)</span>
                            </a>
                        </span>
                        &gt;
                        <span itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
                            <a href="/liste/muenster/wohnungen/mieten?cp=1#52934893" itemprop="url">
                                <span itemprop="title">Ergebnisliste</span>
                            </a>
                        </span>
                        &gt;
Expos&#233;            </div>
        </div>
    </div>


    
    





    <div id="expose" class="expose">

        <div class="content_wrapper">


<div class="browsebar iw_left no_s no_m">

<div class="browse">
        <span class="exposenumber">Expos&#233; 1 von 44</span>
        <a class="nextPage" href="/expose/2LC9G4H?bc=101">
            <i class="icon-angle-right"></i>
        </a>
</div>

</div>

<div class="iw_content topbar clear">

<div class="left">
    <a href="/liste/muenster/wohnungen/mieten?cp=1#52934893" title="Zur&#252;ck zur Ergebnisliste" class="btn_01 lightgray icon-angle-left iw_left margin_right_10 w_auto_s">
        <span class="no_l no_m">Zur&#252;ck</span>
        <span class="no_s">Zur Ergebnisliste</span>
    </a>
</div>

    <div class="iw_dropdown iw_right">
        <a href="#" class="btn_01 lightgray icon-print margin_left_10 no_s js_btn_dropdown ">Drucken</a>
        <ul class="dropdown_right">
            <li>
                <a href="javascript:void(0)" onclick="IwAG.Expose.getInstance().printExpose('expose/2LE9G4H/printrwdpdf?version=11110010&amp;uGid=018CE8C66A621AF748E76DE3BDAFBB4E52E3E90D04A9C44F0A0DC717866157F9A9B79B2DCE0B8B3EB098D601A027EB1932CD8BF3A6027FC0ECE849105AA393E5');"
                   class="icon-pdf ">Exposé nur mit Titelbild</a>
            </li>
            <li>
                <a href="javascript:void(0)" onclick="IwAG.Expose.getInstance().printExpose('expose/2LE9G4H/printrwdpdf?version=11110110&amp;uGid=018CE8C66A621AF748E76DE3BDAFBB4E52E3E90D04A9C44F0A0DC717866157F9A9B79B2DCE0B8B3EB098D601A027EB1932CD8BF3A6027FC0ECE849105AA393E5');"
                   class="icon-pdf ">Exposé mit allen Bildern</a>
            </li>
            <li>
                <a href="javascript:void(0)" onclick="IwAG.Expose.getInstance().printExpose('expose/2LE9G4H/printrwdpdf?version=00000000&amp;uGid=018CE8C66A621AF748E76DE3BDAFBB4E52E3E90D04A9C44F0A0DC717866157F9A9B79B2DCE0B8B3EB098D601A027EB1932CD8BF3A6027FC0ECE849105AA393E5');"
                   class="icon-pdf ">Exposé Kompaktversion</a>
            </li>
            <li>
                <a data-remodal-target="custom_print" href="#" class="icon-pdf ">Benutzerdefinierter Druck</a>
            </li>
        </ul>
    </div>

    <div class="iw_dropdown iw_right">
        <a href="#" class="btn_01 lightgray w_auto_s js_btn_dropdown icon-forward ">Empfehlen</a>
        <ul class="dropdown_right">
            <li><a id="aRecommend" class="icon-mail" href="#" onclick="return false;">Empfehlen per E-Mail</a></li>
            <li><a class="icon-facebook" href="#" onclick="IwAG.Base.goToLink(2,'facebook','com/','sharer/sharer.php?u=https://www.immowelt.de/expose/2LE9G4H', true); return false;">Empfehlen per Facebook</a></li>
            <li><a class="icon-twitter" href="#" onclick="IwAG.Base.goToLink(2, 'twitter', 'com/', 'home?status=Hab ich gerade bei @immowelt gefunden: #Wohnung in #M&#252;nster http://iw.de?2LE9G4H', true); return false;">Empfehlen per Twitter</a></li>
            <li><a class="icon-google-plus" href="#" onclick="IwAG.Base.goToLink(2, 'plus.google', 'com/', 'share?url=https://www.immowelt.de/expose/2LE9G4H', true); return false">Empfehlen per Google+</a></li>
        </ul>
    </div>
</div>



<div class="remodal-bg"></div>
<div class="remodal single_width" data-remodal-id="custom_print" data-remodal-options="hashTracking: false">
    <div class="modal-content">
        <div class="modal-header">
            <button data-remodal-action="close" class="btn_close icon-close iw_right"></button>
            <h3 class="modal_headline js-login" style="display: block;">
                Benutzerdefiniertes PDF
            </h3>
        </div>
        <div class="modal-body">
            Hier können Sie die <strong>Exposé Kompaktversion</strong> um folgende Bereiche ergänzen:
            <div class="h_010"></div>
            <div class="padding_bottom_none">
                <label class="check margin_none">Preise und Kosten<input class="js-enablePreview" data-val="true" data-val-required="The Preise und Kosten field is required." id="cbPdfPreiseKosten" name="cbPdfPreiseKosten" type="checkbox" value="true" /><input name="cbPdfPreiseKosten" type="hidden" value="false" /></label>
                <label class="check margin_none">Ausstattungsmerkmale<input class="js-enablePreview" data-val="true" data-val-required="The Ausstattungsmerkmale field is required." id="cbPdfAusstattungsmerkmale" name="cbPdfAusstattungsmerkmale" type="checkbox" value="true" /><input name="cbPdfAusstattungsmerkmale" type="hidden" value="false" /></label>
                <label class="check margin_none">Beschreibung der Immobilie<input class="js-enablePreview" data-val="true" data-val-required="The Beschreibung der Immobilie field is required." id="cbPdfObjekttexte" name="cbPdfObjekttexte" type="checkbox" value="true" /><input name="cbPdfObjekttexte" type="hidden" value="false" /></label>
                    <label class="check margin_none">Lagebeschreibung<input class="js-enablePreview" data-val="true" data-val-required="The Lagebeschreibung field is required." id="cbPdfLagebeschreibung" name="cbPdfLagebeschreibung" type="checkbox" value="true" /><input name="cbPdfLagebeschreibung" type="hidden" value="false" /></label>
                <label class="check margin_none">alle Bilder<input class="js-enablePreview" data-val="true" data-val-required="The alle Bilder field is required." id="cbPdfBilder" name="cbPdfBilder" type="checkbox" value="true" /><input name="cbPdfBilder" type="hidden" value="false" /></label>
                <label class="check margin_none">Rand f&#252;r Heftung (links)<input class="js-enablePreview" data-val="true" data-val-required="The Rand für Heftung (links) field is required." id="cbPdfLinkerRand" name="cbPdfLinkerRand" type="checkbox" value="true" /><input name="cbPdfLinkerRand" type="hidden" value="false" /></label>
            </div>
            <div class="h_020"></div>
            <a data-remodal-action="close" href="javascript:void(0)" onclick="IwAG.Expose.getInstance().printExpose('expose/2LE9G4H/printrwdpdf?uGid=018CE8C66A621AF748E76DE3BDAFBB4E52E3E90D04A9C44F0A0DC717866157F9A9B79B2DCE0B8B3EB098D601A027EB1932CD8BF3A6027FC0ECE849105AA393E5', 'custom');"
               class="iw_right btn_01 lightgray icon-pdf ">PDF erstellen</a>
        </div>
        <div class="modal-footer">
        </div>
    </div>
</div>


    <div id="carouselContainer" class="margin_right_10_l ">
        <div class="carouselVisibleArea">
                <span id="Flag" class="iw_flag"></span>
            <div id="MediaContainer" class="carouselImageContainer relative">
                        <div id="imagesArea" class="mediastage carousel-full-width-area carouselItems">
                        
                        </div>
                                                                                    <div id="previousImage" class="previous-item-container"></div>
                    <div id="nextImage" class="next-item-container"></div>
            </div>
        </div>


    <div class="js-mediaTabBar thumbContainer relative clear">

            <div class="show_thumbnails">
                <a id="thumbsButton" class="icon-camera">Übersicht ausblenden</a>
            </div>

            <div id="thumbnails-Bilder" class="js-mediaTab ">
                <div id="previousPage" class="previous-thumbnail-pager"></div>
                <div id="nextPage" class="next-thumbnail-pager"></div>
                <div class="thumbnail-full-width-area">
                    <div id="thumbList" class="thumbnail-list">
                    
                    </div>
                </div>
            </div>
        

    </div>


    </div>

<div id="pswp_preloader" class="display_none" style="position: fixed; top: 0px; left: 0px; z-index: 3499;">
    <div class="pswp__bg" style="opacity: 1; z-index: 3499;"></div>
    <div class="pswp__scroll-wrap" style="z-index: 3499;">
        <div class="pswp__ui">
            <div class="pswp__top-bar">
                <button class="pswp__button pswp__button--close" title="Schliessen (Esc)"></button>
            </div>
            <div class="pswp__preloader pswp__preloader--active">
                <div class="pswp__preloader__icn">
                    <div class="pswp__preloader__cut">
                        <div class="pswp__preloader__donut"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<div id="pwsp_lightbox" class="pswp display_none" tabindex="-1" role="dialog" aria-hidden="true">

    
    <div class="pswp__bg"></div>

    
    <div class="pswp__scroll-wrap">

        
        <div class="pswp__container">
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
        </div>

        
        <div class="pswp__ui pswp__ui--hidden">
            <p></p>
            <div class="pswp__top-bar">

                

                <div class="pswp__counter"></div>

                <button class="pswp__button pswp__button--close" title="Schliessen (Esc)"></button>

                <button class="pswp__button pswp__button--share" title="Teilen"></button>

                <button class="pswp__button pswp__button--zoom" title="Zoom +/-"></button>

                
                
                <div class="pswp__preloader">
                    <div class="pswp__preloader__icn">
                        <div class="pswp__preloader__cut">
                            <div class="pswp__preloader__donut"></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                <div class="pswp__share-tooltip"></div>
            </div>

            <a id="js-button_left" style="width:50%; height:100%; position: absolute; left:0; margin-top:15%">
                <button class="pswp__button pswp__button--arrow--left" title="Zurück (Pfeil links)"></button>
            </a>
            <a id="js-button_right" style="width:50%; height:100%; position: absolute; right:0; margin-top:15%">
                <button class="pswp__button pswp__button--arrow--right" title="Weiter (Pfeil rechts)"></button>
            </a>
            <div class="pswp__caption">
                <div class="pswp__caption__center"></div>
            </div>

        </div>

    </div>

</div>
        </div>

<div id="divNotes"  class="section color_bg_14 display_none">
    <div class="content_wrapper">
        <div class="iw_content">
            <form id="formNotiz" data-hasnotes="false" data-result="async" action="javascript:IwAG.Base.asyncFormPost('formNotiz', '/expose/2LE9G4H/saveannotation', '')" method="post">
                <a href="#" id="aCloseNotizen" class="close_large icon-close display_none"></a>
                <h2>Ihre Notizen</h2>
                <input id="EstateGuid" name="EstateGuid" type="hidden" value="7414ECAF2B8444119B6B2C00569AD02C" />
                <input data-val="true" data-val-number="Es muss eine Zahl eingegeben werden." data-val-required="The EstateId field is required." id="EstateId" name="EstateId" type="hidden" value="52934893" />

                <div id="noteEdit" class="notes main_stage contact_form clearfix display_none">
                    <div class="left">
                        <textarea cols="20" id="Notizen_Description" name="Description" placeholder="Geben Sie hier Ihre persönlichen Anmerkungen ein." rows="2" style="height:60px;">
</textarea>
                    </div>
                    <div class="right">
                        <button id="aSaveNotiz" class="btn_01 ci_color js-annotation-login" type="submit">Notiz speichern</button>
                    </div>
                </div>


            </form>
        </div>
    </div>
</div>




        <div class="content_wrapper">
<div class="stage clear">
    <div class="iw_content">
        <div class="quickfacts iw_left">
            <h1>Ansehen: frisch modernisiertes Wohngl&#252;ck</h1>

            <div class="icon-map-marker location-exact stage_icon icon_padding_right_05 iw_left"></div>
            <div class="location">
                <span class="no_s">48155 M&#252;nster (Mauritz), Stehrweg 27</span>
                <a href="#" class="btn_02 black no_m no_l js-aKarte w_auto_s margin_none_s">48155 M&#252;nster (Mauritz), Stehrweg 27</a>
                <a href="#" class="btn_02 black no_s js-aKarte" title="Zur Kartenansicht mit Umgebungsinfos und Routenplan">Zur Karte</a>
            </div>
            
                <div>
                    <a href="https://www.immowelt.de/umzug?entladeLand=Deutschland&amp;entladePlz=48155&amp;entladeOrt=M%c3%bcnster&amp;entladeStr=Stehrweg%2027" data-tealium='{"event_action":"click", "event_category":"expose", "event_label":"umzug link"}'  target="_blank" class="link_01 ad margin_left_20 margin_bottom_10 js-tealium">Umzugsangebote anfordern</a>
                </div>
                
            <div class="icon-check icon_padding_right_05 stage_icon iw_left"></div><div class="merkmale">Kelleranteil, Zentralheizung</div>

                <div class="hardfacts clear">
                                <div class="hardfact ">
<strong>430,16 &euro;&nbsp;</strong>                                    <div class="hardfactlabel color_f_03">
                                        Kaltmiete zzgl. NK
                                    </div>
                                </div>
                                <div class="hardfact ">
49,19 m²                                    <div class="hardfactlabel color_f_03">
                                        Wohnfläche (ca.)
                                    </div>
                                </div>
                                <div class="hardfact rooms">
 2                                    <div class="hardfactlabel color_f_03">
                                        Zimmer
                                    </div>
                                </div>

                </div>

        </div>

        <div class="interaction iw_right">
                <a class="btn_01 ci_color icon-mail w_100p_s margin_right_2p iw_right margin_bottom_10 js-OpenContact" href="#" id="btnContactBroker"><span class="no_s">Anbieter kontaktieren</span><span class="no_m no_l">Kontakt</span></a>

            <a class="js-remember btn_01 lightgray margin_right_2p iw_left icon-heart-empty" href="#" data-estateid="52934893" data-guid="7414ECAF2B8444119B6B2C00569AD02C" data-action="add" data-tealiumeventlabel="merken_expose">
                Merken
            </a>
            <a class="js-display-notes btn_01 lightgray iw_right icon-edit" href="#" title="Eigene Notizen zu diesem Exposé machen">
                Notizen
            </a>

            <a id="aAnbieterLogo" class="clearbefore" href="#" ><div class="customerlogo"><img src="https://filestore.immowelt.de/Logo/top_1645fe74f6b34115bece48d75e6e113d.jpg" /></div></a>
        </div>
    </div>
</div>

<div id="statusAnchor"></div>
<div id="statusBar" class="anchornav clear">
    <div class="anchornav_wrapper no_s">
        <div class="content_wrapper">
            <div id="statusNavi" class="iw_content clear">
                <ul>
                    <li><a id="aUebersicht" href="#" class="current ">Übersicht</a></li>
                    <li><a id="aPreise" href="#" class="">Preise & Kosten</a></li>
                    <li><a id="aImmobilie" href="#" class="">Immobilie</a></li>
                    <li><a id="aLageinfos" href="#" class="">Lage</a></li>
                    <li><a id="aAnbieter" href="#" class="">Anbieter</a></li>
                </ul>
            </div>
        </div>
    </div>

    <div class="content_wrapper">
        <div class="iw_content">
            <div id="statusFacts" class="statusfacts iw_left no_m no_s">
                                <div class="hardfact">
                                    <strong><strong>430,16 &euro;&nbsp;</strong></strong>
                                    <div class="hardfactlabel">Kaltmiete zzgl. NK</div>
                                </div>
                                <div class="hardfact ">
49,19 m²
                                    <div class="hardfactlabel">Wohnfläche (ca.)</div>
                                </div>
                                <div class="hardfact rooms">
 2
                                    <div class="hardfactlabel">Zimmer</div>
                                </div>
            </div>
            <div id="statusActions" class="statusaction clear no_phone">
                    <a id="statusContact" class="btn_01 ci_color margin_right_2p icon-mail js-OpenContact" href="#" >
                        Anbieter kontaktieren
                    </a>
                <a class="js-remember btn_01 icon-heart-empty lightgray margin_right_2p no_s " href="#"
                    data-estateid="52934893" data-guid="7414ECAF2B8444119B6B2C00569AD02C"
                   data-action="add" data-tealiumeventlabel="merken_expose">Merken</a>
                <a class="btn_01 lightgray icon-edit js-display-notes no_s" href="#" >Notizen</a>
            </div>
        </div>
    </div>
</div>


        </div>

<div class="section share_mail section_form display_none js-asyncBox" id="recommendForm">
    <div class="content_wrapper">
        <div class="iw_content padding_none_s clear">
    <a href="javascript:void(0)" class="close_large icon-close js-CloseContactForm"></a>
            <form id="formWeiterempfehlen" action="javascript:IwAG.Base.asyncFormPost('formWeiterempfehlen', '/expose/2LE9G4H/weiterempfehlen', '', '', IwAG.Expose.getInstance().ContactHandler)" method="post">
                <div data-result="async">
                    <h2>Exposé empfehlen</h2>
                    <input id="PortalUrl" name="PortalUrl" type="hidden" value="http://www.immowelt.de" />
                    <input id="EstateGuid" name="EstateGuid" type="hidden" value="7414ECAF2B8444119B6B2C00569AD02C" />
                    <div class="grid_row">
                        <div class="grid_06o12_l grid_06o12_m grid_12o12_s padding_bottom_none">
                            <div class="input_double">
                                <label><label for="SenderName">Ihr Vor- und Nachname *</label></label>
                                <input class="w_100p" data-val="true" data-val-required="Bitte Ihren Namen eingeben." id="SenderName" name="SenderName" type="text" value="" />
                                <span class="field-validation-valid error_text" data-valmsg-for="SenderName" data-valmsg-replace="true"></span>
                            </div>
                            <div class="input_double padding_none">
                                <label><label for="SenderMail">Ihre E-Mail *</label></label>
                                <input class="w_100p" data-val="true" data-val-regex="Bitte gültige E-Mail eingeben." data-val-regex-pattern="^[\\w&amp;-]+[\\w./&amp;+!#$%&amp;&#39;*+/=?^_\`{|}~-]*@[\\w0-9]+[\\w0-9-.]*\\.(?!\\.)[\\w-]+$" data-val-required="Bitte Ihre E-Mail eingeben." id="SenderMail" name="SenderMail" type="email" value="" />
                                <span class="field-validation-valid error_text" data-valmsg-for="SenderMail" data-valmsg-replace="true"></span>
                            </div>

                            <label for="inpEmailAddress">Empfänger E-Mail *</label>
                            <input id="inpEmailAddresEmpf"
                                   class="w_100p js-inpWetierempfehlen "
                                   type="email"
                                   name="inpEmailAddresEmpf"
                                   data-val="true"
                                   data-val-email="Die eingegebene E-Mail-Adresse ist nicht gültig."
                                   placeholder="Mehrere E-Mail-Adressen bitte mit Komma trennen.">

                            <span class="field-validation-valid error_text" data-valmsg-for="ReceiverMails" data-valmsg-replace="true"></span>
                            <span data-valmsg-for="inpEmailAddresEmpf" data-valmsg-replace="true" class="error_text"></span>


                            <div id="divWeiterEmpfMultipleReciev" class="clear">
                            </div>
                            <span class="field-validation-valid error_text margin_none" data-valmsg-for="emailAddressError" data-valmsg-replace="true"></span>
                        </div>
                        <div class="grid_06o12_l grid_06o12_m grid_12o12_s padding_bottom_none">
                            <label><label for="Message">Folgende Nachricht m&#246;chte ich &#252;bermitteln *</label></label>
                            <textarea class="w_100p" cols="20" data-val="true" data-val-required="Bitte den Namen des Empfängers eingeben." id="Message" name="Message" rows="2" type="text" value="Ich empfehle dir dieses Angebot gerne weiter.">
Ich empfehle dir dieses Angebot gerne weiter.</textarea>
                            <label><input checked="checked" data-val="true" data-val-required="The Eine Kopie der E-Mail soll an mich gesendet werden. field is required." id="SendCopyToMe" name="SendCopyToMe" type="checkbox" value="true" /><input name="SendCopyToMe" type="hidden" value="false" /> Eine Kopie der E-Mail soll an mich gesendet werden.</label>
                            <a id="aSubmitRecommend" href="#" class="btn_01 ci_color margin_top_10 icon-mail iw_right" >Jetzt empfehlen</a>
                        </div>
                    </div>
                    <div class="no_l no_m no_s">
    <label>
        Geben Sie eine gültige Email-Adresse an.
        <input type="text" id="ValidatationForEmail583" name="ValidatationForEmail" autocomplete="off" />
    </label>
    <input type="hidden" id="objectVal415" name="objectVal" value="ol70qUDhW3Dq063BSWNdLIMoIYamEn2mQBay3kTiQhbKEBngk9mGN4+YGN3Fn7+EfmgkAls01rA0keadBrFd6Q==" />
</div>
                    <input data-val="true" data-val-required="The ShowContactFormAfterSuccess field is required." id="ShowContactFormAfterSuccess" name="ShowContactFormAfterSuccess" type="hidden" value="True" />
                </div>
            </form>
</div>
    </div>
</div>
<div class="section section_contact section_form display_none js-asyncBox" id="divContact">
    <div class="content_wrapper">
        <div class="iw_content relative clear padding_none_s">
            
            <div id="contactLoader" class="center_all display_none">
                <div>
                    <div>
                        <img src="//media-static.immowelt.org/app_themes/global/images/icon/icon_wait_trans.gif">
                        <div class="h_020"></div>
                        Ihre Kontaktanfrage wird gesendet.
                        <div class="h_020"></div>
                    </div>
                </div>
            </div>

            <form id="formKontaktanfrage" action="javascript:IwAG.Base.getInstance().asyncFormPost({formId:'formKontaktanfrage', action:'/expose/2LE9G4H/Kontaktanfrage', extentionFunc:IwAG.Expose.getInstance().ContactHandler, onError: IwAG.Expose.getInstance().ContactHandler, callback: function(e){IwAG.Advertisement&&IwAG.Advertisement.getInstance().updateExposeTeaser(e);}})" method="post">
                <a href="javascript:void(0);" id="closecontactform" data-remodal-action="close" data-url="/expose/2LE9G4H/GetKontaktanfrage?estateguid=7414ECAF2B8444119B6B2C00569AD02C" class="close_large icon-close"></a>
                <div data-result="async" class="clear">
                    <input name="__RequestVerificationToken" type="hidden" value="qzeAvSw3ap6r_ovuckTAjFUNSunKncXRJ813ZclMgR-HB-TfZSFFEEsRbaBMCQSrqrWSTq9vuyBHu682_uP3oaU9oHlNilPvQMSnO23D_qwtnCM9qWnV8PSWxlyzPc-XlFCJJ6pIKkoq-IQhflDtAvHdS-LC_1WgaxpPiTNvZfY1" />
                    
                    <input type="hidden" id="hidRefreshContactForm" value="false" />
                    <input data-val="true" data-val-required="The EstateGuid field is required." id="EstateGuid" name="EstateGuid" type="hidden" value="7414ECAF2B8444119B6B2C00569AD02C" />
                    <input data-val="true" data-val-number="Es muss eine Zahl eingegeben werden." data-val-required="The EstateId field is required." id="EstateId" name="EstateId" type="hidden" value="52934893" />
                    <input id="EstateGlobalObjectKey" name="EstateGlobalObjectKey" type="hidden" value="98081e39-c331-47b3-a8b3-5587fd86d480" />
                    <input data-val="true" data-val-number="Es muss eine Zahl eingegeben werden." id="GlobalUserId" name="Address.GlobalUserId" type="hidden" value="682790" />
                    <input id="OnlineId" name="OnlineId" type="hidden" value="2LE9G4H" />
                    <input data-val="true" data-val-required="The ShowRegisterSuchauftrag field is required." id="ShowRegisterSuchauftrag" name="ShowRegisterSuchauftrag" type="hidden" value="True" />
<h2>Anbieter kontaktieren</h2>
    <p class="account_check">
        Kontaktdaten aus Ihrem Benutzerkonto übernehmen?
        <a data-remodal-target="dev-modal-auth~login" href="#" class="link_02 js-ContactLogin margin_left_10">jetzt anmelden </a>
    </p>
<div class="grid_row">
    <div class="no_l no_m no_s gradient">
    <label>
        Geben Sie eine gültige Email-Adresse an.
        <input type="text" id="ValidatationForEmail906" name="ValidatationForEmail" autocomplete="off" />
    </label>
    <input type="hidden" id="objectVal45" name="objectVal" value="VM/ZZJTbAZLV+rFK6ajEX0f81ksn4Y3JvMV8yH0Oiq88Q7cb0WnNELI09NlYu2wXfJ7pSnrCIKydxYFUdIcj17AXqUrOZyHL1jmXO1Vyh/I=" />
</div>

    
    <div id="srcLabelMessage" class="display_none no_display">Ihre Nachricht an Kundenservice</div>
    <a id="aKontaktadresse" href="#" class="link_toggle padding_none margin_top_10 margin_bottom_20 display_none no_display">Meine Adresse angeben</a>
    <div class="h_010"></div>

    <input type="hidden" name="ContactPersonName" value="" />
    <input type="hidden" name="GlobalObjectKey" value="98081e39-c331-47b3-a8b3-5587fd86d480" />
    <input type="hidden" name="OffererGlobalUserId" value="682790" />
    <input type="hidden" name="UserGlobalUserId" value="0" />




<div class=" grid_06o12_l grid_06o12_m grid_12o12_s padding_bottom_none ">

        <div class="js-group">
                <div class="clear">
        


    <div class="input_1 salutation ">

    
<label title="Anrede" class="" for="salutation">Anrede *</label>
<div class="picker_wrapper padding_none">
    <select id="salutation"
            name="InputElement[salutation]"
            class="w_100p picker "
            
            
            required="required"
            data-val="true" data-val-required  >

        <option value="" disabled="disabled" selected="True">
            Bitte wählen
        </option>
            <option value="Herr">
                Herr
            </option>
            <option value="Frau">
                Frau
            </option>
            <option value="Firma">
                Firma
            </option>
        </select>
    </div>


        <span class="field-validation-valid" data-valmsg-for="InputElement[salutation]" data-valmsg-replace="true">
        </span>
    </div>


    </div>
    <div class="clear">
    


    <div class="input_2 firstname ">

    


<label title="Vorname"
       for="firstname"
       data-label="Vorname"
       data-required="true">
    Vorname *
</label>
<input type="text"
       id="firstname"
       name="InputElement[firstname]"
       class=""
       size="0"
       value=""
       data-val="true" data-val-length data-val-length-min="2" data-val-length-max="80"  data-val-required   />

        <span class="field-validation-valid" data-valmsg-for="InputElement[firstname]" data-valmsg-replace="true">
        </span>
    </div>
    <div class="input_2 lastname ">

    


<label title="Nachname"
       for="lastname"
       data-label="Nachname"
       data-required="true">
    Nachname *
</label>
<input type="text"
       id="lastname"
       name="InputElement[lastname]"
       class=""
       size="0"
       value=""
       data-val="true" data-val-length data-val-length-min="2" data-val-length-max="80"  data-val-required   />

        <span class="field-validation-valid" data-valmsg-for="InputElement[lastname]" data-valmsg-replace="true">
        </span>
    </div>


    </div>
    <div class="clear">
    


    <div class="input_2 firmname display_none no_display">

    


<label title="Firmenname"
       for="firmname"
       data-label="Firmenname"
       data-required="true">
    Firmenname *
</label>
<input type="text"
       id="firmname"
       name="InputElement[firmname]"
       class=""
       size="0"
       value=""
       data-val="true" data-val-length data-val-length-min="2" data-val-length-max="80"  data-val-required   />

        <span class="field-validation-valid" data-valmsg-for="InputElement[firmname]" data-valmsg-replace="true">
        </span>
    </div>
    <div class="input_2 contactPerson display_none no_display">

    


<label title="Ansprechpartner"
       for="contactPerson"
       data-label="Ansprechpartner"
       data-required="true">
    Ansprechpartner *
</label>
<input type="text"
       id="contactPerson"
       name="InputElement[contactPerson]"
       class=""
       size="0"
       value=""
       data-val="true" data-val-length data-val-length-min="2" data-val-length-max="80"  data-val-required   />

        <span class="field-validation-valid" data-valmsg-for="InputElement[contactPerson]" data-valmsg-replace="true">
        </span>
    </div>


    </div>
    <div class="clear">
    


    <div class="input_2 email ">

    


<label title="E-Mail-Adresse"
       for="email"
       data-label="E-Mail-Adresse"
       data-required="true">
    E-Mail-Adresse *
</label>
<input type="email"
       id="email"
       name="InputElement[email]"
       class=""
       size="0"
       value=""
       data-val="true" data-val-regex data-val-regex-pattern="^[\\w&amp;-]+[\\w./&amp;+-]*@[\\w0-9]+[\\w0-9-.]*\\.(?!\\.)[\\w-]+$"  data-val-required   />

        <span class="field-validation-valid" data-valmsg-for="InputElement[email]" data-valmsg-replace="true">
        </span>
    </div>
    <div class="input_2 tel ">

    


<label title="Telefonnummer"
       for="tel"
       data-label="Telefonnummer"
       data-required="true">
    Telefonnummer *
</label>
<input type="tel"
       id="tel"
       name="InputElement[tel]"
       class=""
       size="0"
       value=""
       data-val="true" data-val-regex data-val-regex-pattern="^(?:\\+\\d{1,3}|0\\d{1,3}|00\\d{1,2})?(?:\\s?\\(\\d+\\))?(?:[-\\/\\s.]|\\d)+$"  data-val-required   />

        <span class="field-validation-valid" data-valmsg-for="InputElement[tel]" data-valmsg-replace="true">
        </span>
    </div>


    </div>

        </div>
        <div class="js-group">
                <div class="clear">
        


    <div class="input_1 street ">

    


<label title="Stra&#223;e &amp; Hausnummer"
       for="street"
       data-label="Stra&#223;e &amp; Hausnummer"
       data-required="true">
    Stra&#223;e &amp; Hausnummer *
</label>
<input type="text"
       id="street"
       name="InputElement[street]"
       class=""
       size="0"
       value=""
       data-val="true" data-val-length data-val-length-min="2" data-val-length-max="80"  data-val-required   />

        <span class="field-validation-valid" data-valmsg-for="InputElement[street]" data-valmsg-replace="true">
        </span>
    </div>


    </div>
    <div class="clear">
    


    <div class="input_2 zipcode ">

    


<label title="Postleitzahl"
       for="zipcode"
       data-label="Postleitzahl"
       data-required="true">
    Postleitzahl *
</label>
<input type="text"
       id="zipcode"
       name="InputElement[zipcode]"
       class=""
       size="0"
       value=""
       data-val="true" data-val-length data-val-length-min="2" data-val-length-max="10"  data-val-required   />

        <span class="field-validation-valid" data-valmsg-for="InputElement[zipcode]" data-valmsg-replace="true">
        </span>
    </div>
    <div class="input_2 city ">

    


<label title="Ort"
       for="city"
       data-label="Ort"
       data-required="true">
    Ort *
</label>
<input type="text"
       id="city"
       name="InputElement[city]"
       class=""
       size="0"
       value=""
       data-val="true" data-val-length data-val-length-min="2" data-val-length-max="80"  data-val-required   />

        <span class="field-validation-valid" data-valmsg-for="InputElement[city]" data-valmsg-replace="true">
        </span>
    </div>


    </div>

        </div>
</div>



<div class="js-page grid_06o12_l grid_06o12_m grid_12o12_s padding_bottom_none ">
        <div class="padding_bottom_10"><strong>Schritt 1/2</strong></div>

        <div class="js-group">
                <div class="clear">
        


    <div class="input_1 message ">

    
<label title="Nachricht" for="message"><span id="lblmessage">Nachricht</span> *</label>
<textarea id="message"
          name="InputElement[message]"
          size="40"
          class=""
          value=""
          rows="4"
          data-val="true" data-val-length data-val-length-min="2" data-val-length-max="2500"  data-val-required  ></textarea>


        <span class="field-validation-valid" data-valmsg-for="InputElement[message]" data-valmsg-replace="true">
        </span>
    </div>


    </div>

        </div>
        <div class="js-group">
                <div class="clear">
        

<fieldset class="horizontal_check">
    <div class="input_3 requestViewing ">

    
<label for="requestViewing" title="Besichtigung vereinbaren">
        <input type="checkbox"
               id="requestViewing"
               name="InputElement[requestViewing]"
               size="0"
               value="true"
               checked="checked"
               data-val="false"
                />

        Besichtigung vereinbaren
</label>


        <span class="field-validation-valid" data-valmsg-for="InputElement[requestViewing]" data-valmsg-replace="true">
        </span>
    </div>
    <div class="input_3 requestMoreInformation ">

    
<label for="requestMoreInformation" title="Infomaterial anfordern">
        <input type="checkbox"
               id="requestMoreInformation"
               name="InputElement[requestMoreInformation]"
               size="0"
               value=""
               data-val="false"
                />

        Infomaterial anfordern
</label>


        <span class="field-validation-valid" data-valmsg-for="InputElement[requestMoreInformation]" data-valmsg-replace="true">
        </span>
    </div>
    <div class="input_3 requestCallback ">

    
<label for="requestCallback" title="R&#252;ckruf">
        <input type="checkbox"
               id="requestCallback"
               name="InputElement[requestCallback]"
               size="0"
               value=""
               data-val="false"
                />

        R&#252;ckruf
</label>


        <span class="field-validation-valid" data-valmsg-for="InputElement[requestCallback]" data-valmsg-replace="true">
        </span>
    </div>
</fieldset>

    </div>

        </div>
</div>


<div class="js-page grid_06o12_l grid_06o12_m grid_12o12_s padding_bottom_none display_none no_display">
        <div class="padding_bottom_10"><strong>Schritt 2/2</strong></div>

        <div class="js-group">
                <div class="clear">
        


    <div class="input_2 householdIncome ">

    
<label title="Mtl. Haushaltsnettoeinkommen" class="" for="householdIncome">Mtl. Haushaltsnettoeinkommen</label>
<div class="picker_wrapper padding_none">
    <select id="householdIncome"
            name="InputElement[householdIncome]"
            class="w_100p picker "
            
            
            
            data-val="false" >

        <option value=""  selected="True">
            Bitte wählen
        </option>
            <option value="unter 1.000 €">
                unter 1.000 €
            </option>
            <option value="1.000 - 2.000 €">
                1.000 - 2.000 €
            </option>
            <option value="2.000 - 3.000 €">
                2.000 - 3.000 €
            </option>
            <option value="3.000 - 4.000 €">
                3.000 - 4.000 €
            </option>
            <option value="&#252;ber 4.000 €">
                &#252;ber 4.000 €
            </option>
        </select>
    </div>


        <span class="field-validation-valid" data-valmsg-for="InputElement[householdIncome]" data-valmsg-replace="true">
        </span>
    </div>
    <div class="input_2 employmentStatus ">

    
<label title="Besch&#228;ftigungsstatus" class="" for="employmentStatus">Besch&#228;ftigungsstatus</label>
<div class="picker_wrapper padding_none">
    <select id="employmentStatus"
            name="InputElement[employmentStatus]"
            class="w_100p picker "
            
            
            
            data-val="false" >

        <option value=""  selected="True">
            Bitte wählen
        </option>
            <option value="Angestellte(r)">
                Angestellte(r)
            </option>
            <option value="Arbeiter(in)">
                Arbeiter(in)
            </option>
            <option value="Selbst&#228;ndige(r)">
                Selbst&#228;ndige(r)
            </option>
            <option value="Beamte(r)">
                Beamte(r)
            </option>
            <option value="Auszubildene(r)">
                Auszubildene(r)
            </option>
            <option value="Studierende(r)">
                Studierende(r)
            </option>
            <option value="Hausfrau/-mann">
                Hausfrau/-mann
            </option>
            <option value="Arbeitssuchende(r)">
                Arbeitssuchende(r)
            </option>
            <option value="Rentner(in)">
                Rentner(in)
            </option>
            <option value="Sonstiges">
                Sonstiges
            </option>
        </select>
    </div>


        <span class="field-validation-valid" data-valmsg-for="InputElement[employmentStatus]" data-valmsg-replace="true">
        </span>
    </div>


    </div>
    <div class="clear">
    


    <div class="input_1 householdSize ">

    
<label title="Haushaltsgr&#246;&#223;e" class="" for="householdSize">Haushaltsgr&#246;&#223;e</label>
<div class="picker_wrapper padding_none">
    <select id="householdSize"
            name="InputElement[householdSize]"
            class="w_100p picker "
            
            
            
            data-val="false" >

        <option value=""  selected="True">
            Bitte wählen
        </option>
            <option value="Einpersonenhaushalt">
                Einpersonenhaushalt
            </option>
            <option value="Zwei Erwachsene">
                Zwei Erwachsene
            </option>
            <option value="Familie">
                Familie
            </option>
            <option value="Wohngemeinschaft">
                Wohngemeinschaft
            </option>
        </select>
    </div>


        <span class="field-validation-valid" data-valmsg-for="InputElement[householdSize]" data-valmsg-replace="true">
        </span>
    </div>


    </div>

        </div>
    </div>


    <div class="grid_06o12_l grid_06o12_m grid_12o12_s iw_right">
        

        <div class="h_010"></div>

        <label class="t_small margin_bottom_10" id="lblDatenschutzhinweis">
            Mit Absenden der Anfrage nehmen Sie den Immowelt-Service in Anspruch. Sie erhalten anhand der von Ihnen eingegebenen Daten, genutzten Services und auf Grundlage unseres Geschäftszwecks auf Ihr Anliegen ausgerichtete Informationen. Diesem Service können Sie jederzeit unter <a href="mailto:datenschutz@immowelt.de">datenschutz@immowelt.de</a> widersprechen. Weitere Informationen finden Sie in der <a target="_blank" href="https://www.immowelt.de/immoweltag/datenschutz">Datenschutzerklärung</a>.
        </label>

        <button class="btn_01 ci_color icon-mail margin_right_10 iw_left btn_100p" type="submit" id="btnContactSend">
            &nbsp; Kontaktanfrage senden
        </button>
        <button class="margin_right_20 btn_01 lightgray iw_left btn_100p display_none no_display" id="btnStepBack" type="button">
            Zurück
        </button>
        <button class="btn_01 ci_color margin_right_10 iw_left btn_100p display_none no_display" id="btnStepForward" type="button">
            Weiter
        </button>


        <div class="clear"></div>
    </div>
</div>                </div>
            </form>
        </div>
    </div>
</div>


<div class="section preise odd" id="divPreise">
    <div class="content_wrapper">
        <div class="iw_content clear">
         
            <div class="section_wrapper iw_left">

                        <h2>Preise & Kosten</h2>
                        <div class="clear">
                            <div class="section_label iw_left">
                                Preise
                            </div>
                            <div class="section_content iw_right">
                                <div class="datatable clear">
                                            <div class="datarow clear">
                                                <div class="datalabel iw_left"><strong>Kaltmiete </strong></div>
                                                <div class="datacontent iw_right"><strong>430,16 €</strong></div>
                                            </div>
                                            <div class="datarow clear">
                                                <div class="datalabel iw_left">Nebenkosten </div>
                                                <div class="datacontent iw_right">108 €</div>
                                            </div>
                                            <div class="datarow clear">
                                                <div class="datalabel iw_left">Heizkosten <br /><small>(in Warmmiete enthalten)</small></div>
                                                <div class="datacontent iw_right">30 €</div>
                                            </div>
                                            <div class="datarow clear">
                                                <div class="datalabel iw_left">Warmmiete </div>
                                                <div class="datacontent iw_right">568,16 €</div>
                                            </div>

                                </div>
                            </div>
                        </div>




                    <div class="clear">
                        <div class="section_label iw_left">
                            Kaution
                        </div>
                        <div class="section_content iw_right padding_none">
                            <p>
                                1290,48 €
                            </p>
                        </div>
                    </div>

                    <div class="clear no_print">
                        <div class="section_label iw_left">
                            &nbsp;
                        </div>
                        <div class="section_content iw_right">
                            <ul class="linklist_icon_01">
                                    <li>
                                        <a href="#" id="aDkkExpose" class="js-tealium" data-tealium="{&quot;enh_promo_id&quot;: [&quot;dkk&quot;], &quot;enh_promo_name&quot;: [&quot;dkk&quot;], &quot;enh_promo_creative&quot;: [&quot;expose_teaser&quot;], &quot;enh_promo_position&quot;: [&quot;expose&quot;], &quot;page_type&quot;: &quot;promo_click&quot; }" onclick="IwAG.Base.goToLink(0, 'customer.immowelt', 'de/', 'dkk', false, true); return false;"
                                           linkid="5">
                                            Jetzt ohne Kaution mieten
                                        </a>

                                    </li>
                                <li class="padding_top_10">
                                    <a href="#" class="js-tealium" data-tealium="{&quot;enh_promo_id&quot;: [&quot;schufa&quot;], &quot;enh_promo_name&quot;: [&quot;schufa&quot;], &quot;enh_promo_creative&quot;: [&quot;expose_teaser&quot;], &quot;enh_promo_position&quot;: [&quot;expose&quot;], &quot;page_type&quot;: &quot;promo_click&quot; }" onclick="IwAG.Base.goToLink(2, 'schufa.immowelt', 'de/', '', false, true);  return false;" id="aSchufa">
                                        Mieterselbstauskunft – SCHUFA-BonitätsCheck zur Besichtigung mitnehmen
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>



            </div>

            <div class="section_ad iw_right no_m_">
                <!-- /46859844/Immowelt.de_Expose_Teaser -->
                <div id="div-gpt-ad-1444661197860-0"></div>
            </div>

        </div>
    </div>
</div>

<div id="divImmobilie" class="section content_wrapper clearfix">
    <div class="iw_content clear">
        <div class="section_wrapper iw_left">
            <div class="clear">
                <div class="section_label iw_left">
                    <h2>Immobilie</h2>
                </div>
                <div class="section_content iw_right">
                    <p>
                        Online-ID: 2LE9G4H<br />
                        Referenznummer: 84-1138510007<br />
                    </p>
                </div>
            </div>
            <div class="clear">
                
                <div class="section_label iw_left">
                    Die Wohnung
                </div>

                        <div class="section_content iw_right">

                                                            <p>

Etagenwohnung<br />                                                <span>2. Geschoss </span>
                                            <br />
<strong>frei ab 24.08.2018</strong><br />
                                </p>
                            
                                    <ul class="textlist_icon_03 padding_top_none ">
                                                <li>
                                                    
                                                        <span>Weitere Räume:</span>

                                                    <span>Kelleranteil</span>

                                                    
                                                </li>
                                    </ul>

                        </div>

            </div>
                    <div class="clear">
                        <div class="section_label iw_left">
                            Wohnanlage
                        </div>
                        <div class="section_content iw_right">
                                <p>
                                    Baujahr 1954<br />
                                </p>
                                                                                </div>
                    </div>


            
        <div class="clear">
            <div class="section_label iw_left" id="divEnergie">
                Energie / Versorgung
            </div>
            <div class="section_content iw_right">
                        <ul class="textlist_icon_03 padding_top_none ">
                                <li>
                                        <span>Energieträger:</span>
                                    <span>
                                        Elektro, Gas
                                    </span>
                                </li>
                                <li>
                                    <span>
                                        Zentralheizung
                                    </span>
                                </li>
                        </ul>

    <a href="#" class="btn_01 lightgray margin_bottom_20" id="aStromverbrauch" data-target="/expose/2LE9G4H/EnergyAsync" data-values="{&quot;ImageLink&quot;:&quot;~/App_Themes/GLOBAL/images/misc/goldgas.png&quot;,&quot;ButtonText&quot;:&quot;Ersparnis berechnen&quot;,&quot;ShowCalculatedUsage&quot;:true,&quot;CalculatorHeadline&quot;:&quot;Stromverbrauch f&#252;r dieses Objekt&quot;,&quot;Type&quot;:3,&quot;TrackingParameter&quot;:&quot;goldgas&quot;,&quot;Anbieter&quot;:2,&quot;LinkToMicrosite&quot;:&quot;http://customer.immowelt.de/goldgas?consumption={0}\u0026zip={1}\u0026energy_type={2}&quot;,&quot;IsStromGasSwitchable&quot;:true,&quot;ShowGasCalculator&quot;:true,&quot;ShowCalculator&quot;:true,&quot;Rooms&quot;:2,&quot;Etype&quot;:1,&quot;Esr&quot;:2,&quot;City&quot;:&quot;M&#252;nster&quot;,&quot;Zip&quot;:&quot;48155&quot;,&quot;Area&quot;:49.19}" data-tealium='{&quot;enh_promo_id&quot;: [&quot;goldgas&quot;], &quot;enh_promo_name&quot;: [&quot;goldgas&quot;], &quot;enh_promo_creative&quot;: [&quot;expose_teaser&quot;], &quot;enh_promo_position&quot;: [&quot;expose&quot;], &quot;page_type&quot;: &quot;promo_click&quot; }'>
            Strom-/Gasverbrauch ermitteln
    </a>
    <div id="divStromverbrauch" data-result="async"></div>
    



<div class="margin_bottom_20 ">
    <strong class="w_100p padding_bottom_20 margin_top_20">Energieausweis (Bedarfsausweis)</strong>

    <div class="w_500 margin_bottom_10 no_print">

            <div class="iw_left margin_right_40">
                <div class="energy_skala">
                    <div class="energy_bar">
                        <div class="energy_class">
                                <ul class="padding_none">
                                            <li>
                                                <span class="eclass_1">A+</span>
                                            </li>
                                            <li>
                                                <span class="eclass_2">A</span>
                                            </li>
                                            <li>
                                                <span class="eclass_3">B</span>
                                            </li>
                                            <li>
                                                <span class="eclass_4">C</span>
                                            </li>
                                            <li>
                                                <span class="eclass_5">D</span>
                                            </li>
                                            <li>
                                                <span class="eclass eclass_6">E</span>
                                            </li>
                                            <li>
                                                <span class="eclass_7">F</span>
                                            </li>
                                            <li>
                                                <span class="eclass_8">G</span>
                                            </li>
                                            <li>
                                                <span class="eclass_9">H</span>
                                            </li>
                                </ul>
                        </div>
                        <div class="energy_mark" style="width:55.64%;"><span class="mark_img"></span></div>
                    </div>
                </div>
            </div>
        
    </div>

    <div class="datatable energytable clear">

        <!-- Energieausweis Art : Verbrauch/Bedarf-->
            <div class="datarow clear">
                <span class="datalabel">Energieausweistyp</span>
                <span class="datacontent">Bedarfsausweis</span>
            </div>

        <!-- Gebäudetyp: Wohn-/Nicht-WohnGebäude -->
            <div class="datarow clear ">
                <span class="datalabel">Gebäudetyp</span>
                <span class="datacontent">Wohngeb&#228;ude</span>
            </div>

        <!-- Baujahr: laut Energieausweis/ von Expose -->
            <div class="datarow clear">
                <span class="datalabel">Baujahr laut Energieausweis</span>
                <span class="datacontent">1954</span>
            </div>

        <!-- Primärer Energieeräger-->

        <!-- Energieausweis Werte -->
            <div class="datarow clear">
                <span class="datalabel">Endenergiebedarf</span>
                <span class="datacontent">
                    153,00 kWh/(m²&middot;a)
                </span>
            </div>

            <div class="datarow clear">
                <span class="datalabel">Energieeffizienzklasse</span>
                <span class="datacontent">E</span>
            </div>



            <div class="datarow clear">
                <span class="datalabel">Gültigkeit</span>
                    <span class="datacontent">bis 27.02.2028</span>
            </div>


    </div>


</div>


            </div>
        </div>

                <div class="read clear">
<div>
    <link href="//vrweb15.linguatec.org/VoiceReaderWeb15User/player/styles/black/player_skin.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        (function (window) {
            window.vrweb_customerid = '11236';
            window.vrweb_player_form = '1';
            window.vrweb_icon = "<i class='icon-volume-up'></i>&nbsp;Vorlesen";
            window.vrweb_iconcolor = 'blue';
            window.vrweb_guilang = 'de';
            window.vrweb_lang = 'de-DE';
            /*window.vrweb_sndgender = 'M';*/
            window.vrweb_readelementsclass = 'read';
            window.vrweb_player_type = 'hover';
            window.vrweb_player_color = 'blue';
        })(this);
    </script>
    <script type="text/javascript" src="//vrweb15.linguatec.org/VoiceReaderWeb15User/player/scripts/readpremium15.js"></script>
</div>
<div class="h_005"></div>

<div class="read clear">
        <div class="clear">
            <div class="section_label iw_left">
                Ausstattung
            </div>
            <div class="section_content iw_right">
                <p>
                    Türsprechanlage; Isolierverglasung; Warmwasserbereiter; Waschmaschinenanschluss; Kellerraum; Gas-Zentralheizung; Rauchwarnmelder; Wärmedämmung der obersten Geschossdecke;
                </p>
            </div>
        </div>
        <div class="clear">
            <div class="section_label iw_left">
                Sonstiges
            </div>
            <div class="section_content iw_right">
                <p>
                    Um einen Besichtigungstermin zu buchen, können Sie jederzeit unter Angabe der Referenz-/Objektnummer 84-1138510007 unseren Chatbot unter bot.vonovia.de erreichen.Weitere Informationen hierzu und zum Thema Datenschutz erhalten Sie unter: www.vonovia.de/messenger<br/><br/>Die Kabelgebühren sind bereits in den Nebenkosten enthalten.<br/><br/> Hinweise darüber, wie die Vonovia Ihre personenbezogenen Daten als Interessent verarbeitet, erhalten Sie unter www.vonovia.de/datenschutzinformation. Alle Angaben in diesem Exposé wurden sorgfältig und so vollständig wie möglich gemacht. Gleichwohl kann das Vorhandensein von Fehlern nicht ausgeschlossen werden. Die Angaben in diesem Exposé erfolgen daher ohne jede Gewähr. Maßgeblich sind die im Mietvertrag geschlossenen Vereinbarungen. Soweit die Grundrissgrafiken Maßangaben und Einrichtungen enthalten, wird auch für diese jegliche Haftung ausgeschlossen. Mietverhandlungen sind ausschließlich über den benannten Ansprechpartner zu führen. Mietpreisänderungen bleiben vorbehalten.<br/><br/>Buchen Sie rund um die Uhr online Ihren Besichtigungstermin unter www.vonovia.de.<br/><br/>
                </p>
            </div>
        </div>
</div>
                </div>

        </div>

<div class="section_ad no_s iw_right">
    <div class="iw_left margin_right_20">
        <!-- /46859844/Immowelt.de_Expose_Teaser_Tablet -->
        <div id="div-gpt-ad-1444721221817-0"></div>
    </div>

    
<div class="text_adbox margin_right_20 no_m no_s"><ul class="padding_top_none"><li>    <a id="aCampaignLink9" href="#" class="js-tealium icon_01 no_margin" onclick="IwAG.Base.goToLink(false, 'customer.immowelt', 'de/', 'huk24/hausratversicherung', false, true);; return false;" linkid="9" data-tealium="{&quot;enh_promo_id&quot;: [&quot;huk24&quot;], &quot;enh_promo_name&quot;: [&quot;huk24_hausratversicherung&quot;], &quot;enh_promo_creative&quot;: [&quot;expose_teaser&quot;], &quot;enh_promo_position&quot;: [&quot;expose&quot;], &quot;page_type&quot;: &quot;promo_click&quot; }">
        <img class="w_100p padding_bottom_20" style="max-width:70%; max-height:70%;" src="../../App_Themes/GLOBAl/images/logo/logo_huk24.png"/><strong>Hausratversicherung</strong><br /> Rundumschutz für Ihre Einrichtung: Jetzt Beitrag berechnen!
    </a>
</li></ul></div>    <div class="text_adbox margin_right_20 no_m no_s"><ul class="linklist_icon_01 padding_top_none"><li>    <a id="aCampaignLink9" href="#" class="js-tealium icon_01 no_margin" onclick="IwAG.Base.goToLink(false, 'pubads.g.doubleclick', 'net/', 'gampad/clk?id=52445684&amp;iu=/46859844/Clicktracker', false, true);; return false;" linkid="9" data-tealium="{&quot;enh_promo_id&quot;: [&quot;goldgas&quot;], &quot;enh_promo_name&quot;: [&quot;goldgas-text&quot;], &quot;enh_promo_creative&quot;: [&quot;expose_teaser&quot;], &quot;enh_promo_position&quot;: [&quot;expose&quot;], &quot;page_type&quot;: &quot;promo_click&quot; }">
        <strong>Energiekosten</strong><br />Jetzt Gaspreis berechnen und sparen!
    </a>
</li></ul></div>

    
    

    <div class="text_adbox margin_right_none">
        <ul class="linklist_icon_01 padding_top_none">

        
        



        
        </ul>
    </div>
</div>
    </div>
</div>



<div id="divLageinfos" class="clear">

<div class="section  no_grid_padding">
    <div class="content_wrapper">
        <div class="iw_content clear">

            <div class="section_wrapper iw_left">

                <h2>Lage</h2>
                <div class="clear">
                    <div class="section_label iw_left">
                        Objektanschrift
                    </div>

                    <div class="section_content iw_right">
                        <p>
Stehrweg 27<br />
                            48155 Münster (Mauritz)
                        </p>

                            <a href="#divLageinfos" data-tealium='{"event_action":"click", "event_category":"expose", "event_label":"umzugskosten_berechnen"}' class="btn_01 lightgray margin_top_10 margin_bottom_10 js-relocationCostButton js-tealium">Umzugskosten berechnen</a>

                        <div id="divRelocationCostContainer" data-url="/expose/2LE9G4H/relocationcostcalculator" data-param="https://www.immowelt.de/umzug?entladeLand=Deutschland&amp;entladePlz=48155&amp;entladeOrt=M%c3%bcnster&amp;entladeStr=Stehrweg%2027"></div>

                    </div>

                </div>

                <div class="clear" id="mapControls">
                    <div class="section_label iw_left">
                        Umgebungsinfos
                    </div>
                    <div class="section_content iw_right">
                        <div class="map_widget clear">
                            <a class="map_teaser location-exact js-aKarte"></a>
                                <ul class="poi_teaser padding_none clear">
                                        <li><a href="#" class="js-poiSelector" id="poilink1" data-name="&#196;rzte" data-value="20611,20531,21313,177"><span class="arzt"></span>&#196;rzte<span class="distance"></span></a></li>
                                        <li><a href="#" class="js-poiSelector" id="poilink2" data-name="B&#228;ckereien" data-value="1221,1163,8409"><span class="baecker"></span>B&#228;ckereien<span class="distance"></span></a></li>
                                        <li><a href="#" class="js-poiSelector" id="poilink3" data-name="Gastst&#228;tten" data-value="5662"><span class="restaurant"></span>Gastst&#228;tten<span class="distance"></span></a></li>
                                        <li><a href="#" class="js-poiSelector" id="poilink4" data-name="Kinderg&#228;rten" data-value="8132"><span class="kindergarten"></span>Kinderg&#228;rten<span class="distance"></span></a></li>
                                        <li><a href="#" class="js-poiSelector" id="poilink5" data-name="Lebensmittel" data-value="9359"><span class="lebensmittel"></span>Lebensmittel<span class="distance"></span></a></li>
                                        <li><a href="#" class="js-poiSelector" id="poilink6" data-name="Metzgereien" data-value="10300,4916"><span class="metzger"></span>Metzgereien<span class="distance"></span></a></li>
                                </ul>
                        </div>
                        <a href="#" class="link_02 js-aKarte no_l no_m">Karte anzeigen</a>
                        <a href="#" class="link_02 margin_right_10 no_s js-poiOpener">Umgebungskarte</a>

                            <a href="#" class="link_02 js-aRouteService no_s">Routenplaner</a>

                        <div class="h_010"></div>
                    </div>
                </div>


                <div class="section_ad iw_right"></div>

            </div>
        </div>
    </div>
</div>

<div class="loadscreen" id="mapLoadScreen" style="display: none;">
    <img src="//media-static.immowelt.org/app_themes/global_rwd/image/misc/map_loader.gif" />
</div>
<div id="mapOverlay" class="map_overlay clear" style="display: none;">
    <div id="exposeMapMenu" class="bingMapMenu">
        <a id="aCloseMap" href="#" class="close_large icon-close " title="Karte schließen"></a>
        <div class="map_navigation clear">
                <a id="aPoiService" href="#">Umgebungsinfos</a>
                            <a id="aRouteService" href="#">Route planen</a>
                            <a id="aCloseAddons" href="#" class="current no_s">Normalansicht</a>
        </div>
    </div>

    <div id="mapContainer" class="mapContainer">
        <div id="exposeMap" class="bingMap"></div>
    </div>

    <div id="mapAddons" class="map_addons clear hide_slide">

<div id="mapRouteSection" class="map-route-container relative" style="display: none;">
    <div id="aCloseRoute" class="slidepanel icon-angle-left no_s"></div>
    <div id="mapRouteInput" class="map-route-menu relative">
        <ul class="route_type clear margin_bottom_10">
            <li><a href="#" class="js-routeModeSelector icon-cab current" title="Auto" data-mode="DRIVING"></a></li>
            
            <li><a href="#" class="js-routeModeSelector icon-male" title="Zu Fuß" data-mode="WALKING"></a></li>
        </ul>
        <div class="clear padding_bottom_10">
            <div class="iw_left margin_right_10"><img src="/app_themes/global/images/maps/controlgifs/bingmap_routestart.png" width="22" height="22" /></div>
            <div class="iw_left">von&nbsp;</div>
        </div>
        
        <div class="input_customsearch w_100p">
            <input type="text" placeholder="Startadresse (Straße, Ort)" class="map-route-input margin_none" id="txtRouteStartAddress" />
        </div>
        <span id="btnGeolocation" class="btnGeolocation relative iw_right clear">
            <a id="aStartRouteGeolocation" title="Aktueller Standort" href="#" class="icon-crosshairs"></a>
        </span>
        <a id="aStartRouteCustom" href="#" class="btn_01 ci_color icon-angle-right search_btn iw_right"></a>


        <div class="color_f_22 margin_top_10" style="display: none;" id="errorRouteSummary"></div>

        <div class="h_020"></div>
        <div class="iw_left margin_right_10"><img src="/app_themes/global/images/maps/controlgifs/bingmap_routeend.png" width="22" height="22" /></div>
        <div class="iw_left">nach&nbsp;</div>
        <div class="h_005 clear"></div>
        <div id="txtRouteEndAddress" class="iw_left padding_bottom_10">
            Stehrweg 27, 48155 M&#252;nster (Mauritz)
        </div>
    </div>
    <div id="mapRouteResult" class="mapRouteResult" style="display: none;">
        <a id="mapRouteBackToInput" class="clear">
            <span class="map_back margin_left_none icon-left-big"></span>
            <h4 class="iw_left padding_bottom_10">Wegbeschreibung</h4>
        </a>
        <div id="routeResultOverview" class="map-route-result margin_bottom_10 clear" style="display: none;">
            <div class="iw_left margin_right_10"><img src="/app_themes/global/images/maps/controlgifs/bingmap_routestart.png" width="22" height="22" /></div>
            <div id="routeResultOverviewStart"></div>
            <div class="h_010"></div>
            <div class="iw_left margin_right_10"><img src="/app_themes/global/images/maps/controlgifs/bingmap_routeend.png" width="22" height="22" /></div>
            <div id="routeResultOverviewEnd" class="padding_bottom_20">
                Stehrweg 27, 48155 M&#252;nster (Mauritz)
            </div>
        </div>
        <div id="divRouteResult" class="map-route-result"></div>
    </div>
</div>

<div id="poiMainSection" class="poi-searchbox relative" style="display: none;">
    <div id="poiResults" class="poi-results">
        <div class="poi_logoContainer">
            <a href="https://www.gelbeseiten.de" rel="nofollow" target="_blank" class="poi_logo"></a>
        </div>
        <div id="poiSelection" class="poi-selection poi-result-list">
            <div class="poi-checkbox-list clear">
                    <div class="NavBar_checkControl">
                        <input type="checkbox" id="poiCheckbox0"
                               value="487"
                               data-id="poiCheckbox0"
                               data-search="487"
                               data-name="Apotheken"
                               class="js-poicheckbox checkbox-item" />
                        <label for="poiCheckbox0" data-id="poiCheckbox0" data-search="487" class="js-poicheckboxlabel">
                            <img src="//media-static.immowelt.org/app_themes/global/images/maps/poi/map_poi_apotheke_white.png">
                            <span> Apotheken</span>
                        </label>
                    </div>
                    <div class="NavBar_checkControl">
                        <input type="checkbox" id="poiCheckbox1"
                               value="20611,20531,21313,177"
                               data-id="poiCheckbox1"
                               data-search="20611,20531,21313,177"
                               data-name="&#196;rzte"
                               class="js-poicheckbox checkbox-item" />
                        <label for="poiCheckbox1" data-id="poiCheckbox1" data-search="20611,20531,21313,177" class="js-poicheckboxlabel">
                            <img src="//media-static.immowelt.org/app_themes/global/images/maps/poi/map_poi_arzt_white.png">
                            <span> &#196;rzte</span>
                        </label>
                    </div>
                    <div class="NavBar_checkControl">
                        <input type="checkbox" id="poiCheckbox2"
                               value="1221,1163,8409"
                               data-id="poiCheckbox2"
                               data-search="1221,1163,8409"
                               data-name="B&#228;ckereien"
                               class="js-poicheckbox checkbox-item" />
                        <label for="poiCheckbox2" data-id="poiCheckbox2" data-search="1221,1163,8409" class="js-poicheckboxlabel">
                            <img src="//media-static.immowelt.org/app_themes/global/images/maps/poi/map_poi_baecker_white.png">
                            <span> B&#228;ckereien</span>
                        </label>
                    </div>
                    <div class="NavBar_checkControl">
                        <input type="checkbox" id="poiCheckbox3"
                               value="5662"
                               data-id="poiCheckbox3"
                               data-search="5662"
                               data-name="Gastst&#228;tten"
                               class="js-poicheckbox checkbox-item" />
                        <label for="poiCheckbox3" data-id="poiCheckbox3" data-search="5662" class="js-poicheckboxlabel">
                            <img src="//media-static.immowelt.org/app_themes/global/images/maps/poi/map_poi_gaststaette_white.png">
                            <span> Gastst&#228;tten</span>
                        </label>
                    </div>
                    <div class="NavBar_checkControl">
                        <input type="checkbox" id="poiCheckbox4"
                               value="8132"
                               data-id="poiCheckbox4"
                               data-search="8132"
                               data-name="Kinderg&#228;rten"
                               class="js-poicheckbox checkbox-item" />
                        <label for="poiCheckbox4" data-id="poiCheckbox4" data-search="8132" class="js-poicheckboxlabel">
                            <img src="//media-static.immowelt.org/app_themes/global/images/maps/poi/map_poi_kindergarten_white.png">
                            <span> Kinderg&#228;rten</span>
                        </label>
                    </div>
                    <div class="NavBar_checkControl">
                        <input type="checkbox" id="poiCheckbox5"
                               value="9359"
                               data-id="poiCheckbox5"
                               data-search="9359"
                               data-name="Lebensmittel"
                               class="js-poicheckbox checkbox-item" />
                        <label for="poiCheckbox5" data-id="poiCheckbox5" data-search="9359" class="js-poicheckboxlabel">
                            <img src="//media-static.immowelt.org/app_themes/global/images/maps/poi/map_poi_lebensmittel_white.png">
                            <span> Lebensmittel</span>
                        </label>
                    </div>
                    <div class="NavBar_checkControl">
                        <input type="checkbox" id="poiCheckbox6"
                               value="10300,4916"
                               data-id="poiCheckbox6"
                               data-search="10300,4916"
                               data-name="Metzgereien"
                               class="js-poicheckbox checkbox-item" />
                        <label for="poiCheckbox6" data-id="poiCheckbox6" data-search="10300,4916" class="js-poicheckboxlabel">
                            <img src="//media-static.immowelt.org/app_themes/global/images/maps/poi/map_poi_metzger_white.png">
                            <span> Metzgereien</span>
                        </label>
                    </div>
                    <div class="NavBar_checkControl">
                        <input type="checkbox" id="poiCheckbox7"
                               value="20351,11899,1778,260,2055,24135,24138,7585,217"
                               data-id="poiCheckbox7"
                               data-search="20351,11899,1778,260,2055,24135,24138,7585,217"
                               data-name="Schulen"
                               class="js-poicheckbox checkbox-item" />
                        <label for="poiCheckbox7" data-id="poiCheckbox7" data-search="20351,11899,1778,260,2055,24135,24138,7585,217" class="js-poicheckboxlabel">
                            <img src="//media-static.immowelt.org/app_themes/global/images/maps/poi/map_poi_schule_white.png">
                            <span> Schulen</span>
                        </label>
                    </div>
            </div>

            <div class="h_010"></div>
            <label>Nach Stichwort suchen&nbsp;</label>
            <div class="input_customsearch w_100p">
                <input type="text" placeholder="z.B. Tankstellen" class="margin_none" id="inputCustom" value="">
            </div>
            <a id="btnPoiCustom" href="#" class="btn_01 ci_color icon-angle-right search_btn iw_right margin_top_05"></a>
            <a id="btnClearSearch" href="#" class="btn_01 lightgray w_100p margin_top_20 icon-close" title="Aktuelle Suchkreterien der Umgebungsinfos entfernen">
                Umgebungssuche löschen
            </a>

        </div>
        <div id="aClosePoi" class="slidepanel icon-angle-left"></div>

        <div id="poiList" class="poi-result-list" style="display: none;">
            <a id="mapPoiBackToInput" class="clear">
                <span class="map_back icon-left-big"></span>
                <h4 id="mapPoiCategory" class="iw_left w_080p padding_bottom_10"></h4>
            </a>
            <div id="divErr">
            </div>

            <div id="divResultOuter" class="poi-result margin_bottom_10 clear">
                <img src="/app_themes/global_rwd/image/misc/map_loader.gif" class="iw_center" id="imgPoiLoader">
                <div id="divResultInner" class="poi-result-inner">
                </div>
            </div>

            <div id="divPaging">
            </div>
        </div>
    </div>
    <div id="poiAdverts" class="poi-adverts">
        <div class="poi-adverts-header">Lokale Anzeigen</div>
        <div id="divAdsContent">
        </div>
    </div>
</div>

    </div>
</div>



        <div class="section SectionAfterMap">
            <div class="content_wrapper">
                <div class="iw_content clear">

                        <div class="section_wrapper clear">

<div class="read clear">
        <div class="clear">
            <div class="section_label iw_left">
                Lage
            </div>
            <div class="section_content iw_right">
                <p>
                    Nur fünf Minuten zur Bushaltestelle, ganz schnell an der B51: Auch wenn Sie nicht mitten in Münster wohnen, haben Sie eine tolle Anbindung und können so das Münsteraner Flair genießen! Und auch mit dem Rad kommen Sie problemlos in die Innenstadt. Wer gern abends ausgeht, findet schöne Cafés in den umliegenden Straßen. Große Grünflächen in der Umgebung laden zu Spaziergängen ein.
                </p>
            </div>
        </div>
</div>

                        
                        </div>


<div class="section_wrapper clear">
    <div class="section_label iw_left">
        Stadtteilinfos
    </div>

    <div class="section_content iw_right">

        <div class="wl_infos iw_left">
            <strong>M&#252;nster (Mauritz)</strong>

                <div class="h_005"></div>
                <div id="divRating" class="fa-star_4"></div>
(4,4)                            <div class="h_010"></div>
                <a class="link_02" href="https://www.immowelt.de/regioinfos/regioinfosdetail.aspx?geoid=10805515000016" target="_blank">Stadtteil entdecken</a>
        </div>
    </div>

        <div class="section_content iw_right">
            <strong class="w_100p margin_bottom_10">Mietpreise in Münster (Mauritz)</strong>

            <a href="https://www.immowelt.de/immobilienpreise/muenster-mauritz/mietspiegel" class="link_02 no_margin" target="_blank">zum Mietspiegel in M&#252;nster (Mauritz) </a>
        </div>
</div>


                </div>
            </div>
        </div>
</div>
<div id="divAnbieter" class="js-content section section_anbieter clearfix">
    <div class="content_wrapper">
        <div class="iw_content clear">
            <div class="section_wrapper iw_left">

                <h2>Anbieter dieser Immobilie</h2>

                <div class="clear">
                    <div class="section_label iw_left">Kontakt</div>

                        <div class="clear">
                            <div class="section_content iw_right relative">
                                <div class="grid_row order_flex_s">
                                    <div class="grid_06o12_l grid_06o12_m grid_12o12_s order_2_s padding_bottom_none">
<strong><a href="https://www.immowelt.de/profil/5c57dbf035d14db480c633dcc7d40138" target="_blank" class="linklist_icon_header"><span id="f8f3f5e6e3174c419ab82903876084f0"></span></a></strong>                                                                                    <p>
                                                <span id="0f0bd7edc2294fa78d0b881c310820b6"></span><br />
                                                <span id="4f2e6c6aa0e74e08af62f017cb5f8371"></span>
                                                <span id="a2e3b2e90ea848ea98d81c1a6d4e206e"></span>
                                            </p>
                                                                                                                            <ul class="linklist_icon_01">
                                                <li><a href="https://www.immowelt.de/profil/5c57dbf035d14db480c633dcc7d40138#impressum" target="_blank"><span>Impressum</span></a></li>
                                                <li><a href="https://www.immowelt.de/profil/5c57dbf035d14db480c633dcc7d40138" target="_blank"><span>mehr Infos zum Anbieter</span></a></li>
                                                <li><a href="http://www.vonovia.de" target="_blank" rel="nofollow"><span>zur Homepage des Anbieters</span></a></li>
                                            </ul>
                                                                            </div>
                                        <div class="grid_06o12_l grid_06o12_m grid_12o12_s order_1_s no_grid_padding">
                                            <div class="customerlogo_big">
                                                <img src="https://filestore.immowelt.de/Logo/top_1645fe74f6b34115bece48d75e6e113d.jpg" />
                                            </div>
                                        </div>
                                    
                             
                                            <div class="grid_03o12_l grid_03o12_m grid_12o12_s order_3_s no_grid_padding partner_batch_logo margin_top_10_s margin_bottom_10">
                                                <img class="" src="//media-static.immowelt.org/app_themes/global_rwd/image/logo/partner-awards/partneraward_partner_duo.svg" width="120" title="Dieser Anbieter ist erfolgreicher Partner der Immowelt AG." id="imgPartnerBadge">
                                            </div>
                                        
                                        

                                            
                                </div>
                            </div>
                        </div>
                            <div class="clear margin_bottom_20">
                                <div class="section_label iw_left">
                                    Ansprechpartner
                                </div>
                                <div class="section_content iw_right">



                                    <div class="iw_left">
                                        <p>
                                            <strong><span id="593849eebcde40beb651206c6a28e5ce"></span></strong><br />
                                        </p>

                                        <div id="anbieterKontaktData"></div>
                                    </div>

                                </div>
                            </div>
                </div>
            </div>
                <div id="furtherResultData"></div>
        </div>
    </div>
</div>



<div id="divService" class="section service">
    <div class="content_wrapper">
        <div class="iw_content clear">
            <div class="relative">
                <div class="section_wrapper iw_left">
                    <h2>Servicebereich</h2>

                    <div class="clear">
                        <div class="section_label iw_left">
                            Dienstleistungen
                        </div>

                        <div class="section_content iw_right">
                            <ul class="linklist_icon_01 iw_left">

                                                                    <li><a href="/immobilien/finanzierungsrechner.aspx?npv=50" target="_blank">Finanzierungsrechner</a></li>
                                                                                                    <li><a href="javascript:IwAG.Expose.getInstance().OpenODA('52934893');">Immobilienfinanzierung</a></li>
                                                                    <li><a href="/finanzierung/kreditvergleich.aspx" target="_blank">Smava Kreditvergleich</a></li>
                                                                                                                                    <li><a href="http://customer.immowelt.de/hausdesmonats/index" target="_blank">H&auml;user: Bildergalerie ansehen</a></li>
                                                                    <li><a href="/prospektservice/suche.aspx" target="_blank">kostenloser Prospektservice</a></li>
                                                                    <li><a href="/anbieten/mietvertrag-pruefen" target="_blank">Mietvertrag prüfen</a></li>
                                                                    <li class="no_l">
                                        <a href="http://pubads.g.doubleclick.net/gampad/clk?id=52445684&iu=/46859844/Clicktracker" class="js-tealium" data-tealium="{&quot;enh_promo_id&quot;: [&quot;quot;goldgas&quot;], &quot;enh_promo_name&quot;: [&quot;goldgas-text&quot;], &quot;enh_promo_creative&quot;: [&quot;expose_teaser&quot;], &quot;enh_promo_position&quot;: [&quot;expose&quot;], &quot;page_type&quot;: &quot;promo_click&quot; }" target="_blank">
                                            Energiekosten: Gaspreis berechnen und sparen
                                        </a>
                                    </li>

                            </ul>
                        </div>
                    </div>

                        <div class="clear">
                            <div class="section_label iw_left">
                                Regionale Anbieter
                            </div>
                            <div class="section_content iw_right">
                                <ul class="linklist_icon_01 iw_left">
                                        <li><a id="hplUmzug" href="https://ratgeber.immowelt.de/umzugsunternehmen.html?entladeLand=Deutschland&amp;entladePlz=48155&amp;entladeOrt=M%c3%bcnster&amp;entladeStr=Stehrweg%2027" class="icon_01 no_margin" target="_blank">Umzugsunternehmen</a></li>
                                                                                                                <li><a id="link_maler" href="https://maler.immowelt.de/start/" class="js-tealium" data-tealium="{&quot;enh_promo_id&quot;: [&quot;malervergleich&quot;], &quot;enh_promo_name&quot;: [&quot;malervergleich&quot;], &quot;enh_promo_creative&quot;: [&quot;expose_teaser&quot;], &quot;enh_promo_position&quot;: [&quot;expose&quot;], &quot;page_type&quot;: &quot;promo_click&quot; }" target="_blank">Malerunternehmen</a></li>
                                </ul>
                            </div>

                        </div>
                </div>


                <!-- /46859844/Immowelt.de_Expose_MediumRectangle -->
                <div class="service_ad" id="div-gpt-ad-1442929670949-0"></div>

            </div><!-- relative -->

        </div>
    </div>
</div>
<div class="content_wrapper">
    <div class="iw_content topbar margin_bottom_20 margin_top_30 clear">
    
<div class="left">
    <a href="/liste/muenster/wohnungen/mieten?cp=1#52934893" title="Zur&#252;ck zur Ergebnisliste" class="btn_01 lightgray icon-angle-left iw_left margin_right_10 w_auto_s">
        <span class="no_l no_m">Zur&#252;ck</span>
        <span class="no_s">Zur Ergebnisliste</span>
    </a>
</div>

       
        <a class="btn_01 lightgray icon-angle-up iw_right no_s" href="#" onclick="IwAG.Base.getInstance().scrollTop(); return false;">nach oben</a>
        
        <a class="btn_01 lightgray icon-angle-up iw_right btn_only_icon no_m no_l w_auto_s" href="#" onclick="IwAG.Base.getInstance().scrollTop(); return false;"></a>
    </div>

    <div class="browsebar bottom iw_left no_s no_m">

<div class="browse">
        <span class="exposenumber">Expos&#233; 1 von 44</span>
        <a class="nextPage" href="/expose/2LC9G4H?bc=101">
            <i class="icon-angle-right"></i>
        </a>
</div>

    </div>
</div>
<div id="divFraud" class="section melden section_form display_none js-asyncBox">
    <div class="content_wrapper">
        <div class="iw_content relative clear padding_none_s">

            <a href="javascript:void(0)" class="close_large icon-close js-CloseContactForm"></a>

                <form id="formBetrugMelden" data-result="async" action="javascript:IwAG.Base.asyncFormPost('formBetrugMelden', '/expose/2LE9G4H/betrugmelden', '')" method="post">
                    <input id="EstateGuid" name="EstateGuid" type="hidden" value="7414ECAF2B8444119B6B2C00569AD02C" />
                    <input data-val="true" data-val-required="The OnlineId field is required." id="OnlineId" name="OnlineId" type="hidden" value="2LE9G4H" />
                    <input id="AnzeigenUeberschrift" name="AnzeigenUeberschrift" type="hidden" value="Ansehen: frisch modernisiertes Wohnglück" />
                    <div class="no_l no_m no_s">
    <label>
        Geben Sie eine gültige Email-Adresse an.
        <input type="text" id="ValidatationForEmail291" name="ValidatationForEmail" autocomplete="off" />
    </label>
    <input type="hidden" id="objectVal800" name="objectVal" value="ol70qUDhW3Dq063BSWNdLIMoIYamEn2mQBay3kTiQhbKEBngk9mGN4+YGN3Fn7+EfmgkAls01rA0keadBrFd6Q==" />
</div>

                    <h2 class="no_padding_bottom">Dieses Angebot an Immowelt melden</h2>
                    <div class="clear">
                        <div class="grid_06o12_l grid_06o12_m grid_12o12_s padding_bottom_none">
                            <h4>Das Immobilienangebot...</h4>


                            <label class="check margin_none">wirbt mit unglaubw&#252;rdigen Inhalten. Es k&#246;nnte sich um ein unseri&#246;ses Angebot handeln.<input id="Dubious" name="Dubious" type="checkbox" value="true" /><input name="Dubious" type="hidden" value="false" /></label>
                            <label class="check margin_none">ist keine reale Immobilie, sondern vage Planung<input id="NoRealEstate" name="NoRealEstate" type="checkbox" value="true" /><input name="NoRealEstate" type="hidden" value="false" /></label>
                            <label class="check margin_none">wird zweckentfremdet als Anzeige (Unternehmensdarstellung, ...)<input id="Advertisement" name="Advertisement" type="checkbox" value="true" /><input name="Advertisement" type="hidden" value="false" /></label>
                            <label class="check margin_none">enth&#228;lt falsche Ortsangaben<input id="WrongLocation" name="WrongLocation" type="checkbox" value="true" /><input name="WrongLocation" type="hidden" value="false" /></label>
                            <label class="check">ist bereits verkauft/vermietet<input id="NotAvailable" name="NotAvailable" type="checkbox" value="true" /><input name="NotAvailable" type="hidden" value="false" /></label>
                        </div>

                        <div class="grid_06o12_l grid_06o12_m grid_12o12_s no_grid_padding">
                            <label>Erläuterung zur Beanstandung</label>
                            <textarea class="w_100p" cols="20" id="Freetext" name="Freetext" rows="5">
</textarea>

                            <span class="field-validation-valid" data-valmsg-for="Freetext" data-valmsg-replace="true"></span>
                            <small class="comment">(Die Eingabe ist auf 5000 Zeichen begrenzt.)</small>

                            <button class="btn_01 ci_color icon-mail iw_right margin_top_10" type="submit">
                                &nbsp;Absenden
                            </button>
                        </div>

                    </div>


                </form>
        </div>
    </div>
</div>

<div class="notification_popup">
    <div class="js-tealium" id="notificationPopup" data-updateurl="/expose/2LE9G4H/NotificationPopupClosed" data-tealium='{"event_action":"click", "event_category":"button-intern", "event_label":"expose_notification_contact", "event_type":"click"}'>
        <div class="teaser_special" style="cursor: pointer; position:relative;">
            <a href="javascript:void(0);" class="close_large icon-close js-Close padding_top_none"></a>
            <p class="padding_none">
                <span class="icon-clock"></span>
                Sichern Sie sich jetzt einen <br />Besichtigungstermin.
            </p>
        </div>
    </div>
</div>

    

    <div class="feedback color_bg_05 section no_print">
        <div class="t_center padding_top_30">
            <div class="iw_content">
                <div class="grid_row">
                    <form data-result="async" id="formMeinungAbgeben">
                        <div class="grid_12o12_l grid_12o12_m grid_12o12_s padding_bottom_none">
                            <h2 class="padding_bottom_20">Wie können wir Ihnen helfen?</h2>
                            <div class="grid_03o12_l grid_06o12_m grid_12o12_s padding_right_none">
                                <div class="grid_04o12  margin_top_10 padding_right_20"><p class="icon-home big-icon t_right"> </p></div>
                                <div class="grid_08o12 t_left padding_right_none"><p class="padding_bottom_none"><strong>Fragen zum Objekt?</strong> </p><a class="link_02 js-OpenContact">Anbieter kontaktieren</a></div>
                            </div>
                            <div class="grid_03o12_l grid_06o12_m grid_12o12_s padding_right_none">
                                <div class="grid_04o12  margin_top_10 padding_right_20"><p class="icon-life-ring big-icon t_right"> </p></div>
                                <div class="grid_08o12 padding_right_none t_left"><p class="padding_bottom_none"><strong>Brauchen Sie Hilfe?</strong> </p><a class="link_02" href="https://www.immowelt.de/immoweltag/kundenservice/hotline" target="_blank">Support kontaktieren</a></div>
                            </div>
                            <div class="grid_03o12_l grid_06o12_m grid_12o12_s padding_right_none">
                                <div class="grid_04o12 margin_top_10 padding_right_20"><p class="icon-user-secret big-icon t_right"> </p></div>
                                <div class="grid_08o12 t_left padding_right_none"><p class="padding_bottom_none"><strong>Ist das Exposé verdächtig?</strong> </p><a class="link_02 js-OpenFraud">Exposé melden</a></div>
                            </div>
                            <div class="grid_03o12_l grid_06o12_m grid_12o12_s padding_right_none">
                                <div class="grid_04o12 margin_top_10 padding_right_20"><p class="icon-smile big-icon t_right"> </p></div>
                                <div class="grid_08o12 t_left padding_right_none"><p class="padding_bottom_none"><strong>Haben Sie Anregungen?</strong> </p><a class="iw_left link_02" href="javascript:IwAG.Base.asyncHtmlGet('formMeinungAbgeben', { 'datainput': true, 'expose': true }, 'meinungabgeben', 'meinungabgeben', 'mvcroot');">Ihre Meinung zu Immowelt</a></div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


    </div>

    


<div class="footer no_print color_bg_06 arrange_layer">
    <div class="content_wrapper">
        <div class="iw_content">
            <ul class="linklist_service">
                <li><a href="https://www.immowelt.de/immoweltag/kundenservice/hotline">Hilfe & Kontakt</a></li>
                <li><a href="/immoweltag/agb">AGB</a></li>
                <li><a href="/immoweltag/datenschutz">Datenschutz</a></li>
                <li><a href="/immoweltag/onlinewerbung">Nutzungsbasierte Online Werbung</a></li><br class="no_s" />
                <li><a href="/immoweltag/impressum">Impressum</a></li>
                <li><a href="/immoweltag/wir/index">Über uns</a></li>
                <li><a href="https://presse.immowelt.de">Presse</a></li>
                <li><a href="/immoweltag/newsletter">Newsletter</a></li>
                <li><a href="https://jobs.immowelt.de" target="_blank">Jobs</a></li>
                <li><a href="/sitemap">Sitemap</a></li>
            </ul>
            <div class="h_050 color_bg_06 no_s"></div>
            <div class="h_040 color_bg_06 no_m no_l"></div>
            <ul class="linklist_social">
                
                <li>
                    <!-- facebook -->
                    <a onclick="IwAG.Base.goToLink(1,'facebook','com/','immowelt');" href="javascript:void(0);" class="icon-facebook  color_f_31" title="Immowelt auf Facebook"></a>
                </li>
                <li>
                    <!-- instagram -->
                    <a onclick="IwAG.Base.goToLink(1,'instagram','com/','immowelt');" href="javascript:void(0);" class="icon-instagram  color_f_01" title="Immowelt auf Instagram"></a>
                </li>
                <li>
                    <!-- xing -->
                    <a onclick="IwAG.Base.goToLink(1, 'xing', 'com/', 'companies/immoweltag');" href="javascript:void(0);" class="icon-xing color_f_33" title="Immowelt auf Xing"></a>
                </li>
                <li>
                    <!-- twitter -->
                    <a onclick="IwAG.Base.goToLink(0, 'twitter', 'com/', 'immowelt');" href="javascript:void(0);" class="icon-twitter  color_f_34" title="Immowelt auf Twitter"></a>
                </li>
                <li>
                    <!-- youtube -->
                    <a onclick="IwAG.Base.goToLink(1, 'youtube', 'com/', 'immowelt');" href="javascript:void(0);" class="icon-youtube-play color_f_35" title="Immowelt auf YouTube"></a>
                </li>
                <li>
                    <!-- LinkedIn -->
                    <a onclick="IwAG.Base.goToLink(1, 'linkedin', 'com/', 'company/immowelt-ag');" href="javascript:void(0);" title="Immowelt auf LinkedIn" class="icon-linkedin color_f_36"></a>
                </li>
                <li>
                    <!-- pinterest -->
                    <a onclick="IwAG.Base.goToLink(1, 'pinterest', 'de/', 'immowelt');" href="javascript:void(0);" title="Immowelt auf Pinterest" class="icon-pinterest-circled color_f_37"></a>
                </li>
            </ul>
            <div class="h_050 no_s relative"></div>
            <div class="h_040 color_bg_06 no_m no_l relative"></div>
            <ul class="linklist_service">
                <li><a href="/anbieten/gewerbe/immowelt-software">Immobilien-Software</a></li>
                <li><a onclick="IwAG.Base.goToLink(1, 'immowelt-media', 'de', '');" href="javascript:void(0);">Werben auf immowelt.de</a></li>
                <li><a href="/immoweltag/internetprodukte/immobilien-app.aspx">mobile Apps/Gadgets</a></li>
            </ul>
        </div>
    </div>
</div>
<div class="h_080 no_s color_bg_06 no_print relative"></div>
<div class="h_050 no_m color_bg_06 no_l relative"></div>

            

            <div class="color_bg_06 relative">
                <div class="content_wrapper">
                    <div class="iw_content">
                        <p class="footer-copyright">
                            &copy; Immowelt AG 2018

                            <span class="registered">&reg; Marktplatz für Wohnungen, Immobilien und Häuser zum Kaufen oder Mieten</span>
                        </p>
                    </div>
                </div>
            </div>
        </div>

<script>
    (function ($) {
        IwAG.Vars = $.extend(IwAG.Vars, {
            googleDoubleClickValues: {"Title":"Ansehen: frisch modernisiertes Wohnglück","Plz":"48155","Baujahr":"1954","Preis":"430","ZimmerAnzahl":"2","Wohnflaeche":"49,19","Grundstueckflaeche":"","Bild":"https://media-thumbs1.immowelt.org/1/d/e/6/100_2dbefc28aa624ca1a0a33d6f3bff6ed1.jpg","AnbieterGuid":"5C57DBF035D14DB480C633DCC7D40138","PriceRange":"0-500","AdGroup":"IMMOBILIEN_EXPOSE","SubAdGroup":null,"Immobilienart":1,"Kategorie":"Etagenwohnung","MieteKauf":2,"GeoId":"10805515000016","Ort":"Münster","OrtClean":"Münster","Nutzertyp":12,"DisableAdvertisement":false,"PageHasContentAdd":false}
        });
    }(IwAG.$ || jQuery));
</script>


<div id="container-superbanner" class="superbanner hide_ad">
    <div id="div-gpt-ad-1426592241524-1"></div>
</div>
<div id="container-skyscraper" class="skyscraper hide_ad">
    <div id="div-gpt-ad-1426592241524-0"></div>
</div>


        <script type="text/javascript" src="//media-static.immowelt.org/_scripts/mvc/bundles/masterbody.rwdDesktop.pack.js?b58c4301768e001f20f7262a83c82f1a" crossorigin ></script>

        <script type="text/javascript" src="//media-static.immowelt.org/_ts/iwag/basewithglobals.pack.js?636662108473026290" crossorigin></script>
        <script type="text/javascript" src="//media-static.immowelt.org/_scripts/mvc/images/picturefill_2.2.0.pack.js?636662108473026290" crossorigin></script>
        <script type="text/javascript" src="//media-static.immowelt.org/_ts/iwag/header/default.pack.js?636662108473026290" crossorigin></script>


        

        <script type="text/javascript" src="//media-static.immowelt.org/_scripts/mvc/bundles/owl.carouselDesktop.pack.js?2380989abaa4694a318605a594a50154" crossorigin ></script>
        <script type="text/javascript" src="//media-static.immowelt.org/_scripts/mvc/bundles/expose_rwd_newDesktop.pack.js?8d7241d66b39e4147bc6fe04fe93e4f8" crossorigin ></script>





        <!--[if !(IE 8)]><!-->
        <script src="//media-static.immowelt.org/_scripts/mvc/_plugin/hammer.min.js?636662108473026290"></script>
        <!--[endif]-->


<script type="text/javascript" src="//media-static.immowelt.org/_ts/iwag/expose/expose.bundle.pack.js?636662108473026290" crossorigin></script>

        <script type="text/javascript" crossorigin>
            (function ($) {
                IwAG.Vars = $.extend(IwAG.Vars, {
                    youtube: false,
                    exposeConfig: {"estateId":52934893,"estateGuid":"7414ECAF2B8444119B6B2C00569AD02C","globalObjectKey":"98081e39-c331-47b3-a8b3-5587fd86d480","slideShow":false,"startWithPanorama":false,"imageData":[{"order":0,"caption":"200 AUSSENANSICHTEN","srcThumbnail":"https://media-thumbs1.immowelt.org/1/D/E/6/100_2DBEFC28AA624CA1A0A33D6F3BFF6ED1.jpg","srcImageStage":"https://media-pics1.immowelt.org/1/D/E/6/600_2DBEFC28AA624CA1A0A33D6F3BFF6ED1.jpg","width":0,"height":0},{"order":1,"caption":"201 AUSSENANSICHTEN","srcThumbnail":"https://media-thumbs1.immowelt.org/1/F/F/3/100_7B45144E59974EEBBEF4BAAAD1123FF1.jpg","srcImageStage":"https://media-pics1.immowelt.org/1/F/F/3/600_7B45144E59974EEBBEF4BAAAD1123FF1.jpg","width":0,"height":0},{"order":2,"caption":"202 AUSSENANSICHTEN","srcThumbnail":"https://media-thumbs2.immowelt.org/8/5/7/A/100_7B890CD47212413E9A134487071AA758.jpg","srcImageStage":"https://media-pics2.immowelt.org/8/5/7/A/600_7B890CD47212413E9A134487071AA758.jpg","width":0,"height":0},{"order":3,"caption":"99998 USP Vermietung","srcThumbnail":"https://media-thumbs1.immowelt.org/4/5/5/A/100_E0E2953F48694BC597548C1D6F60A554.jpg","srcImageStage":"https://media-pics1.immowelt.org/4/5/5/A/600_E0E2953F48694BC597548C1D6F60A554.jpg","width":0,"height":0},{"order":4,"caption":"Vonovia_Chatbot","srcThumbnail":"https://media-thumbs2.immowelt.org/F/5/3/F/100_ECCB1A7E228E4E9F9463FC5FAD53F35F.jpg","srcImageStage":"https://media-pics2.immowelt.org/F/5/3/F/600_ECCB1A7E228E4E9F9463FC5FAD53F35F.jpg","width":0,"height":0}],"mediaCount":1},
                    MapOptions: {"description":"Münster (Mauritz)","zip":"481xx","country":"108","lat":51.95274,"lon":7.66586,"pinLat":51.95274,"pinLon":7.66586,"zoom":16,"showPin":true,"showPoi":true,"street":"Stehrweg 27","address":"48155 Münster (Mauritz)"},
                    streamingAp: 'Kundenservice'
                });
                $(function () {
                    if (IwAG.Common && IwAG.Common.Remember) {
                        IwAG.Common.Remember.getInstance().Initialize(0, false);
                    }
                    IwAG.Base.setData({eo:[{ i:'4f2e6c6aa0e74e08af62f017cb5f8371', v:'&#52;&#52;&#56;&#48;&#51;' },{ i:'af1f9953ad1642b89cdb417f76930ff3', v:'&#111;&#117;&#116;&#98;&#111;&#117;&#110;&#100;&#64;&#118;&#111;&#110;&#111;&#118;&#105;&#97;&#46;&#100;&#101;' },{ i:'e10fd193b5dc4be290dbe9ed92e214d5', v:'&#48;&#50;&#51;&#52;&#32;&#51;&#49;&#52;&#56;&#56;&#56;&#45;&#52;&#52;&#49;&#52;' },{ i:'a2e3b2e90ea848ea98d81c1a6d4e206e', v:'&#66;&#111;&#99;&#104;&#117;&#109;' },{ i:'0f0bd7edc2294fa78d0b881c310820b6', v:'&#80;&#104;&#105;&#108;&#105;&#112;&#112;&#115;&#116;&#114;&#46;&#32;&#51;' },{ i:'f8f3f5e6e3174c419ab82903876084f0', v:'&#86;&#111;&#110;&#111;&#118;&#105;&#97;&#32;&#75;&#117;&#110;&#100;&#101;&#110;&#115;&#101;&#114;&#118;&#105;&#99;&#101;&#32;&#71;&#109;&#98;&#72;' },{ i:'6ecb10d9aa8240eaa1827631d82af751', v:'&#48;&#50;&#51;&#52;&#32;&#52;&#49;&#52;&#55;&#48;&#48;&#48;&#45;&#48;&#53;' },{ i:'593849eebcde40beb651206c6a28e5ce', v:'&#32;&#75;&#117;&#110;&#100;&#101;&#110;&#115;&#101;&#114;&#118;&#105;&#99;&#101;' }]});

                });

            }(IwAG.$));
        </script>
    


<!-- ComScore Tracking -->

<script type="text/javascript">
    var _comscore = _comscore || [];
    _comscore.push({ c1: "2", c2: "11224366", c3: "", c4: "" });

    (function () {
        var s = document.createElement("script"), el = document.getElementsByTagName("script")[0]; s.async = true;
        s.src = (document.location.protocol == "https:" ? "https://sb" : "http://b") + ".scorecardresearch.com/beacon.js";
        el.parentNode.insertBefore(s, el);
    })();
</script>
<noscript>
    <img src="http://b.scorecardresearch.com/p?c1=2&amp;c2=11224366&amp;c3=&amp;c4=&amp;c5=&amp;c6=&amp;c15=&amp;cv=2.0&amp;cj=1" alt="" title="" style="border-style:none;" />
</noscript>




        <script type="text/javascript" src="//media-static.immowelt.org/_scripts/mvc/bundles/advertisementDesktop.pack.js?906feb60f6e4f0ee93796994fc2a25b8" crossorigin ></script>

    <script src="//media-static.immowelt.org/_scripts/mvc/novomindchat/novomind.pack.js?636662108473026290" type="text/javascript"></script>
    <a id="js-openNovomindChat" class="novomind_chat_wrapper" style="display: none;">
        <div class="chat_content"><span class="icon-chat-empty"></span><p>Live-Chat</p></div>
    </a>
    <div id="novomind_chat" class="nm-wrapper" style="display:none;">
        <span data-animation="hide-layer"></span>
        <iframe src=""></iframe>
    </div>
    </div>
</body>
</html>`;
