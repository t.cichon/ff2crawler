module.exports.data = `
<!DOCTYPE html>
<html lang="de" class="no-js">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,t,n){function r(n){if(!t[n]){var o=t[n]={exports:{}};e[n][0].call(o.exports,function(t){var o=e[n][1][t];return r(o||t)},o,o.exports)}return t[n].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<n.length;o++)r(n[o]);return r}({1:[function(e,t,n){function r(){}function o(e,t,n){return function(){return i(e,[f.now()].concat(u(arguments)),t?null:this,n),t?void 0:this}}var i=e("handle"),a=e(2),u=e(3),c=e("ee").get("tracer"),f=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,t){s[t]=o(d+t,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),t.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,t){var n={},r=this,o="function"==typeof t;return i(l+"tracer",[f.now(),e,n],r),function(){if(c.emit((o?"":"no-")+"fn-start",[f.now(),r,o],n),o)try{return t.apply(this,arguments)}catch(e){throw c.emit("fn-err",[arguments,this,e],n),e}finally{c.emit("fn-end",[f.now()],n)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,t){m[t]=o(l+t)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,f.now()])}},{}],2:[function(e,t,n){function r(e,t){var n=[],r="",i=0;for(r in e)o.call(e,r)&&(n[i]=t(r,e[r]),i+=1);return n}var o=Object.prototype.hasOwnProperty;t.exports=r},{}],3:[function(e,t,n){function r(e,t,n){t||(t=0),"undefined"==typeof n&&(n=e?e.length:0);for(var r=-1,o=n-t||0,i=Array(o<0?0:o);++r<o;)i[r]=e[t+r];return i}t.exports=r},{}],4:[function(e,t,n){t.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,t,n){function r(){}function o(e){function t(e){return e&&e instanceof r?e:e?c(e,u,i):i()}function n(n,r,o,i){if(!d.aborted||i){e&&e(n,r,o);for(var a=t(o),u=m(n),c=u.length,f=0;f<c;f++)u[f].apply(a,r);var p=s[y[n]];return p&&p.push([b,n,r,a]),a}}function l(e,t){v[e]=m(e).concat(t)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(n)}function g(e,t){f(e,function(e,n){t=t||"feature",y[n]=t,t in s||(s[t]=[])})}var v={},y={},b={on:l,emit:n,get:w,listeners:m,context:t,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",c=e("gos"),f=e(2),s={},p={},d=t.exports=o();d.backlog=s},{}],gos:[function(e,t,n){function r(e,t,n){if(o.call(e,t))return e[t];var r=n();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,t,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[t]=r,r}var o=Object.prototype.hasOwnProperty;t.exports=r},{}],handle:[function(e,t,n){function r(e,t,n,r){o.buffer([e],r),o.emit(e,t,n)}var o=e("ee").get("handle");t.exports=r,r.ee=o},{}],id:[function(e,t,n){function r(e){var t=typeof e;return!e||"object"!==t&&"function"!==t?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");t.exports=r},{}],loader:[function(e,t,n){function r(){if(!x++){var e=h.info=NREUM.info,t=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&t))return s.abort();f(y,function(t,n){e[t]||(e[t]=n)}),c("mark",["onload",a()+h.offset],null,"api");var n=d.createElement("script");n.src="https://"+e.agent,t.parentNode.insertBefore(n,t)}}function o(){"complete"===d.readyState&&i()}function i(){c("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),c=e("handle"),f=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,SI:p.setImmediate,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1071.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=t.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),c("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script>
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1.0, user-scalable=no">

            <meta name="google-site-verification" content="HAzvsHV13rWnbTt0s2Q-zTdu3CQ1MBnfRRmZ-ngQ2NQ" />
    
    <script type="text/javascript">
        dataLayer = [];
                    </script>

    
        <meta name="servertime" content="1534022185" />


            
            <meta name="robots" content="index, follow, noarchive, unavailable_after: Thursday, 07-Jun-18 00:00:00 UTC">

            <title>Großzügige Maisonettewohnung im Zentrum von Hiltrup! - immomarkt.ms</title>

            
                            <meta name="description" content="Mieten Wohnung Münster - Großzügige Maisonettewohnung im Zentrum von Hiltrup! - 5 Zimmer 190.00 m² für 1615 € - immomarkt.ms">
            
            
            

    
        <meta property="og:title" content="Großzügige Maisonettewohnung im Zentrum von Hiltrup!" />
        <meta property="og:description" content="Diese schön gelegene Maisonettewohnung befindet sich in einem sehr gepflegten Zweifamilienhaus im schönen Hiltrup und hat eine großzügige" />
        <meta property="og:image" content="https://cmcdn.de/c/082/9f4716d19cc4f6008580d3f12a0a3dced60c0.jpg" />
        <meta property="og:url" content="https://immomarkt.ms/immobilien/grosszuegige-maisonettewohnung-im-zentrum-von-hiltrup-8HFRLQ" />
    <meta property="og:locale" content="de_DE" />
    <meta property="og:site_name" content="immomarkt.ms" />
    <meta property="og:type" content="website" />
    
                                <script type="text/javascript">
                document.documentElement.className = "";

                window.onjQuery = {
                  queue: [],
                  push: function(f) {
                    if (typeof window.jQuery === 'function') {
                      f();
                    } else {
                      this.queue.push(f)
                    }
                  }
                };
            </script>
        

        <script type="text/javascript" role="cm-tracking">
            window.app = window.app || {};
            window.app.cmTracking = {"domain":"immomarkt.ms","site_code":"wfn_iwfn","external_unique_id":"4146569","listing_id":"8HFRLQ","advertizer_id":"51668"};
        </script>
    

        
<script type="text/javascript" role="public-api">

/**
 * Use this to customize your advertisement based on the search parameters or
 * listing.
 */
window.cmAdvertisementContext = {
    /**
     * pageType
     *
     * Type of the currently active page. Possible values are "homepage", "serp"
     * (search result page), "expose", or the empty string otherwise.
     */
    pageType: "expose",
    /**
     * areaId
     *
     * The desired location of the listing on a serp. The value is either the
     * location id as a string (such as "de.berlin") or the empty string if the
     * location is undetermined.
     */
    areaId: "",
    /**
     * searchTerm
     *
     * The exact search term the user has entered on a serp. The value is either the
     * search term as a string (such as "Neustadt") or the empty string if no search
     * term was given.
     */
    searchTerm: "",
    /**
     * locality
     *
     * The locality of listing. The value is either the locality name as a string
     * (such as "Berlin") or the empty string if the locality is undetermined.
     */
    locality: "M\u00fcnster",
    /**
     * geoHierarchy
     *
     * The geographical hierarchy starting from country
     */
    geoHierarchy: {
        "country": {
            "id": "de.deutschland",
            "name": "Deutschland"
        },
        "state": {
            "id": "de.nordrhein-westfalen",
            "name": "Nordrhein-Westfalen"
        },
        "district": {
            "id": "de.muenster",
            "name": "M\u00fcnster"
        },
        "city": {
            "id": "de.muenster",
            "name": "M\u00fcnster"
        },
        "citydistrict": {
            "id": "de.muenster-hiltrup-mitte",
            "name": "M\u00fcnster-Hiltrup-Mitte"
        },
        "zip": {
            "id": "de.48165",
            "name": "48165"
        }
    },
    /**
     * postalCode
     *
     * The zip code of the listing or search. The value is either the zip code as a string
     * (such as "12345") or the empty string if the zip code is undetermined.
     */
    postalCode: "48165",
    /**
     * marketingTypes
     *
     * Whether the real estate (or desired real estate) is for rent or sale. Comma
     * separated list of the following values, in alphabetical order: "rental",
     * "sale".
     */
    marketingTypes: "rental",
    /**
     * propertyTypes
     *
     * The type of the real estate (or desired real estate). Comma separated list of
     * the following values, in alphabetical order: "house", "apartment",
     * "flat_share", "plot", "commercial".
     */
    propertyTypes: "apartment",
    /**
     * propertyPrice
     *
     * The price of the property, usually the monthly rent or one-time sale price. The
     * value is same that is displayed in places where only a single value is shown
     * (as opposed to a break down into e.g. price, tax and commission). One of these
     * places is the search result page.
     *
     * The value is expressed as the price in cents (or the equivalent for currencies
     * other than Euro).
     */
    propertyPrice: 161500,
    /**
     * advertiserId
     *
     * The id of the current listing's advertiser.
     */
    advertiserId: "51668",


};

</script>
    

            <!-- Sitename und Zone bitte auf Ihre Seite anpassen -->
<script language="JavaScript" type="text/javascript">
    var marketing = window.cmAdvertisementContext.marketingTypes;
    var zone = window.cmAdvertisementContext.pageType;

    if (marketing.length > 0) {
        zone = marketing + '_' + zone;
    }

    var oms_site = 'oms.immomarkt.ms';
    var oms_zone = zone;

    var isDesktop = window.innerWidth > 800;
</script>

<script type="text/javascript" src="https://www.video.oms.eu/ada/cloud/omsv_container_151.js"></script>

<script>
    try {
        var ystr = "";
        var y_adj = "";

        for (var id in yl.YpResult.getAll()) {

            c = yl.YpResult.get(id);
            ystr += ';y_ad=' + c.id;
            if (c.format) {
                y_adj = ';y_adj=' + c.format;
            }
        }

        ystr += y_adj + ';';
        WLRCMD = WLRCMD + ystr + segQS + crtg_content;
    }
    catch (err) {
    }
</script>

<script type='text/javascript'>
    //Synchron Call
    (function() {
        var useSSL = 'https:' == document.location.protocol;
        var src = (useSSL ? 'https:' : 'http:') + '//www.googletagservices.com/tag/js/gpt.js';
        document.write('<scr' + 'ipt src="' + src + '"></scr' + 'ipt>');
    })();
</script>


<script type='text/javascript'>
    googletag.cmd.push(function() {

        if (isDesktop) {
            googletag.defineSlot('/5766/' + oms_site + '/' + oms_zone, [728, 90], 'oms_gpt_superbanner').addService(googletag.pubads());
            googletag.defineSlot('/5766/' + oms_site + '/' + oms_zone, [[120, 600], [160, 600], [200, 600]], 'oms_gpt_skyscraper').addService(googletag.pubads());
        }

            googletag.defineSlot('/5766/' + oms_site + '/' + oms_zone, [[300, 250],[300, 600]], 'oms_gpt_rectangle').addService(googletag.pubads());
    googletag.defineSlot('/5766/' + oms_site + '/' + oms_zone, [300, 251], 'oms_gpt_home_rect_251').addService(googletag.pubads());

        googletag.defineOutOfPageSlot('/5766/' + oms_site + '/' + oms_zone, 'oms_gpt_outofpage').addService(googletag.pubads());

        googletag.pubads().enableSingleRequest();
        googletag.pubads().enableSyncRendering(); // Add sync rendering mode

        googletag.pubads().setTargeting("mtype", window.cmAdvertisementContext && window.cmAdvertisementContext.marketingTypes);
        googletag.pubads().setTargeting('bundesland', 'NW');
        googletag.enableServices();


        if (typeof WLRCMD != 'undefined' && WLRCMD != '') {
            temp = WLRCMD.split(";");
            for (var id in temp) {
                if (temp[id].indexOf('=') != -1) {

                    values = temp[id].split('=')[1];

                    for (var id2 in temp) {
                        if ((temp[id2].indexOf('=') != -1) && (temp[id].split('=')[0] == temp[id2].split('=')[0]) && (id < id2)) {
                            values += ';' + temp[id2].split('=')[1];
                            delete temp[id2];
                        }
                    }
                    temp2 = values.split(";");
                    //	console.log(temp[id].split('=')[0]+' '+temp2)
                    //console.log("googletag.pubads().setTargeting("+temp[id].split('=')[0]+", "+temp2+")");
                    googletag.pubads().setTargeting(temp[id].split('=')[0], temp2);
                }
            }
        }
    });
</script>
    

                        <noscript>
            <link rel="stylesheet" href="/css/19a4a92-f894dae.css?99f1c194" />
        </noscript>
        
        <link rel="stylesheet" href="/css/efa6ae7-db00dd8.css?99f1c194" />

    

        <link rel="stylesheet" href="/css/74854bc-ac8f035.css?99f1c194" />
    
    
        <link rel="stylesheet" media="only print" href="/css/a830fe7-657bd42.css?99f1c194" />
    
    
    
    
            <link rel="shortcut icon" type="image/x-icon" href="/bundles/wfniwfnsite/images/favicon.ico?99f1c194" />
        </head>

<body class="expose default body-bg--2">



<!-- Google Tag Manager -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PDTRCH" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>

<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-PDTRCH');
</script>
<!-- End Google Tag Manager -->


<div id="js-screen-size" class="screen-size"></div>




<div class="container root-container">

    <div class="page-wrapper">

    
<div class="head-banners-wrap js-head-banners-wrap clearfix hidden-print">
    <div id="superbanner" class="super">
            
            
            
            
            <div id='oms_gpt_superbanner'>
    <script type="text/javascript">
        if (isDesktop) {
            googletag.cmd.push(function() {
                googletag.display('oms_gpt_superbanner')
            });
        }
    </script>
</div>
    
                        </div>

    <div id="skyscraperLeft" class="skyscraper-left js-skyscraper">
                        
                        
                        
                        
                
                        </div>

    <div id="skyscraper" class="skyscraper js-skyscraper">
            
            
            
            
            <div id="oms_gpt_skyscraper" class="visible-lg">
    <script type="text/javascript">
        if (isDesktop) {
            googletag.cmd.push(function() {
                googletag.display('oms_gpt_skyscraper')
            });
        }
    </script>
</div>
    
                        </div>
</div>

        <div class="background-placeholder background-placeholder-3"></div>

        <div class="header th-header">
            <div class="header-top th-header-top">
                                    <div class="related-sites hidden-xs">
    <ul class="list-inline">
        <li><a href="http://www.kfzmarkt.ms/" target="_blank" rel="nofollow">kfzmarkt.ms</a></li>
        <li><a href="http://www.karriere.ms/" target="_blank" rel="nofollow">karriere.ms</a></li>
        <li><a href="http://www.azubi.ms/" target="_blank" rel="nofollow">azubi.ms</a></li>
        <li><a href="http://www.flohmarkt.ms/" target="_blank" rel="nofollow">flohmarkt.ms</a></li>
        <li><a href="http://www.trauer.ms/" target="_blank" rel="nofollow">trauer.ms</a></li>
        <li><a href="http://www.gruss.ms/" target="_blank" rel="nofollow">gruss.ms</a></li>
    </ul>
</div>

<div class="header-logos clearfix">
    <a href="http://www.zeitungsgruppe-muenster.de/" target="_blank" class="logo-ternary visible-xs">
        <img src="/bundles/wfniwfnsite/images/zeitungsgruppe-ms-logo.png?99f1c194" alt="zeitungsgruppe-muenster.de"/>
    </a>

    <a href="/" class="logo-primary"></a>
    <a href="http://www.zeitungsgruppe-muenster.de/" target="_blank" class="logo-secondary hidden-xs"></a>

    <div class="soc soc--follow">
    <div class="soc-elements">
        <div class="soc-elements__buttons"></div>    </div>
</div>
</div> <!-- .header-logos -->

<div class="header-sticky-wrap js-header-wrap clearfix">
    <div class="header-sticky-pusher js-header-sticky-pusher"></div>
    <div class="header-nav js-header-nav">
        <div class="header-nav-inner">
        





<nav class="navbar navbar-default main-navbar th-main-navbar" role="navigation">
    <div class="container-fluid">

        <div class="navbar-header">
            <div class="bookmark-list-trigger-wrap" id="notifications" data-url="/notifications"></div>
            <ul class="nav navbar-nav bookmark-list-trigger-wrap">
                <li>
                    <a href="#" data-target=".js-bookmarks-modal" class="bookmark-list-trigger js-bookmark-list-trigger ci-bookmark-list-trigger">
                        Merkliste
                        <span class="badge bookmarks-count js-bookmarks-count th-bookmarks-count">0</span>
                    </a>
                </li>

                
            </ul>

            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-nav-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <button type="button" class="navbar-toggle show-filters-btn js-show-filters-btn">
                <span class="glyphicon glyphicon-search"></span>
            </button>
        </div>

        <div class="collapse navbar-collapse" id="main-nav-collapse">

            <ul class="nav navbar-nav">
            
            
    <li>
        <a href="/"
            
                                    class=""

                                    >
            Startseite        </a>
    </li>
    
    
    <li>
        <a href="/inserieren/intro"
            
                                    class="js-booking-link"

                                        data-event_label="navi-button"
                        >
            Anzeige aufgeben        </a>
    </li>
    
    
    <li>
        <a href="http://admin.immomarkt.ms/"
            target="_blank"
                                    class=""

            rel="nofollow"                        >
            Für Makler        </a>
    </li>
    
    
    <li>
        <a href="/anbieter"
            
                                    class=""

                                    >
            Anbieter        </a>
    </li>
    
    
    <li>
        <a href="/kontakt"
            
                                    class=""

                                    >
            Kontakt        </a>
    </li>
    
            

            
            </ul>

        </div>

    </div><!-- /.container-fluid -->
</nav>
                    </div>
    </div> <!-- .header-nav -->
</div>                             </div>

            
            <div class="header-bottom th-header-bottom">
                    <div id="banner-subnav">
            
                    
                    
                    
                
    </div>

    <div class="breadcrumb-wrap-expose">
        <div class="breadcrumb-wrap">
    <ol class="breadcrumb ci-breadcrumbs cm-breadcrumb th-breadcrumb hidden-xs">
                                        <li>
                                    <a href="/" title="Immobilienmarktplatz">
                    <span class="glyphicon glyphicon-home"></span>                                    <span class="breadcrumb__text ">Immobilienmarktplatz</span>
                                    </a>
                            </li>
                    <li>
                                    <a href="/suche/mieten-wohnung-in-muenster" title="Münster">
                                                        <span class="breadcrumb__text ">Münster</span>
                                    </a>
                            </li>
                    <li>
                                    <a href="/suche/mieten-wohnung-in-muenster-hiltrup-mitte" title="Wohnung mieten in Münster-Hiltrup-Mitte">
                                                        <span class="breadcrumb__text breadcrumb__active">Wohnung mieten in Münster-Hiltrup-Mitte</span>
                                    </a>
                            </li>
            
                <li class="active">Exposé</li>
                </ol>
    <div class="breadcrumb-mobile th-breadcrumb-mobile visible-xs">
                    
        
                    <a href="/suche/mieten-wohnung-in-muenster-hiltrup-mitte" title="Wohnung mieten in Münster-Hiltrup-Mitte" class="btn btn-default btn-block-xs">
                <span class="glyphicon glyphicon-arrow-up"></span>
                Wohnung mieten in Münster-Hiltrup-Mitte
            </a>
            </div>
    <div style="display:none"><script type="application/ld+json">
{
    "@type": "BreadcrumbList",
    "itemListElement": [
        {
            "@type": "ListItem",
            "position": 1,
            "item": {
                "@id": "/",
                "name": "Immobilienmarktplatz"
            }
        },
        {
            "@type": "ListItem",
            "position": 2,
            "item": {
                "@id": "/suche/mieten-wohnung-in-muenster",
                "name": "M\u00fcnster"
            }
        },
        {
            "@type": "ListItem",
            "position": 3,
            "item": {
                "@id": "/suche/mieten-wohnung-in-muenster-hiltrup-mitte",
                "name": "Wohnung mieten in M\u00fcnster-Hiltrup-Mitte"
            }
        }
    ],
    "@context": "http://schema.org"
}
</script></div>
</div>
    </div>
            </div>
        </div>

        <div class="content th-content js-content">
        
        

    
    
        <div class="expose-navigation">

        <div class="back-and-share-widgets hidden-print">
            <div class="row">
                <div class="col-xs-12 col-sm-8">
                                    </div>

                <div class="col-xs-12 col-sm-4">
                
    
                <div class="share--real-estate js-share--real-estate">
            <div class="soc-elements">
            <div class="soc-elements__buttons"><a
                    href="http://www.facebook.com/sharer.php?u=https%3A%2F%2Fimmomarkt.ms%2Fimmobilien%2Fgrosszuegige-maisonettewohnung-im-zentrum-von-hiltrup-8HFRLQ"
                    rel="nofollow"
                    target="_blank"
                    data-provider="facebook"
                    class="soc-elements__buttons__btn soc-elements__buttons__btn--share soc-elements__buttons__btn--facebook"
                ><span class="soc-icon"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 32 32" version="1.1" role="img" style="fill: rgb(255, 255, 255); width: 32px; height: 32px;"><g><path d="M22 5.16c-.406-.054-1.806-.16-3.43-.16-3.4 0-5.733 1.825-5.733 5.17v2.882H9v3.913h3.837V27h4.604V16.965h3.823l.587-3.913h-4.41v-2.5c0-1.123.347-1.903 2.198-1.903H22V5.16z" fill-rule="evenodd"></path></g></svg></span></a><a
                    href="https://twitter.com/share?url=https%3A%2F%2Fimmomarkt.ms%2Fimmobilien%2Fgrosszuegige-maisonettewohnung-im-zentrum-von-hiltrup-8HFRLQ&text=Gro%C3%9Fz%C3%BCgige%20Maisonettewohnung%20im%20Zentrum%20von%20Hiltrup%21%20-%20immomarkt.ms"
                    rel="nofollow"
                    target="_blank"
                    data-provider="twitter"
                    class="soc-elements__buttons__btn soc-elements__buttons__btn--share soc-elements__buttons__btn--twitter"
                ><span class="soc-icon"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 32 32" version="1.1" role="img" style="fill: rgb(255, 255, 255); width: 32px; height: 32px;"><g><path d="M27.996 10.116c-.81.36-1.68.602-2.592.71a4.526 4.526 0 0 0 1.984-2.496 9.037 9.037 0 0 1-2.866 1.095 4.513 4.513 0 0 0-7.69 4.116 12.81 12.81 0 0 1-9.3-4.715 4.49 4.49 0 0 0-.612 2.27 4.51 4.51 0 0 0 2.008 3.755 4.495 4.495 0 0 1-2.044-.564v.057a4.515 4.515 0 0 0 3.62 4.425 4.52 4.52 0 0 1-2.04.077 4.517 4.517 0 0 0 4.217 3.134 9.055 9.055 0 0 1-5.604 1.93A9.18 9.18 0 0 1 6 23.85a12.773 12.773 0 0 0 6.918 2.027c8.3 0 12.84-6.876 12.84-12.84 0-.195-.005-.39-.014-.583a9.172 9.172 0 0 0 2.252-2.336" fill-rule="evenodd"></path></g></svg></span></a><a
                    href="https://plus.google.com/share?url=https%3A%2F%2Fimmomarkt.ms%2Fimmobilien%2Fgrosszuegige-maisonettewohnung-im-zentrum-von-hiltrup-8HFRLQ"
                    rel="nofollow"
                    target="_blank"
                    data-provider="gplus"
                    class="soc-elements__buttons__btn soc-elements__buttons__btn--share soc-elements__buttons__btn--gplus"
                ><span class="soc-icon"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 32 32" version="1.1" role="img" style="fill: rgb(255, 255, 255); width: 32px; height: 32px;"><g><path d="M12 15v2.4h3.97c-.16 1.03-1.2 3.02-3.97 3.02-2.39 0-4.34-1.98-4.34-4.42s1.95-4.42 4.34-4.42c1.36 0 2.27.58 2.79 1.08l1.9-1.83C15.47 9.69 13.89 9 12 9c-3.87 0-7 3.13-7 7s3.13 7 7 7c4.04 0 6.72-2.84 6.72-6.84 0-.46-.05-.81-.11-1.16H12zm15 0h-2v-2h-2v2h-2v2h2v2h2v-2h2v-2z" fill-rule="evenodd"></path></g></svg></span></a><a
                    href="https://api.whatsapp.com/send?text=https%3A%2F%2Fimmomarkt.ms%2Fimmobilien%2Fgrosszuegige-maisonettewohnung-im-zentrum-von-hiltrup-8HFRLQ"
                    rel="nofollow"
                    target="_blank"
                    data-provider="whatsapp"
                    class="soc-elements__buttons__btn soc-elements__buttons__btn--share soc-elements__buttons__btn--whatsapp"
                ><span class="soc-icon"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 32 32" version="1.1" role="img" style="fill: rgb(255, 255, 255); width: 32px; height: 32px;"><g><path d="M19.11 17.205c-.372 0-1.088 1.39-1.518 1.39a.63.63 0 0 1-.315-.1c-.802-.402-1.504-.817-2.163-1.447-.545-.516-1.146-1.29-1.46-1.963a.426.426 0 0 1-.073-.215c0-.33.99-.945.99-1.49 0-.143-.73-2.09-.832-2.335-.143-.372-.214-.487-.6-.487-.187 0-.36-.043-.53-.043-.302 0-.53.115-.746.315-.688.645-1.032 1.318-1.06 2.264v.114c-.015.99.472 1.977 1.017 2.78 1.23 1.82 2.506 3.41 4.554 4.34.616.287 2.035.888 2.722.888.817 0 2.15-.515 2.478-1.318.13-.33.244-.73.244-1.088 0-.058 0-.144-.03-.215-.1-.172-2.434-1.39-2.678-1.39zm-2.908 7.593c-1.747 0-3.48-.53-4.942-1.49L7.793 24.41l1.132-3.337a8.955 8.955 0 0 1-1.72-5.272c0-4.955 4.04-8.995 8.997-8.995S25.2 10.845 25.2 15.8c0 4.958-4.04 8.998-8.998 8.998zm0-19.798c-5.96 0-10.8 4.842-10.8 10.8 0 1.964.53 3.898 1.546 5.574L5 27.176l5.974-1.92a10.807 10.807 0 0 0 16.03-9.455c0-5.958-4.842-10.8-10.802-10.8z" fill-rule="evenodd"></path></g></svg></span></a></div></div>    </div>
                </div>
            </div>
        </div>


        <div class="title-main-specs-wrap">

            <h1 class="expose-property-title">Großzügige Maisonettewohnung im Zentrum von Hiltrup!</h1>

            <div class="expose-property-specs clearfix">

                                    <div class="eps-item eps-item-price">
                        1.615,00 € Miete
                    </div>
                
                                    <div class="eps-item eps-item-rooms">
                        5 Zimmer
                    </div>
                
                                    <div class="eps-item eps-item-area">
                        190 m<sup>2</sup>
                    </div>
                
            </div>
        </div>

        <div class="row">
            <div class="col-md-8">
                <div class="location-primary-actions-wrap">
                    <div class="row">

                        <div class="col-sm-6">
                            <div class="eps-item-location">
                                <span class="glyphicon glyphicon-map-marker"></span>
                                48165 Münster
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="primary-action-btns clearfix">
                                <span>
    <button
        class="btn btn-bookmark js-bookmark-btn"          type="button"
        data-id="8HFRLQ"
        data-title="Großzügige Maisonettewohnung im Zentrum von Hiltrup!"
        data-url="/immobilien/grosszuegige-maisonettewohnung-im-zentrum-von-hiltrup-8HFRLQ"
        data-price="1.615,00 €"
        data-rooms="5"
        data-space="190"
        data-address="48165 Münster"
        data-image="https://cmcdn.de/img-service/gZC35Wg4MG_oSvykbmlwf6JzvNPsZIpOJcJtEaiMoiKMpV3ujqrZsvjLtXgXr_njaqOT4Jbbe9aa_y_v-1KroMnAwlhBw-cnPl4J0ELtjRKs;safescale=130x100,crop=130x100"
        data-hover-activated="true"
        
    >
        <span class="glyphicon glyphicon-star-empty"></span>
        <span class="btn-bookmark__text js-btn-bookmark-text">
            Merken
        </span>
    </button>
</span>

                                    <button class="btn btn-contact-provider js-contact-provider-btn-top btn-block-xs "
                type="button"
                data-toggle="modal"
                data-target=".contact-modal">

                                    <span class="glyphicon glyphicon-envelope"></span>
                                Anbieter kontaktieren
        </button>
                                </div>
                        </div>

                    </div>                 </div>
            </div>

            <div class="col-md-4">
                <div class="action-buttons">
            <div class="btn-group btn-group-justified">
        <div class="btn-group">
            <a class="btn btn-default js-print-btn" href="/drucken/8HFRLQ" target="_blank" rel="nofollow">
                <span class="glyphicon glyphicon-print"></span>
                <span class="btn-text">Drucken</span>
            </a>
        </div>

                <div class="btn-group">
            <a href="mailto:?subject=Empfehlung%20einer%20Immobilie%20auf%20immomarkt.ms&amp;body=Hallo%2C%0A%0Aich%20habe%20eine%20interessante%20Immobilie%20auf%20immomarkt.ms%20gefunden%3A%0A%0AGro%C3%9Fz%C3%BCgige%20Maisonettewohnung%20im%20Zentrum%20von%20Hiltrup%21%0Ahttps%3A%2F%2Fimmomarkt.ms%2Fimmobilien%2Fgrosszuegige-maisonettewohnung-im-zentrum-von-hiltrup-8HFRLQ%0A%0AViele%20Gr%C3%BC%C3%9Fe%0A" class="btn btn-default js-recommend-btn">
    <span class="glyphicon glyphicon-envelope"></span>
    <span class="btn-text">Empfehlen</span>
</a>
        </div>
            </div>

</div>             </div>
        </div>
    </div>

    <div class="expose-content">

        <div class="row">

            <div class="col-md-8 col-expose-details">
                <div class='ci-attachments'>
                                                                    <div class="expose-photo-gallery photo-gallery">
    <div
        class="fotorama"
        data-width="100%"
        data-ratio="800/600"
        data-nav="thumbs"
        data-fit="scaledown"
        data-allowfullscreen="true" data-loop="true"
        data-keyboard="true"
    >
                                    <a
                    href="https://cmcdn.de/img-service/gZC35Wg4MG_oSvykbmlwf6JzvNPsZIpOJcJtEaiMoiKMpV3ujqrZsvjLtXgXr_njaqOT4Jbbe9aa_y_v-1KroMnAwlhBw-cnPl4J0ELtjRKs;scale=800x600"
                    rel="nofollow"
                    data-full="https://cmcdn.de/img-service/gZC35Wg4MG_oSvykbmlwf6JzvNPsZIpOJcJtEaiMoiKMpV3ujqrZsvjLtXgXr_njaqOT4Jbbe9aa_y_v-1KroMnAwlhBw-cnPl4J0ELtjRKs;"                        data-caption="Außenansicht"></a>
                                                <a
                    href="https://cmcdn.de/img-service/gZC35Wg4MG_oSvykbmlwf6JzvNPsZIpOJcJtEaiMoiKMpV3ujqrZsvjLtXgXr_njaqOT4Jbbe9aa_y_v-1KroMnAwlhBw-cnPl4J0ELtjRKs;scale=800x600"
                    rel="nofollow"
                    data-full="https://cmcdn.de/img-service/gZC35Wg4MG_oSvykbmlwf6JzvNPsZIpOJcJtEaiMoiKMpV3ujqrZsvjLtXgXr_njaqOT4Jbbe9aa_y_v-1KroMnAwlhBw-cnPl4J0ELtjRKs;"                        data-caption="Außenansicht"></a>
                                                <a
                    href="https://cmcdn.de/img-service/gZC35Wg4MG_oSvykbmlwf6Do3DkSKcO9tPUUSFBY9xrNo35Aj4A3syA7cnKhOgTzk5Rxt1tiyvL5zSLVECI7q3zIjV2NhsNrQ-mB57pGXCyU;scale=800x600"
                    rel="nofollow"
                    data-full="https://cmcdn.de/img-service/gZC35Wg4MG_oSvykbmlwf6Do3DkSKcO9tPUUSFBY9xrNo35Aj4A3syA7cnKhOgTzk5Rxt1tiyvL5zSLVECI7q3zIjV2NhsNrQ-mB57pGXCyU;"                        data-caption="Wohnbereich"></a>
                                                <a
                    href="https://cmcdn.de/img-service/gZC35Wg4MG_oSvykbmlwf6BMzQrT8S3oMu5zcElj-6J9p1X5pjOXYyQgbl6KCc-8uPqKLgaWnJE7dj6x4x2K5XRavof0c-7ToJ6lJViFpfGg;scale=800x600"
                    rel="nofollow"
                    data-full="https://cmcdn.de/img-service/gZC35Wg4MG_oSvykbmlwf6BMzQrT8S3oMu5zcElj-6J9p1X5pjOXYyQgbl6KCc-8uPqKLgaWnJE7dj6x4x2K5XRavof0c-7ToJ6lJViFpfGg;"                        data-caption="Bad en suit"></a>
                                                <a
                    href="https://cmcdn.de/img-service/gZC35Wg4MG_oSvykbmlwf6E3SmiQtZSLuYKtHNgsMJgsyi58vZDAvHS_PiQqP6Jmmqec-NvXOHebGL9516yioZ6CKG083Ct63AmYB4d7psDw;scale=800x600"
                    rel="nofollow"
                    data-full="https://cmcdn.de/img-service/gZC35Wg4MG_oSvykbmlwf6E3SmiQtZSLuYKtHNgsMJgsyi58vZDAvHS_PiQqP6Jmmqec-NvXOHebGL9516yioZ6CKG083Ct63AmYB4d7psDw;"                        data-caption="Essbereich"></a>
                                                <a
                    href="https://cmcdn.de/img-service/gZC35Wg4MG_oSvykbmlwf6N9ixD7_m5oWQnG4CVUI8rhJR2E7MvTso_zYZGf8JUh1rHkiM3AXKiIkZHD4EVxGUWz8FJNlQZcmQ877t4Pewt0;scale=800x600"
                    rel="nofollow"
                    data-full="https://cmcdn.de/img-service/gZC35Wg4MG_oSvykbmlwf6N9ixD7_m5oWQnG4CVUI8rhJR2E7MvTso_zYZGf8JUh1rHkiM3AXKiIkZHD4EVxGUWz8FJNlQZcmQ877t4Pewt0;"                        data-caption="Diele"></a>
                                                <a
                    href="https://cmcdn.de/img-service/gZC35Wg4MG_oSvykbmlwf6HXrXhLMfRyvC8RtdlScPWYUDDsIVjMEAGAhos9EZPqWgSQlxwmlKAcNDRFmSNAMJ6GeslAanvJLh6L9CMKs8DQ;scale=800x600"
                    rel="nofollow"
                    data-full="https://cmcdn.de/img-service/gZC35Wg4MG_oSvykbmlwf6HXrXhLMfRyvC8RtdlScPWYUDDsIVjMEAGAhos9EZPqWgSQlxwmlKAcNDRFmSNAMJ6GeslAanvJLh6L9CMKs8DQ;"                        data-caption="Arbeitszimmer"></a>
                                                <a
                    href="https://cmcdn.de/img-service/gZC35Wg4MG_oSvykbmlwf6PhrzpaRzOZz8TAkn-nnZH9G9kUSTR-V598pYZ38ZVacoA0ziqVu7H78o8or52uPUogkWsXJbTLIOPMzZkL3OuE;scale=800x600"
                    rel="nofollow"
                    data-full="https://cmcdn.de/img-service/gZC35Wg4MG_oSvykbmlwf6PhrzpaRzOZz8TAkn-nnZH9G9kUSTR-V598pYZ38ZVacoA0ziqVu7H78o8or52uPUogkWsXJbTLIOPMzZkL3OuE;"                        data-caption="Schlafzimmer"></a>
                                                <a
                    href="https://cmcdn.de/img-service/gZC35Wg4MG_oSvykbmlwf6K3K6m4JhMMGnAhOVsBpK9xAHSEP6vii2eK6L2mwt_-meQiy6zslyymg2q21gG5KLuMjirf82At2RZRzzboNuzU;scale=800x600"
                    rel="nofollow"
                    data-full="https://cmcdn.de/img-service/gZC35Wg4MG_oSvykbmlwf6K3K6m4JhMMGnAhOVsBpK9xAHSEP6vii2eK6L2mwt_-meQiy6zslyymg2q21gG5KLuMjirf82At2RZRzzboNuzU;"                        data-caption="Loggia"></a>
                                                <a
                    href="https://cmcdn.de/img-service/gZC35Wg4MG_oSvykbmlwf6C9S6if_KNLFwHnnTA1Fd6zUKRIg_CEEXWUHTAqNCjOxAFOkKMx9Et8YtnrYc6HSbiL2ZMm0XvO5BFA1hyXSeKc;scale=800x600"
                    rel="nofollow"
                    data-full="https://cmcdn.de/img-service/gZC35Wg4MG_oSvykbmlwf6C9S6if_KNLFwHnnTA1Fd6zUKRIg_CEEXWUHTAqNCjOxAFOkKMx9Et8YtnrYc6HSbiL2ZMm0XvO5BFA1hyXSeKc;"                        data-caption="Balkon OG"></a>
                    
        
                <div class="fotorama__html vertical-center-dialog" data-thumb="https://pubads.g.doubleclick.net/gampad/ad?iu=/2823228/Galerie_Banner_Thumbnail&sz=64x64&t=sc%3Dwfn_iwfn%26mtype%3Drental%26ptype%3Dapartment&c=579936182">
            <a class="fotorama__select" href="https://pubads.g.doubleclick.net/gampad/jump?iu=/2823228/Galerie_Banner&sz=748x561&t=sc%3Dwfn_iwfn%26mtype%3Drental%26ptype%3Dapartment&c=579936182">
                <img class="js-galad" src="https://pubads.g.doubleclick.net/gampad/ad?iu=/2823228/Galerie_Banner&sz=748x561&t=sc%3Dwfn_iwfn%26mtype%3Drental%26ptype%3Dapartment&c=579936182" alt="">
            </a>
        </div>

    </div> </div>
                    
                                    </div>

                

<div class="expose-details">

    
    <div class="expose-detail expose-details-data">

        <h2 class="expose-section-title collapse-handle" data-toggle="collapse" data-target="#edc-data">
            Objektdaten
        </h2>

        <div id="edc-data" class="expose-details-content collapse in">

                            <div>
                    <div class="row">
                        <div class="col-xs-4 col-sm-3">Adresse</div>
                        <div class="col-xs-8 col-sm-9"><strong>48165 Münster</strong></div>
                    </div>
                </div>
                            
            
            
                
                            <div>
                    <div class="row">
                        <div class="col-xs-4 col-sm-3">Immobilien&shy;typ</div>
                        <div class="col-xs-8 col-sm-9">
                                                                                            Wohnung,                                                                                             Wohnen,                                                                                             Maisonette                                                    </div>
                    </div>
                </div>
                                        <div>
                    <div class="row">
                        <div class="col-xs-4 col-sm-3">Zimmer</div>
                        <div class="col-xs-8 col-sm-9"><strong>5</strong></div>
                    </div>
                </div>
                                                                            <div>
                    <div class="row">
                        <div class="col-xs-4 col-sm-3">Wohnfläche</div>
                        <div class="col-xs-8 col-sm-9">190 m<sup>2</sup></div>
                    </div>
                </div>
                                                                                                                                                                                                                                        <div>
                    <div class="row">
                        <div class="col-xs-4 col-sm-3">Zustand</div>
                        <div class="col-xs-8 col-sm-9">gepflegt</div>
                    </div>
                </div>
                                                                            <div>
                    <div class="row">
                        <div class="col-xs-4 col-sm-3">Baujahr</div>
                        <div class="col-xs-8 col-sm-9">1976</div>
                    </div>
                </div>
                                                    <div>
                    <div class="row">
                        <div class="col-xs-4 col-sm-3">verfügbar ab</div>
                        <div class="col-xs-8 col-sm-9">ab August 2018</div>
                    </div>
                </div>
                            
            
            
                
            
                            <div class="text-muted">
                    <div class="row">
                        <div class="col-xs-4 col-sm-3">Online-ID</div>
                        <div class="col-xs-8 col-sm-9">4146569</div>
                    </div>
                </div>
            
                            <div class="text-muted">
                    <div class="row">
                        <div class="col-xs-4 col-sm-3">Anbieter-ID</div>
                        <div class="col-xs-8 col-sm-9">8699</div>
                    </div>
                </div>
            
            
                            <div class="text-muted">
                    <div class="row">
                        <div class="col-xs-4 col-sm-3">Stand vom</div>
                        <div class="col-xs-8 col-sm-9">24.05.2018</div>
                    </div>
                </div>
                    </div>

    </div>
    <div class="expose-detail expose-costs-data">

        <h2 class="expose-section-title collapse-handle collapsed" data-toggle="collapse" data-target="#ecc-data">
            Preise
        </h2>

        <div id="ecc-data" class="expose-details-content collapse">
                    
                    
                    

            
                
            
            
                

                    
                    
                    
                    

                    
                    
                    
                    <div>
                        <div class="row">
                                                            <div class="col-xs-3 col-sm-6 ">
                                    Kaution und Genos&shy;sen&shy;schafts&shy;anteile
                                                                    </div>
                                                                                                                                    <div class="col-xs-3 col-sm-2 expense">
                                        <small class="text-muted">Brutto</small><br>
                                                                                    1.615,00 €
                                                                            </div>
                                                                                    </div>

                                            </div>
                            

            
            
            <div class="vspace">
                            </div>

            
                            
                            
                            
                    <div>
                        <div class="row">
                                                            <div class="col-xs-3 col-sm-6 strong">
                                    Warm&shy;miete
                                                                    </div>
                                                                                                                                    <div class="col-xs-3 col-sm-2 expense">
                                        <small class="text-muted">Brutto</small><br>
                                                                                    1.995,00 €
                                                                            </div>
                                                                                    </div>

                                            </div>
                            
                            
                                            
                            
                                            
                            
                                            

                            
                                            

                            
                                
                                        
                    <div>
                        <div class="row">
                                                            <div class="col-xs-3 col-sm-6 strong">
                                    Netto&shy;kalt&shy;miete
                                                                    </div>
                                                                                                                                    <div class="col-xs-3 col-sm-2 expense">
                                        <small class="text-muted">Brutto</small><br>
                                                                                    1.615,00 €
                                                                            </div>
                                                                                    </div>

                                            </div>
                            

                            
                                            
                            
                                            

                            
                                
                                        
                    <div>
                        <div class="row">
                                                            <div class="col-xs-3 col-sm-6 strong">
                                    Betriebs- / Neben&shy;kos&shy;ten
                                                                    </div>
                                                                                                                                    <div class="col-xs-3 col-sm-2 expense">
                                        <small class="text-muted">Brutto</small><br>
                                                                                    380,00 €
                                                                            </div>
                                                                                    </div>

                                            </div>
                            
                
            
            
                


                            
                                            

                            
                                            

                            
                                            
                            
                                            
                            
                                            
                            
                                            
                            
                                            

                            
            
            
                
        </div>

    </div>

    

        <div class="expose-detail expose-details-equipment">
        <h2 class="expose-section-title collapse-handle collapsed" data-toggle="collapse" data-target="#edc-equipment">
            Ausstattung</h2>

        <div id="edc-equipment" class="expose-details-content collapse">
            <div>
            <div class="row">
                <div class="col-xs-4 col-sm-3">Badezimmer</div>
                <div class="col-xs-8 col-sm-9">3.00</div>
            </div>
        </div>
    
    
    
    
            <div>
            <div class="row">
                <div class="col-xs-4 col-sm-3">unterkellert</div>
                <div class="col-xs-8 col-sm-9">ja</div>
            </div>
        </div>
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
                    
                                                                                                                                                                                                    <div>
                        <div class="row">
                            <div class="col-xs-4 col-sm-3">Einbauküche</div>
                            <div class="col-xs-8 col-sm-9"><span class="glyphicon glyphicon-ok text-success"></span></div>
                        </div>
                    </div>
                
                                                                                                                                                                        <div>
                        <div class="row">
                            <div class="col-xs-4 col-sm-3">Parkmöglichkeit</div>
                            <div class="col-xs-8 col-sm-9"><span class="glyphicon glyphicon-ok text-success"></span></div>
                        </div>
                    </div>
                
                                                                                        <div>
                        <div class="row">
                            <div class="col-xs-4 col-sm-3">Unterkellert</div>
                            <div class="col-xs-8 col-sm-9"><span class="glyphicon glyphicon-ok text-success"></span></div>
                        </div>
                    </div>
                
                                                                                    
                    
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                <div>
                        <div class="row">
                            <div class="col-xs-4 col-sm-3">Kfz-Stellplatz</div>
                            <div class="col-xs-8 col-sm-9"><span class="glyphicon glyphicon-ok text-success"></span></div>
                        </div>
                    </div>
        </div>
    </div>
    
            <div class="expose-detail expose-details-energy-pass">

            <h2 class="expose-section-title collapse-handle collapsed" data-toggle="collapse" data-target="#edc-energy-pass">
                Energie&shy;aus&shy;weis
            </h2>

            <div id="edc-energy-pass" class="expose-details-content collapse">
                            <div>
            <div class="row">
                <div class="col-xs-4 col-sm-3">Energie&shy;aus&shy;weis:</div>
                <div class="col-xs-8 col-sm-9">
                    vorhanden

                                            (bedarfsorientiert)
                                    </div>
            </div>
        </div>
                <div>
            <div class="row">
                <div class="col-xs-4 col-sm-3">Endenergiebedarf</div>
                <div class="col-xs-8 col-sm-9">
                    186.60 kWh/m<sup>2</sup>a
                                    </div>
            </div>
        </div>
    
    
        
            
            
            

    
            <div>
            <div class="row">
                <div class="col-xs-4 col-sm-3">Aus&shy;stell&shy;datum</div>
                <div class="col-xs-8 col-sm-9">23.04.2018</div>
            </div>
        </div>
                                                <div>
            <div class="row">
                <div class="col-xs-4 col-sm-3">Energieeffizienzklasse</div>
                <div class="col-xs-8 col-sm-9">F</div>
            </div>
        </div>
                            <div>
            <div class="row">
                <div class="col-xs-4 col-sm-3">Heizungsart</div>
                <div class="col-xs-8 col-sm-9">
                                                                    Zentralheizung                                    </div>
            </div>
        </div>
    
            <div>
            <div class="row">
                <div class="col-xs-4 col-sm-3">Befeu&shy;e&shy;rungs&shy;art</div>
                <div class="col-xs-8 col-sm-9">
                                                                    Öl                                    </div>
            </div>
        </div>
        
            </div>
        </div>

    
    
            <div class="expose-detail expose-details-parking">

            <h2 class="expose-section-title collapse-handle collapsed" data-toggle="collapse" data-target="#edc-parking">
                Parken
            </h2>

            <div id="edc-parking" class="expose-details-content collapse">
                                    
                            
                                    <div>
                <div class="row">
                    <div class="col-xs-4 col-sm-3">Kfz-Stellplatz</div>
                    <div class="col-xs-8 col-sm-9">
                        
                        
                        
                        
                        2
                    </div>
                </div>
            </div>
            
                            
                            
                            
                                    <div>
                <div class="row">
                    <div class="col-xs-4 col-sm-3">Parkmöglichkeiten insgesamt</div>
                    <div class="col-xs-8 col-sm-9">
                        
                        
                        
                        
                        2
                    </div>
                </div>
            </div>
            
    
            </div>
        </div>

    <div class="expose-content-extra clearfix hidden-xs hidden-print">
            <button class="btn btn-contact-provider js-contact-provider-btn-middle btn-block-xs "
                type="button"
                data-toggle="modal"
                data-target=".contact-modal">

                                    <span class="glyphicon glyphicon-envelope"></span>
                                Anbieter kontaktieren
        </button>
    
        <span>
    <button
        class="btn btn-bookmark js-bookmark-btn"          type="button"
        data-id="8HFRLQ"
        data-title="Großzügige Maisonettewohnung im Zentrum von Hiltrup!"
        data-url="/immobilien/grosszuegige-maisonettewohnung-im-zentrum-von-hiltrup-8HFRLQ"
        data-price="1.615,00 €"
        data-rooms="5"
        data-space="190"
        data-address="48165 Münster"
        data-image="https://cmcdn.de/img-service/gZC35Wg4MG_oSvykbmlwf6JzvNPsZIpOJcJtEaiMoiKMpV3ujqrZsvjLtXgXr_njaqOT4Jbbe9aa_y_v-1KroMnAwlhBw-cnPl4J0ELtjRKs;safescale=130x100,crop=130x100"
        data-hover-activated="true"
        
    >
        <span class="glyphicon glyphicon-star-empty"></span>
        <span class="btn-bookmark__text js-btn-bookmark-text">
            Merken
        </span>
    </button>
</span>
    </div>

        <div class="expose-detail expose-details-description">

        <h2 class="expose-section-title collapse-handle collapsed" data-toggle="collapse" data-target="#edc-description">
            Objektbeschreibung
        </h2>

        <div id="edc-description" class="expose-details-content collapse">

            <div class="edc-description-inner">
                <p>Diese schön gelegene Maisonettewohnung befindet sich in einem sehr gepflegten Zweifamilienhaus im schönen Hiltrup und hat eine großzügige Wohnfläche von ca. 190 m². Sie wohnen hier im Obergeschoss und Dachgeschoss.

Vor dem Haus stehen Ihnen auch 2 Stellplätze zur Verfügung.
Des Weiteren gehört noch ein eigener Kellerraum, für zusätzlichen Stauraum dazu.
Die Wohnung ist ab August bezugsfrei.

Der monatliche Mietpreis beträgt 1.615,00€ + 380,00€ NK.
Eine Mietkaution in Höhe von 1 Monatskaltmieten ist erforderlich.</p>

                                    <hr/>
                    <h3 class="h4">Ausstattung</h3>
                    <p>Großzügige Räume und viel Platz werden hier großgeschrieben!

Im Obergeschoss, der ersten Ebene, befindet sich die Diele, eine Garderobe, ein Gäste-WC, eine große helle Küche mit angrenzenden Vorratsraum, ein ca. 48 m² großes Wohn- und Esszimmer mit Kamin und ein Schlafzimmer mit einem Bad en Suit. Das Tageslichtbad verfügt über eine Badewanne, eine Dusche und ein BD. Die Einbauküche ohne Elektrogeräte kann bei Bedarf kostenfrei übernommen werden.
Helle und freundliche Räume sind hier keine Seltenheit!
Ein Highlight ist der große Balkon, der über das Schlafzimmer und den Essbereich zugänglich ist. Der Balkon verläuft fast einmal um das gesamte Haus, sodass Sie zu jeder Tageszeit die Sonne genießen können oder an heißen Sommertagen im Schatten sitzen können.

Das Dachgeschoss bietet Ihnen weitere 3 gut geschnittene Zimmer, eine Diele und zwei kleinere Bäder. Eins mit Dusche und eins mit Wanne. Außerdem können Sie auf der Süd-West-Loggia entspannen!

Beide Ebenen sind mit hochwertigen Bodenbelägen und Echtholztüren ausgestattet.

Wichtig zu wissen:
-Kamin im Wohn- und Essbereich
-Waschmaschienenanschluss in der Küche
-hochwertige Bodenbeläge OG (Fliesen + Parkett)
-Echtholztüren
-Einbauschränke im DG können mit übernommen werden
-Heizkosten in NK enthalten</p>
                
                                    <hr/>
                    <h3 class="h4">Lage</h3>
                    <p>Hier wohnen Sie in zentraler Lage von Hiltrup. Haltestellen für den öffentlichen Nahverkehr sind zudem fußläufig zu erreichen. Alle Einkäufe können im direkten Umfeld bequem erledigt werden. Die münsteraner Innenstadt ist in nur ca. 15 min. mit dem Auto zu errichen.
Landschaftlich hat Hiltrup auch viele schöne Ecken. Besonder der Hiltruper See und seine Umgebung lädt ausgiebigen Spaziergängen und zum Verweilen ein.
Im Sport- und Freizeitbereich existiert neben einem Hallenbad ein bei der Bevölkerung beliebtes Freibad zwischen Kanal und Hiltruper See. In Hiltrup befinden sich zwei Gymnasien, jeweils eine Real-, Haupt- und Sonderschule und vier Grundschulen.</p>
                
                                    <hr/>
                    <h3 class="h4">Sonstiges</h3>
                    <p>Heizungsart: Öl-Heizung
Befeuerung/Energieträger: Öl
Energieausweistyp: Bedarfsausweis
Energiekennwert: 186,6 kWh/(m²*a)
Energieeffizienzklasse: F
Baujahr: 1976

Folgende Unterlagen sollten Sie bereit halten:

- letzten 3 Gehaltsnachweise
- Selbstauskunft
- Schufaauskunft</p>
                
                            </div>
        </div>
    </div>
    <div class="expose-content-extra clearfix visible-xs hidden-print">
            <button class="btn btn-contact-provider js-contact-provider-btn-middle btn-block-xs "
                type="button"
                data-toggle="modal"
                data-target=".contact-modal">

                                    <span class="glyphicon glyphicon-envelope"></span>
                                Anbieter kontaktieren
        </button>
    
        <span>
    <button
        class="btn btn-bookmark js-bookmark-btn"          type="button"
        data-id="8HFRLQ"
        data-title="Großzügige Maisonettewohnung im Zentrum von Hiltrup!"
        data-url="/immobilien/grosszuegige-maisonettewohnung-im-zentrum-von-hiltrup-8HFRLQ"
        data-price="1.615,00 €"
        data-rooms="5"
        data-space="190"
        data-address="48165 Münster"
        data-image="https://cmcdn.de/img-service/gZC35Wg4MG_oSvykbmlwf6JzvNPsZIpOJcJtEaiMoiKMpV3ujqrZsvjLtXgXr_njaqOT4Jbbe9aa_y_v-1KroMnAwlhBw-cnPl4J0ELtjRKs;safescale=130x100,crop=130x100"
        data-hover-activated="true"
        
    >
        <span class="glyphicon glyphicon-star-empty"></span>
        <span class="btn-bookmark__text js-btn-bookmark-text">
            Merken
        </span>
    </button>
</span>
    </div>

    
    
    
    

</div>             </div>

            <div class="col-md-4 col-expose-sidebar">
                                    <div class="widget widget-contact hidden-xs hidden-sm th-widget">
    <p class="contact-title"><span class="glyphicon glyphicon-envelope"></span>
        Anfrage senden
    </p>

    <div class="agency clearfix">
        <div class="row">
            
                
                <div class="col-xs-4">
                    <img src="https://cmcdn.de/img-service/gZC35Wg4MG_oSvykbmlwf6K5xs6OXqyG41u9KAlOpvqTGbO611T4Mvj4nwfjWI2rxOIiBq7gyWdem5jUpGGgqr-h1thOGMCTAbeNJyoDYkDs;scale=100x60" alt="logo" class="img-responsive utils-img-border agency-logo"/>
                </div>
            
            <div class="col-xs-8">
                                    <h4 class="agency-name">Vereinigte Volksbank Münster eG</h4>
                
                                    <p class="agency-person">
                        Lisa Wegener
                    </p>
                            </div>
        </div>
    </div>

    <form name="contact_form" method="post" action="#" data-action="/contact/expose/8HFRLQ" class="form-horizontal contact-provider-form js-contact-provider-form" role="form" novalidate="novalidate" data-preview-action="">        <noscript>
            <div class="disabled-form">
                <div class="disabled-form__info-wrapper">
                    <div class="disabled-form__info-wrapper__hint text-danger">
                        <span class="lead">Javascript erforderlich!</span>
                        <p>Bitte aktivieren Sie Javascript in Ihrem Browser und laden Sie die Seite erneut.</p>
                    </div>
                </div>
            </div>
        </noscript>
    

    <div class="salutation js-salutation form-group">
                    <div class="col-xs-6">
                <select id="contact_form_salutation" name="contact_form[salutation]" required="required" class="form-control form-control"><option value="" selected="selected">Anrede</option><option value="mr">Herr</option><option value="ms">Frau</option></select>
                <div class="error-msg">
                    Pflichtfeld
                </div>
            </div>
                <div class="col-xs-6">
            <div style="line-height: 30px;" class="text-right">* Pflichtfeld</div>
        </div>
    </div>

            <div class="name js-name form-group">
            <div class="col-xs-12">
                <div class="">                     <input type="text" id="contact_form_name" name="contact_form[name]" required="required" class="form-control form-control" placeholder="Name *" />
                    <div class="error-msg">
                        Bitte geben Sie Ihren Namen an.
                    </div>
                </div>
            </div>
        </div>
    
            <div class="email js-email form-group">
            <div class="col-xs-12">
                <div class="">                     <input type="email" id="contact_form_email" name="contact_form[email]" required="required" class="form-control form-control" placeholder="E-Mail *" />
                    <div class="error-msg">
                        Bitte geben Sie eine gültige E-Mail-Adresse an.
                    </div>
                </div>
            </div>
        </div>
    
            <div class="phone js-phone form-group">
            <div class="col-xs-12">
                <div class="">                     <input type="text" id="contact_form_phone" name="contact_form[phone]" class="form-control form-control" placeholder="Telefon" />
                    <div class="error-msg">
                        Bitte geben Sie Ihre Telefonnummer an.
                    </div>
                </div>
            </div>
        </div>
    
    <div class="appointment form-group">
        <div class="col-xs-12">
            <div class="checkbox">
                    <div class="checkbox"><label for="contact_form_appointment_chk" class="required"><input type="checkbox" id="contact_form_appointment_chk" name="contact_form[appointment_chk]" required="required" value="1" /> Besichtigungstermin vereinbaren</label></div>
            </div>
        </div>
    </div>

    <div class="property-info form-group">
        <div class="col-xs-12">
            <div class="checkbox">
                    <div class="checkbox"><label for="contact_form_property_info_chk" class="required"><input type="checkbox" id="contact_form_property_info_chk" name="contact_form[property_info_chk]" required="required" value="1" /> Objektinformationen anfordern</label></div>
            </div>
        </div>
    </div>

            
    
        
                    <div class="my-address ">

                <div class="my-address-inputs">
                                            <div class="form-group street js-street">
                            <div class="col-xs-12">
                                <input type="text" id="contact_form_street" name="contact_form[street]" required="required" class="form-control form-control" placeholder="Straße *" />
                                <div class="error-msg">
                                    Pflichtfeld
                                </div>
                            </div>
                        </div>
                    
                                            <div class="form-group">
                                                            <div class="col-xs-4 utils-padding-r0">
                                    <div class="zip-code js-zip-code">
                                        <input type="text" id="contact_form_zipCode" name="contact_form[zipCode]" class="form-control form-control" placeholder="PLZ" />
                                        <div class="error-msg">
                                            Pflichtfeld
                                        </div>
                                    </div>
                                </div>
                            
                                                            <div class="col-xs-8">
                                    <div class="place js-place">
                                        <input type="text" id="contact_form_place" name="contact_form[place]" required="required" class="form-control form-control" placeholder="Ort *" />
                                        <div class="error-msg">
                                            Pflichtfeld
                                        </div>
                                    </div>
                                </div>
                                                    </div>
                                    </div>
            </div>
    <div class="message form-group">
        <div class="col-xs-12">
            <textarea id="contact_form_message" name="contact_form[message]" required="required" class="form-control form-control" rows="3">Ich interessiere mich für Ihr Angebot und möchte gerne weitere Informationen erhalten.</textarea>
            <div class="error-msg">
                Pflichtfeld
            </div>
        </div>
    </div>

    <div class="js-alerts-wrap"></div>

            
            <div class="alert alert-info">
            Ihre personenbezogenen Daten verwenden wir, soweit keine darüberhinausgehende Einwilligung vorliegt, nur zur Abwicklung des der Erhebung zugrundeliegenden Zwecks. Nähere Informationen zu unserem Umgang mit personenbezogenen Daten erhalten Sie unter <a href="/datenschutz" target="_blank" rel="nofollow">Datenschutzerklärung</a>. Hier kommen wir auch unseren Informationspflichten nach der EU-Datenschutzgrundverordnung nach.

        </div>
    
    <div class="form-footer clearfix">
            
            <button type="submit" id="contact_form_submit" data-intent="submit"                     class="btn btn-block btn-lg btn-primary-action submit-contact-widget-btn js-submit-contact-widget-btn">
                Anfrage senden
            </button>

                        </div>

    <input type="hidden" id="contact_form__token" name="contact_form[_token]" value="SvGvWNYzv6so0QIjjREkuRNeLQkMgi_GKk_zUjeM-ec" />

    <script type="text/template" class="js-alert-template">
    <div class="alert {className} alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert">
            <span aria-hidden="true">&times;</span>
            <span class="sr-only">Close</span>
        </button>
        <span class="js-status-text alert-status-text">{text}</span>
    </div>
</script>

<script type="text/translation" class="js-unknown-error">
    Ein unbekannter Fehler ist beim Versand Ihrer E-Mail aufgetreten. Bitte versuchen Sie es zu einem spateren Zeitpunkt noch einmal.<br><br>Vielen Dank.
</script>

<script type="text/template" class="js-progress-bar">
    <div class="progress utils-margin-b0">
        <div class="progress-bar progress-bar-striped active"  role="progressbar"
             aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
    </div>
</script>
</form>
</div>
                
                
                <div id="banner-expose-1">
                
            
            
                
                </div>

                
                <div class="widget widget-map th-widget th-widget-map">
    <div class="js-map-single" data-map-config="{&quot;pin_url_template&quot;:&quot;\\/images\\/leaflet\\/marker-icon{size}.png&quot;,&quot;subdomains&quot;:[&quot;a&quot;,&quot;b&quot;,&quot;c&quot;,&quot;d&quot;],&quot;attribution&quot;:&quot;&lt;a href=\\&quot;https:\\/\\/www.mapbox.com\\/about\\/maps\\/\\&quot; target=\\&quot;_blank\\&quot; rel=\\&quot;nofollow\\&quot;&gt;\u00a9 Mapbox&lt;\\/a&gt; &lt;a href=\\&quot;http:\\/\\/www.openstreetmap.org\\/\\&quot; target=\\&quot;_blank\\&quot; rel=\\&quot;nofollow\\&quot;&gt;\u00a9 OpenStreetMap&lt;\\/a&gt; &lt;a href=\\&quot;https:\\/\\/www.mapbox.com\\/map-feedback\\/\\&quot; target=\\&quot;_blank\\&quot; rel=\\&quot;nofollow\\&quot;&gt;&lt;strong&gt;Improve this map&lt;\\/strong&gt;&lt;\\/a&gt;&quot;,&quot;provider&quot;:&quot;osm&quot;,&quot;url&quot;:&quot;https:\\/\\/cmcdn.de\\/osm\\/v2\\/streets-v9\\/{z}\\/{x}\\/{y}.png&quot;}"
                    data-center="51.90575,7.63645"    data-show-pin=""
    >

    <div class="map-placeholder">
        <span class="glyphicon glyphicon-map-marker"></span>
        <span class="js-map-paceholder-label">Karte nicht verfügbar</span>
    </div>

</div>
    <div class="map-poi-toggles js-map-poi-toggles" data-poi-pins-hidden="transport">
                                <label><input type="checkbox" checked="checked" data-category="shop" /> <i class="fontello-shop"></i> Einkaufen</label>
                                <label><input type="checkbox" checked="checked" data-category="transport" /> <i class="fontello-transport"></i> Verkehr</label>
            </div>
</div>



    <div class="widget widget-poi js-widget-poi th-widget">
    
<h3 class="widget-title th-widget-title"><span class="glyphicon glyphicon-map-marker"></span>
    In Ihrer Umgebung
</h3>

<div class="panel-group panel-group-poi ci-poi">
                <div class="panel panel-default panel-collapsible" data-category="shop">
        <div class="panel-heading">
            <p class="panel-title">
                                    <a data-toggle="collapse" href="#shop-collapsed" class="collapsed collapse-handle collapse-handle--right">
                                    <i class="fontello-shop"></i>
                    Einkaufen
                </a>
            </p>
        </div>

        <div class="panel-body">
            <ul class="list-unstyled">
                                    <li class="poi-item clearfix" data-lat="51.9055422" data-long="7.6357931" data-name="Baekerei Klostermann">
                    <div class="poi-name">Baekerei Klostermann</div>
                                </li>
                                                <li class="poi-item clearfix" data-lat="51.905716952005" data-long="7.6342976481124" data-name="Bröker">
                    <div class="poi-name">Bröker</div>
                                </li>
                                                <li id="shop-collapsed" class="collapse">
                    <ul>
                            <li class="poi-item clearfix" data-lat="51.905775832122" data-long="7.6333912080689" data-name="Lidl">
                    <div class="poi-name">Lidl</div>
                                </li>
                                                <li class="poi-item clearfix" data-lat="51.904210090518" data-long="7.6384577241514" data-name="Friseursalon Otto">
                    <div class="poi-name">Friseursalon Otto</div>
                                </li>
                                                <li class="poi-item clearfix" data-lat="51.9063825" data-long="7.6399883" data-name="Krimphove">
                    <div class="poi-name">Krimphove</div>
                                </li>
                                </ul>
                </li>
                                </ul>
        </div>
    </div>
                <div class="panel panel-default panel-collapsible" data-category="transport">
        <div class="panel-heading">
            <p class="panel-title">
                                    <a data-toggle="collapse" href="#transport-collapsed" class="collapsed collapse-handle collapse-handle--right">
                                    <i class="fontello-transport"></i>
                    Verkehr
                </a>
            </p>
        </div>

        <div class="panel-body">
            <ul class="list-unstyled">
                                    <li class="poi-item clearfix" data-lat="51.9052914" data-long="7.6359623" data-name="An der Alten Kirche">
                    <div class="poi-name">An der Alten Kirche</div>
                                </li>
                                                <li class="poi-item clearfix" data-lat="51.9042935" data-long="7.6369615" data-name="Total">
                    <div class="poi-name">Total</div>
                                </li>
                                                <li id="transport-collapsed" class="collapse">
                    <ul>
                            <li class="poi-item clearfix" data-lat="51.9072038" data-long="7.6385873" data-name="Hülsebrockstraße">
                    <div class="poi-name">Hülsebrockstraße</div>
                                </li>
                                </ul>
                </li>
                                </ul>
        </div>
    </div>
        <div class="panel-footer">
        <span class="small">Daten von <a href="http://www.openstreetmap.org/" target="_blank" rel="nofollow">OpenStreetMap</a> - Veröffentlicht unter <a href="http://opendatacommons.org/licenses/odbl/" target="_blank" rel="nofollow">ODbL</a></span>
    </div>
</div>
    </div>

            
    
    
            <div class="widget widget-banner">
            
            
            
            <div id="oms_gpt_rectangle">
    <script type="text/javascript">
        googletag.cmd.push(function() {
            googletag.display('oms_gpt_rectangle')
        });
    </script>
</div>
    
        </div>
        
    
    
            <div class="widget widget-banner">
            
            
            
            <div id="oms_gpt_home_rect_251" >
    <script type="text/javascript">
        googletag.cmd.push(function() {
            googletag.display('oms_gpt_home_rect_251')
        });
    </script>
</div>
    
        </div>
        
    
    
            <div class="widget widget-banner">
            
            
            
            <div id='oms_gpt_outofpage'>
    <script type="text/javascript">
        if (isDesktop) {
            googletag.cmd.push(function() {
                googletag.display('oms_gpt_outofpage');
            });
        }
    </script>
</div>
    
        </div>
                </div>

        </div>     </div>
    <div class="expose-agency-info th-expose-agency-info">
    <div class="row">

        <div class="col-sm-4">

            <div class="expose-agency-main">
                <div class="expose-agency-contact">
                                            <p><strong>Vereinigte Volksbank Münster eG</strong></p>
                    
                                            <p>Ihr Ansprechpartner</p>
                    
                    <div class="contact-person ci-contact">
                                                    <strong>
                                Frau
                                
                                Lisa Wegener
                            </strong>
                        
                        
                                                    <div class="telephone-wrap">
                                <div class="js-content-swap">
                                    <a class="placeholder js-phone-number">Telefonnummer anzeigen</a>
                                    <div class="content">
                                                                            <p class="telephone">
                                            <span class="text-muted utils">Zentrale:</span>
                                            <a href="tel:(0251)5005580">(0251) 5005 580</a>
                                        </p>
                                                                            <p class="telephone">
                                            <span class="text-muted utils">Fax:</span>
                                            <a href="tel:(0251)50055899">(0251) 5005 5899</a>
                                        </p>
                                                                        </div>
                                </div>
                            </div>                                             </div>
                </div>
                                <div class="item-id-wrap">
                    <p>Bitte beziehen Sie sich bei Ihrer Kontaktaufnahme auf immomarkt.ms, Objekt 8699 - vielen Dank!</p>
                </div>
                            </div>

        </div>

        <div class="col-sm-4">
            <div class="agency-action-buttons">
                <div class="btn-wrap contact">
                        <button class="btn btn-contact-provider js-contact-provider-btn-bottom btn-block-xs "
                type="button"
                data-toggle="modal"
                data-target=".contact-modal">

                                    <span class="glyphicon glyphicon-envelope"></span>
                                Anbieter kontaktieren
        </button>
                    </div>

                                <div class="btn-wrap profile">
                    <a class="js-profile" href="/anbieter/volksbank-muenster-immobilien-51668">
                        <span class="glyphicon glyphicon-user"></span> Firmenprofil des Anbieters
                    </a>
                </div>
                <div class="btn-wrap profile">
                    <a class="js-profile" href="/anbieter/volksbank-muenster-immobilien-51668#imprint-anchor">
                        <span class="glyphicon glyphicon-info-sign"></span> Impressum des Anbieters
                    </a>
                </div>
                            </div>
        </div>

        <div class="col-sm-4">
                            <div class="expose-agency-logo">
                                    <a href="/anbieter/volksbank-muenster-immobilien-51668">
                                    <img src="https://cmcdn.de/img-service/gZC35Wg4MG_oSvykbmlwf6JQxjGA0yNyz0jHUKFWHEkuee9kAfF3IKwVvJ6mQq4q7khi-vIsXyk7umIEle1LJ-AXFCJFT5zVTiDR-Prw32gM;scale=300x180" class="utils-img-border" alt="logo"/>
                                    </a>
                                </div>
                    </div>

    </div>

</div>

        </div>

        <div class="content-bottom">
                    </div>

        <div class="footer th-footer">
                <div id="banner-footer">
            
                
                
                
            <div id="oms_gpt_serp_footer">
    <script type="text/javascript">
        if (isDesktop) {
            googletag.cmd.push(function() {
                googletag.display('oms_gpt_serp_footer');
            });
        }
    </script>
</div>
    
    </div>

    
<div class="footer-row-1">
    <ul class="list-inline footer-links">
    
    <li>
        <a href="/kontakt"
            
                                    class=""

                                    >
            Kontakt        </a>
    </li>
    
    
    <li>
        <a href="/impressum"
            
                                    class=""

            rel="nofollow"                        >
            Impressum        </a>
    </li>
    
    
    <li>
        <a href="/agb"
            
                                    class=""

            rel="nofollow"                        >
            Nutzungsbedingungen &amp; AGB        </a>
    </li>
    
    
    <li>
        <a href="/datenschutz"
            
                                    class=""

            rel="nofollow"                        >
            Datenschutz        </a>
    </li>
    
    
    <li>
        <a href="http://www.oms.eu/nutzungsbasierte-online-werbung"
            target="_blank"
                                    class=""

            rel="nofollow"                        >
            Nutzungsbasierte Online-Werbung        </a>
    </li>
    
    
    <li>
        <a href="http://admin.immomarkt.ms/"
            target="_blank"
                                    class=""

            rel="nofollow"                        >
            Für Makler        </a>
    </li>
    
    
    <li>
        <a href="/glossar"
            
                                    class=""

            rel="nofollow"                        >
            Glossar        </a>
    </li>
    
    
    <li>
        <a href="http://www.zeitungsgruppe-muenster.de/"
            target="_blank"
                                    class=""

            rel="nofollow"                        >
            Mediadaten        </a>
    </li>
    
            </ul>

    <p>&copy; Aschendorff Medien GmbH & Co. KG</p>
</div>

        
        </div>

    </div>
</div>
            <div class="modal fade bookmarks-modal js-bookmarks-modal ci-bookmarks-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <p class="modal-title th-modal-title h4">
                    <span class="glyphicon glyphicon-star"></span>
                    Merkliste
                </p>
            </div>

            <div class="modal-body">

                <div class="bookmarked-items js-bookmarked-items">
                    <div class="bookmark-list-title bookmarked-items-title">Favoriten</div>
                    <div class="js-empty-message ci-no-exposes-bookmarked-message">
                        <div class="text-muted">Keine Favoriten. Klicken Sie auf das Sternsymbol, um Favoriten zu speichern.</div>
                    </div>

                    <div class="js-bookmarks-list"></div>

                    <script type="text/template" class="js-bookmark-template">
                        <div class="bookmarked-item th-bookmarked-item clearfix" data-id="{id}">

    <div class="item-media-wrap">
        <div class="item-img-wrap">
            <a href="{url}">
                <img src="{image}" class="utils-img-border"/>
            </a>
        </div>
    </div>

    <div class="item-info-wrap">
        <p class="title clearfix">
            <button class="btn btn-danger remove-bookmark js-remove-bookmark"
                    type="button"  data-id="{id}">
                <span aria-hidden="true" class="glyphicon glyphicon-trash"></span>
                <span class="sr-only">Entfernen</span>
            </button>
            <a href="{url}">{title}</a>
        </p>
        <div class="item-specs-wrap clearfix">
            <div class="item-spec item-spec-price">{price}</div>
            <div class="item-spec item-spec-rooms">{rooms} Zimmer</div>
            <div class="item-spec item-spec-space">{space} m<sup>2</sup></div>
        </div>
        <address class="address">
            <span class="glyphicon glyphicon-map-marker"></span>
            {address}
        </address>

    </div>
</div>
                    </script>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary ci-dismiss-bookmark-modal" data-dismiss="modal">
                    <span class="glyphicon glyphicon-remove"></span> Schließen
                </button>
            </div>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


    

            <div class="modal fade contact-modal js-contact-modal" tabindex="-1" role="dialog" aria-labelledby="contact-modal-title" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <p class="h4 modal-title th-modal-title" id="contact-modal-title">Anfrage senden</p>
            </div>
            <div class="modal-body">

                <div class="agency-info well well-sm">
                    <div class="row">
                        <div class="col-xs-9">
                                                        <p><strong>Vereinigte Volksbank Münster eG</strong></p>
                            
                            <div>
                                                                    <p>Ihr Ansprechpartner</p>
                                    <strong>Lisa Wegener</strong><br/>
                                
                                                            </div>
                        </div>
                        <div class="col-xs-3">
                                                            <img src="https://cmcdn.de/img-service/gZC35Wg4MG_oSvykbmlwf6K5xs6OXqyG41u9KAlOpvqTGbO611T4Mvj4nwfjWI2rxOIiBq7gyWdem5jUpGGgqr-h1thOGMCTAbeNJyoDYkDs;scale=100x60" alt="logo" class="img-responsive utils-img-border pull-right"/>
                                                    </div>
                    </div>
                </div>

                <hr/>

                <form name="contact_form" method="post" action="#" data-action="/contact/expose/8HFRLQ" class="form-horizontal contact-provider-form js-contact-provider-form" role="form" novalidate="novalidate" data-preview-action="">        <noscript>
            <div class="disabled-form">
                <div class="disabled-form__info-wrapper">
                    <div class="disabled-form__info-wrapper__hint text-danger">
                        <span class="lead">Javascript erforderlich!</span>
                        <p>Bitte aktivieren Sie Javascript in Ihrem Browser und laden Sie die Seite erneut.</p>
                    </div>
                </div>
            </div>
        </noscript>
    

    <div class="salutation js-salutation form-group">
                    <div class="col-xs-6">
                <select id="contact_form_salutation" name="contact_form[salutation]" required="required" class="form-control form-control"><option value="" selected="selected">Anrede</option><option value="mr">Herr</option><option value="ms">Frau</option></select>
                <div class="error-msg">
                    Pflichtfeld
                </div>
            </div>
                <div class="col-xs-6">
            <div style="line-height: 30px;" class="text-right">* Pflichtfeld</div>
        </div>
    </div>

            <div class="name js-name form-group">
            <div class="col-xs-12">
                <div class="">                     <input type="text" id="contact_form_name" name="contact_form[name]" required="required" class="form-control form-control" placeholder="Name *" />
                    <div class="error-msg">
                        Bitte geben Sie Ihren Namen an.
                    </div>
                </div>
            </div>
        </div>
    
            <div class="email js-email form-group">
            <div class="col-xs-12">
                <div class="">                     <input type="email" id="contact_form_email" name="contact_form[email]" required="required" class="form-control form-control" placeholder="E-Mail *" />
                    <div class="error-msg">
                        Bitte geben Sie eine gültige E-Mail-Adresse an.
                    </div>
                </div>
            </div>
        </div>
    
            <div class="phone js-phone form-group">
            <div class="col-xs-12">
                <div class="">                     <input type="text" id="contact_form_phone" name="contact_form[phone]" class="form-control form-control" placeholder="Telefon" />
                    <div class="error-msg">
                        Bitte geben Sie Ihre Telefonnummer an.
                    </div>
                </div>
            </div>
        </div>
    
    <div class="appointment form-group">
        <div class="col-xs-12">
            <div class="checkbox">
                    <div class="checkbox"><label for="contact_form_appointment_chk" class="required"><input type="checkbox" id="contact_form_appointment_chk" name="contact_form[appointment_chk]" required="required" value="1" /> Besichtigungstermin vereinbaren</label></div>
            </div>
        </div>
    </div>

    <div class="property-info form-group">
        <div class="col-xs-12">
            <div class="checkbox">
                    <div class="checkbox"><label for="contact_form_property_info_chk" class="required"><input type="checkbox" id="contact_form_property_info_chk" name="contact_form[property_info_chk]" required="required" value="1" /> Objektinformationen anfordern</label></div>
            </div>
        </div>
    </div>

            
    
        
                    <div class="my-address-modal ">

                <div class="my-address-inputs">
                                            <div class="form-group street js-street">
                            <div class="col-xs-12">
                                <input type="text" id="contact_form_street" name="contact_form[street]" required="required" class="form-control form-control" placeholder="Straße *" />
                                <div class="error-msg">
                                    Pflichtfeld
                                </div>
                            </div>
                        </div>
                    
                                            <div class="form-group">
                                                            <div class="col-xs-4 utils-padding-r0">
                                    <div class="zip-code js-zip-code">
                                        <input type="text" id="contact_form_zipCode" name="contact_form[zipCode]" class="form-control form-control" placeholder="PLZ" />
                                        <div class="error-msg">
                                            Pflichtfeld
                                        </div>
                                    </div>
                                </div>
                            
                                                            <div class="col-xs-8">
                                    <div class="place js-place">
                                        <input type="text" id="contact_form_place" name="contact_form[place]" required="required" class="form-control form-control" placeholder="Ort *" />
                                        <div class="error-msg">
                                            Pflichtfeld
                                        </div>
                                    </div>
                                </div>
                                                    </div>
                                    </div>
            </div>
    <div class="message form-group">
        <div class="col-xs-12">
            <textarea id="contact_form_message" name="contact_form[message]" required="required" class="form-control form-control" rows="3">Ich interessiere mich für Ihr Angebot und möchte gerne weitere Informationen erhalten.</textarea>
            <div class="error-msg">
                Pflichtfeld
            </div>
        </div>
    </div>

    <div class="js-alerts-wrap"></div>

            
            <div class="alert alert-info">
            Ihre personenbezogenen Daten verwenden wir, soweit keine darüberhinausgehende Einwilligung vorliegt, nur zur Abwicklung des der Erhebung zugrundeliegenden Zwecks. Nähere Informationen zu unserem Umgang mit personenbezogenen Daten erhalten Sie unter <a href="/datenschutz" target="_blank" rel="nofollow">Datenschutzerklärung</a>. Hier kommen wir auch unseren Informationspflichten nach der EU-Datenschutzgrundverordnung nach.

        </div>
    
    <div class="form-footer clearfix">
                        
                        <div class="row">
                            <div class="col-xs-6">
                                <button type="button" class="btn btn-block btn-default" data-dismiss="modal">
                                    <span class="glyphicon glyphicon-remove"></span>
                                    Schließen
                                </button>
                            </div>

                            <div class="col-xs-6">
                                <button type="submit"                                         class="btn btn-block btn-primary submit-contact-modal-btn js-submit-contact-modal-btn">
                                    Anfrage senden
                                </button>
                            </div>
                        </div>
                        </div>

    <input type="hidden" id="contact_form__token" name="contact_form[_token]" value="SvGvWNYzv6so0QIjjREkuRNeLQkMgi_GKk_zUjeM-ec" />

    <script type="text/template" class="js-alert-template">
    <div class="alert {className} alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert">
            <span aria-hidden="true">&times;</span>
            <span class="sr-only">Close</span>
        </button>
        <span class="js-status-text alert-status-text">{text}</span>
    </div>
</script>

<script type="text/translation" class="js-unknown-error">
    Ein unbekannter Fehler ist beim Versand Ihrer E-Mail aufgetreten. Bitte versuchen Sie es zu einem spateren Zeitpunkt noch einmal.<br><br>Vielen Dank.
</script>

<script type="text/template" class="js-progress-bar">
    <div class="progress utils-margin-b0">
        <div class="progress-bar progress-bar-striped active"  role="progressbar"
             aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
    </div>
</script>
</form>

            </div>         </div>
    </div>
</div>
    
    
        
    
        
    
        <script type="text/javascript" src="//script.ioam.de/iam.js"></script>

        <!-- SZM VERSION="2.0" -->
        <script type="text/javascript">
            var desktopTracking = {"st":"westnach","cp":"rub_immo_suche","sv":"in","co":"kommentar"};

            
            var iam_data = desktopTracking;
            var windowWidth = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;

            if (windowWidth < 768 && window.mobileTracking !== undefined) {
                iam_data = mobileTracking;
            }

            
            
            if (window.iom && window.iom.c) {
                iom.c(iam_data,1);
            }
        </script>
        <!--/SZM -->

    

<!-- js -->
        <div class="js-placeholder-waypoints" data-src="/js/4432cf5-d7ddffa.js?99f1c194"></div>
    
        
        <script>
        module = false;
    </script>
    <script type="text/javascript" src="https://cmcdn.de/vendor/jquery/2.1.1/jquery-2.1.1.min.js"></script>

    

    
<script>
    window.app = window.app || {};
    window.app.trans = {"common.add":"Hinzuf\u00fcgen","common.search":"Suchen","common.radius":"Umkreis","common.selected":"ausgew\u00e4hlt","common.close":"Schlie\u00dfen","common.error":"Fehler","common.delete":"L\u00f6schen","common.delete.title":"Eintrag l\u00f6schen","common.delete.label":"Soll dieser Eintrag wirklich gel\u00f6scht werden?","common.name":"Name","common.apply":"\u00dcbernehmen","common.from":"ab","common.to":"bis","common.any":"beliebig","common.please_select":"Bitte ausw\u00e4hlen","common.new":"Neu","common.runtime":"Laufzeit","common.credits":"Kontingent","common.contracts":"Flatrates & Kontingente","common.contract_templates":"Produkte","common.notifications":"Benachrichtigungen","common.notifications.all":"Alle Benachrichtigungen","common.error.loading":"Beim Laden der Seite ist ein Fehler aufgetreten.","common.on":"An","common.off":"Aus","common.confirm":"Best\u00e4tigen","common.and":"und","bookmarks.message.add":"In die Merkliste","bookmarks.message.added":"In der Merkliste","bookmarks.message.remove":"Aus Merkliste entfernen","bookmarks.message.removed":"Aus Merkliste entfernt","bookmarks.message.disabled":"Merken ist nicht m\u00f6glich","bookmarks.label.bookmark":"Merken","bookmarks.label.bookmarked":"Gemerkt","map.show_surroundings":"Umgebung auf Karte anzeigen","recommend.unknown_error":"Ein unbekannter Fehler ist beim Versand Ihrer E-Mail aufgetreten. Bitte versuchen Sie es zu einem spateren Zeitpunkt noch einmal.<br><br>Vielen Dank.","subscription.search_agent.page_title":"Suchagent","subscription.search_agent.server_error":"Ein Fehler ist aufgetreten. Bitte versuchen Sie es sp\u00e4ter erneut.","subscription.search_agent.form.resubmit.label":"Weiteren Suchagenten anlegen","errors.invalid_value":"Ung\u00fcltiger Wert","common.cancel":"Abbrechen","cms.labels.edit":"Bearbeiten","cms.labels.delete.message":"Sind sie sicher, dass Sie diese CMS-Seite l\u00f6schen wollen?","cms.searchagent.button.label":"Suchagent","cms.searchagent.button.tooltip":"Suchagenten Widget einf\u00fcgen","cms.searchagent.already_existing":"Dieses Widget ist bereits vorhanden und kann daher nicht erneut eingef\u00fcgt werden.","errors.unknown_error":"Ein unbekannter Fehler ist aufgetreten. Bitte versuchen Sie es sp\u00e4ter erneut.","location_form.map.pin_manual":"Klicken Sie auf die Karte, oder verschieben Sie den Pin, um die Position zu korrigieren.","location_form.map.pin_auto":"Pin automatisch platzieren","price.netValueCents.label":"Preis (netto)","duration.day":"Tag(e)","duration.week":"Woche(n)","duration.month":"Monat(e)","duration.year":"Jahr(e)","pagination.results":"%count% Ergebnisse","geo.country":"Land","geo.state":"Bundesland","geo.district":"Kreis","geo.city":"Stadt","geo.citydistrict":"Stadtteil","geo.neighbourhood":"Ortsteil","geo.zip":"Postleitzahl","geo.pobox":"Postfach","geo.usercomposed":"Sonstige","country.de":"Deutschland","country.at":"\u00d6sterreich","country.ch":"Schweiz","country.ad":"Andorra","country.al":"Albanien","country.ba":"Bosnien und Herzegowina","country.be":"Belgien","country.bg":"Bulgarien","country.cy":"Zypern","country.cz":"Tschechien","country.dk":"D\u00e4nemark","country.ee":"Estland","country.fi":"Finnland","country.fr":"Frankreich","country.gb":"Vereinigtes K\u00f6nigreich Gro\u00dfbritannien und Nordirland","country.gi":"Gibraltar","country.gr":"Griechenland","country.hr":"Kroatien","country.hu":"Ungarn","country.ie":"Irland","country.im":"Insel Man","country.is":"Island","country.it":"Italien","country.li":"Liechtenstein","country.lt":"Litauen","country.lu":"Luxemburg","country.lv":"Lettland","country.mc":"Monaco","country.md":"Moldawien","country.me":"Montenegro","country.mk":"Mazedonien","country.mt":"Malta","country.nl":"Niederlande","country.no":"Norwegen","country.pl":"Polen","country.pt":"Portugal","country.ro":"Rum\u00e4nien","country.rs":"Serbien","country.ru":"Russland","country.se":"Schweden","country.si":"Slowenien","country.sk":"Slowakei","country.ua":"Ukraine","hint.redirect_to_url":"Sie werden auf folgende Seite weitergeleitet: %url%\\n","location.autocomplete.term":"Nach \\"%term%\\" suchen","bo.contract.warning_voucher_without_portal.text":"Sie haben f\u00fcr die Gutscheinvergabe kein zugeh\u00f6riges Portal ausgew\u00e4hlt. Der damit verbundene Umsatz wird dadurch f\u00fcr kein Portal verbucht.","gdpr.cookie_consent.info_text":"Wir setzen auf unseren Internetseiten Cookies und andere Technologien ein, um Ihnen unsere Dienste technisch bereitstellen zu k\u00f6nnen, Inhalte und Anzeigen f\u00fcr Sie zu personalisieren sowie anonyme Nutzungsstatistiken zu analysieren. Dabei arbeiten wir mit Drittanbietern zusammen und tauschen Informationen zur Nutzung unserer Dienste zur Analyse und Werbung aus. Durch die weitere Nutzung unserer Internetseite erkl\u00e4ren Sie sich mit dem Einsatz von Cookies einverstanden.","gdpr.cookie_consent.more_text":"Datenschutzhinweise","gdpr.cookie_consent.impressum_text":"Impressum","gdpr.cookie_consent.close_button_text":"Schlie\u00dfen"};
    window.app.getTrans = function(string, placeholders) {
        if (window.app.trans.hasOwnProperty(string)) {
            var translation = window.app.trans[string];

            if (typeof placeholders === 'object') {
                var exp = new RegExp('(' + Object.keys(placeholders).map(function(i) {
                    return i.replace(/[.?*+^$[\\]\\\\(){}|-]/g, "\\\\$&")
                }).join('|') + ')', 'g');

                translation = translation.replace(exp, function(s) {
                    return placeholders[s]
                });
            }

            return translation;
        }

        return string;
    };
</script>

        <script type="text/javascript" src="/js/d16da62-69f11b1.js?99f1c194"></script>
    
    <script type="text/javascript">
                        window.cookieConsentOptions = {
            infoText: window.app.getTrans('gdpr.cookie_consent.info_text'),
            moreText: window.app.getTrans('gdpr.cookie_consent.more_text'),
            imprintText: window.app.getTrans('gdpr.cookie_consent.impressum_text'),
            closeButtonText: window.app.getTrans('gdpr.cookie_consent.close_button_text'),
                moreLink: '/datenschutz',
                imprintLink:
                        'https://immomarkt.ms/impressum',
                };
    </script>
    <script type="text/javascript" src="https://cmcdn.de/CookieConsent/CookieConsent.js" async></script>

    <script type="text/javascript">
        $(function() {
          $.each(window.onjQuery.queue, function(_, f) {
            f();
          });
        });
    </script>

    
    

    
        <script type="text/javascript" src="/js/ee731c3-e029a06.js?99f1c194"></script>
    
        <script type="text/javascript" src="/js/acd10fc-0113b2e.js?99f1c194"></script>

<script type="text/javascript">
window.onjQuery.push(function() {
cmtp("expose_impression", {"listing_id":"8HFRLQ","external_unique_id":"4146569","object_number":"8699","origin":{"feed_id":"wfn_iwfn_realestate","authority":"self"},"advertizer_id":"51668","domain":"immomarkt.ms","site_code":"wfn_iwfn"});

});
</script>

    <script type="text/javascript" src="/js/e0d0ca2-6638f84.js?99f1c194"></script>
    


    
<script>
    window.app.trans["booking_process.address.violations.postal_code"] = "Bitte geben Sie eine g\u00fcltige Postleitzahl an.";
    window.app.trans["common.rent"] = "Mieten";
    window.app.trans["common.rental"] = "mieten";
    window.app.trans["common.rental.capitalized"] = "Mieten";
    window.app.trans["common.sale"] = "kaufen";
    window.app.trans["common.sale.capitalized"] = "Kaufen";
    window.app.trans["common.rooms"] = "Zimmer";
    window.app.trans["common.sale.capitalized"] = "Kaufen";
    window.app.trans["common.property_type.input.placeholder.rental"] = "%type% mieten";
    window.app.trans["common.property_type.input.placeholder.sale"] = "%type% kaufen";
    window.app.trans["contact.error.contact_request_failed"] = "Beim Versenden der Anfrage ist ein Fehler aufgetreten.<br><br>Bitte versuchen Sie es in ein paar Minuten erneut. <br>Vielen Dank";
    window.app.trans["contact.error.no_data_received"] = "Beim Versenden der Anfrage ist ein Fehler aufgetreten. Leere Antwort vom Server.<br><br>Bitte versuchen Sie es in ein paar Minuten erneut. <br>Vielen Dank";
    window.app.trans["demand.marketing_types"] = "Immobilienart";
    window.app.trans["demand.property_types"] = "Immobilientyp";
    window.app.trans["profile.label.specializations"] = "Spezialisierung";
    window.app.trans["expose.photosphere.load.label"] = "Zum Laden des Panoramabildes hier klicken";
    window.app.trans["expose.contact_agent_notification.text"] = "Klicken Sie hier, um direkt mit dem Anbieter Kontakt aufzunehmen.";
    window.app.trans["serp.deleted_expose"] = "Die aufgerufene Immobilie wurde zwischenzeitlich vom Anbieter deaktiviert bzw. entfernt. Wir haben f\u00fcr Sie nach \u00e4hnlichen Inseraten gesucht.";
</script>

        <script type="text/javascript" src="/js/671bf91-338c761.js?99f1c194"></script>
        
    <script type="text/javascript">
        var iomV = 1;
        var conversionData = {
            category: '48165|M\u00FCnster|apartment_living_maisonette|rental|51668'
        };
        </script>

    <script id="expose-swipe" type="text/x-tmpl">
    <div class="expose-swipe--backdrop fade">
        <span class="expose-swipe--backdrop__spinner"></span>
        <br/>
        <div class="expose-swipe--backdrop__hint"></div>
    </div>
    <div class="expose-swipe--hint">
        <div class="icon">
            <svg class="cm-icon-swipe" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 401 266"><path class="cm-icon-swipe__path" d="M124.19,140.21c-16-38.66-1.88-84.37,35-105.43C194.92,14.34,242.68,24.35,267,57.84c17.45,24.05,20.26,54.84,9.13,82.16-2,5,2.21,11,7,12.3,5.73,1.57,10.25-2,12.3-7a103.41,103.41,0,0,0-6.35-90.16A104.2,104.2,0,0,0,232.16,9.27C183.74-7,128.45,16.64,107.2,63.21,95.37,89.14,94,119,104.9,145.52c2.06,5,6.64,8.54,12.3,7,4.72-1.3,9.06-7.28,7-12.3Z"/><path class="cm-icon-swipe__path" d="M74.59,111l-36-2.17,8.47-8.47s7.35-6.71,1.53-12.54c-5.67-5.67-12.7,1.37-12.7,1.37L8.37,116.7a4.72,4.72,0,0,0,0,6.65l27.54,27.54s6.71,7.35,12.54,1.53c5.67-5.67-1.37-12.7-1.37-12.7l-7.7-7.7,35.21-2.76s8.91,0,8.91-9.14S74.59,111,74.59,111Z"/><path class="cm-icon-swipe__path" d="M252.79,132c-6.93-17.37-23.52-42-52.79-42s-46.38,23.66-53,41c-5.7,14.94-10.38,95.59-2.5,109.5s39.94,20,56,20c16.85,0,49.34-6.54,57-21C264.79,225.75,258.56,146.46,252.79,132Zm-93.46-9.24c4.29-8.1,32.29-11.77,41.72-11.76,9,0,35.86,3.38,40.29,11.2s-1,36.83-4.2,45.19c-3.7,9.71-19.42,22.48-35.81,22.48s-32.69-13.32-36.57-23C161.53,158.74,155.25,130.46,159.33,122.76Z"/><path class="cm-icon-swipe__path" d="M326.91,129.27l36,2.17-8.47,8.47s-7.35,6.71-1.53,12.54c5.67,5.67,12.7-1.37,12.7-1.37l27.54-27.54a4.72,4.72,0,0,0,0-6.65L365.59,89.36s-6.71-7.35-12.54-1.53c-5.67,5.67,1.37,12.7,1.37,12.7l7.7,7.7L326.91,111s-8.91,0-8.91,9.14S326.91,129.27,326.91,129.27Z"/></svg>
        </div>
        <div class="text">Ganz einfach per wischen zur nächsten oder vorherigen Immobilie.</div>
    </div>
</script>

        <script type="text/javascript" src="/js/77a35e6-0a6691b.js?99f1c194"></script>
    
<script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"beacon":"bam.nr-data.net","licenseKey":"399c1f1667","applicationID":"4241962","transactionName":"NFMGMBZRWkZUVURbXQ0ZJQcQWVtbGlVdbVcbRgsXAQ==","queueTime":1,"applicationTime":221,"atts":"GBQFRl5LSUg=","errorBeacon":"bam.nr-data.net","agent":""}</script></body>
</html>
`;
