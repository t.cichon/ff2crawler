module.exports.data = `
<!DOCTYPE html>
<html lang="de">











<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# website: http://ogp.me/ns/website#">
	<meta charset="utf-8">
	<title>Ansehen: frisch modernisiertes Wohnglück</title>
	<meta name="robots" content="index, follow">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
	<meta name="verify-v1" content="E7WpjnwYzDDUsZ4PifavdFRPI+LYgbNJmhDddKe97qw=">
	<meta name="description" content=""/>

	<meta property="og:title" content="Ansehen: frisch modernisiertes Wohnglück" />
	<meta property="og:type" content="website"/>
	<meta property="og:url" content="https://www.immonet.de/angebot/34443703"/>
	<meta property="og:image" content="https://i.immonet.de/63/36/33/509633633_456x341.jpg"/>
	<meta property="og:site_name" content="Immonet"/>
	<meta property="og:description" content=""/>

	<link rel="apple-touch-icon" sizes="152x152" href="//www.immonet.de/angebot/resources/images/logo/apple_touch-icon-ipad-retina.png">
	<link rel="apple-touch-icon" sizes="120x120" href="//www.immonet.de/angebot/resources/images/logo/apple_touch-icon-iphone-retina.png">
	<link rel="apple-touch-icon" sizes="76x76" href="//www.immonet.de/angebot/resources/images/logo/apple_touch-icon-ipad.png">
	<link rel="apple-touch-icon" href="//www.immonet.de/angebot/resources/images/logo/apple_touch_icon_48x48.png">
	<link rel="shortcut icon" href="//www.immonet.de/angebot/resources/images/logo/favicon.ico" type="image/ico">

	<link rel="canonical" href="https://www.immonet.de/angebot/34443703">

	<link rel="stylesheet" href="//cdn.immonet.de/frontend-resources/4.8.0/lib/pannellum/pannellum.css">
	<link rel="stylesheet" href="//cdn.immonet.de/frontend-resources/4.8.0/lib/fotorama/fotorama.css">
	<link rel="stylesheet" href="//cdn.immonet.de/frontend-resources/4.8.0/immonet/immonet.min.css?v=131">
	<link rel="stylesheet" href="//cdn.immonet.de/frontend-resources/4.8.0/immonet/modules/expose.min.css?v=136">
	<link rel="stylesheet" href="//cdn.immonet.de/frontend-resources/4.8.0/immonet/modules/headerfooter/v2/headerfooter.min.css">

	<link rel="stylesheet" href="//cdn.immonet.de/frontend-resources/4.8.0/lib/owl.carousel/assets/owl.carousel.css">
	<link rel="stylesheet" href="//cdn.immonet.de/frontend-resources/4.8.0/lib/owl.carousel/assets/owl.theme.css">
	<link rel="stylesheet" href="//cdn.immonet.de/frontend-resources/4.8.0/lib/owl.carousel/assets/owl.immonet.carousel.css">

	<link href="https://umziehen.immonet.de/costcalculator-client/css/costcalculator-in-1.0.69.css" rel="stylesheet" />


	
	



	
		<script defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDKhGf1y2TOI87VowIXSEiDPaCcOaJPntg&v=3.32.5a&callback=initModalMap"></script>
		<script src="//cdn.immonet.de/frontend-resources/4.8.0/lib/gmaps/gmaps.js"></script>
	

	<script src="//cdn.immonet.de/frontend-resources/4.8.0/lib/toast/toast.min.js"></script>
	<script src="//cdn.immonet.de/frontend-resources/4.8.0/immonet/js/expose-head.min.js?v=187"></script>


	

	
	

	
		<script src="//cdn.optimizely.com/js/4979030052.js"></script>
	

	
	
		<script type="text/javascript">
			
			var expireCookie = new Date();
			expireCookie.setTime(expireCookie.getTime()+(10*24*60*60*1000));
			expires = "expires=" + expireCookie.toGMTString();
			document.cookie="abtest=enabled; " + expires + "; secure";
		</script>
	

	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="mobile-web-app-capable" content="yes">
</head>
<body id="top" class="overflow-x-hidden-sm bg-medium-grey">
			<div class="canvas-wrapper ">

			



   

<script src="//cdn.immonet.de/frontend-resources/4.8.0/lib/adtag-snippets/viewport.min.js"></script>


<script src="//cdn.immonet.de/frontend-resources/4.8.0/lib/adtag-snippets/adtags.min.js?v=138"></script>












<script type="text/javascript">
    var sdmTargetingParams = JSON.parse('{"area":49.19,"mobex":true,"zip":"48155","objectcat":"Wohnung","pers":"655587","rooms":2,"buildyear":1954,"fed":"Nordrhein-Westfalen","city":"Münster","obcat":"Etagenwohnung","balcn":false,"heatr":"Zentralheizung","pic":"https://i.immonet.de/63/36/33/509633633_156x117.jpg","title":"Ansehen: frisch modernisiertes Wohnglück","kitch":false,"marketingtype":"Miete","bank":false,"obtyp":"Wohnen","prrng":"0-500","price":430.16,"gardn":false,"state":"Deutschland"}');
</script>


<script src="//cdn.immonet.de/frontend-resources/4.8.0/lib/adtag-snippets/adconfig_expose.min.js"></script>


<script>
    
    immonetAds.setOpenXBidderEnabled(true);
</script>


<script src="//cdn.immonet.de/frontend-resources/4.8.0/lib/adtag-snippets/get_gpt.min.js"></script>
<script src="//cdn.immonet.de/frontend-resources/4.8.0/lib/adtag-snippets/adtags_immowelt.min.js"></script>
<script src="//cdn.immonet.de/frontend-resources/4.8.0/lib/adtag-snippets/parameter-mapper.min.js"></script>



<script>
    var targetingParams = {};
    
    targetingParams = JSON.parse('{"area":49.19,"mobex":true,"zip":"48155","objectcat":"Wohnung","pers":"655587","rooms":2,"buildyear":1954,"fed":"Nordrhein-Westfalen","city":"Münster","obcat":"Etagenwohnung","balcn":false,"heatr":"Zentralheizung","pic":"https://i.immonet.de/63/36/33/509633633_156x117.jpg","title":"Ansehen: frisch modernisiertes Wohnglück","kitch":false,"marketingtype":"Miete","bank":false,"obtyp":"Wohnen","prrng":"0-500","price":430.16,"gardn":false,"state":"Deutschland"}');
    
    targetingParams['IMMO_ADGROUP'] = 'IMMOBILIEN_EXPOSE';
    targetingParams['IMMO_OBJECTTYPE'] = '1';

    immonetAds.setParams(targetingParams);
    immonetAds.init('IMMOWELT', 'Wohnimmo_Mieten_Exposes');
</script>








<script>
	var timestamp = Date.now();


    (function(){
    	// Expose Infos
        dataLayer.push({"marketingtype":"Miete","zip":"48155","objectcat":"Etagenwohnung","pgpersonid":655587,"city":"Münster","state":"Deutschland","immonetnr":34443703,"objecttype":"Wohnen","federalstate":"Nordrhein-Westfalen"});

        var data = {
            "pageID": "1001",
            "page_type": "productdetailpage",
            "userAgent" : navigator.userAgent,
            "time" : timestamp,
            "releasename" : "mystique",
            "iavn" : "",
            "host" : "",
            "trackImmoPage" : "true"
        };

        var viewPort;
        if (typeof(immonetViewport) !== 'undefined') {
            // add view port if possible
            viewPort  = immonetViewport.getViewport();
        } else {
            viewPort = undefined;
        }
        data['viewport'] = viewPort;

        dataLayer.push(data);

        dataLayer.push({
            "event" : "trackExpose",
            "transactionId" : "34443703/"+timestamp,
            "transactionTotal": 0.00,
            "transactionProducts": [{
                "sku": "Expose/48155/M\u00FCnster",
                "name": "Expose",
                "category": "Expose/Nordrhein-Westfalen/Miete/Wohnen",
                "price": 0.00,
                "quantity": 1.00
            }]
        });
    })();

</script>







<script>
	var utag_data = {"object_state":["Deutschland"],"object_count_photos":[5],"object_count_floorplans":[0],"object_count_floorplans_pdf":[0],"object_count_documents":[0],"object_release_name":["mystique"],"object_address_is_visible":[true],"object_is_premium":[false],"page_type":"productdetailpage","broker_id":[655587],"object_area":[49.19],"object_city":["Münster"],"object_currency":["EUR"],"object_district":[null],"object_features":[""],"object_federalstate":["Nordrhein-Westfalen"],"object_gok":["98081e39-c331-47b3-a8b3-5587fd86d480"],"object_id":[34443703],"object_is_special":[""],"object_label":["new"],"object_marketingtype":["Miete"],"object_name":["Contact"],"object_objekt_nutzungsart":["WOHNEN"],"object_objekt_zustand":[""],"object_objektart":["wohnung"],"object_objektart_sub":["Etagenwohnung"],"object_price":[430.16],"object_rooms":[2.0],"object_zip":["48155"],"object_listingtype":["standard"],"app_server":"docker","app_version":"","app_time":1533899235039,"customer_cookie_accepted":false};
</script>

<script type="text/javascript">
	(function (a, b, c, d) {
		a = '//tags.tiqcdn.com/utag/axelspringer/immonet-immonet.de/prod/utag.js';
		b = document;
		c = 'script';
		d = b.createElement(c);
		d.src = a;
		d.type = 'text/java' + c;
		d.async = true;
		a = b.getElementsByTagName(c)[0];
		a.parentNode.insertBefore(d, a);
	})();
</script>



<div class="row hidden-print">
	<div id="ads-banner-top" class="container-slide pos-relative container-fluid">
		
		
		
		
		
			<div id="ads-banner" class="immonetads col-xs-12 text-center"></div>
		
		


<div class="hidden-print visible-lg pos-absolute margin-left-12 skyscraper">
	
	
	
	
	
		<div id="ads-sky" class="immonetads text-center"></div>
	
</div>

	</div>
</div>
<div class="shadow-box-4 container-slide bg-medium-grey">
		





	
	
		<header class="bg-white">
	<div class="header-mobile hidden-sm hidden-md hidden-lg hidden-print ">
	<div class=" visible-xs">
	<div class="navbar-fixed-top z-index-navbar-fixed bg-white">
	<div class="text-center navbar navbar-default display-flex align-items-flex-center full-height">
		<button id="btn-navbar" type="button" class="pos-absolute pos-top navbar-toggle margin-top-6" data-toggle="offcanvas" data-disable-scrolling="false" data-target=".navmenu" onclick="immonet.headerfooter.fitPosition();">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<button id="btn-navbar-close" class="hidden pos-absolute pos-top navbar-toggle margin-top-3" data-toggle="offcanvas" data-disable-scrolling="false" data-target=".navmenu">
			<span class="ico ico-primary icoCross ico-size-15"></span>
			</button>
			<div id="header-logo-xs" class="full-width">
				<div>
					<p style="display: none;" class="text-uppercase text-10 padding-0">Unser Regiopartner</p>
					<a id="tsr-int-logo-xs" href="//www.immonet.de">
						<img class="margin-bottom-3" title="Immonet" alt="Immonet" height="28" src="//cdn.immonet.de/frontend-resources/4.7.1/immonet/img/logo.svg" />
					</a>
				</div>
			</div>
		</div>
	</div>
	</div>
	<div id="offcanvas-navmenu" class="pos-absolute navmenu offcanvas z-index-navbar-sticky full-height bg-light-blue">
		<div class="pos-relative swipe-wrap full-height">
			<div class="menu-wrapper pos-absolute img-full full-height">
				<ul class="list-unstyled cursor-hand full-height bg-light-blue">
					<li id="entry-wohnen">
						<div class="bg-light-blue navmenu-entry border-bottom-grey-light margin-left-24 padding-left-0 padding-top-18 padding-right-24 padding-bottom-18 active" data-target="wohnen" id="lnk-int-header-wohnen-xs">
							
							
								<a class="text-gray-light text-uppercase">Wohnen</a>
								<span class="ico icoChevronRight50 pull-right text-white "></span>
							
						</div>
						<div class="content-wrapper pos-absolute pos-top img-full hidden list-unstyled pos-right-negative bg-white full-height" id="wohnen">
							<div class="bg-light-blue entry-back navmenu-back-link col-xs-12 padding-18 border-bottom-grey-light" id="lnk-int-header-wohnen-back-xs">
								<span class="ico icoChevronLeft50 text-white padding-right-15 "></span><a class="text-gray-light text-uppercase">ZURÜCK</a>
							</div>
							<ul class="col-xs-12 list-unstyled bg-white padding-bottom-48">
								<li>
									<div class="navmenu-page-link border-bottom-grey-light margin-left-24 padding-left-0 padding-top-18 padding-right-24 padding-bottom-18" id="lnk-int-header-wohnen-suche-xs">
										<a href="//www.immonet.de/">Wohnimmobilien suchen</a>
									</div>
								</li>
								<li>
									<div class="navmenu-page-link border-bottom-grey-light margin-left-24 padding-left-0 padding-top-18 padding-right-24 padding-bottom-18" id="lnk-int-header-wohnen-seniorenwohnen-xs">
										<a href="//www.immonet.de/service/seniorenwohnen.html">Seniorenwohnen</a>
									</div>
								</li>
								<li>
									<div class="navmenu-page-link border-bottom-grey-light margin-left-24 padding-left-0 padding-top-18 padding-right-24 padding-bottom-18" id="lnk-int-header-wohnen-neubau-xs">
										<a href="//www.immonet.de/neubauimmobilien.html">Neubauimmobilien</a>
									</div>
								</li>
								<li>
									<div class="navmenu-page-link border-bottom-grey-light margin-left-24 padding-left-0 padding-top-18 padding-right-24 padding-bottom-18" id="lnk-int-header-wohnen-hausbau-xs">
										<a href="//www.immonet.de/service/hausbau.html">Hausbau</a>
									</div>
								</li>
								<li>
									<div class="navmenu-page-link border-bottom-grey-light margin-left-24 padding-left-0 padding-top-18 padding-right-24 padding-bottom-18" id="lnk-int-header-wohnen-finanzierung-xs">
										<a href="//www.immonet.de/service/finanzierung.html">Finanzierung</a>
									</div>
								</li>
								<li>
									<div class="navmenu-page-link border-bottom-grey-light margin-left-24 padding-left-0 padding-top-18 padding-right-24 padding-bottom-18" id="lnk-int-header-wohnen-renovieren-xs">
										<a href="//www.immonet.de/service/ausbau-und-umbau.html">Renovieren</a>
									</div>
								</li>
								<li>
									<div class="navmenu-page-link border-bottom-grey-light margin-left-24 padding-left-0 padding-top-18 padding-right-24 padding-bottom-18" id="lnk-int-header-wohnen-kapitalanlage-immobilien-xs">
										<a href="//www.immonet.de/kapitalanlage.html">Kapitalanlage</a>
									</div>
								</li>
								<li>
									<div class="navmenu-page-link border-bottom-grey-light margin-left-24 padding-left-0 padding-top-18 padding-right-24 padding-bottom-18" id="lnk-int-header-wohnen-zwangsversteigerung-xs">
										<a href="//www.immonet.de/zwangsversteigerung.html">Zwangsversteigerungen</a>
									</div>
								</li>
								<li>
									<div class="navmenu-page-link border-bottom-grey-light margin-left-24 padding-left-0 padding-top-18 padding-right-24 padding-bottom-18" id="lnk-int-header-wohnen-suchanzeigen-xs">
										<a href="//www.immonet.de/service/suchanzeige.html">Suchanzeige aufgeben</a>
									</div>
								</li>
								<li>
									<div class="navmenu-page-link border-bottom-grey-light margin-left-24 padding-left-0 padding-top-18 padding-right-24 padding-bottom-18" id="lnk-int-header-wohnen-einrichten-xs">
										<a href="//www.immonet.de/service/einrichten-ratgeber.html">Einrichten</a>
									</div>
								</li>
								<li>
									<div class="navmenu-page-link border-bottom-grey-light margin-left-24 padding-left-0 padding-top-18 padding-right-24 padding-bottom-18" id="lnk-int-header-wohnen-schufa-xs">
										<a href="//www.immonet.de/service/schufa-bonitaetsauskunft.html">SCHUFA-Auskunft</a>
									</div>
								</li>
								<li>
									<div class="navmenu-page-link border-bottom-grey-light margin-left-24 padding-left-0 padding-top-18 padding-right-24 padding-bottom-18" id="lnk-int-header-wohnen-versicherungen-xs">
										<a href="//www.immonet.de/service/versicherungen.html">Versicherungen</a>
									</div>
								</li>
							</ul>
						</div>
					</li>
					<li id="entry-gewerbe">
						<div class="bg-light-blue navmenu-entry border-bottom-grey-light margin-left-24 padding-left-0 padding-top-18 padding-right-24 padding-bottom-18" data-target="gewerbe" id="lnk-int-header-gewerbe-xs">
							
							
								<a class="text-gray-light text-uppercase">Gewerbe</a>
								<span class="ico icoChevronRight50 pull-right text-white "></span>
							
						</div>
						<div class="content-wrapper pos-absolute pos-top img-full hidden list-unstyled pos-right-negative bg-white full-height" id="gewerbe">
							<div class="bg-light-blue entry-back navmenu-back-link col-xs-12 padding-18 border-bottom-grey-light" id="lnk-int-header-gewerbe-back-xs">
								<span class="ico icoChevronLeft50 text-white padding-right-15 "></span><a class="text-gray-light text-uppercase">ZURÜCK</a>
							</div>
							<ul class="col-xs-12 list-unstyled bg-white padding-bottom-48">
								<li>
									<div class="navmenu-page-link border-bottom-grey-light margin-left-24 padding-left-0 padding-top-18 padding-right-24 padding-bottom-18" id="lnk-int-header-gewerbe-suche-xs">
										<a href="//www.immonet.de/gewerbe.html">Gewerbeimmobilien suchen</a>
									</div>
								</li>
								<li>
									<div class="navmenu-page-link border-bottom-grey-light margin-left-24 padding-left-0 padding-top-18 padding-right-24 padding-bottom-18" id="lnk-int-header-gewerbe-ratgeber-xs">
										<a href="//www.immonet.de/service/gewerbe-services.html">Gewerbe Ratgeber</a>
									</div>
								</li>
								<li>
									<div class="navmenu-page-link border-bottom-grey-light margin-left-24 padding-left-0 padding-top-18 padding-right-24 padding-bottom-18" id="lnk-int-header-gewerbe-ausland-xs">
										<a href="//www.immonet.de/auslandsimmobilien-gewerbe.html">Gewerbeimmobilien im Ausland</a>
									</div>
								</li>
								<li>
									<div class="navmenu-page-link border-bottom-grey-light margin-left-24 padding-left-0 padding-top-18 padding-right-24 padding-bottom-18" id="lnk-int-header-gewerbe-buero-xs">
										<a href="//www.immonet.de/buero-praxis.html">Büro und Praxis</a>
									</div>
								</li>
								<li>
									<div class="navmenu-page-link border-bottom-grey-light margin-left-24 padding-left-0 padding-top-18 padding-right-24 padding-bottom-18" id="lnk-int-header-gewerbe-einzelhandel-xs">
										<a href="//www.immonet.de/laden-einzelhandel.html">Einzelhandel</a>
									</div>
								</li>
								<li>
									<div class="navmenu-page-link border-bottom-grey-light margin-left-24 padding-left-0 padding-top-18 padding-right-24 padding-bottom-18" id="lnk-int-header-gewerbe-lagerhalle-xs">
										<a href="//www.immonet.de/halle-lager-produktion.html">Lagerhalle</a>
									</div>
								</li>
								<li>
									<div class="navmenu-page-link border-bottom-grey-light margin-left-24 padding-left-0 padding-top-18 padding-right-24 padding-bottom-18" id="lnk-int-header-gewerbe-gastgewerbe-xs">
										<a href="//www.immonet.de/gastgewerbe-hotel.html">Gastgewerbe und Hotel</a>
									</div>
								</li>
								<li>
									<div class="navmenu-page-link border-bottom-grey-light margin-left-24 padding-left-0 padding-top-18 padding-right-24 padding-bottom-18" id="lnk-int-header-gewerbe-kapitalanlage-immobilien-xs">
										<a href="//www.immonet.de/kapitalanlage.html">Kapitalanlage</a>
									</div>
								</li>
							</ul>
						</div>
					</li>
					<li id="entry-anbieten">
						<div class="bg-light-blue navmenu-entry border-bottom-grey-light margin-left-24 padding-left-0 padding-top-18 padding-right-24 padding-bottom-18" data-target="anbieten" id="lnk-int-header-anbieten-xs">
							
							
								<a class="text-gray-light text-uppercase">Anbieten</a>
								<span class="ico icoChevronRight50 pull-right text-white "></span>
							
						</div>
						<div class="content-wrapper pos-absolute pos-top img-full hidden list-unstyled pos-right-negative bg-white full-height" id="anbieten">
							<div class="bg-light-blue entry-back navmenu-back-link col-xs-12 padding-18 border-bottom-grey-light" id="lnk-int-header-anbieten-back-xs">
								<span class="ico icoChevronLeft50 text-white padding-right-15 "></span><a class="text-gray-light text-uppercase">ZURÜCK</a>
							</div>
							<ul class="col-xs-12 list-unstyled bg-white padding-bottom-48">
								<li>
									<div class="navmenu-page-link border-bottom-grey-light margin-left-24 padding-left-0 padding-top-18 padding-right-24 padding-bottom-18" id="lnk-int-header-anbieten-uebersicht-xs">
										<a href="//www.immonet.de/anbieten.html">Anzeige aufgeben</a>
									</div>
								</li>
								<li>
									<div class="navmenu-page-link border-bottom-grey-light margin-left-24 padding-left-0 padding-top-18 padding-right-24 padding-bottom-18" id="lnk-int-header-anbieten-privat-xs">
										<a href="//www.immonet.de/service/einzelne-immobilien-anbieten.html">Immobilien privat anbieten</a>
									</div>
								</li>
								<li>
									<div class="navmenu-page-link border-bottom-grey-light margin-left-24 padding-left-0 padding-top-18 padding-right-24 padding-bottom-18" id="lnk-int-header-anbieten-gewerblich-xs">
										<a href="//www.immonet.de/service/duo-partnerschaft.html">Immobilien gewerblich anbieten</a>
									</div>
								</li>
								<li>
									<div class="navmenu-page-link border-bottom-grey-light margin-left-24 padding-left-0 padding-top-18 padding-right-24 padding-bottom-18" id="lnk-int-header-anbieten-immobilienverkauf-xs">
										<a href="//www.immonet.de/service/checkliste-immobilienverkauf.html">Immobilienverkauf</a>
									</div>
								</li>
								<li>
									<div class="navmenu-page-link border-bottom-grey-light margin-left-24 padding-left-0 padding-top-18 padding-right-24 padding-bottom-18" id="lnk-int-header-anbieten-haus-verkaufen-xs">
										<a href="//www.immonet.de/service/haus-verkaufen-hausverkauf.html">Haus verkaufen</a>
									</div>
								</li>
								<li>
									<div class="navmenu-page-link border-bottom-grey-light margin-left-24 padding-left-0 padding-top-18 padding-right-24 padding-bottom-18" id="lnk-int-header-anbieten-wohnung-vermieten-xs">
										<a href="//www.immonet.de/service/wohnungen-vermieten-wohnung-vermieten.html">Wohnung vermieten</a>
									</div>
								</li>
								<li>
									<div class="navmenu-page-link border-bottom-grey-light margin-left-24 padding-left-0 padding-top-18 padding-right-24 padding-bottom-18" id="lnk-int-header-anbieten-makler-suchen-xs">
										<a href="//www.immonet.de/immobilienmakler.html">Immobilienmakler suchen</a>
									</div>
								</li>
								<li>
									<div class="navmenu-page-link border-bottom-grey-light margin-left-24 padding-left-0 padding-top-18 padding-right-24 padding-bottom-18" id="lnk-int-header-anbieten-immobilie-vermarkten-xs">
										<a href="//www.immonet.de/maklerempfehlung.html">Maklerempfehlung</a>
									</div>
								</li>
								<li>
									<div class="navmenu-page-link border-bottom-grey-light margin-left-24 padding-left-0 padding-top-18 padding-right-24 padding-bottom-18" id="lnk-int-header-anbieten-immobilienbewertung-xs">
										<a href="//www.immonet.de/service/immobilienbewertung.html">Immobilienbewertung</a>
									</div>
								</li>
								<li>
									<div class="navmenu-page-link border-bottom-grey-light margin-left-24 padding-left-0 padding-top-18 padding-right-24 padding-bottom-18" id="lnk-int-header-anbieten-suchanzeigen-xs">
										<a href="//www.immonet.de/suchanzeigen.html">Suchanzeigen durchsuchen</a>
									</div>
								</li>
							</ul>
						</div>
					</li>
					<li id="entry-hausbau">
						<div class="bg-light-blue navmenu-entry border-bottom-grey-light margin-left-24 padding-left-0 padding-top-18 padding-right-24 padding-bottom-18" data-target="hausbau" id="lnk-int-header-hausbau-xs">
							
							
								<a class="text-gray-light text-uppercase">Hausbau</a>
								<span class="ico icoChevronRight50 pull-right text-white "></span>
							
						</div>
						<div class="content-wrapper pos-absolute pos-top img-full hidden list-unstyled pos-right-negative bg-white full-height" id="hausbau">
							<div class="bg-light-blue entry-back navmenu-back-link col-xs-12 padding-18 border-bottom-grey-light" id="lnk-int-header-hausbau-back-xs">
								<span class="ico icoChevronLeft50 text-white padding-right-15 "></span><a class="text-gray-light text-uppercase">ZURÜCK</a>
							</div>
							<ul class="col-xs-12 list-unstyled bg-white padding-bottom-48">
								<li>
									<div class="navmenu-page-link border-bottom-grey-light margin-left-24 padding-left-0 padding-top-18 padding-right-24 padding-bottom-18" id="lnk-int-header-hausbau-xs">
										<a href="//www.immonet.de/service/hausbau.html">Haus bauen</a>
									</div>
								</li>
								<li>
									<div class="navmenu-page-link border-bottom-grey-light margin-left-24 padding-left-0 padding-top-18 padding-right-24 padding-bottom-18" id="lnk-int-header-hausbau-suche-xs">
										<a href="//www.immonet.de/hausbau-kataloge.html">Hausbaukataloge</a>
									</div>
								</li>
								<li>
									<div class="navmenu-page-link border-bottom-grey-light margin-left-24 padding-left-0 padding-top-18 padding-right-24 padding-bottom-18" id="lnk-int-header-bauunternehmen-xs">
										<a href="//www.immonet.de/bauunternehmen.html">Bauunternehmen</a>
									</div>
								</li>
								<li>
									<div class="navmenu-page-link border-bottom-grey-light margin-left-24 padding-left-0 padding-top-18 padding-right-24 padding-bottom-18" id="lnk-int-header-musterhauspark-xs">
										<a href="//www.immonet.de/service/musterhauspark.html">Musterhausparks</a>
									</div>
								</li>
								<li>
									<div class="navmenu-page-link border-bottom-grey-light margin-left-24 padding-left-0 padding-top-18 padding-right-24 padding-bottom-18" id="lnk-int-header-sachverstaendige-xs">
										<a href="//www.immonet.de/service/sachverstaendige.html">Sachverständige</a>
									</div>
								</li>
								<li>
									<div class="navmenu-page-link border-bottom-grey-light margin-left-24 padding-left-0 padding-top-18 padding-right-24 padding-bottom-18" id="lnk-int-header-haustypen-xs">
										<a href="//www.immonet.de/service/haustypen.html">Haustypen</a>
									</div>
								</li>
							</ul>
						</div>
					</li>
					<li id="entry-finanzierung">
						<div class="bg-light-blue navmenu-entry border-bottom-grey-light margin-left-24 padding-left-0 padding-top-18 padding-right-24 padding-bottom-18" data-target="finanzierung" id="lnk-int-header-finanzierung-xs">
							
							
								<a class="text-gray-light text-uppercase">Finanzierung</a>
								<span class="ico icoChevronRight50 pull-right text-white "></span>
							
						</div>
						<div class="content-wrapper pos-absolute pos-top img-full hidden list-unstyled pos-right-negative bg-white full-height" id="finanzierung">
							<div class="bg-light-blue entry-back navmenu-back-link col-xs-12 padding-18 border-bottom-grey-light" id="lnk-int-header-finanzierung-back-xs">
								<span class="ico icoChevronLeft50 text-white padding-right-15 "></span><a class="text-gray-light text-uppercase">ZURÜCK</a>
							</div>
							<ul class="col-xs-12 list-unstyled bg-white padding-bottom-48">
								<li>
									<div class="navmenu-page-link border-bottom-grey-light margin-left-24 padding-left-0 padding-top-18 padding-right-24 padding-bottom-18" id="lnk-int-header-baufinanzierung-xs">
										<a href="//www.immonet.de/service/baufinanzierung.html">Baufinanzierung</a>
									</div>
								</li>
								<li>
									<div class="navmenu-page-link border-bottom-grey-light margin-left-24 padding-left-0 padding-top-18 padding-right-24 padding-bottom-18" id="lnk-int-header-finanzierungsangebote-xs">
										<a href="https://secure.immonet.de/immobilienfinanzierung?mid=195">Finanzierungsangebote</a>
									</div>
								</li>
								<li>
									<div class="navmenu-page-link border-bottom-grey-light margin-left-24 padding-left-0 padding-top-18 padding-right-24 padding-bottom-18" id="lnk-int-header-baufinanzierungsrechner-xs">
										<a href="//www.immonet.de/service/baufinanzierungsrechner.html">Baufinanzierungsrechner</a>
									</div>
								</li>
								<li>
									<div class="navmenu-page-link border-bottom-grey-light margin-left-24 padding-left-0 padding-top-18 padding-right-24 padding-bottom-18" id="lnk-int-header-finanzierungsrechner-xs">
										<a href="//www.immonet.de/service/finanzierungsrechner.html">Weitere Finanzierungsrechner</a>
									</div>
								</li>
								<li>
									<div class="navmenu-page-link border-bottom-grey-light margin-left-24 padding-left-0 padding-top-18 padding-right-24 padding-bottom-18" id="lnk-int-header-finanzierung-versicherungen-xs">
										<a href="//www.immonet.de/service/versicherungen.html">Versicherungen</a>
									</div>
								</li>
							</ul>
						</div>
					</li>
					<li id="entry-umzug">
						<div class="bg-light-blue navmenu-entry border-bottom-grey-light margin-left-24 padding-left-0 padding-top-18 padding-right-24 padding-bottom-18" data-target="umzug" id="lnk-int-header-umzug-xs">
							
							
								<a class="text-gray-light text-uppercase">Umzug</a>
								<span class="ico icoChevronRight50 pull-right text-white "></span>
							
						</div>
						<div class="content-wrapper pos-absolute pos-top img-full hidden list-unstyled pos-right-negative bg-white full-height" id="umzug">
							<div class="bg-light-blue entry-back navmenu-back-link col-xs-12 padding-18 border-bottom-grey-light" id="lnk-int-header-umzug-back-xs">
								<span class="ico icoChevronLeft50 text-white padding-right-15 "></span><a class="text-gray-light text-uppercase">ZURÜCK</a>
							</div>
							<ul class="col-xs-12 list-unstyled bg-white padding-bottom-48">
								<li>
									<div class="navmenu-page-link border-bottom-grey-light margin-left-24 padding-left-0 padding-top-18 padding-right-24 padding-bottom-18" id="lnk-int-header-umzug-suchen-xs">
										<a href="//www.immonet.de/umzug.html">Umzugsunternehmen suchen</a>
									</div>
								</li>
								<li>
									<div class="navmenu-page-link border-bottom-grey-light margin-left-24 padding-left-0 padding-top-18 padding-right-24 padding-bottom-18" id="lnk-int-header-umzug-unternehmen-xs">
										<a href="//www.immonet.de/umzug/umzugsunternehmen.html">Umzugsunternehmen</a>
									</div>
								</li>
								<li>
									<div class="navmenu-page-link border-bottom-grey-light margin-left-24 padding-left-0 padding-top-18 padding-right-24 padding-bottom-18" id="lnk-int-header-umzug-kosten-xs">
										<a href="//www.immonet.de/umzug/umzugskosten.html">Umzugskosten</a>
									</div>
								</li>
								<li>
									<div class="navmenu-page-link border-bottom-grey-light margin-left-24 padding-left-0 padding-top-18 padding-right-24 padding-bottom-18" id="lnk-int-header-umzug-checkliste-xs">
										<a href="//www.immonet.de/umzug/umzugscheckliste.html">Umzugscheckliste</a>
									</div>
								</li>
								<li>
									<div class="navmenu-page-link border-bottom-grey-light margin-left-24 padding-left-0 padding-top-18 padding-right-24 padding-bottom-18" id="lnk-int-header-umzug-organisation-xs">
										<a href="//www.immonet.de/umzug/umzugsorganisation.html">Umzugsorganisation</a>
									</div>
								</li>
								<li>
									<div class="navmenu-page-link border-bottom-grey-light margin-left-24 padding-left-0 padding-top-18 padding-right-24 padding-bottom-18" id="lnk-int-header-umzug-selfstorage-xs">
										<a href="//www.immonet.de/umzug/selfstorage.html">Selfstorage / Möbel einlagern</a>
									</div>
								</li>
								<li>
									<div class="navmenu-page-link border-bottom-grey-light margin-left-24 padding-left-0 padding-top-18 padding-right-24 padding-bottom-18" id="lnk-int-header-umzug-tipps-xs">
										<a href="//www.immonet.de/umzug/umzugstipps.html">Umzugstipps</a>
									</div>
								</li>
								<li>
									<div class="navmenu-page-link border-bottom-grey-light margin-left-24 padding-left-0 padding-top-18 padding-right-24 padding-bottom-18" id="lnk-int-header-umzug-schufa-xs">
										<a href="//www.immonet.de/service/schufa-bonitaetsauskunft.html">SCHUFA-Auskunft</a>
									</div>
								</li>
							</ul>
						</div>
					</li>
					<li id="entry-meinimmonet">
						<div class="bg-light-blue navmenu-entry border-bottom-grey-light margin-left-24 padding-left-0 padding-top-18 padding-right-24 padding-bottom-18" data-target="meinimmonet" id="lnk-int-header-meinimmonet-xs">
							
								<span class="ico ico-size-24 icoUserCir text-white vertical-align-middle "></span>
								<a class="text-gray-light text-uppercase padding-left-3">Mein Immonet</a>
								<span class="ico icoChevronRight50 pull-right text-white padding-top-3 hidden-sm"></span>
							
							
						</div>
						<div class="content-wrapper pos-absolute pos-top img-full hidden list-unstyled pos-right-negative bg-white full-height" id="meinimmonet">
							<div class="bg-light-blue entry-back navmenu-back-link col-xs-12 padding-18 border-bottom-grey-light" id="lnk-int-header-meinimmonet-back-xs">
								<span class="ico icoChevronLeft50 text-white padding-right-15 "></span><a class="text-gray-light text-uppercase">ZURÜCK</a>
							</div>
							<ul class="col-xs-12 list-unstyled bg-white padding-bottom-48">
								<li>
									<div class="navmenu-page-link border-bottom-grey-light margin-left-24 padding-left-0 padding-top-18 padding-right-24 padding-bottom-18" id="lnk-int-header-meinimmonet-anmelden-xs">
										<a href="//www.immonet.de/immonet/logon.do">Anmelden</a>
									</div>
								</li>
								<li>
									<div class="navmenu-page-link border-bottom-grey-light margin-left-24 padding-left-0 padding-top-18 padding-right-24 padding-bottom-18" id="lnk-int-header-meinimmonet-merkzettel-xs">
										<a href="//www.immonet.de/memo/memos.jsf">Merkzettel</a>
									</div>
								</li>
								<li>
									<div class="navmenu-page-link border-bottom-grey-light margin-left-24 padding-left-0 padding-top-18 padding-right-24 padding-bottom-18" id="lnk-int-header-meinimmonet-suchauftrag-xs">
										<a href="//www.immonet.de/suchagent.do">Suchauftrag</a>
									</div>
								</li>
							</ul>
						</div>
					</li>
					<li>
						<div class="margin-left-24 padding-left-0 padding-top-18">
							<img class="immowelt-headerlogo" title="Ein Portal von immowelt.de" alt="Ein Portal von immowelt.de" src="//cdn.immonet.de/frontend-resources/4.7.1/images/logo_portal_immowelt_white.svg" />
						</div>
					</li>
				</ul>
			</div>
		</div>
	</div>
<div id="navmenu-backdrop" class="menu-backdrop hidden pos-absolute img-full full-height z-index-modal-background"></div>
</div>
	<div id="cookiebar-container"></div>
	<div class="header-desktop bg-white hidden-xs hidden-print container-fluid max-page-width pos-relative">
		<div>
			<div class="row hidden-xs pos-relative">
				<div class="col-sm-8 padding-left-24">
					<a id="tsr-int-logo" href="//www.immonet.de">
						<img class="margin-bottom-sm-12 margin-top-sm-12 navbar-brand-logo" title="Immobilien, Wohnungen und Häuser" alt="Immobilien, Wohnungen und Häuser" src="//cdn.immonet.de/frontend-resources/4.7.1/immonet/img/logo_with_claim.svg" />
					</a>
				</div>
				<div class="col-sm-4 padding-right-12 padding-top-12">
					<img class="pull-right immowelt-headerlogo" title="Ein Portal von immowelt.de" alt="Ein Portal von immowelt.de" src="//cdn.immonet.de/frontend-resources/4.7.1/immonet/img/headerfooter/logo_portal_immowelt_black.svg" />
				</div>
			</div>
		</div>

		<nav class="navbar navbar-default">
			<div>
				<div class="navbar-collapse collapse" id="mainnaviPanel" role="navigation" aria-expanded="false">
					<ul class="nav navbar-nav full-width margin-right-sm-12 margin-right-md-24 margin-right-lg-12 hidden-xs pos-relative ">
						
						
						
						
						
						
						<li class="cursor-hand dropdown-toggle text-size-sm-15 padding-left-12 padding-right-12 padding-left-md-24 padding-right-md-24 padding-top-12 padding-bottom-12  pull-right" data-toggle="meinimmonet-dropdown">
							<span class="ico ico-size-24 icoUserCir text-white vertical-align-middle padding-right-3 "></span>
							<span class="hidden-sm text-opensans text-white text-strong" id="lnk-int-header-meinimmonet">Mein Immonet</span>
						</li>
						<li class="cursor-hand dropdown-toggle text-size-sm-15 padding-left-24 padding-right-24 padding-top-15 padding-bottom-15" data-toggle="wohnen-dropdown">
							<span class="text-opensans text-white text-strong" id="lnk-int-header-wohnen">Wohnen</span>
						</li>
						<li class="cursor-hand dropdown-toggle text-size-sm-15 padding-left-24 padding-right-24 padding-top-15 padding-bottom-15" data-toggle="gewerbe-dropdown">
							<span class="text-opensans text-white text-strong" id="lnk-int-header-gewerbe">Gewerbe</span>
						</li>
						<li class="cursor-hand dropdown-toggle text-size-sm-15 padding-left-24 padding-right-24 padding-top-15 padding-bottom-15" data-toggle="anbieten-dropdown">
							<span class="text-opensans text-white text-strong" id="lnk-int-header-anbieten">Anbieten</span>
						</li>
						<li class="cursor-hand dropdown-toggle text-size-sm-15 padding-left-24 padding-right-24 padding-top-15 padding-bottom-15" data-toggle="hausbau-dropdown">
							<span class="text-opensans text-white text-strong" id="lnk-int-header-hausbau">Hausbau</span>
						</li>
						<li class="cursor-hand dropdown-toggle text-size-sm-15 padding-left-24 padding-right-24 padding-top-15 padding-bottom-15" data-toggle="finanzierung-dropdown">
							<span class="text-opensans text-white text-strong" id="lnk-int-header-finanzierung">Finanzierung</span>
						</li>
						<li class="cursor-hand dropdown-toggle text-size-sm-15 padding-left-24 padding-right-24 padding-top-15 padding-bottom-15" data-toggle="umzug-dropdown">
							<span class="text-opensans text-white text-strong" id="lnk-int-header-umzug">Umzug</span>
						</li>
						
					</ul>
				</div>
			</div>

			<div class=" sub-menu-wrapper bg-white full-width pos-absolute z-index-navbar padding-left-12 padding-right-12 overflow-hidden">
				<div>
					<ul class="sub-menu-content hidden margin-top-12 pos-relative overflow-hidden display-flex flex-direction-column align-items-flex-start flex-wrap align-content-flex-start" id="wohnen-dropdown">
						<li class="col-xs-4 col-lg-3 padding-left-12 padding-right-12">
							<a class="text-gray-darker text-gray-dark-hover" href="//www.immonet.de/" title="Wohnimmobilien suchen">
								<div class="padding-top-12 padding-right-12 padding-bottom-12 border-bottom-grey-light" id="lnk-int-header-wohnen-suche">Wohnimmobilien suchen</div>
							</a>
						</li>
						<li class="col-xs-4 col-lg-3 padding-left-12 padding-right-12">
							<a class="text-gray-darker text-gray-dark-hover" href="//www.immonet.de/service/seniorenwohnen.html" title="Seniorenwohnen">
								<div class="padding-top-12 padding-right-12 padding-bottom-12 border-bottom-grey-light" id="lnk-int-header-wohnen-seniorenwohnen">Seniorenwohnen</div>
							</a>
						</li>
						<li class="col-xs-4 col-lg-3 padding-left-12 padding-right-12">
							<a class="text-gray-darker text-gray-dark-hover" href="//www.immonet.de/neubauimmobilien.html" title="Neubauimmobilien">
								<div class="padding-top-12 padding-right-12 padding-bottom-12 border-bottom-grey-light" id="lnk-int-header-wohnen-neubau">Neubauimmobilien</div>
							</a>
						</li>
						<li class="col-xs-4 col-lg-3 padding-left-12 padding-right-12">
							<a class="text-gray-darker text-gray-dark-hover" href="//www.immonet.de/service/hausbau.html" title="Hausbau">
								<div class="padding-top-12 padding-right-12 padding-bottom-12 border-bottom-grey-light" id="lnk-int-header-wohnen-hausbau">Hausbau</div>
							</a>
						</li>
						<li class="col-xs-4 col-lg-3 padding-left-12 padding-right-12">
							<a class="text-gray-darker text-gray-dark-hover" href="//www.immonet.de/service/finanzierung.html" title="Finanzierung">
								<div class="padding-top-12 padding-right-12 padding-bottom-12 border-bottom-grey-light" id="lnk-int-header-wohnen-finanzierung">Finanzierung</div>
							</a>
						</li>
						<li class="col-xs-4 col-lg-3 padding-left-12 padding-right-12">
							<a class="text-gray-darker text-gray-dark-hover" href="//www.immonet.de/service/ausbau-und-umbau.html" title="Renovieren">
								<div class="padding-top-12 padding-right-12 padding-bottom-12 border-bottom-grey-light" id="lnk-int-header-wohnen-renovieren">Renovieren</div>
							</a>
						</li>
						<li class="col-xs-4 col-lg-3 padding-left-12 padding-right-12">
							<a class="text-gray-darker text-gray-dark-hover" href="//www.immonet.de/kapitalanlage.html" title="Kapitalanlage">
								<div class="padding-top-12 padding-right-12 padding-bottom-12 border-bottom-grey-light" id="lnk-int-header-wohnen-kapitalanlage-immobilien">Kapitalanlage</div>
							</a>
						</li>
						<li class="col-xs-4 col-lg-3 padding-left-12 padding-right-12">
							<a class="text-gray-darker text-gray-dark-hover" href="//www.immonet.de/zwangsversteigerung.html" title="Zwangsversteigerungen">
								<div class="padding-top-12 padding-right-12 padding-bottom-12 border-bottom-grey-light" id="lnk-int-header-wohnen-zwangsversteigerung">Zwangsversteigerungen</div>
							</a>
						</li>
						<li class="col-xs-4 col-lg-3 padding-left-12 padding-right-12">
							<a class="text-gray-darker text-gray-dark-hover" href="//www.immonet.de/service/suchanzeige.html" title="Suchanzeige aufgeben">
								<div class="padding-top-12 padding-right-12 padding-bottom-12 border-bottom-grey-light" id="lnk-int-header-wohnen-suchanzeigen">Suchanzeige aufgeben</div>
							</a>
						</li>
						<li class="col-xs-4 col-lg-3 padding-left-12 padding-right-12">
							<a class="text-gray-darker text-gray-dark-hover" href="//www.immonet.de/service/einrichten-ratgeber.html" title="Einrichten">
								<div class="padding-top-12 padding-right-12 padding-bottom-12 border-bottom-grey-light" id="lnk-int-header-wohnen-einrichten">Einrichten</div>
							</a>
						</li>
						<li class="col-xs-4 col-lg-3 padding-left-12 padding-right-12">
							<a class="text-gray-darker text-gray-dark-hover" href="//www.immonet.de/service/schufa-bonitaetsauskunft.html" title="SCHUFA-Auskunft">
								<div class="padding-top-12 padding-right-12 padding-bottom-12 border-bottom-grey-light" id="lnk-int-header-wohnen-schufa">SCHUFA-Auskunft</div>
							</a>
						</li>
						<li class="col-xs-4 col-lg-3 padding-left-12 padding-right-12">
							<a class="text-gray-darker text-gray-dark-hover" href="//www.immonet.de/service/versicherungen.html" title="Versicherungen">
								<div class="padding-top-12 padding-right-12 padding-bottom-12 border-bottom-grey-light" id="lnk-int-header-wohnen-versicherungen">Versicherungen</div>
							</a>
						</li>
					</ul>
					<ul class="sub-menu-content hidden margin-top-12 pos-relative overflow-hidden display-flex flex-direction-column align-items-flex-start flex-wrap align-content-flex-start" id="gewerbe-dropdown">
						<li class="col-xs-4 col-lg-3 padding-left-12 padding-right-12">
							<a class="text-gray-darker text-gray-dark-hover" href="//www.immonet.de/gewerbe.html" title="Gewerbeimmobilien suchen">
								<div class="padding-top-12 padding-right-12 padding-bottom-12 border-bottom-grey-light" id="lnk-int-header-gewerbe-suche">Gewerbeimmobilien suchen</div>
							</a>
						</li>
						<li class="col-xs-4 col-lg-3 padding-left-12 padding-right-12">
							<a class="text-gray-darker text-gray-dark-hover" href="//www.immonet.de/service/gewerbe-services.html" title="Gewerbe Ratgeber">
								<div class="padding-top-12 padding-right-12 padding-bottom-12 border-bottom-grey-light" id="lnk-int-header-gewerbe-ratgeber">Gewerbe Ratgeber</div>
							</a>
						</li>
						<li class="col-xs-4 col-lg-3 padding-left-12 padding-right-12">
							<a class="text-gray-darker text-gray-dark-hover" href="//www.immonet.de/auslandsimmobilien-gewerbe.html" title="Gewerbeimmobilien im Ausland">
								<div class="padding-top-12 padding-right-12 padding-bottom-12 border-bottom-grey-light" id="lnk-int-header-gewerbe-ausland">Gewerbeimmobilien im Ausland</div>
							</a>
						</li>
						<li class="col-xs-4 col-lg-3 padding-left-12 padding-right-12">
							<a class="text-gray-darker text-gray-dark-hover" href="//www.immonet.de/buero-praxis.html" title="Büro und Praxis">
								<div class="padding-top-12 padding-right-12 padding-bottom-12 border-bottom-grey-light" id="lnk-int-header-gewerbe-buero">Büro und Praxis</div>
							</a>
						</li>
						<li class="col-xs-4 col-lg-3 padding-left-12 padding-right-12">
							<a class="text-gray-darker text-gray-dark-hover" href="//www.immonet.de/laden-einzelhandel.html" title="Einzelhandel">
								<div class="padding-top-12 padding-right-12 padding-bottom-12 border-bottom-grey-light" id="lnk-int-header-gewerbe-einzelhandel">Einzelhandel</div>
							</a>
						</li>
						<li class="col-xs-4 col-lg-3 padding-left-12 padding-right-12">
							<a class="text-gray-darker text-gray-dark-hover" href="//www.immonet.de/halle-lager-produktion.html" title="Lagerhalle">
								<div class="padding-top-12 padding-right-12 padding-bottom-12 border-bottom-grey-light" id="lnk-int-header-gewerbe-lagerhalle">Lagerhalle</div>
							</a>
						</li>
						<li class="col-xs-4 col-lg-3 padding-left-12 padding-right-12">
							<a class="text-gray-darker text-gray-dark-hover" href="//www.immonet.de/gastgewerbe-hotel.html" title="Gastgewerbe und Hotel">
								<div class="padding-top-12 padding-right-12 padding-bottom-12 border-bottom-grey-light" id="lnk-int-header-gewerbe-gastgewerbe">Gastgewerbe und Hotel</div>
							</a>
						</li>
						<li class="col-xs-4 col-lg-3 padding-left-12 padding-right-12">
							<a class="text-gray-darker text-gray-dark-hover" href="//www.immonet.de/kapitalanlage.html" title="Kapitalanlage">
								<div class="padding-top-12 padding-right-12 padding-bottom-12 border-bottom-grey-light" id="lnk-int-header-gewerbe-kapitalanlage-immobilien">Kapitalanlage</div>
							</a>
						</li>
					</ul>
					<ul class="sub-menu-content hidden margin-top-12 pos-relative overflow-hidden display-flex flex-direction-column align-items-flex-start flex-wrap align-content-flex-start" id="anbieten-dropdown">
						<li class="col-xs-4 col-lg-3 padding-left-12 padding-right-12">
							<a class="text-gray-darker text-gray-dark-hover" href="//www.immonet.de/anbieten.html" title="Anzeige aufgeben">
								<div class="padding-top-12 padding-right-12 padding-bottom-12 border-bottom-grey-light" id="lnk-int-header-anbieten-uebersicht">Anzeige aufgeben</div>
							</a>
						</li>
						<li class="col-xs-4 col-lg-3 padding-left-12 padding-right-12">
							<a class="text-gray-darker text-gray-dark-hover" href="//www.immonet.de/service/einzelne-immobilien-anbieten.html" title="Immobilien privat anbieten">
								<div class="padding-top-12 padding-right-12 padding-bottom-12 border-bottom-grey-light" id="lnk-int-header-anbieten-privat">Immobilien privat anbieten</div>
							</a>
						</li>
						<li class="col-xs-4 col-lg-3 padding-left-12 padding-right-12">
							<a class="text-gray-darker text-gray-dark-hover" href="//www.immonet.de/service/duo-partnerschaft.html" title="Immobilien gewerblich anbieten">
								<div class="padding-top-12 padding-right-12 padding-bottom-12 border-bottom-grey-light" id="lnk-int-header-anbieten-gewerblich">Immobilien gewerblich anbieten</div>
							</a>
						</li>
						<li class="col-xs-4 col-lg-3 padding-left-12 padding-right-12">
							<a class="text-gray-darker text-gray-dark-hover" href="//www.immonet.de/service/checkliste-immobilienverkauf.html" title="Immobilienverkauf">
								<div class="padding-top-12 padding-right-12 padding-bottom-12 border-bottom-grey-light" id="lnk-int-header-anbieten-immobilienverkauf">Immobilienverkauf</div>
							</a>
						</li>
						<li class="col-xs-4 col-lg-3 padding-left-12 padding-right-12">
							<a class="text-gray-darker text-gray-dark-hover" href="//www.immonet.de/service/haus-verkaufen-hausverkauf.html" title="Haus verkaufen">
								<div class="padding-top-12 padding-right-12 padding-bottom-12 border-bottom-grey-light" id="lnk-int-header-anbieten-haus-verkaufen">Haus verkaufen</div>
							</a>
						</li>
						<li class="col-xs-4 col-lg-3 padding-left-12 padding-right-12">
							<a class="text-gray-darker text-gray-dark-hover" href="//www.immonet.de/service/wohnungen-vermieten-wohnung-vermieten.html" title="Wohnung vermieten">
								<div class="padding-top-12 padding-right-12 padding-bottom-12 border-bottom-grey-light" id="lnk-int-header-anbieten-wohnung-vermieten">Wohnung vermieten</div>
							</a>
						</li>
						<li class="col-xs-4 col-lg-3 padding-left-12 padding-right-12">
							<a class="text-gray-darker text-gray-dark-hover" href="//www.immonet.de/immobilienmakler.html" title="Immobilienmakler suchen">
								<div class="padding-top-12 padding-right-12 padding-bottom-12 border-bottom-grey-light" id="lnk-int-header-anbieten-makler-suchen">Immobilienmakler suchen</div>
							</a>
						</li>
						<li class="col-xs-4 col-lg-3 padding-left-12 padding-right-12">
							<a class="text-gray-darker text-gray-dark-hover" href="//www.immonet.de/maklerempfehlung.html" title="Maklerempfehlung">
								<div class="padding-top-12 padding-right-12 padding-bottom-12 border-bottom-grey-light" id="lnk-int-header-anbieten-immobilie-vermarkten">Maklerempfehlung</div>
							</a>
						</li>
						<li class="col-xs-4 col-lg-3 padding-left-12 padding-right-12">
							<a class="text-gray-darker text-gray-dark-hover" href="//www.immonet.de/service/immobilienbewertung.html" title="Immobilienbewertung">
								<div class="padding-top-12 padding-right-12 padding-bottom-12 border-bottom-grey-light" id="lnk-int-header-anbieten-immobilienbewertung">Immobilienbewertung</div>
							</a>
						</li>
						<li class="col-xs-4 col-lg-3 padding-left-12 padding-right-12">
							<a class="text-gray-darker text-gray-dark-hover" href="//www.immonet.de/suchanzeigen.html" title="Suchanzeigen durchsuchen">
								<div class="padding-top-12 padding-right-12 padding-bottom-12 border-bottom-grey-light" id="lnk-int-header-anbieten-suchanzeigen">Suchanzeigen durchsuchen</div>
							</a>
						</li>
					</ul>
					<ul class="sub-menu-content hidden margin-top-12 pos-relative overflow-hidden display-flex flex-direction-column align-items-flex-start flex-wrap align-content-flex-start" id="hausbau-dropdown">
						<li class="col-xs-4 col-lg-3 padding-left-12 padding-right-12">
							<a class="text-gray-darker text-gray-dark-hover" href="//www.immonet.de/service/hausbau.html" title="Haus bauen">
								<div class="padding-top-12 padding-right-12 padding-bottom-12 border-bottom-grey-light" id="lnk-int-header-hausbau">Haus bauen</div>
							</a>
						</li>
						<li class="col-xs-4 col-lg-3 padding-left-12 padding-right-12">
							<a class="text-gray-darker text-gray-dark-hover" href="//www.immonet.de/hausbau-kataloge.html" title="Hausbaukataloge">
								<div class="padding-top-12 padding-right-12 padding-bottom-12 border-bottom-grey-light" id="lnk-int-header-hausbau-suche">Hausbaukataloge</div>
							</a>
						</li>
						<li class="col-xs-4 col-lg-3 padding-left-12 padding-right-12">
							<a class="text-gray-darker text-gray-dark-hover" href="//www.immonet.de/bauunternehmen.html" title="Bauunternehmen">
								<div class="padding-top-12 padding-right-12 padding-bottom-12 border-bottom-grey-light" id="lnk-int-header-bauunternehmen">Bauunternehmen</div>
							</a>
						</li>
						<li class="col-xs-4 col-lg-3 padding-left-12 padding-right-12">
							<a class="text-gray-darker text-gray-dark-hover" href="//www.immonet.de/service/musterhauspark.html" title="Musterhausparks">
								<div class="padding-top-12 padding-right-12 padding-bottom-12 border-bottom-grey-light" id="lnk-int-header-musterhauspark">Musterhausparks</div>
							</a>
						</li>
						<li class="col-xs-4 col-lg-3 padding-left-12 padding-right-12">
							<a class="text-gray-darker text-gray-dark-hover" href="//www.immonet.de/service/sachverstaendige.html" title="Sachverständige">
								<div class="padding-top-12 padding-right-12 padding-bottom-12 border-bottom-grey-light" id="lnk-int-header-sachverstaendige">Sachverständige</div>
							</a>
						</li>
						<li class="col-xs-4 col-lg-3 padding-left-12 padding-right-12">
							<a class="text-gray-darker text-gray-dark-hover" href="//www.immonet.de/service/haustypen.html" title="Haustypen">
								<div class="padding-top-12 padding-right-12 padding-bottom-12 border-bottom-grey-light" id="lnk-int-header-haustypen">Haustypen</div>
							</a>
						</li>
					</ul>
					<ul class="sub-menu-content hidden margin-top-12 pos-relative overflow-hidden display-flex flex-direction-column align-items-flex-start flex-wrap align-content-flex-start" id="finanzierung-dropdown">
						<li class="col-xs-4 col-lg-3 padding-left-12 padding-right-12">
							<a class="text-gray-darker text-gray-dark-hover" href="//www.immonet.de/service/baufinanzierung.html" title="Baufinanzierung">
								<div class="padding-top-12 padding-right-12 padding-bottom-12 border-bottom-grey-light" id="lnk-int-header-baufinanzierung">Baufinanzierung</div>
							</a>
						</li>
						<li class="col-xs-4 col-lg-3 padding-left-12 padding-right-12">
							<a class="text-gray-darker text-gray-dark-hover" href="https://secure.immonet.de/immobilienfinanzierung?mid=195" title="Finanzierungsangebote">
								<div class="padding-top-12 padding-right-12 padding-bottom-12 border-bottom-grey-light" id="lnk-int-header-finanzierungsangebote">Finanzierungsangebote</div>
							</a>
						</li>
						<li class="col-xs-4 col-lg-3 padding-left-12 padding-right-12">
							<a class="text-gray-darker text-gray-dark-hover" href="//www.immonet.de/service/baufinanzierungsrechner.html" title="Baufinanzierungsrechner">
								<div class="padding-top-12 padding-right-12 padding-bottom-12 border-bottom-grey-light" id="lnk-int-header-baufinanzierungsrechner">Baufinanzierungsrechner</div>
							</a>
						</li>
						<li class="col-xs-4 col-lg-3 padding-left-12 padding-right-12">
							<a class="text-gray-darker text-gray-dark-hover" href="//www.immonet.de/service/finanzierungsrechner.html" title="Weitere Finanzierungsrechner">
								<div class="padding-top-12 padding-right-12 padding-bottom-12 border-bottom-grey-light" id="lnk-int-header-finanzierungsrechner">Weitere Finanzierungsrechner</div>
							</a>
						</li>
						<li class="col-xs-4 col-lg-3 padding-left-12 padding-right-12">
							<a class="text-gray-darker text-gray-dark-hover" href="//www.immonet.de/service/versicherungen.html" title="Versicherungen">
								<div class="padding-top-12 padding-right-12 padding-bottom-12 border-bottom-grey-light" id="lnk-int-header-finanzierung-versicherungen">Versicherungen</div>
							</a>
						</li>
					</ul>
					<ul class="sub-menu-content hidden margin-top-12 pos-relative overflow-hidden display-flex flex-direction-column align-items-flex-start flex-wrap align-content-flex-start" id="umzug-dropdown">
						<li class="col-xs-4 col-lg-3 padding-left-12 padding-right-12">
							<a class="text-gray-darker text-gray-dark-hover" href="//www.immonet.de/umzug.html" title="Umzugsunternehmen suchen">
								<div class="padding-top-12 padding-right-12 padding-bottom-12 border-bottom-grey-light" id="lnk-int-header-umzug-suchen">Umzugsunternehmen suchen</div>
							</a>
						</li>
						<li class="col-xs-4 col-lg-3 padding-left-12 padding-right-12">
							<a class="text-gray-darker text-gray-dark-hover" href="//www.immonet.de/umzug/umzugsunternehmen.html" title="Umzugsunternehmen">
								<div class="padding-top-12 padding-right-12 padding-bottom-12 border-bottom-grey-light" id="lnk-int-header-umzug-unternehmen">Umzugsunternehmen</div>
							</a>
						</li>
						<li class="col-xs-4 col-lg-3 padding-left-12 padding-right-12">
							<a class="text-gray-darker text-gray-dark-hover" href="//www.immonet.de/umzug/umzugskosten.html" title="Umzugskosten">
								<div class="padding-top-12 padding-right-12 padding-bottom-12 border-bottom-grey-light" id="lnk-int-header-umzug-kosten">Umzugskosten</div>
							</a>
						</li>
						<li class="col-xs-4 col-lg-3 padding-left-12 padding-right-12">
							<a class="text-gray-darker text-gray-dark-hover" href="//www.immonet.de/umzug/umzugscheckliste.html" title="Umzugscheckliste">
								<div class="padding-top-12 padding-right-12 padding-bottom-12 border-bottom-grey-light" id="lnk-int-header-umzug-checkliste">Umzugscheckliste</div>
							</a>
						</li>
						<li class="col-xs-4 col-lg-3 padding-left-12 padding-right-12">
							<a class="text-gray-darker text-gray-dark-hover" href="//www.immonet.de/umzug/umzugsorganisation.html" title="Umzugsorganisation">
								<div class="padding-top-12 padding-right-12 padding-bottom-12 border-bottom-grey-light" id="lnk-int-header-umzug-organisation">Umzugsorganisation</div>
							</a>
						</li>
						<li class="col-xs-4 col-lg-3 padding-left-12 padding-right-12">
							<a class="text-gray-darker text-gray-dark-hover" href="//www.immonet.de/umzug/selfstorage.html" title="Selfstorage / Möbel einlagern">
								<div class="padding-top-12 padding-right-12 padding-bottom-12 border-bottom-grey-light" id="lnk-int-header-umzug-selfstorage">Selfstorage / Möbel einlagern</div>
							</a>
						</li>
						<li class="col-xs-4 col-lg-3 padding-left-12 padding-right-12">
							<a class="text-gray-darker text-gray-dark-hover" href="//www.immonet.de/umzug/umzugstipps.html" title="Umzugstipps">
								<div class="padding-top-12 padding-right-12 padding-bottom-12 border-bottom-grey-light" id="lnk-int-header-umzug-tipps">Umzugstipps</div>
							</a>
						</li>
						<li class="col-xs-4 col-lg-3 padding-left-12 padding-right-12">
							<a class="text-gray-darker text-gray-dark-hover" href="//www.immonet.de/service/schufa-bonitaetsauskunft.html" title="SCHUFA-Auskunft">
								<div class="padding-top-12 padding-right-12 padding-bottom-12 border-bottom-grey-light" id="lnk-int-header-umzug-schufa">SCHUFA-Auskunft</div>
							</a>
						</li>
					</ul>
					<ul class="sub-menu-content hidden margin-top-12 pos-relative overflow-hidden display-flex flex-direction-column align-items-flex-start flex-wrap align-content-flex-start" id="meinimmonet-dropdown">
						<li class="col-xs-4 col-lg-3 padding-left-12 padding-right-12  pos-absolute pos-top pos-right">
							<a class="text-gray-darker text-gray-dark-hover" href="//www.immonet.de/immonet/logon.do" title="Anmelden">
								<div class="padding-top-12 padding-right-12 padding-bottom-12 border-bottom-grey-light" id="lnk-int-header-meinimmonet-anmelden">Anmelden</div>
							</a>
						</li>
						<li class="col-xs-4 col-lg-3 padding-left-12 padding-right-12">
							<a class="text-gray-darker text-gray-dark-hover" href="//www.immonet.de/memo/memos.jsf" title="Merkzettel">
								<div class="padding-top-12 padding-right-12 padding-bottom-12 border-bottom-grey-light" id="lnk-int-header-meinimmonet-merkzettel">Merkzettel</div>
							</a>
						</li>
						<li class="col-xs-4 col-lg-3 padding-left-12 padding-right-12">
							<a class="text-gray-darker text-gray-dark-hover" href="//www.immonet.de/suchagent.do" title="Suchauftrag">
								<div class="padding-top-12 padding-right-12 padding-bottom-12 border-bottom-grey-light" id="lnk-int-header-meinimmonet-suchauftrag">Suchauftrag</div>
							</a>
						</li>
					</ul>
				</div>
			</div>
		</nav>

		
	</div>
</header>



<div class="hidden-print">
					<div class="breadcrumb hidden-xs bg-default">
		<div class="display-flex align-items-flex-center">
			<span>
				<a href="/" class=""><span id="" class="ico icoHouseO text-gray-dark ico-size-24 ico-greydark-hover ico-grey-dark"></span></a>
			</span>
			
			
			<span id="" class="padding-left-4 ico icoChevronRight50 text-gray-dark ico-size-15 ico-grey-dark"></span>
			
			<span class="padding-left-4">
				<!-- key otype1mtype2pcat1ocat-1 -->
				<a href="/wohnung-mieten.html" class="text-size-15  text-gray-darker-hover">Wohnung mieten</a>
				</span>
			<span id="" class="ico icoChevronRight50 text-gray-dark ico-size-15 ico-grey-dark"></span>
			<span class="padding-left-4">
				<!-- key  -->
				<a href="/nordrhein-westfalen/wohnung-mieten.html" class="text-size-15  text-gray-darker-hover">Nordrhein-Westfalen</a>
				</span>
			<span id="" class="ico icoChevronRight50 text-gray-dark ico-size-15 ico-grey-dark"></span>
			<span class="padding-left-4">
				<!-- key  -->
				<a href="/nordrhein-westfalen/muenster-wohnung-mieten.html" class="text-size-15  text-gray-darker-hover">Münster</a>
				</span>
			<span id="" class="ico icoChevronRight50 text-gray-dark ico-size-15 ico-grey-dark"></span>
			<span class="padding-left-4">
				<span class="text-size-15 text-gray-dark small">Exposé</span>
			</span>
		</div>
	</div>

	
	
</div>
				<img class="visible-print navbar-brand-logo" title="Immobilien, Wohnungen und Häuser" alt="Immobilien, Wohnungen und Häuser" src="https://www.immonet.de/immonet/img/logo/logo_with_claim.svg">
				<div class="visible-print immo-numbers">
					<p class="text-100">Immonet-Nr.: 34443703</p>
					<p class="text-100">Anbieter-Objekt-ID: 84-1138510007-655587</p>
				</div>

				<div class="bg-default">
					<main class="container-fluid max-page-width pos-relative">
					


<div class="row bg-white bg-default-sm padding-left-24 padding-right-24 display-flex hidden-print">
	<div class="col-xs-4 hidden-xs margin-top-12">
	
	</div>

	<div class="col-xs-4 padding-bottom-12 padding-bottom-sm-0 text-center align-self-center hidden-xs">
		
		
			<div class="row bg-white bg-default-sm">
				<div class="col-xs-12 text-center">
					<div class="box-25 padding-bottom-0">
						<p class="text-uppercase text-10">Unser Regiopartner</p>
					</div>
					<div class="text-center" mandanten-id="Welt ">
						<a id="lnk-ext-regiopartner-we" href="http://www.welt.de/" rel="nofollow" target="_blank">
							<img src="https://www.immonet.de/immonet/img/mandanten/we.png"
								alt="Unser Regiopartner"
								title="Unser Regiopartner"
								aria-label="Unser Regiopartner">
						</a>
					</div>
				</div>
			</div>
		
	</div>







	<div class="col-xs-4 hidden-xs margin-top-12">
	
	</div>
</div>
<div class="row margin-top-12">
















<div id="objectHeader" role="toolbar">
	<div id="keyfacts-bar" class="flex-direction-row z-index-navbar-fixed display-flex justify-content-center justify-content-xs-flex-start bg-overlay-dark">
		
			
			
				<figure style="display: none;" class="thumbnail align-self-center padding-left-24 hidden-xs hidden-print">
					<img src="https://i.immonet.de/63/36/33/509633633_72x54.jpg" height="42" alt="Wir sind Immobilien">
				</figure>
			
			
		

		
		<div class="text-center align-self-center align-self-sm-stretch flex-order-1 flex-order-sm-9 padding-top-9 padding-bottom-9 padding-right-12 padding-right-sm-24 padding-left-24 hidden-print text-white" onclick="immonet.memo.likeObject('34443703');">
			<span id="memoLikeLabel" class="align-self-flex-start text-white text-80 maring-bottom-3 show hidden-xs">Merken<br></span>
			<span id="btn-int-keyfacts-in-favorite-list" class="align-self-flex-end ico icoHeartO text-white ico-size-24 ico-grey-light ico-white-hover cursor-hand ico-action-hover"></span>
		</div>

		<div class="flex-order-2 flex-grow-sm-0 flex-grow-2 hidden-print"></div>

		<div class="text-center align-self-center padding-top-9 flex-order-3 padding-bottom-9 padding-left-24">
			<span id="kfpriceLabel" class="align-self-flex-start text-80 text-white show">
				<span class="hidden-xs hidden-sm">Miete zzgl. NK</span>
				<span class="visible-xs visible-sm hidden-print">NKM</span>
			</span>

			
			
				
				
				
					
					<span id="kfpriceValue" class="align-self-flex-end text-250 text-primary-highlight">
						
							
							
							
								
								430.16&nbsp;&euro;
							
						
					</span>
				
			
		</div>

		
		
		

		
			<div class="text-center align-self-center flex-order-4 padding-top-9 padding-bottom-9 padding-left-24">
				<span id="kffirstareaLabel" class="align-self-flex-start text-white text-80 show">
					<span class="hidden-xs hidden-sm">Wohnfläche&nbsp;ca.</span>
					<span class="visible-xs visible-sm hidden-print">Wohnfl.&nbsp;ca.</span>
				</span>
				<span id="kffirstareaValue" class="align-self-flex-end text-primary-highlight text-250"> 49.19&nbsp;m&sup2;</span>
			</div>
		

		
		
			<div class="text-center align-self-center flex-order-5 padding-top-9 padding-bottom-9 padding-left-24">
				<span id="kfroomsLabel" class="text-white text-80 show">
					<span class="hidden-xs hidden-sm">Zimmer</span>
					<span class="visible-xs visible-sm hidden-print">Zi.</span>
				</span>
				<span id="kfroomsValue" class="align-self-flex-end text-primary-highlight text-250"> 2.0
				</span>
			</div>
		

		
		
			
			
			
		
		<div class="flex-order-sm-7 flex-grow-sm-2"></div>

		<div class="dropdown text-center flex-order-8 padding-top-9 padding-bottom-9 padding-left-24 hidden-xs hidden-sm hidden-print" role="menu">
			<span class="align-self-flex-start text-white text-80 show">Mehr<br></span>
			<span id="btn-int-keyfacts-more-options" class="align-self-flex-end ico icoChevronDown50 text-white ico-size-24 cursor-hand dropdown-toggle keyfacticon" data-toggle="dropdown" role="menuitem"></span>

			<ul class="dropdown-menu show-on-hover" role="menu" aria-labelledby="btn-int-keyfacts-more-options">
				<li role="presentation"><a id="btn-int-keyfacts-more-options-print" tabindex="-1" href="#" onclick="window.print(); return false;" class="ico icoPrint ico-padding-right-6 text-100" role="menuitem">Drucken</a></li>

				

				
				

				<li role="presentation"><a id="btn-int-keyfacts-more-options-fraud-report" role="menuitem" tabindex="-1" href="#" class="ico icoAttention ico-padding-right-6 text-100" onclick="immonet.expose.openAbuseModal('//www.immonet.de/angebot', '34443703', 'true');">Objekt melden</a></li>
			</ul>
		</div>

		<div class="flex-order-10 flex-grow-sm-0 flex-grow-2"></div>

		
		<div style="display: none;" class="text-center align-self-center flex-order-12 padding-top-9 padding-bottom-9 padding-right-24 hidden-xs hidden-print">
			
				
				
					<a id="btn-int-keyfacts-contact" role="button" href="#" class="trigger-contact btn btn-action col-sm-12" rel="nofollow" onclick="immonet.expose.openContactModal('//www.immonet.de/angebot', '34443703', '164077', false, null, ''); immonet.tealium.trackOpenModal(); return false;">Anbieter kontaktieren</a>
				
			
		</div>
	</div>
</div>




<div id="mediaContainer" class="row display-flex container-flex-stretch pos-relative bg-secondary">















<script>
	var panorama_enabled = true;
	var panoramas = [];
	var mediasize = 0;
</script>








	
		<div class="col-xs-12  col-sm-8 media-stage bg-white">
			<div class="fotorama " data-auto="false">

				
					
					
						
						
							
								
								
								
									
									
									
										
									
								
							

							
								
								
								
								
									
										<div data-img="https://i.immonet.de/63/36/33/509633633_800x599.jpg"
										data-thumb="https://i.immonet.de/63/36/33/509633633_107x80.jpg"
										data-full="https://i.immonet.de/63/36/33/509633633_800x599.jpg"
										data-caption="200 AUSSENANSICHTEN"
										title="200 AUSSENANSICHTEN">
										</div>
									
								
							

						
						
						
							
								
								
								
									
									
									
										
									
								
							

							
								
								
								
								
									
										<div data-img="https://i.immonet.de/63/36/34/509633634_449x600.jpg"
										data-thumb="https://i.immonet.de/63/36/34/509633634_80x107.jpg"
										data-full="https://i.immonet.de/63/36/34/509633634_449x600.jpg"
										data-caption="201 AUSSENANSICHTEN"
										title="201 AUSSENANSICHTEN">
										</div>
									
								
							

						
						
						
							
								
								
								
									
									
									
										
									
								
							

							
								
								
								
								
									
										<div data-img="https://i.immonet.de/63/36/35/509633635_800x599.jpg"
										data-thumb="https://i.immonet.de/63/36/35/509633635_107x80.jpg"
										data-full="https://i.immonet.de/63/36/35/509633635_800x599.jpg"
										data-caption="202 AUSSENANSICHTEN"
										title="202 AUSSENANSICHTEN">
										</div>
									
								
							

						
						
						
							
								
								
								
									
									
									
										
									
								
							

							
								
								
								
								
									
										<div data-img="https://i.immonet.de/63/36/36/509633636_800x536.jpg"
										data-thumb="https://i.immonet.de/63/36/36/509633636_107x72.jpg"
										data-full="https://i.immonet.de/63/36/36/509633636_800x536.jpg"
										data-caption="99998 USP Vermietung"
										title="99998 USP Vermietung">
										</div>
									
								
							

						
						
						
							
								
								
								
									
									
									
										
									
								
							

							
								
								
								
								
									
										<div data-img="https://i.immonet.de/63/36/46/509633646_800x450.jpg"
										data-thumb="https://i.immonet.de/63/36/46/509633646_107x60.jpg"
										data-full="https://i.immonet.de/63/36/46/509633646_800x450.jpg"
										data-caption="Vonovia_Chatbot"
										title="Vonovia_Chatbot">
										</div>
									
								
							

						
						
					
				

			</div>

		</div>

		
		


<script>
	
	var initialFotoramaShow = true;
	jQuery('.fotorama').on('fotorama:show', function() {
		if(!initialFotoramaShow) {
			dataLayer.push({'event': 'trackEvent', 'eventCategory': 'image', 'eventAction': 'click', 'eventLabel': 'expose-main-image-transition'});
		}
		initialFotoramaShow = false;
	});

	
	jQuery('.fotorama').on('fotorama:fullscreenenter', function() {
		dataLayer.push({'event': 'trackEvent', 'eventCategory': 'image', 'eventAction': 'click', 'eventLabel': 'expose-main-fullscreen'});
	});
</script>


















<div id="media-contact-container" class="col-sm-4 hidden-xs display-flex align-items-flex-center bg-white hidden-print">
    <div id="media-contact" class="col-xs-12 box-50 bg-white text-center">
        
            <div class="box-100 margin-bottom-12">
                
                    
                        
                            <img id="media-contact-broker-logo" src="https://media.immowelt.org/6/4/5/6457fb464e4aeb2cafd81752e5367bd1/7/200_c57a55edefa54baea1c3ecef9f35eea3.png" alt="Anbieterlogo" />
                        
                    
                    
                
            </div>
        

        

        <p id="media-contact-headline" class="headline-195 right-column">Kontakt zum Anbieter</p>

        
        <div class="row margin-top-12">
            <div class="col-xs-12">
                
                
                    
                        
                            
                                
                                    <p id="bdlName" class="text-100">
                                        Kundenservice
                                    </p>
                                

                                
                                
                                
                            
                            
                        
                    
                    
                

                
                
                    <p id="bdlFirmname" class="text-100">Vonovia Kundenservice GmbH</p>
                
            </div>
        </div>

        
        <div class="row">
            
                <div class="col-xs-12 box-50">
                    
                    <div class="row">
                        <div id="linkPhone1" class="hidden-print">
                            <a id="bdlPhoneLink" onclick="immonet.commons.showPhoneNumbers('#linkPhone1', '#boxPhone1'); immonet.tracking.trackPhone(); immonet.expose.writeStatistic('PHONEHIT', 34443703, 655587, '98081e39-c331-47b3-a8b3-5587fd86d480'); return false;" class="btn btn-secondary ico icoTelephone ico-padding-right-6">Telefonnummer</a>
                        </div>
                        <div id="boxPhone1" class="visible-print hidden">
                            
                                <p id="bdlPhone" class="text-100">Tel.: 0234 4147000-05</p>
                            
                            
                        </div>
                    </div>
                </div>
            

            <div class="col-xs-12 box-50 margin-top-12">
                
                    
                    
                        <a id="btn-int-media-contact" role="button" href="#" class="btn btn-action trigger-contact padding-left-24 padding-right-24" rel="nofollow" onclick="immonet.expose.openContactModal('//www.immonet.de/angebot', '34443703', '164077', false, null, ''); immonet.tealium.trackOpenModal(); return false;">Anbieter kontaktieren</a>
                    
                
            </div>
            <div class="col-xs-12">
                <a href="#brokerDetailsFull" onclick="return immonet.commons.slideTo('#brokerDetailsFull', -60);" class="padding-left-15">Details zum Anbieter</a>
            </div>
            
        </div>
    </div>
</div>



</div>
</div>

						<div class="col-xs-12 box-50">
						






<div class="row hidden-print">
	<div id="dyn-expose-toplinks" class="col-xs-12 box-50 display-flex flex-wrap justify-content-space-between justify-content-space-around-md"></div>
</div>










<div class="row box-50">
	<div class="col-xs-12 bg-white">
		
		<div class="col-xs-12 col-sm-12">
			<div class="col-xs-12 box-100">
				<div class="pull-left margin-top-sm-6 hidden-print">
					<span class="new-svg pull-left margin-right-sm-6"></span>
					
				</div>
				<h1 id="expose-headline" onclick="immonet.expose.showFullTitle();" class="headline-250 line-height-normal">
					Ansehen: frisch modernisiertes Wohnglück
				</h1>
			</div>

			<div class="col-xs-12">
				<div class="col-xs-12 box-45 margin-left-6">
					<h2 id="sub-headline-expose" class="text-100">
						
						Etagenwohnung
						mieten

						
						
							in Münster
						
					</h2>
					
				</div>
			</div>
		</div>

		
			<div class="row visible-print-block"></div>
			<div class="col-xs-12 col-sm-6 box-100 text-left">
				

				<span class="show">
					<a id="tsr-int-map-anchor-xs-icon" href="#" class="mini-map-icon-svg margin-right-9 pull-left hidden-print"></a>
     
					<img class="mini-map-icon-svg margin-right-9 pull-left visible-print" src="//cdn.immonet.de/frontend-resources/4.8.0/immonet/img/mini_map_icon.svg">
					<p class="text-100 pull-left">
						Stehrweg 27<br />
						
						48155&nbsp;
						Münster
						
						<br /><a id="tsr-int-map-anchor-xs-link" class="hidden-print" href="#">Auf Karte anzeigen</a>
					</p>
				</span>
			</div>
		

		<div class="col-xs-12 col-sm-6 box-100 hidden-print">
			<div class="text-center pull-right-sm hidden-xs">
			




<div class="row box-25 text-center hidden-xs margin-bottom-12 hidden-print">
	<p class="text-100 margin-bottom-6">Dieses Angebot teilen</p>
	<ul class="list-unstyled list-inline">
		<li class="margin-right-12">
			<a id="btn-int-map-share-fb"
				href="https://www.facebook.com/share.php?u=https://www.immonet.de/angebot/34443703&amp;clear=yes&amp;utm_medium=sm-post-expose&amp;utm_campaign=social-media&amp;utm_source=facebook.com"
				rel="nofollow"
				onclick="window.open('https://www.facebook.com/share.php?u=https:\/\/www.immonet.de\/angebot\/34443703&amp;clear=yes&amp;utm_medium=sm-post-expose&amp;utm_campaign=social-media&amp;utm_source=facebook.com', 'Facebook'); immonet.expose.writeStatistic('FORWARDHIT',34443703,655587); return false;"
				class="ico icoFacebookCir ico-size-30 ico-default"
				title="Dieses Exposé bei Facebook teilen"
				target="_blank"></a>
		</li>
		<li class="margin-right-12">
			<a id="btn-int-map-share-twitter"
				href="https://twitter.com/intent/tweet?text=Ansehen:%20frisch%20modernisiertes%20Wohngl%C3%BCck%20https://www.immonet.de/angebot/34443703"
				rel="nofollow"
				class="ico icoTwitterCir ico-size-30 ico-default"
				title="Dieses Exposé bei Twitter teilen"
				target="_blank"></a>
		</li>
		<li class="margin-right-12">
			<a id="btn-int-map-share-googleplus"
				href="https://plus.google.com/share?url=https://www.immonet.de/angebot/34443703&amp;clear=yes&amp;utm_medium=sm-post-expose&amp;utm_campaign=social-media&amp;utm_source="
				rel="nofollow"
				class="ico icoGooglePlusCir ico-size-30 ico-default"
				title="Dieses Exposé bei Google+ teilen"
				target="_blank"></a>
		</li>
		<li>
			<a id="btn-int-map-send-mail"
				href="mailto:?subject=Interessante%20Immobilie%20bei%20Immonet%20gefunden&amp;body=Hallo,%0D%0A%0D%0Aich%20habe%20eine%20interessante%20Immobilie%20bei%20Immonet%20gefunden:%0D%0A%0D%0Ahttps://www.immonet.de/angebot/34443703%0D%0A%0D%0AViele%20Gr&uuml;&szlig;e"
				rel="noFollow"
				onclick="immonet.expose.writeStatistic('FORWARDHIT', 34443703, 655587); return true;"
				class="ico icoLetterCir ico-size-30 ico-default"
				title="Dieses Exposé per Mail teilen"></a>
		</li>
	</ul>
</div>

			</div>
			<div class="row visible-xs">
				<div class="col-xs-12 list-100 margin-bottom-12">
				
				</div>
				<div class="col-xs-12 text-center">
				





<p class="text-100 margin-bottom-9 hidden-xs">Dieses Angebot teilen</p>
<ul class="list-unstyled list-inline">
	<li class="margin-right-12">
		<a id="btn-int-map-share-fb"
			 href="https://www.facebook.com/share.php?u=https://www.immonet.de/angebot/34443703&amp;clear=yes&amp;utm_medium=sm-post-expose&amp;utm_campaign=social-media&amp;utm_source=facebook.com"
			 rel="nofollow"
			 onclick="window.open('https://www.facebook.com/share.php?u=https:\/\/www.immonet.de\/angebot\/34443703&amp;clear=yes&amp;utm_medium=sm-post-expose&amp;utm_campaign=social-media&amp;utm_source=facebook.com', 'Facebook'); immonet.expose.writeStatistic('FORWARDHIT',34443703,655587); return false;"
			 class="ico icoFacebookCir ico-size-30 ico-default"
			 title="Dieses Exposé bei Facebook teilen"
			 target="_blank">
		</a>
	</li>

	<li class="margin-right-12">
		<a id="btn-int-map-share-twitter"
			 href="https://twitter.com/intent/tweet?text=Ansehen:%20frisch%20modernisiertes%20Wohngl%C3%BCck%20https://www.immonet.de/angebot/34443703"
			 rel="nofollow"
			 class="ico icoTwitterCir ico-size-30 ico-default"
			 title="Dieses Exposé bei Twitter teilen"
			 target="_blank">
		</a>
	</li>

	<li class="margin-right-12">
		<a id="btn-int-map-share-googleplus"
			 href="https://plus.google.com/share?url=https://www.immonet.de/angebot/34443703&amp;clear=yes&amp;utm_medium=sm-post-expose&amp;utm_campaign=social-media&amp;utm_source="
			 rel="nofollow"
			 class="ico icoGooglePlusCir ico-size-30 ico-default"
			 title="Dieses Exposé bei Google+ teilen"
			 target="_blank">
		</a>
	</li>

	<li class="margin-right-12">
		<a id="btn-int-map-send-mail"
			 href="mailto:?subject=Interessante%20Immobilie%20bei%20Immonet%20gefunden&amp;body=Hallo,%0D%0A%0D%0Aich%20habe%20eine%20interessante%20Immobilie%20bei%20Immonet%20gefunden:%0D%0A%0D%0Ahttps://www.immonet.de/angebot/34443703%0D%0A%0D%0AViele%20Gr&uuml;&szlig;e"
			 rel="noFollow"
			 onclick="immonet.expose.writeStatistic('FORWARDHIT', 34443703, 655587); return true;"
			 class="ico icoLetterCir ico-size-30 ico-default"
			 title="Dieses Exposé per Mail teilen">
		</a>
	</li>

	<li>
		<a id="btn-int-map-whatsapp"
			 href="whatsapp://send?text=Schau%20dir%20diese%20Immobilie%20an%3A%20https://www.immonet.de/angebot/34443703&amp;clear=yes&amp;utm_medium=sm-post-expose&amp;utm_campaign=social-media&amp;utm_source="
			 rel="noFollow"
			 class="ico icoWhatsappCir ico-size-30 ico-default"
			 title="Dieses Exposé per Whatsapp teilen">

		</a>
	</li>
</ul>


				</div>
			</div>
		</div>
	</div>
</div>






<div class="row">
	<div id="specifications" class="col-xs-12 col-md-6">
		<div class="row box-50">
	<div class="col-xs-12 bg-white box-100">
	





<div class="col-xs-12">
	<a data-toggle="collapse" data-parent="#specifications" href="#panelPrices">
		<h2 class="text-gray-darker headline-290">Preise & Kosten<span class="hidden-print pull-right ico ico-size-15 icoChevronCollapse50 ico-primary"></span></h2>
	</a>
</div>
<div id="panelPrices" class="col-xs-12 panel-collapse collapse in">
			<div class="row margin-top-18">
				<div class="row list-100">
									<div id="pricename_2" class="col-xs-12 col-sm-6 list-30 text-180">
										Miete zzgl. NK</div>

									<div id="priceid_2" class="col-xs-12 col-sm-6 text-200">
										4.230.6&nbsp;&euro;
											</div>
								</div>
							<div class="row list-100">
									<div id="pricename_4" class="col-xs-12 col-sm-6 list-30 text-95">
										Miete inkl. NK</div>

									<div id="priceid_4" class="col-xs-12 col-sm-6 text-100">
										4.068.16&nbsp;&euro;
											</div>
								</div>
							<div class="row list-100">
									<div id="pricename_20" class="col-xs-12 col-sm-6 list-30 text-95">
										Nebenkosten</div>

									<div id="priceid_20" class="col-xs-12 col-sm-6 text-100">
										108.0&nbsp;&euro;
											</div>
								</div>
							<div class="row list-100">
									<div id="pricename_5" class="col-xs-12 col-sm-6 list-30 text-95">
										Heizkosten</div>

									<div id="priceid_5" class="col-xs-12 col-sm-6 text-100">
										3.002.20&nbsp;&euro;
											</div>
								</div>
							<div class="row list-100">
									<div class="col-xs-12 col-sm-6 list-30 text-95">Heizkosten in Warmmiete enthalten:</div>
									<div id="rentincludingheating" class="col-xs-12 col-sm-6 text-100">Ja</div>
								</div>
							<div class="row list-100">
									<div id="pricename_19" class="col-xs-12 col-sm-6 list-30 text-95">
										Kaution/ Genossenschaftsanteile</div>

									<div id="priceid_19" class="col-xs-12 col-sm-6 text-100">
										1290.48&nbsp;&euro;
											</div>
								</div>
							<div id="dyn-expose-inline-mietkaution" class="row list-100"></div>
							<div id="dyn-expose-price" class="row list-100 hidden-print"></div>
						<div id="dyn-expose-inline-1" class="row list-100 hidden-print"></div>
				<div id="dyn-expose-inline-2" class="row list-100 hidden-print"></div>

				







</div>
		</div>
	</div>
</div>













<div class="row box-50">
	<div class="col-xs-12 box-100 bg-white">
	





<div class="col-xs-12">
	<a data-toggle="collapse" data-parent="" href="#panelObjectstate">
		<h2 class="text-gray-darker headline-290">Größe & Zustand<span class="hidden-print pull-right ico ico-size-15 icoChevronCollapse50 ico-primary"></span></h2>
	</a>
</div>


		<div id="panelObjectstate" class="col-xs-12 panel-collapse collapse in">
			<div class="row margin-top-18">
				
				
					<div class="row list-100">
						<div class="col-xs-12 col-sm-6 list-30 text-180">
							
								
								Zimmer
							
						</div>
						<div id="equipmentid_1" class="col-xs-12 col-sm-6 text-200">
							2.0
						</div>
					</div>
				

				
				
					
				

				
				
					<div class="row list-100">
						<div id="areaname_1" class="col-xs-12 col-sm-6 list-30 text-180">Wohnfläche ca.</div>
						<div id="areaid_1" class="col-xs-12 col-sm-6 text-200">
						49.19&nbsp;m&sup2;</div>
					</div>
				

				
				

				
				
					<div class="row list-100">
						<div class="col-xs-12 col-sm-6 list-30 text-95">Baujahr</div>
						<div id="yearbuild" class="col-xs-12 col-sm-6 text-100">1954</div>
					</div>
				

				
				

				
				

				
				
					<div class="row list-100">
						<div class="col-xs-12 col-sm-6 list-30 text-95">Verfügbar ab</div>
						<div id="deliveryValue" class="col-xs-12 col-sm-6 text-100">
							
							
							
								24.08.2018
							
						</div>
					</div>
				

				
				

				
				

				
				
					<div id="dyn-expose-object-1" class="row list-100"></div>
				

					
				
				
				
				
			</div>
		</div>
	</div>
</div>

		

		










	
		<div class="row box-50">
			<div class="col-xs-12 box-100 bg-white">
			





<div class="col-xs-12">
	<a data-toggle="collapse" data-parent="" href="#panel-energy-pass">
		<h2 class="text-gray-darker headline-290">Energie<span class="hidden-print pull-right ico ico-size-15 icoChevronCollapse50 ico-primary"></span></h2>
	</a>
</div>


				<div id="panel-energy-pass" class="col-xs-12 panel-collapse collapse in">
					<div class="row margin-top-18">
						
						
							<div class="row list-100">
								<div class="col-xs-12 col-sm-6 list-30 text-95">Energieeffizienzklasse</div>
								<div id="efficiencyValue" class="col-xs-12 col-sm-6 text-100">Klasse E</div>
							</div>
						

						
						
							<div class="row list-100">
								<div class="col-xs-12 col-sm-6 list-30 text-95">
									
										Endenergiebedarf
									
									
								</div>
								<div id="energyValue" class="col-xs-12 col-sm-6 text-100">
									153.0 kWh/(m²*a)
								</div>
							</div>
						

						
						

						
						

						
						

						
						

						
						

						
						
						
						
							<div class="row list-100">
								<div class="col-xs-12 col-sm-6 list-30 text-95">Energieausweis</div>
								<div id="electricityConsumptionValue" class="col-xs-12 col-sm-6 text-100">
									Energiebedarfsausweis
									
									
									
								</div>
							</div>

							
								<div class="row list-100">
									<div class="col-xs-12 col-sm-6 list-30 text-95">Ausgestellt</div>
									<div id="energyPassYearValue" class="col-xs-12 col-sm-6 text-100">
										
										ab dem 1.5.2014
									</div>
								</div>
							
						
						
						
							<div class="row list-100">
								<div class="col-xs-12 col-sm-6 list-30 text-95">Heizungsart</div>
								<div id="heatTypeValue"  class="col-xs-12 col-sm-6 text-100">Zentralheizung</div>
							</div>
						

						
						
							<div class="row list-100">
								<div class="col-xs-12 col-sm-6 list-30 text-95">Befeuerungsart</div>
								<div id="heaterSupplierValue" class="col-xs-12 col-sm-6 text-100">
									
										Gas,
										
									
										
										Strom
									
								</div>
							</div>
						

						
						
							<div class="row list-100">
								<div class="col-xs-12 col-sm-6 list-30 text-95">Baujahr (laut Energieausweis)</div>
								<div id="yearBuildByPassValue" class="col-xs-12 col-sm-6 text-100">1954</div>
							</div>
						

						
						

						
						


						
						
					</div>
				</div>
			</div>
		</div>
	
	


		








	<div class="row box-50">
		<div class="col-xs-12 bg-white box-100">
		





<div class="col-xs-12">
	<a data-toggle="collapse" data-parent="" href="#panelFeatures">
		<h2 class="text-gray-darker headline-290">Ausstattung<span class="hidden-print pull-right ico ico-size-15 icoChevronCollapse50 ico-primary"></span></h2>
	</a>
</div>


			<div id="panelFeatures" class="col-xs-12 panel-collapse collapse in">
				<div class="row margin-top-18">
					<div class="col-xs-12">
					









<div class="col-xs-12 col-sm-6">
	<ul class="list-unstyled">
		
			
			

			

			
				
			

			

			
				
				
					
						
							
								
								
								
									<li id="featureId_123" class="col-xs-12 margin-bottom-9 text-100">
										<span class="ico padding-right-sm-9 padding-right-md-0 text-gray-dark grey icoSwoosh ico-action ico-size-15 ico-size-sm-21 ico-size-md-15 ico-padding-top-9 pull-left"></span>
										<span class="block padding-left-21">
											Etage: 2
										</span>
									</li>
								
							
						
						
					
				
			

			
		
			
			
		</ul>
	</div>

	<div class="col-xs-12 col-sm-6">
		<ul class="list-unstyled">
			
			

			

			

			

			
				
				
					
						
						
							<li id="featureId_69" class="col-xs-12 margin-bottom-9 text-100">
								<span class="ico padding-right-sm-9 padding-right-md-0 text-gray-dark icoSwoosh ico-action ico-size-15 ico-size-sm-21 ico-size-md-15 ico-padding-top-9 pull-left"></span>
								<span class="block padding-left-21">
									Keller
								</span>
							</li>
						
					
				
			

			
		
	</ul>
</div>

					</div>
					
						
						<div id="dyn-expose-feature-1"></div>
					
		
					
					
						<div class="clearfix visible-print"></div>
						<div id="ausstattung" class="col-xs-12 margin-top-12 text-100">
							Türsprechanlage; Isolierverglasung; Warmwasserbereiter; Waschmaschinenanschluss; Kellerraum; Gas-Zentralheizung; Rauchwarnmelder; Wärmedämmung der obersten Geschossdecke;
						</div>
					
				</div>
					
					<div id="dyn-expose-feature-2"></div>
				
			</div>
		</div>
	</div>












	









	<div class="row box-50">
		<div class="col-xs-12 bg-white box-100">
		





<div class="col-xs-12">
	<a data-toggle="collapse" data-parent="" href="#panelLocationDescription">
		<h2 class="text-gray-darker headline-290">Lage<span class="hidden-print pull-right ico ico-size-15 icoChevronCollapse50 ico-primary"></span></h2>
	</a>
</div>


			<div id="panelLocationDescription" class="col-xs-12 panel-collapse collapse in" aria-expanded="true" role="region">
				<div class="margin-top-18">
					<p id="locationDescription" class="text-100">Nur fünf Minuten zur Bushaltestelle, ganz schnell an der B51: Auch wenn Sie nicht mitten in Münster wohnen, haben Sie eine tolle Anbindung und können so das Münsteraner Flair genießen! Und auch mit dem Rad kommen Sie problemlos in die Innenstadt. Wer gern abends ausgeht, findet schöne Cafés in den umliegenden Straßen. Große Grünflächen in der Umgebung laden zu Spaziergängen ein.</p>
				</div>
			</div>
		</div>
	</div>










	<div class="row box-50">
		<div class="col-xs-12 bg-white box-100">
		





<div class="col-xs-12">
	<a data-toggle="collapse" data-parent="" href="#panelOther">
		<h2 class="text-gray-darker headline-290">Sonstiges<span class="hidden-print pull-right ico ico-size-15 icoChevronCollapse50 ico-primary"></span></h2>
	</a>
</div>


			<div id="panelOther" class="col-xs-12 panel-collapse collapse in" aria-expanded="true" role="region">
				<div class="margin-top-18">
					<p id="otherDescription" class="text-100">Um einen Besichtigungstermin zu buchen, können Sie jederzeit unter Angabe der Referenz-/Objektnummer 84-1138510007 unseren Chatbot unter bot.vonovia.de erreichen.Weitere Informationen hierzu und zum Thema Datenschutz erhalten Sie unter: www.vonovia.de/messenger<br/><br/>Die Kabelgebühren sind bereits in den Nebenkosten enthalten.<br/><br/> Hinweise darüber, wie die Vonovia Ihre personenbezogenen Daten als Interessent verarbeitet, erhalten Sie unter www.vonovia.de/datenschutzinformation. Alle Angaben in diesem Exposé wurden sorgfältig und so vollständig wie möglich gemacht. Gleichwohl kann das Vorhandensein von Fehlern nicht ausgeschlossen werden. Die Angaben in diesem Exposé erfolgen daher ohne jede Gewähr. Maßgeblich sind die im Mietvertrag geschlossenen Vereinbarungen. Soweit die Grundrissgrafiken Maßangaben und Einrichtungen enthalten, wird auch für diese jegliche Haftung ausgeschlossen. Mietverhandlungen sind ausschließlich über den benannten Ansprechpartner zu führen. Mietpreisänderungen bleiben vorbehalten.<br/><br/>Buchen Sie rund um die Uhr online Ihren Besichtigungstermin unter www.vonovia.de.<br/><br/></p>
				</div>
			</div>
		</div>
	</div>



	

	



<div class="row box-50">
	<div class="col-xs-12 bg-white box-100">
		<p class="text-100  hidden-print">Immonet-Nr.: 34443703</p>
		<p class="text-100  hidden-print">Anbieter-Objekt-ID: 84-1138510007-655587</p>

		
	</div>
</div>


		
		
		<div id="dyn-expose-specification-1"></div>
	</div>
	<div class="hidden-xs col-sm-12 col-md-6">
		
			
			
			
				<div class="row box-50 hidden-print">
					<div class="col-xs-12 bg-light-blue box-100 margin-bottom-24">
						<div id="shortContactContent" class="nested contact">
						
						
						






	
	


<form id="sbc_contactForm" name="sbc_contactForm" action="/in-expose/showContactFormPage.do;jsessionid=3EBD98B1DFFF6261F7ECE37BF408AFDB" method="post" class="margin-bottom-0">
	<input type="hidden" name="contactForm.senderPersonId" value="" id="sbc_contactForm_contactForm_senderPersonId"/>
	<input type="hidden" name="contactForm.objectId" value="34443703" id="sbc_contactForm_contactForm_objectId"/>
	<input type="hidden" name="contactForm.exposeReferrer" value="Ohne Suche" id="sbc_contactForm_contactForm_exposeReferrer"/>

	<a data-dismiss="modal" class="btn-close ico icoCrossCir cursor-hand h3" href="#"></a>

	<div class="modal-header padding-top-0 padding-right-0 padding-bottom-18 padding-left-0">
		<p role="heading" class="headline-290">Kontakt zum Anbieter</p>
	</div>

	<div class="modal-body padding-0">
		<div class="col-xs-12">
			

			
			







    
    
    
    
    
    
    
    
    
    
    
        
        
            
            
            
            <div class="row form-group">
                <div class="col-xs-12 col-sm-6 padding-bottom-12 padding-bottom-sm-0">
                    <div aria-required="true" role="listbox" id="select-salutation">
                        <div aria-required="true" role="listbox" >
                            <label class="cursor-hand text-normal text-200" for="sbc_salutation">Anrede*</label>
                            <select name="contactForm.salutation" tabindex="-1" id="sbc_salutation" class="form-control">
    <option value=""
    >Bitte wählen</option>
    <option value="Herr">Herr</option>
    <option value="Frau">Frau</option>
    <option value="Firma">Firma</option>


</select>


                        
                        </div>
                    </div>
                </div>
                
            </div>
            
        
        
        
    

    
    
    
    
    
    
    
    
    
    
    
        
        
        
        
            
                <div class="row form-group" style="">
                    <div class="col-xs-12 col-sm-6 padding-bottom-12 padding-bottom-sm-0  ">
                        <label class="cursor-hand text-normal text-200" for="sbc_prename">Vorname*</label>
                        <input type="text" name="contactForm.prename" maxlength="80" value="" id="sbc_prename" class="form-control"/>
                        
                    </div>
                    
                        
                        
                        
                        
                        
                        
                        
                        <div class="col-xs-12 col-sm-6 padding-left-sm-12 ">
                            <label class="cursor-hand text-normal text-200" for="sbc_surname">Nachname*</label>
                            <input type="text" name="contactForm.surname" maxlength="80" value="" id="sbc_surname" class="form-control"/>
                            
                        </div>
                    
                </div>
            
        
    

    
    
    
    
    
    
    
    
    
    
    
        
        
        
        
            
        
    

    
    
    
    
    
    
    
    
    
    
    
        
        
        
        
            
                <div class="row form-group" style="display: none;">
                    <div class="col-xs-12 col-sm-6 padding-bottom-12 padding-bottom-sm-0  ">
                        <label class="cursor-hand text-normal text-200" for="sbc_firmname">Firmenname*</label>
                        <input type="text" name="contactForm.firmname" maxlength="80" value="" id="sbc_firmname" class="form-control"/>
                        
                    </div>
                    
                        
                        
                        
                        
                        
                        
                        
                        <div class="col-xs-12 col-sm-6 padding-left-sm-12 ">
                            <label class="cursor-hand text-normal text-200" for="sbc_contactPerson">Ansprechpartner*</label>
                            <input type="text" name="contactForm.contactPerson" maxlength="80" value="" id="sbc_contactPerson" class="form-control"/>
                            
                        </div>
                    
                </div>
            
        
    

    
    
    
    
    
    
    
    
    
    
    
        
        
        
        
            
        
    

    
    
    
    
    
    
    
    
    
    
    
        
        
        
        
            
                <div class="row form-group" style="">
                    <div class="col-xs-12 col-sm-6 padding-bottom-12 padding-bottom-sm-0  ">
                        <label class="cursor-hand text-normal text-200" for="sbc_email">E-Mail-Adresse*</label>
                        <input type="text" name="contactForm.email" maxlength="200" value="" id="sbc_email" class="form-control"/>
                        
                    </div>
                    
                        
                        
                        
                        
                        
                        
                        
                        <div class="col-xs-12 col-sm-6 padding-left-sm-12 ">
                            <label class="cursor-hand text-normal text-200" for="sbc_phone">Telefonnummer*</label>
                            <input type="text" name="contactForm.phone" maxlength="50" value="" id="sbc_phone" class="form-control"/>
                            
                        </div>
                    
                </div>
            
        
    

    
    
    
    
    
    
    
    
    
    
    
        
        
        
        
            
        
    

    
    
    
    
    
    
    
    
    
    
    
        
        
        
            <div class="row form-group">
                <div class="col-xs-12 ">
                    <label class="cursor-hand text-normal text-200" for="sbc_annotations">Nachricht*</label>
                    <textarea name="contactForm.annotations" cols="20" rows="3" id="sbc_annotations" class="form-control" maxlength="2500">Ich interessiere mich f&uuml;r Ihr Angebot. Bitte nehmen Sie Kontakt mit mir auf.</textarea>
                    
                </div>
            </div>
        
        
    


<script>
    window.onload = function () {
        if (immonet.expose){
            immonet.expose.contactForm.changeSalutationFields(false);
        }
        $('.modal-body').on('change', '#sbc_salutation', function(){ immonet.expose.contactForm.changeSalutationFields(false); });
    };
</script>

			<div class="col-xs-12">
				<a id="sbc_panelOptionalFieldsHeadline" href="#sbc_panelOptionalFields" class="" data-toggle="collapse" aria-expanded="true">
					<h2 class="text-200 padding-top-15 padding-bottom-15 padding-top-sm-18 padding-bottom-sm-18">
						Weitere Angaben<span class="hidden-print ico ico-size-15 icoChevronCollapse50 padding-left-6"></span>
					</h2>
				</a>
			</div>
			
				<div id="sbc_panelOptionalFields" class="panel-collapse collapse in">
					
					







    
    
    
    
    
    
    
    
    
    
    
        
        
        
        
            
                <div class="row form-group" style="">
                    <div class="col-xs-12  ">
                        <label class="cursor-hand text-normal text-200" for="sbc_contact_street">Straße & Hausnummer*</label>
                        <input type="text" name="contactForm.street" maxlength="80" value="" id="sbc_contact_street" class="form-control"/>
                        
                    </div>
                    
                </div>
            
        
    

    
    
    
    
    
    
    
    
    
    
    
        
        
        
        
            
                <div class="row form-group" style="">
                    <div class="col-xs-12 col-sm-6 padding-bottom-12 padding-bottom-sm-0  ">
                        <label class="cursor-hand text-normal text-200" for="sbc_contact_zip">Postleitzahl*</label>
                        <input type="text" name="contactForm.zip" maxlength="10" value="" id="sbc_contact_zip" class="form-control"/>
                        
                    </div>
                    
                        
                        
                        
                        
                        
                        
                        
                        <div class="col-xs-12 col-sm-6 padding-left-sm-12 ">
                            <label class="cursor-hand text-normal text-200" for="sbc_contact_city">Ort*</label>
                            <input type="text" name="contactForm.city" maxlength="80" value="" id="sbc_contact_city" class="form-control"/>
                            
                        </div>
                    
                </div>
            
        
    

    
    
    
    
    
    
    
    
    
    
    
        
        
        
        
            
        
    

    
    
    
    
    
    
    
    
    
    
    
        
        
        
        
            
                <div class="row form-group" style="display: none;">
                    <div class="col-xs-12 col-sm-6 padding-bottom-12 padding-bottom-sm-0  ">
                        <label class="cursor-hand text-normal text-200" for="sbc_requestViewing">Besichtigung vereinbaren</label>
                        <input type="text" name="contactForm.requestViewing" maxlength="80" value="" id="sbc_requestViewing" class="form-control"/>
                        
                    </div>
                    
                        
                        
                        
                        
                        
                        
                        
                        <div class="col-xs-12 col-sm-6 padding-left-sm-12 ">
                            <label class="cursor-hand text-normal text-200" for="sbc_requestMoreInformation">Infomaterial anfordern</label>
                            <input type="text" name="contactForm.requestMoreInformation" maxlength="80" value="" id="sbc_requestMoreInformation" class="form-control"/>
                            
                        </div>
                    
                </div>
            
        
    

    
    
    
    
    
    
    
    
    
    
    
        
        
        
        
            
        
    

    
    
    
    
    
    
    
    
    
    
    
        
        
        
        
            
                <div class="row form-group" style="display: none;">
                    <div class="col-xs-12  ">
                        <label class="cursor-hand text-normal text-200" for="sbc_requestCallback">Rückruf</label>
                        <input type="text" name="contactForm.requestCallback" maxlength="80" value="" id="sbc_requestCallback" class="form-control"/>
                        
                    </div>
                    
                </div>
            
        
    

    
    
    
    
    
    
    
    
    
    
    
        
        
            
            
            
            <div class="row form-group">
                <div class="col-xs-12 col-sm-6 padding-bottom-12 padding-bottom-sm-0">
                    <div aria-required="true" role="listbox" id="select-householdIncome">
                        <div aria-required="true" role="listbox" >
                            <label class="cursor-hand text-normal text-200" for="sbc_householdIncome">Mtl. Haushaltsnettoeinkommen</label>
                            <select name="contactForm.householdIncome" tabindex="-1" id="sbc_householdIncome" class="form-control">
    <option value=""
    >Bitte wählen</option>
    <option value="unter 1.000 €">unter 1.000 €</option>
    <option value="1.000 - 2.000 €">1.000 - 2.000 €</option>
    <option value="2.000 - 3.000 €">2.000 - 3.000 €</option>
    <option value="3.000 - 4.000 €">3.000 - 4.000 €</option>
    <option value="über 4.000 €">über 4.000 €</option>


</select>


                        
                        </div>
                    </div>
                </div>
                
                    
                    
                    
                    
                    
                    
                    
                    <div class="col-xs-12 col-sm-6 padding-left-sm-12 ">
                        <label class="cursor-hand text-normal text-200" for="sbc_employmentStatus">Beschäftigungsstatus</label>
                        <select name="contactForm.employmentStatus" tabindex="-1" id="sbc_employmentStatus" class="form-control">
    <option value=""
    >Bitte wählen</option>
    <option value="Angestellte(r)">Angestellte(r)</option>
    <option value="Arbeiter(in)">Arbeiter(in)</option>
    <option value="Selbständige(r)">Selbständige(r)</option>
    <option value="Beamte(r)">Beamte(r)</option>
    <option value="Auszubildene(r)">Auszubildene(r)</option>
    <option value="Studierende(r)">Studierende(r)</option>
    <option value="Hausfrau/-mann">Hausfrau/-mann</option>
    <option value="Arbeitssuchende(r)">Arbeitssuchende(r)</option>
    <option value="Rentner(in)">Rentner(in)</option>
    <option value="Sonstiges">Sonstiges</option>


</select>


                    
                    </div>
                
            </div>
            
        
        
        
    

    
    
    
    
    
    
    
    
    
    
    
        
        
            
        
        
        
    

    
    
    
    
    
    
    
    
    
    
    
        
        
            
            
            
            <div class="row form-group">
                <div class="col-xs-12 col-sm-6 padding-bottom-12 padding-bottom-sm-0">
                    <div aria-required="true" role="listbox" id="select-householdSize">
                        <div aria-required="true" role="listbox" >
                            <label class="cursor-hand text-normal text-200" for="sbc_householdSize">Haushaltsgröße</label>
                            <select name="contactForm.householdSize" tabindex="-1" id="sbc_householdSize" class="form-control">
    <option value=""
    >Bitte wählen</option>
    <option value="Einpersonenhaushalt">Einpersonenhaushalt</option>
    <option value="Zwei Erwachsene">Zwei Erwachsene</option>
    <option value="Familie">Familie</option>
    <option value="Wohngemeinschaft">Wohngemeinschaft</option>


</select>


                        
                        </div>
                    </div>
                </div>
                
            </div>
            
        
        
        
    


<script>
    window.onload = function () {
        if (immonet.expose){
            immonet.expose.contactForm.changeSalutationFields(false);
        }
        $('.modal-body').on('change', '#sbc_salutation', function(){ immonet.expose.contactForm.changeSalutationFields(false); });
    };
</script>

				</div>
			
			<div class="row margin-top-24" />

			<div class="row margin-bottom-12">
				<div class="col-xs-12">
					<input type="submit" value="Anfrage senden" id="sbc_submit" class="btn btn-action pull-right col-xs-12 col-auto-sm trigger-contact"/>

				</div>
			</div>
			<div class="row margin-bottom-12">
				<div class="col-xs-12 text-50">
					Mit Absenden der Anfrage nehmen Sie den Immonet-Service in Anspruch. Sie erhalten anhand der von
					Ihnen eingegebenen Daten, genutzten Services und auf Grundlage unseres Gesch&auml;ftszwecks auf Ihr
					Anliegen ausgerichtete Informationen. Diesem Service k&ouml;nnen Sie jederzeit unter
					<a href="mailto:datenschutz@immonet.de">datenschutz@immonet.de</a> widersprechen. Weitere
					Informationen finden Sie in der <a href="https://www.immonet.de/service/datenschutz.html" target="_blank"
													   class="white" title="Datenschutzbestimmungen">Datenschutzerkl&auml;rung</a>.
				</div>
			</div>
		</div>
	</div>
	<script>
        if (immonet.expose) {
            immonet.expose.contactForm.changeSalutationFields(false);
            immonet.expose.contactForm.changeSalutationFields(true);
        }
        $('.modal-body').on('change', '#sbc_salutation', function(){ immonet.expose.contactForm.changeSalutationFields(false); });
        $('#modalContact').on('change', '#bc_salutation', function(){ immonet.expose.contactForm.changeSalutationFields(true); });
	</script>
</form>




						</div>
					</div>
				</div>
			
		
		
			
			
				



<div id="zeitungsanbieter">



</div>

		
		

		<div class="hidden-sm hidden-print">
			
				
					<div id="dyn-expose-12"></div>
				
				
			

			


<div class="row box-50 hidden-print">
	
	
	
	
	
		<div id="ads-rect" class="immonetads col-xs-12 text-center"></div>
	
</div>




<div class="row hidden-print">
	
		<div class="col-xs-6 box-50">
			<div id="ads-partner1" class="partnertile immonetads"></div>
		</div>
		<div class="col-xs-6 box-50">
			<div id="ads-partner2" class="partnertile immonetads"></div>
		</div>
	
</div>









<div class="hidden-xs hidden-sm box-50">
<div class="row box-100 bg-white">






<div class="col-xs-12">
	<a data-toggle="collapse" data-parent="#specifications" href="#energy-calculator-teaser">
		<h2 class="text-gray-darker headline-290">Stromrechner<span class="hidden-print pull-right ico ico-size-15 icoChevronCollapse50 ico-primary"></span></h2>
	</a>
</div>

		<div id="energy-calculator-teaser" class="col-xs-12 panel-collapse collapse in">
		<div class="row margin-top-18">
			<div class="col-xs-5 padding-bottom-12 text-strong">Wohnfläche</div>
			<div class="col-xs-7 padding-bottom-12 text-right text-strong">49.19 m²</div>
			<div class="row border-bottom-grey-light margin-bottom-24">
				<div class="col-xs-5 padding-top-12 text-strong">Personen im Haushalt</div>
				<div class="col-xs-7 padding-bottom-12">
					<div class="btn-group pull-right" role="group">
						<button class="btn" onclick="immonet.energy.adjustEnergyConsumption(0)">1</button>
						<button class="btn" onclick="immonet.energy.adjustEnergyConsumption(1)">2</button>
						<button class="btn" onclick="immonet.energy.adjustEnergyConsumption(2)">3</button>
						<button class="btn" onclick="immonet.energy.adjustEnergyConsumption(3)">4</button>
					</div>
				</div>
			</div>
			<div class="row border-bottom-grey-light margin-bottom-12">
				<div class="col-xs-7 padding-bottom-12">Geschätzter Jahresverbrauch*</div>
				<div id="energy-consumption-field" class="col-xs-5 padding-bottom-12 text-right"></div>
				<div class="col-xs-7 padding-bottom-12">mtl. Kosten beim Grundversorger*</div>
				<div id="basic-price-field" class="col-xs-5 padding-bottom-12 text-right"></div>
				<div class="col-xs-7 padding-bottom-12 text-strong">
					mtl. Einsparpotenzial mit dem unabhängigen Wechselpilot-Service*
				</div>
				<div id="price-difference-field" class="col-xs-5 padding-bottom-12 text-right text-strong"></div>
			</div>
			<div class="col-xs-7 padding-bottom-12">mtl. Kosten*</div>
			<div id="energy-price-field" class="col-xs-5 padding-bottom-12 text-right"></div>
		</div>

		<div class="row box-50">
			<div class="col-xs-12 box-50 text-right">
				<a id="expose-strompartner"
				   class="btn btn-action"
				   href="https://www.immonet.de/umzug/wechselpilot.html"
				   target="_blank"
                   title="Ihr Strom Wechselservice von Wechselpilot"
                   data-track='{"category":"button-intern","action":"click","label":"expose-strompartner JETZT SPAREN LASSEN"}'
				   style="background-color: #f9aa00;">
					JETZT SPAREN LASSEN
				</a>
			</div>
		</div>
		
		<div class="row text-50 margin-bottom-24">
			<div class="col-xs-6">
				* Wechselpilot-Schätzung
			</div>
			<div class="col-xs-6 text-right vertical-align-middle">
				Ein Service von <img class="margin-left-3" src="//www.immonet.de/angebot/resources/images/logo/wechselpilot-logo.png" title="Wechselpilot" alt="Wechselpilot Logo">
			</div>
		</div>
		
		<div class="row">
			<div id="dyn-expose-calc-2"></div>
		</div>
	</div>
</div>
</div>









<div class="hidden-xs hidden-sm  box-50">
<div class="row box-100  bg-white">






<div class="col-xs-12">
	<a data-toggle="collapse" data-parent="#specifications" href="#relocation-costcalculator-teaser">
		<h2 class="text-gray-darker headline-290">Umzugskostenrechner<span class="hidden-print pull-right ico ico-size-15 icoChevronCollapse50 ico-primary"></span></h2>
	</a>
</div>

		<div id="relocation-costcalculator-teaser" class="col-xs-12 panel-collapse collapse in">
			<div id="relocation-costcalculator" class="relocation-costcalculator" data-url="https://www.immonet.de/umzug.html">

			</div>

		</div>

</div>
</div>

	
	




	<div class="row hidden-print">
			<div data-display-type="content" data-display-name="teaser" data-display-variant="tipps-der-redaktion-wohnen-wohnung-miete" data-display-attributes="" class="row box-50"> <div class="col-xs-12 box-100 bg-white"> <h2 class="text-gray-darker headline-290">Tipps der Immonet Redaktion</h2> <div id="editors-tipp-box"><div data-display-item="nebenkostenabrechnung" data-display-attributes=""> <a href="/umzug/wissenswertes-rechtliches-mietrecht-nebenkostenabrechnung.html" id="tsr-int-content-nebenkostenabrechnung" rel="follow" target="_self" title="Tipps zur Nebenkostenabrechnung"> <div class="col-xs-12 row margin-top-18 text-gray-darker"> <div class="col-xs-2"> <img alt="Tipps zur Nebenkostenabrechnung" class="img-responsive" src="/service/fileadmin/responsive/teaser/editorstipp/mieterhoehung.jpg" /> </div> <div class="col-xs-10 padding-left-12"> <p class="headline-50 text-primary">Tipps zur Nebenkostenabrechnung</p> <p class="text-100 text-gray-darker"><p>Die Nebenkostenabrechnung sorgt immer wieder für Uneinigkeit und Unklarheiten zwischen Mietern und Vermietern. Was muss aufgeführt werden und was nicht?</p></p> </div> </div> </a> </div><div data-display-item="moebel-einlagern" data-display-attributes=""> <a href="/umzug/selfstorage.html" id="tsr-int-content-moebeleinlagern" rel="follow" target="_self" title="Möbel einlagern – Stauraum auf Zeit"> <div class="col-xs-12 row margin-top-18 text-gray-darker"> <div class="col-xs-2"> <img alt="Möbel einlagern – Stauraum auf Zeit" class="img-responsive" src="/service/fileadmin/responsive/teaser/editorstipp/selfstorage.jpg" /> </div> <div class="col-xs-10 padding-left-12"> <p class="headline-50 text-primary">Möbel einlagern – Stauraum auf Zeit</p> <p class="text-100 text-gray-darker"><p>Stauraum für Möbel, Akten und Umzugskartons: So lagern Sie Ihre Möbel richtig ein. Tipps für Selfstorage-Lösungen in Ihrer Nähe finden Sie hier.</p></p> </div> </div> </a> </div><div data-display-item="wohnungsbesichtigung" data-display-attributes=""> <a href="/umzug/wissenswertes-tricks-wohnungsbesichtigung-tipps.html" id="tsr-int-content-wohnungsbesichtigung" rel="follow" target="_self" title="Wohnungsbesichtigung: Das sollten Sie beachten"> <div class="col-xs-12 row margin-top-18 text-gray-darker"> <div class="col-xs-2"> <img alt="Wohnungsbesichtigung: Das sollten Sie beachten" class="img-responsive" src="/service/fileadmin/responsive/teaser/editorstipp/wohnungsbesichtigung.jpg" /> </div> <div class="col-xs-10 padding-left-12"> <p class="headline-50 text-primary">Wohnungsbesichtigung: Das sollten Sie beachten</p> <p class="text-100 text-gray-darker"><p>Nach langer Suche haben Sie ihre Traumwohnung. Jetzt steht die Besichtigung an. Worauf man dabei achten sollte, hat Immonet für Sie zusammengestellt. </p></p> </div> </div> </a> </div><div data-display-item="wohnungssuche-bewerbung" data-display-attributes=""> <a href="/service/umzug/wissenswertes-tricks-redaktionsservice-bewerbung.html" id="tsr-int-content-wohnungssuche-bewerbung" rel="follow" target="_self" title="Wohnungssuche: So bewerben Sie sich richtig"> <div class="col-xs-12 row margin-top-18 text-gray-darker"> <div class="col-xs-2"> <img alt="Wohnungssuche: So bewerben Sie sich richtig" class="img-responsive" src="/service/fileadmin/responsive/teaser/editorstipp/bewerbungsmappe.jpg" /> </div> <div class="col-xs-10 padding-left-12"> <p class="headline-50 text-primary">Wohnungssuche: So bewerben Sie sich richtig</p> <p class="text-100 text-gray-darker"><p>Seit einiger Zeit verlangen immer mehr Vermieter eine Bewerbung. Wie eine Bewerbungsmappe aussehen sollte, hat Immonet zusammengestellt.</p></p> </div> </div> </a> </div></div> </div> </div>
	</div>


		</div>
	</div>
</div>
</div>
						<div class="col-xs-12 box-50">
						







	<div class="row box-50">
		<div id="brokerDetailsFull" class="col-xs-12 bg-white box-100 pos-relative">
		





<div class="col-xs-12">
	<a data-toggle="collapse" data-parent="" href="#panelContactPex">
		<h2 class="text-gray-darker headline-290">Anbieter<span class="hidden-print pull-right ico ico-size-15 icoChevronCollapse50 ico-primary"></span></h2>
	</a>
</div>


			<div id="panelContactPex" class="col-xs-12 panel-collapse collapse in" aria-expanded="true" role="note">
				<div class="row margin-top-18">

					
					<div class="col-xs-12 col-sm-4 col-md-3 padding-right-24 pos-relative-sm margin-bottom-24">
						
						
						<div class="col-xs-12">
							
								
								
								
									
									
										<a id="tsr-int-broker-logo-full-link" href="/anbieter/682790"><img id="tsr-int-broker-logo-full" class="img-responsive margin-bottom-30" src="https://media.immowelt.org/6/4/5/6457fb464e4aeb2cafd81752e5367bd1/7/200_c57a55edefa54baea1c3ecef9f35eea3.png" height="80" alt="Anbieterlogo" /></a>
									
									
								
							
						</div>
						
					</div>

					
					
						<div class="col-xs-12 col-sm-4 col-md-3 padding-right-24 pos-relative-sm margin-bottom-24">
							
							
								<h3 class="text-200">Ihr Ansprechpartner</h3>
								<p id="bdContactName" class="text-100">
									Kundenservice
								</p>
							
							
								<p id="bdContactStreet" class="text-100">
									Philippstr. 3
								</p>
							
							
								<p id="bdContactZipCity" class="text-100">
									
									44803 Bochum
								</p>
							
							
						</div>
					

					
						
					<div class="col-xs-12 col-sm-4 col-md-3 padding-right-24 pos-relative-sm margin-bottom-24">

							

							
							
								<h3 class="text-200">Ihr Anbieter</h3>
								<p class="text-100">
									
										
											
												
												
													<a href="https://www.immonet.de/anbieter/682790" id="bdfirmName"> <span id="bdBrokerFirmname">Vonovia Kundenservice GmbH</span>
													</a>
												
											
										
										
									
								</p>
							
							
							
								<p id="bdBrokerStreet" class="text-100">
									Philippstr. 3
								</p>
							
							
								<p id="bdBrokerZipCity" class="text-100">
									44803 Bochum
								</p>
							
					</div>
					

					

					

					
					<div class="col-xs-12 hidden-sm col-md-3 padding-right-24 pos-relative-sm margin-bottom-24"></div>
				</div>

				
				<div class="row margin-bottom-sm-18">
					<div class="col-xs-12  col-sm-4 col-md-3 hidden-xs hidden-print">
						<div class="col-xs-12">
							
							

							
								
								
								
									<a href="#" rel="nofollow" class="btn btn-action trigger-contact" onclick="immonet.expose.openContactModal('//www.immonet.de/angebot', '34443703', '164077', false, null, ''); immonet.tealium.trackOpenModal(); return false;">Anbieter kontaktieren</a>
								
							
						</div>
					</div>

					
						<div class="col-xs-12  col-sm-4 col-md-3 hidden-xs ">
							
							
							

							
								<div class="hidden-xs" id="contactPhoneContainer">
									<div class="visible-print">
										<h3 class="text-200">Ansprechpartner</h3>
									</div>
									<div id="linkPhone2" class="hidden-print">
										<a id="bdContactPhoneLink" onclick="immonet.commons.showPhoneNumbers('#linkPhone2', '#boxPhone2');immonet.tracking.trackPhone(); immonet.expose.writeStatistic('PHONEHIT',34443703,655587, '98081e39-c331-47b3-a8b3-5587fd86d480'); return false;" class="align-items-flex-end-md  btn btn-secondary ico icoTelephone ico-padding-right-6">Telefonnummer</a>
									</div>
									<div id="boxPhone2" class="visible-print hidden">
										
											<p id="bdContactPhone" class="text-100">
												Tel.:
												0234 4147000-05
											</p>
										
										
										
											<p id="bdContactFax" class="text-100">
												Fax:
												0234 314888-4414
											</p>
										
									</div>
								</div>
							
						</div>
					

					<div class="col-xs-12  col-sm-4 col-md-3 hidden-xs ">
					
					
					

					
					</div>
					<div class="col-xs-12  col-sm-4 col-md-3 hidden-xs "></div>
				</div>

				<div class="col-sm-12 visible-sm padding-right-24 pos-relative-sm margin-bottom-24"></div>

				
				<ul class="row pos-relative display-flex-md align-items-flex-center-md turn-bullets-off pipes">
					

					
					
						<li class="hidden-print"><a id="lnk-int-broker-more-objects" rel="nofollow" class="text-100" href="https://www.immonet.de/anbieter/682790#objects-fragment">Weitere Objekte des Anbieters</a></li>
					

					
					
						<li class="hidden-print"><a id="lnk-ext-broker-homepage" rel="nofollow" class="text-100" onclick="immonet.expose.writeStatistic('BROKERURLHIT',34443703,655587, '98081e39-c331-47b3-a8b3-5587fd86d480'); return true;"
							href="https://www.immonet.de/redirect?objectId=34443703&amp;personId=655587&amp;cs=4f4158fceb7778372f904ed662da863bc7587daf83257543a28c3298db93f45b&amp;url=http://www.vonovia.de" target="_blank">Homepage des Anbieters</a></li>
					

					
					<!-- Impressumslink -->
					
						<li>
							<a id="lnk-int-broker-imprint" rel="nofollow" target="blank" class="text-100 hidden-print" href="https://www.immonet.de/immobilienmakler/impressum-655587.html"
							onclick="immonet.expose.openImprintModal('//www.immonet.de/angebot', '655587'); immonet.tealium.trackOpenModal(); return false;">Anbieterimpressum</a>
						</li>
					

					
					<li class="pos-absolute padding-bottom-18 pos-bottom pos-right pos-relative-print">
						
							
							
							
								<img id="bdBrokerMembershipLogo" src="https://www.immonet.de/img/verbandslogos/logo_annington_ses.gif" alt="Verbandslogo" />
							
						
					</li>
				</ul>
			</div>
		</div>
	</div>




<div class="row box-50 display-flex-sm hidden-print">
	<div class="col-xs-12 flex-order-2">
	
	</div>
	<div class="col-xs-12 flex-order-1">
	
	</div>
</div>
<div class="row box-50 hidden-xs hidden-sm hidden-print">
		<div class="col-xs-12 bg-white box-100">
			<div class="row">
				<div>
					<a data-toggle="collapse" data-parent="" href="#similar-searches">
						<h2 class="text-gray-darker headline-290 padding-right-print-0 padding-left-print-0">
							Ähnliche Suchen<span class="hidden-print pull-right ico ico-size-15 icoChevronCollapse50 ico-primary"></span>
						</h2>
					</a>
				</div>
			</div>
			<div id="similar-searches" class="padding-top-12 panel-collapse collapse in" aria-expanded="true" role="note">
				<div class="padding-print-0 column-count-3">
				<a href="/nordrhein-westfalen/muenster-immobilien.html" class="text-100">Immobilien Münster</a><br />
				<a href="/nordrhein-westfalen/muenster-immobilien-mieten.html" class="text-100">Immobilie mieten Münster</a><br />
				<a href="/nordrhein-westfalen/muenster-immobilien-kaufen.html" class="text-100">Immobilie kaufen Münster</a><br />
				<a href="/nordrhein-westfalen/muenster-wohnung-mieten.html" class="text-100">Wohnung Münster</a><br />
				<a href="/nordrhein-westfalen/muenster-wohnung-kaufen.html" class="text-100">Wohnung kaufen Münster</a><br />
				<a href="/nordrhein-westfalen/muenster-wohnung-suchen.html" class="text-100">Wohnungssuche Münster</a><br />
				<a href="/nordrhein-westfalen/immobilien-mieten.html" class="text-100">Immobilie mieten Nordrhein-Westfalen</a><br />
				<a href="/nordrhein-westfalen/immobilien-kaufen.html" class="text-100">Immobilie kaufen Nordrhein-Westfalen</a><br />
				<a href="/nordrhein-westfalen/wohnung-mieten.html" class="text-100">Wohnung Nordrhein-Westfalen</a><br />
				<a href="/nordrhein-westfalen/wohnung-kaufen.html" class="text-100">Wohnung kaufen Nordrhein-Westfalen</a><br />
				</div>
			</div>
		</div>
	</div>
</div>
					</main>

					


<div class="container-fluid max-page-width hidden-print">
	<div class="row">
		
		
		
			<div id="ads-footer" class="immonetads col-xs-12 text-center"></div>
		
	</div>
</div>









<div class="container-fluid margin-bottom-12 max-page-width">
	<div class="col-xs-12 padding-right-15 padding-left-15 padding-right-md-24 padding-left-md-24">
		<p class="text-gray-dark text-100 brand-disclaimer">Die Immowelt Hamburg GmbH übernimmt keine Gewähr für von Dritten gemachte Angaben.<span class="pull-right small hidden-xs hidden-print"><a  onclick="immonet.commons.slideTo('#top', -60);" href="#top">Zum Seitenanfang <span class="padding-left-6 ico icoChevronUp50 small"></span></a></span></p>
	</div>
</div>


<nav class="visible-xs navbar bg-overlay-dark navbar-fixed-bottom hidden-print">
	<div class="navbar-header text-center">
		
			
			
				
					<button id="btn-int-broker-phone-open-modal" class="btn btn-default ico icoTelephone ico-padding-right-6" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#modalPhone" aria-disabled="false" onclick="immonet.tracking.trackPhone(); immonet.expose.writeStatistic('PHONEHIT',34443703,655587, '98081e39-c331-47b3-a8b3-5587fd86d480');">Anrufen</button>
				
				<button id="contactBrokerBtnFooter"
					class="btn btn-action"
					data-backdrop="static"
					data-keyboard="false"
					aria-disabled="false"
					onclick="immonet.expose.openContactModal('//www.immonet.de/angebot', '34443703', '164077', false, null, '');immonet.tealium.trackOpenModal();">Anbieter kontaktieren</button>
			
		
	</div>
</nav>



<!-- footer --><footer class="container-fluid max-page-width hidden-print">

	<div class="row bg-overlay-dark-solid box-50">

		<div>

			<!-- newsletter -->
			<div class="col-xs-12 col-sm-6 box-50 margin-bottom-24">
				<p class="headline-50 text-80 text-white padding-bottom-6">Jetzt den Newsletter erhalten.</p>
				<div class="bg-overlay-dark">
					<a id="lnk-int-footer-newsletter" title="Newsletter bestellen" class="col-xs-12 col-sm-8 btn btn-80 btn-default blank" href="//www.immonet.de/newsletter/bestellen.html">kostenlos bestellen</a>
				</div>
			</div>

			<!-- link list -->
			<div class="col-xs-12 col-sm-6 margin-bottom-24">
				<div class="col-xs-6 col-sm-4 box-45">
						<div class="border-bottom-grey-light">

							

							<a class="block padding-top-9 padding-right-12 padding-bottom-9 padding-left-0 footer-link text-left text-50 text-gray-light" id="lnk-int-footer-hilfefaq" href="//www.immonet.de/service/hilfe-und-faq.html" title="Hilfe &amp; FAQ" target="_self">Hilfe &amp; FAQ</a>
						</div>
				</div>
				<div class="col-xs-6 col-sm-4 box-45">
						<div class="border-bottom-grey-light">

							

							<a class="block padding-top-9 padding-right-12 padding-bottom-9 padding-left-0 footer-link text-left text-50 text-gray-light" id="lnk-int-footer-kontakt" href="//www.immonet.de/service/kontakt.html" title="Kontakt" target="_self">Kontakt</a>
						</div>
				</div>
				<div class="col-xs-6 col-sm-4 box-45">
						<div class="border-bottom-grey-light">

							

							<a class="block padding-top-9 padding-right-12 padding-bottom-9 padding-left-0 footer-link text-left text-50 text-gray-light" id="lnk-int-footer-agb" href="//www.immonet.de/service/agb.html" title="AGB" target="_self">AGB</a>
						</div>
				</div>
				<div class="col-xs-6 col-sm-4 box-45">
						<div class="border-bottom-grey-light">

							

							<a class="block padding-top-9 padding-right-12 padding-bottom-9 padding-left-0 footer-link text-left text-50 text-gray-light" id="lnk-int-footer-datenschutz" href="//www.immonet.de/service/datenschutz.html" title="Datenschutz" target="_self">Datenschutz</a>
						</div>
				</div>
				<div class="col-xs-6 col-sm-4 box-45">
						<div class="border-bottom-grey-light">

							

							<a class="block padding-top-9 padding-right-12 padding-bottom-9 padding-left-0 footer-link text-left text-50 text-gray-light" id="lnk-int-footer-impressum" href="//www.immonet.de/service/impressum.html" title="Impressum" target="_self">Impressum</a>
						</div>
				</div>
				<div class="col-xs-6 col-sm-4 box-45">
						<div class="border-bottom-grey-light">

							<a class="block padding-top-9 padding-right-12 padding-bottom-9 padding-left-0 footer-link text-left text-50 text-gray-light" id="lnk-ext-footer-mediadaten" href="https://www.immowelt-media.de/" title="Mediadaten" rel="nofollow" target="_blank">Mediadaten</a>

							
						</div>
				</div>
				<div class="col-xs-6 col-sm-4 box-45">
						<div class="border-bottom-grey-light">

							

							<a class="block padding-top-9 padding-right-12 padding-bottom-9 padding-left-0 footer-link text-left text-50 text-gray-light" id="lnk-int-footer-jobkarriere" href="https://jobs.immonet.de" title="Job &amp; Karriere" target="_blank">Job &amp; Karriere</a>
						</div>
				</div>
				<div class="col-xs-6 col-sm-4 box-45">
						<div class="border-bottom-grey-light">

							

							<a class="block padding-top-9 padding-right-12 padding-bottom-9 padding-left-0 footer-link text-left text-50 text-gray-light" id="lnk-int-footer-ueberimmonet" href="//www.immonet.de/service/unternehmen.html" title="Über Immonet" target="_self">Über Immonet</a>
						</div>
				</div>
				<div class="col-xs-6 col-sm-4 box-45">
						<div class="border-bottom-grey-light">

							

							<a class="block padding-top-9 padding-right-12 padding-bottom-9 padding-left-0 footer-link text-left text-50 text-gray-light" id="lnk-int-footer-nutzungsbasierteonlinewerbung" href="//www.immonet.de/service/nutzungsbasierte-online-werbung.html" title="Nutzungsbasierte Online-Werbung" target="_self">Nutzungsbasierte Online-Werbung</a>
						</div>
				</div>
			</div>

			<!-- social media-->
			<div class="col-xs-12 box-50 margin-bottom-12">
				<div class="text-center">
						<a id="lnk-ext-facebook-xs" href="https://www.facebook.com/immonet" target="_blank" rel="nofollow" title="Immonet auf Facebook" class="ico ico-size-30 icoFacebookCir ico-footer-xs-social ico-footer-sm-social text-gray"></a>
						<a id="lnk-ext-twitter-xs" href="https://twitter.com/immonet" target="_blank" rel="nofollow" title="Immonet auf Twitter" class="ico ico-size-30 icoTwitterCir ico-footer-xs-social ico-footer-sm-social text-gray"></a>
						<a id="lnk-ext-xing-xs" href="https://www.xing.com/go/login/companies/immonetgmbh" target="_blank" rel="nofollow" title="Immonet bei XING" class="ico ico-size-30 icoXingCir ico-footer-xs-social ico-footer-sm-social text-gray"></a>
						<a id="lnk-ext-linkedin-xs" href="https://www.linkedin.com/company/immonet-gmbh" target="_blank" rel="nofollow" title="Immonet bei LinkedIn" class="ico ico-size-30 icoLinkedinCir ico-footer-xs-social ico-footer-sm-social text-gray"></a>
						<a id="lnk-ext-pinterest-xs" href="https://www.pinterest.com/immonet" target="_blank" rel="nofollow" title="Immonet in Pinterest" class="ico ico-size-30 icoPinterestCir ico-footer-xs-social ico-footer-sm-social text-gray"></a>
						<a id="lnk-ext-phone-xs" href="tel:+494034728900" class="ico ico-size-30 icoPhoneCir ico-footer-xs-social ico-footer-sm-social text-gray"><span class="phonenumber hidden-xs text-180">040 - 347 289 00</span></a>
				</div>
			</div>

			<!-- sitemap and other links -->
			<div class="col-xs-12 box-45 margin-bottom-12 text-center text-10 text-gray">
				<span>
					
					<a class="text-gray" id="lnk-int-footer-sitemap" href="//www.immonet.de/sitemap.html" title="Immobilien">Sitemap</a>
				</span>
				<span>
					<span>|</span>
					<a class="text-gray" id="lnk-int-footer-metropolen" href="//www.immonet.de/immobilien-metropolen.html" title="Metropolen">Metropolen</a>
				</span>
				<span>
					<span>|</span>
					<a class="text-gray" id="lnk-int-footer-grossstaedte" href="//www.immonet.de/immobilien-grossstaedte.html" title="Großstädte">Großstädte</a>
				</span>
			</div>

			<!-- Copyright -->
			<div class="col-xs-12 box-50 text-center text-gray text-10">
				<span>&#169; Copyright Immowelt Hamburg GmbH 2002 - <span>2018</span></span>
				<span>: Ihr Partner im Bereich Immobilien und Wohnungen</span>
				<span>
					<span class="hidden-xs"> | </span> Version: <span>1.3.91</span>
					<span class="visible-xs-inline">xs</span>
					<span class="visible-sm-inline">sm</span>
					<span class="visible-md-inline">md</span>
					<span class="visible-lg-inline">lg</span>
				</span>

			</div>

		</div>

	</div>

	<!-- logos -->
	<div class="row bg-white text-center box-150">

		<div>
	<div class="headline-50 padding-top-12">Immowelt Group</div>

	<ul class="footer-logo-list box-145">
		<li class="iwde">
			<img alt="immowelt.de" title="immowelt.de" src="//cdn.immonet.de/frontend-resources/4.7.1/images/footer_logos_sprite.png" />
		</li>
		<li class="inde">
			<img alt="immonet.de" title="immonet.de" src="//cdn.immonet.de/frontend-resources/4.7.1/images/footer_logos_sprite.png" />
		</li>
		<li class="iwat">
			<img alt="immowelt.at" title="immowelt.at" src="//cdn.immonet.de/frontend-resources/4.7.1/images/footer_logos_sprite.png" />
		</li>
		<li class="iwch">
			<img alt="immowelt.ch" title="immowelt.ch" src="//cdn.immonet.de/frontend-resources/4.7.1/images/footer_logos_sprite.png" />
		</li>
		<li class="baun">
			<img alt="bauen.de" title="bauen.de" src="//cdn.immonet.de/frontend-resources/4.7.1/images/footer_logos_sprite.png" />
		</li>
		<li class="imso">
			<img alt="immosolve" title="immosolve" src="//cdn.immonet.de/frontend-resources/4.7.1/images/footer_logos_sprite.png" />
		</li>
		<li class="umza">
			<img alt="umzugsauktion" title="umzugsauktion" src="//cdn.immonet.de/frontend-resources/4.7.1/images/footer_logos_sprite.png" />
		</li>
		<li class="croz">
			<img alt="crozilla.com" title="crozilla.com" src="//cdn.immonet.de/frontend-resources/4.7.1/images/footer_logos_sprite.png" />
		</li>
		<li class="wgde">
			<img alt="wohngemeinschaft.de" title="wohngemeinschaft.de" src="//cdn.immonet.de/frontend-resources/4.7.1/images/footer_logos_sprite.png" />
		</li>
	</ul>
</div>

	</div>

	<!-- HeaderFooter App-Version 1.2.19, Page was generated at 10/Aug/2018 13:04 -->

</footer>


<!-- spacer for sticky footer -->
<div class="col-xs-12 margin-bottom-24 margin-bottom-sm-0"></div>




<script>
	var immonet = immonet || {};

	immonet.tracking = {
		trackPhone: function() {
			var timestamp = Date.now();
			try{
			immonet.tag.trackEvent({
				"event_type": "phone_contact",
                "object_variant":["Phonecontact"],
				"order_id":["98081e39-c331-47b3-a8b3-5587fd86d480/" + timestamp],
				"product_price":["0.00"],
				"product_quantity" : ["1.00"],
				"broker_guid":["279129"],
				"page_type" : "thankyoupage",
                "object_listingtype":["standard"]
			});
			}catch(err) {}

			dataLayer.push({
				"event": "trackPhone",
				"transactionId": "34443703",
				"transactionTotal": 0.00,
				"transactionProducts": [ {
					"sku": "Phonecontact/48155/Münster",
					"name": "Phonecontact",
					"category": "Phonecontact/Nordrhein-Westfalen/Miete/Wohnen",
					"price": 0.00,
					"quantity": 1.00
				} ]
			});
			
			
		},

		trackPhoneCall: function() {
			var timestamp = Date.now();
			try{
			immonet.tag.trackEvent({
				"event_type": "call",
				"object_variant":["Call"],
				"order_id":["98081e39-c331-47b3-a8b3-5587fd86d480/" + timestamp],
				"product_price":["0.00"],
				"product_quantity" : ["1.00"],
				"broker_guid":["279129"],
				"page_type" : "thankyoupage"
			});
			}catch(err) {}
		}
	}
	
</script>





<script>
	var immonet = immonet || {};

	immonet.tealium = {

		trackOpenModal: function() {
			// track open modal contact
			try {
				immonet.tag.trackPageView({'page_type':'contactpage', 'event_type' : 'page_view', 'page_name':'expose/' + 'wohnen' + '/contact'});
			} catch(e) {
				// nothing to do
			}
		}
	}

</script>











<div id="contact-teaser" class="immogrowl immogrowl-bottom-left hidden cursor-hand hidden-print">
	<div class="bg-white padding-15 display-flex">
		
			
				<div id="lnk-int-growl-expose-contact" class="display-flex trigger-contact" onclick="immonet.expose.openContactModal('//www.immonet.de/angebot', '34443703', '164077', false, null, ''); immonet.tealium.trackOpenModal(); return false;">
					<span class="ico icoClockCir ico-size-45 text-primary padding-right-15"></span>
					<p class="padding-right-15 text-primary">Sichern Sie sich jetzt einen Besichtigungstermin.</p>
				</div>
			
			
			
		
		<a class="ico icoCrossCir ico-size-18 cursor-hand trigger-contact"></a>
	</div>
</div>







<script>
	toast(['//cdn.immonet.de/frontend-resources/4.8.0/lib/polyfill/matchMedia.js', function() {
		return !Modernizr.matchmedia;
	}]);

	toast(
		'//cdn.immonet.de/frontend-resources/4.8.0/immonet/js/headerfooter/v2/headerfooter.js',
	 	'https://umziehen.immonet.de/costcalculator-client/js/costcalculator-in-1.0.69.js',
		'https://umziehen.immonet.de/config/config.js',
		'//cdn.immonet.de/frontend-resources/4.8.0/immonet/js/expose-footer.min.js?v=337',
		function() {
			
			immonet.mediaquery.init();
			immonet.expose.init('//www.immonet.de/angebot','34443703','1','164077', false, false, false);
			

			
				immonet.headerfooter.initLogoRotation([{
					duration : 15000,
					id : 'tsr-int-logo-xs',
					url : '/',
					image: 'https://www.immonet.de/immonet/img/logo/logo.svg'
				},{
					type : 'regio',
					duration : 3000,
					id : 'lnk-ext-regiopartner-we',
					url : 'http://www.welt.de/',
					image: 'https://www.immonet.de/immonet/img/mandanten/we.png'
				}
				]);
			
			immonet.exposeSearchAgent.setSearchParams('');

			jQuery('#searchagentFormEmail').on("change", function() {
				jQuery('#searchagentFormEmail').parent().removeClass('has-error');
				jQuery('#searchagentErrorText').addClass('hide');
			});

			
            	immonet.energy.init(49.19, 48155);
			

			// Growl for making contact
			var immogrowl = jQuery('#contact-teaser');
			var immogrowlTimer = setTimeout(function() {
			    if (jQuery('#lnk-int-growl-expose-capital').hasClass('capitalInvestmentExpose')) {
                    utag.link({
                        "promotion_id": ["523"],"promotion_name": ["kapitalanlage"],
                        "promotion_creative" : ["kostenlos beraten"],
                        "promotion_position" : ["expose-growl"],
                        "event_category": "ecommerce",
                        "event_action": "promotion"
                    });
				}
				immogrowl.removeClass('hidden');
			}, 30000);

			jQuery('.trigger-contact').on('click', function() {
				clearTimeout(immogrowlTimer);
				immogrowl.addClass('hidden');
			});
		});
</script>


<script src="https://script.ioam.de/iam.js"></script>


<script>
	var iomData = {
		"st": "welt",
		"cp": "09125_immART",
		"sv": "in"
	};
	if (typeof immonetViewport !== 'undefined' && immonetViewport.getViewport() !== 'lg') {
		iomData = {
			"st": "welt",
			"cp": "09125_immART",
			"sv": "in"
		};
	}
	iom.c(iomData);
</script>
</div>






<div id="modalPhone" class="modal fade in modal-fullscreen-xs-only" tabindex="-1" role="dialog" aria-hidden="true">
	<div id="modalPhoneDialog" class="modal-dialog bg-white">
		<div id="modalPhoneContent" class="modal-content">
			<a data-dismiss="modal" class="btn-close ico icoCrossCir cursor-hand h3" href="#"></a>
			<div class="modal-header padding-bottom-0 padding-top-12 padding-top-sm-24 padding-left-sm-24 padding-right-sm-24">
				<p role="heading" class="headline-290">Anbieter anrufen</p>
			</div>
			<div class="modal-body padding-top-24 padding-sm-24 ">
				<div class="bg-default-sm col-xs-12">
					<div class="col-xs-12 col-md-6">
						
							<div class="col-xs-12 col-md-6 margin-bottom-15  padding-right-md-12">
								
								
								
								
								

								
									<p id="bdContactModalName" class="text-200">
										Kundenservice
									</p>
								
								
									<p id="bdcontactPhoneModalStreet" class="text-100">
										Philippstr. 3
									</p>
								
								
									<p id="bdcontactPhoneModalZipCity" class="text-100">
										
										44803 Bochum
									</p>
								
								
								
									<a id="btn-call-phone-1" onclick="immonet.tracking.trackPhoneCall();" href="tel:0234%204147000-05" class="col-xs-12 btn btn-secondary text-gray-darker ico icoTelephone ico-padding-right-6 margin-bottom-6 margin-top-6">0234 4147000-05</a>
								
								
							</div>
						
						
							<div class="col-xs-12 col-md-6 margin-bottom-15 padding-top-sm-24 padding-top-md-0">
								
								
								
								
								

								
								
									<p class="text-200">
										
											
												
													
													
														<a href="https://www.immonet.de/anbieter/682790" id="bdModalfirmName"><span id="bdBrokerModalFirmname">Vonovia Kundenservice GmbH</span></a>
													
												
											
											
										
									</p>
								
								
								
									<p id="bdBrokerModalStreet" class="text-100">
										Philippstr. 3
									</p>
								
								
									<p id="bdBrokerModalZipCity" class="text-100">
										44803 Bochum
									</p>
								
								
								
							</div>
						
					</div>
				</div>
			</div>
		</div>
	</div>
</div>




<div id="modalContact" class="modal fade in modal-fullscreen-xs-only" tabindex="-1" role="dialog" aria-hidden="true">
	<div id="modalContactDialog" class="modal-dialog bg-white">
		<div id="modalContactContent" class="modal-content bg-white box-100">
			<div class="col-xs-12 text-center" style="z-index: 2000">
				<div class="progress-bubbles center-block"></div>
			</div>
		</div>
	</div>
</div>
<div id="modalSearchagent" class="modal fade in" tabindex="-1" role="dialog" aria-hidden="true">

	<div id="modalSearchagentDialog" class="modal-dialog bg-white">
		<div id="modalSearchagentContent" class="modal-content bg-white box-100">
		<a class="btn-close ico icoCrossCir cursor-hand h3" style="top: 0px; text-align: right; right: 0px; display: block; position: relative;" href="#" data-dismiss="modal"></a>

			<div id="searchAgentFormBox">



<form method="post" id="searchagentForm" class="margin-bottom-0" onsubmit="immonet.exposeSearchAgent.send(); return false;">
	<div class="text-center">
		<h2 class="padding-left-24 headline-250 margin-bottom-15">Neue Angebote erhalten</h2>
		<div class="last-search margin-bottom-15"></div>
	</div>
	<div id="selSagent_mobile" class="sa-tsr inverted padding-15 margin-bottom-30">

		<div  class="col-xs-12 margin-bottom-12">
			<div class="cursor-hand text-gray-darker text-225 margin-bottom-12" for="searchagentFormEmail">Per E-Mail</div>
			<input type="email" class="form-control" name="mailaddress" id="searchagentFormEmail" maxlength="70" placeholder="Bitte E-Mail-Adresse eingeben" value=""/>
			<span id="searchagentErrorText" class="text-danger small hide">Bitte korrigieren Sie Ihre Eingabe.</span>
		</div>


		
		<div id="telefonOptin" class="clearfix margin-top-12">
			<input type="checkbox" id="searchagentNewsletter" class="pull-left margin-right-12 cursor-hand" name="searchagentForm.searchagentNewsletter"/>
			<p class="padding-left-24 small">
				Ja, ich möchte auch den kostenlosen Newsletter der Immowelt AG per E-Mail. Ich kann meine Einwilligung jederzeit widerrufen.
			</p>
		</div>

		<div class="clearfix  margin-top-12">
			<input type="submit" id="btn-int-suchagent-searchagentsubmit" class="btn btn-action pull-right col-xs-12 col-auto-sm" value="Jetzt senden" onclick="immonet.exposeSearchAgent.setNewsletter(jQuery('#searchagentNewsletter').prop('checked'));"/>
		</div>

		
		<p class="small margin-top-12">
			Mit dem Suchauftrag nehmen Sie den Immonet-Service in Anspruch. Sie erhalten anhand der von Ihnen eingegebenen Daten, genutzten Services und auf Grundlage unseres Geschäftszwecks auf Ihr Anliegen ausgerichtete Informationen. Diesem Service können Sie jederzeit unter <a href="mailto:datenschutz@immonet.de">datenschutz@immonet.de</a> widersprechen. Weitere Informationen finden Sie in der <a href="https://www.immonet.de/service/datenschutz.html" title="Datenschutzerklärung" target="_blank">Datenschutzerklärung</a>.
		</p>
	</div>
</form>
</div>
			<div id="searchAgentSuccessBox" class="hide">





<div id="selSagent_mobile" class="sa-tsr inverted padding-15 margin-bottom-30 margin-top-12">

	<div id="success" class="modal-body padding-0">
		<div class="display-flex">
			<div class="ico ico-size-24 icoSuccess"></div>
			<div class="col-xs-12">
				<h2 class="headline-290 margin-bottom-6 text-gray-darker">Vielen Dank!</h2>
				<h4 class="text-250 text-gray-darker">Ihr Suchauftrag wurde gespeichert.</h4>
			</div>
		</div>
		<div class="clearfix"></div>
		
			<div class="col-xs-12 margin-top-24 margin-bottom-12">
				<p>Sie erhalten neue Angebote, sobald Sie auf den Link in der Bestätigungs-E-Mail geklickt haben.</p>
			</div>
		
		
	</div>
</div>
</div>


			</div>
	</div>
</div>




<div id="modalInfoSparkasse" class="modal fade in modal-fullscreen-xs-only" tabindex="-1" role="dialog" aria-hidden="true">
	<div id="modalInfoSparkasseDialog" class="modal-dialog bg-white">
		<div id="modalInfoSparkasseContent" class="modal-content bg-white box-100">
			<a data-dismiss="modal" class="btn-close ico icoCrossCir cursor-hand h3" href="#"></a>
			<div class="modal-header padding-top-0 padding-right-0 padding-bottom-18 padding-left-0">
				<p role="heading" class="headline-290">Unverbindliche Beispielrechnung</p>
			</div>
			<div class="modal-body padding-0">
				<div class="col-xs-12">
					<p id="spk-rate-info" class="text-100"></p>
				</div>
			</div>
		</div>
	</div>
</div>





<div id="modalInfoFinancing" class="modal fade in modal-fullscreen-xs-only" tabindex="-1" role="dialog" aria-hidden="true">
	<div id="modalInfoFinancingDialog" class="modal-dialog bg-white">
		<div id="modalInfoFinancingContent" class="modal-content bg-white box-100">
			<a data-dismiss="modal" class="btn-close ico icoCrossCir cursor-hand h3" href="#"></a>
			<div class="modal-header padding-top-0 padding-right-0 padding-bottom-18 padding-left-0">
				<p role="heading" class="headline-290">Unverbindliche Beispielrechnung</p>
				<p class="text-100">Bitte beachten Sie die für die Beispielrechnung getroffenen Annahmen:</p>
			</div>
			<div class="modal-body padding-0">
				<div data-display-type="" data-display-name="" data-display-variant="" data-display-attributes="" class="margin-bottom-24"></div><div data-display-type="" data-display-name="" data-display-variant="" data-display-attributes="" class="margin-bottom-12 margin-top-24"><p class="headline-100 ">Eigenkapital</p></div><p class="text-50">Mit der Angabe Ihres tatsächlichen Eigenkapitals können Sie die monatliche Rate variieren.</p><p class="text-50">Die unverbindliche Berechnung ist lediglich ein Richtwert für eine mögliche Finanzierung.</p><p class="text-50">Wenn Sie das Feld leer lassen und auf &quot;Berechnen&quot; klicken, wird automatisch wieder die Immonet-Beispielrechnung verwendet.</p><div data-display-type="" data-display-name="" data-display-variant="" data-display-attributes="" class="margin-bottom-12 margin-top-24"><p class="headline-100">Maklercourtage</p></div><p class="text-50">Sollte bei dem Angebot keine Maklercourtage angegeben sein, dann wird der Prozentsatz für die Maklercourtage abhängig vom angegebenen Standort berechnet.</p><div data-display-type="" data-display-name="" data-display-variant="" data-display-attributes="" class="margin-bottom-12 margin-top-24"><p class="headline-100">Nebenkosten</p></div><p class="text-50">Als Nebenkosten werden 2,0% Notargebühren sowie 5,0% Grunderwerbsteuer* angenommen. <br> (* Schleswig-Holstein, Brandenburg, Thüringen, Saarland und Nordrhein-Westfalen 6,5%, Berlin und Hessen 6,0%, Hamburg 4,5%, Bayern und Sachsen 3,5%.)</p><p class="text-50">Das Beispiel basiert außerdem auf einem Sollzinssatz von 1,5% und einer anfänglichen jährlichen Tilgung von 2% p.a. <br> Der Nettodarlehensbetrag (benötigtes Darlehen) ergibt sich somit aus dem Finanzierungsrechner. <br> Der Zinsfestschreibungszeitraum beträgt 10 Jahre. Nach der Sollzinsbindung kann ein neuer gebundener Sollzins, aber auch ein veränderlicher Sollzins vereinbart werden.</p><p class="text-50">Die hier angezeigte unverbindliche Beispielberechnung ist lediglich ein Richtwert für eine mögliche Finanzierung.</p><div data-display-type="" data-display-name="" data-display-variant="" data-display-attributes="" class="margin-bottom-12 margin-top-24"><p class="headline-100">Monatliche Rate</p></div><p class="text-50">Bitte beachten Sie die für die Beispielrechnung getroffenen Annahmen:</p><p class="text-50">Eine solide Finanzierung sollte aus mindestens 20% Eigenkapital bestehen. Der Beispielrechnung liegen in der Grundeinstellung 30% Eigenkapital zugrunde. Sie haben die Möglichkeit die Höhe des Eigenkapitals zu verändern.</p>
			</div>
		</div>
	</div>
</div>




<div id="modalAbuse" class="modal fade in modal-fullscreen-xs-only" tabindex="-1" role="dialog" aria-hidden="true">
	<div id="modalAbuseDialog" class="modal-dialog bg-default">
		<div id="modalAbuseContent" class="modal-content bg-default">
			<div class="col-xs-12 text-center" style="z-index: 2000">
				<div class="progress-bubbles center-block"></div>
			</div>
		</div>
	</div>
</div>




<div id="modalImprint" class="modal fade in modal-fullscreen-xs-only" tabindex="-1" role="dialog" aria-hidden="true">
	<div id="modalImprintDialog" class="modal-dialog bg-white">
		<div id="modalImprintContent" class="modal-content bg-white box-100"></div>
	</div>
</div>






<script>
	function initModalMap() {
		var mapConfig = {
			geoLocation: {
				lat: 51.95274,
				lng: 7.66586
			},
			zoom: 17,
			mapContainerElementId: 'modal-map-body',
			addressLayer: {
				wrapper: {
					styleClasses: ['popover', 'google-maps-popover', 'address-layer', 'text-center'],
					elementId: 'popover-map'
				},
				content: { styleClasses: ['text-100'] }
			}
		};

		// prepare addressLayer
		
			mapConfig.isMarkerVisible = true;
			mapConfig.addressLayer.content.text = "" +
				"Stehrweg 27<br />"
                
                    + "48155&nbsp;"
                
				+ "Münster";
			
			
				
                    mapConfig.addressLayer.anchor = {
                        styleClasses: ['trigger-contact', 'lnk-int-map-contact-visit', 'margin-top-12', 'btn', 'btn-default', 'btn-80', 'hidden-print', 'hidden-xs'],
                        text: "Besichtigungstermin vereinbaren",
                        link: '#',
                        onclick: "immonet.expose.openContactModal('//www.immonet.de/angebot', '34443703','164077', '', event, ''); immonet.tealium.trackOpenModal(); return false;",
                        elementId: 'lnk-int-map-contact-visit-fullscreen'
                    };
				
			
		
		
		var modalMapConfig = {
			mapConfig: mapConfig,
			modalContainerElementId: 'modal-map',
			modalBehaviour: 'bootstrap'
		};
		modalMapConfig.modalTriggerElementId = 'tsr-int-map-anchor-xs-icon';
		gmaps.initModalMap(modalMapConfig);
		modalMapConfig.modalTriggerElementId = 'tsr-int-map-anchor-xs-link';
		gmaps.initModalMap(modalMapConfig);
	}
</script>







<div id="modal-map" class="modal fade modal-fullscreen-xs-only" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content bg-white">
			<a href="#" class="btn-close ico icoCrossCir cursor-hand h3" data-dismiss="modal"></a>
			<div class="modal-header"></div>

			
			<div id="modal-map-body" class="modal-body"></div>
		</div>
	</div>
</div>


</div>




























</div>
		</body>
</html>`
