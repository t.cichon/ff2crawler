exports.data = `
<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="UTF-8">
    <title>!! Gemütliche Altbauwohnung mit großem Balkon in zentraler Lage... in Bremen (Stadt) - Burg-Grambke | Etagenwohnung mieten | eBay Kleinanzeigen</title>
    <link rel="SHORTCUT ICON" href="/static/img/favicon.1hf6kaeaeu89h.png" type="image/png"/>
    <script type="text/javascript">window.pageType = 'VIP';</script>

    <meta name="description" content="!! Gemütliche Altbauwohnung mit großem Balkon in zentraler Lage !!

Lagebeschreibung:
Dieses...,!! Gemütliche Altbauwohnung mit großem Balkon in zentraler Lage... in Bremen (Stadt) - Burg-Grambke"/>
    <meta property="al:ios:url" content="ebayk://s-anzeige/-gemuetliche-altbauwohnung-mit-grossem-balkon-in-zentraler-lage-/921550031-203-34?utm_source=SmartBanner&utm_medium=web&utm_campaign=SmartBanner"/>
        <meta property="al:ios:app_store_id" content="382596778"/>
        <meta property="al:ios:app_name" content="eBay Kleinanzeigen - Kostenlos. Einfach. Lokal."/>
    <link rel="canonical" href="https://www.ebay-kleinanzeigen.de/s-anzeige/-gemuetliche-altbauwohnung-mit-grossem-balkon-in-zentraler-lage-/921550031-203-34"/>
    <link rel="alternate" href="ios-app://382596778/ebayk/s-anzeige/-gemuetliche-altbauwohnung-mit-grossem-balkon-in-zentraler-lage-/921550031-203-34"/>
    <link rel="alternate" href="android-app://com.ebay.kleinanzeigen/ebayk/s-anzeige/-gemuetliche-altbauwohnung-mit-grossem-balkon-in-zentraler-lage-/921550031-203-34"/>
    <link rel="alternate" media="only screen and (max-width: 640px)"
              href="https://m.ebay-kleinanzeigen.de/s-anzeige/-gemuetliche-altbauwohnung-mit-grossem-balkon-in-zentraler-lage-/921550031-203-34"/>
    <meta name="_csrf" content="e288d6fd-b9ed-4a8f-b817-4b2de9890c9b"/>
        <meta name="_csrf_header" content="X-CSRF-TOKEN"/>
    <meta name="robots" content="index, follow"/>
        <link rel="search" type="application/opensearchdescription+xml" title="eBay Kleinanzeigen"
          href="/static/xml/opensearch.1kpmfaurxnu52.xml"/>

    <meta name="p:domain_verify" content="91e8305109f591220127ad7b21c06ae1"/>

    <meta property="fb:app_id" content="129026183788759"/>
    <meta property="fb:page_id" content="118074381563675"/>
    <meta property="og:site_name" content="eBay Kleinanzeigen"/>
    <meta property="og:country-name" content="Germany"/>

    <meta property="og:image" content="https://i.ebayimg.com/00/s/MzM3WDQ1MA==/z/UdUAAOSwIOtbbIQk/$_20.JPG"/>
        <meta property="og:type" content="product"/>
        <meta property="og:description" content="!! Gemütliche Altbauwohnung mit großem Balkon in zentraler Lage !!

Lagebeschreibung:
Dieses...,!! Gemütliche Altbauwohnung mit großem Balkon in zentraler Lage... in Bremen (Stadt) - Burg-Grambke"/>
        <meta property="og:title" content="!! Gemütliche Altbauwohnung mit großem Balkon in zentraler Lage..."/>
        <meta property="og:url" content="https://www.ebay-kleinanzeigen.de/s-anzeige/-gemuetliche-altbauwohnung-mit-grossem-balkon-in-zentraler-lage-/921550031-203-34"/>
    <meta property="og:latitude" content="53.15461999999999"/>
    <meta property="og:longitude" content="8.70455"/>
    <meta property="og:locality" content="Bremen (Stadt) - Burg-Grambke"/>
    <meta property="og:region" content="Bremen"/>
    <meta name="msvalidate.01" content="FB821594C9F8B6D5BA9D0847E8838D8A"/>

    <meta name="google-site-verification" content="mKHxIfCqo8NfW85vQla0XDNT2FPIEt85-o_DsUrrMpA"/>

    <!--[if lt IE 9]>
    <script src="/static/js/selectivizr-min.bozw3bl1a87n.js"></script>
    <![endif]-->


    <link rel="stylesheet" type="text/css" href="/static/css/all.1nui2ywidoe0v.css"/>

    <!--[if lt IE 9]>
    <script type="text/javascript" src="/static/js/belen/common/Html5.1nn1xuerkfkjt.js"></script>
    <![endif]-->

    <script type="text/javascript" src="/static/js/belen/tracking/advertising.3jh3tmrxn2t1.js"></script>

    <script type="application/ld+json">
            {  "@context" : "http://schema.org",
               "@type" : "WebSite",
               "name" : "eBay Kleinanzeigen",
               "url" : "https://www.ebay-kleinanzeigen.de"
            }
    </script>

    <script type="text/javascript">
        </script>

    <script type="text/javascript">
        window.BelenConf = {
            jsBaseUrl: '/static/js',
            prebidVersion: '1-12-0',
            prebidFileSrc: '/static/js/belen/advertiser/lib/prebid-1-12-0.6xxm8cjuggvh.js',
            isBrowse: 'true',
            ebdaEnabled: false,
            skyLeftStickyEnabled: false
        };
    </script>

    <script src="https://www.google.com/recaptcha/api.js" async defer></script>

    <script type="text/javascript" src="/static/js/top.1jb9f3iceb9n3.js"></script>

            <script type="text/javascript">
                window.belenScripts = [];
                
                        window.belenScripts.push("/static/js/base.1f432auxqu7v5.js");
                    
                window.belenScripts.push("/static/js/vap-all.17237u505z55i.js");
                
                window.belenScripts.push("/static/js/reinsert-all.c50o5gpny2bz.js");
                </script>
        <script type="text/javascript">
        


        var externalScripts = [];

        function handleDomReady() {
            $(document).ready(function () {
                // Tracking
                
                Belen.Tracking.initTrackingData({pageTypeShort:"p",pageId:"1605053",postingId:"921550031",sellerId:"37136318",locationId:"34",l1CategoryId:"195",l2CategoryId:"203",city:"Bremen (Stadt) - Burg-Grambke",adType2:"t",adAge:"0",adExpired:"n",adDescriptionLength:"3198",attributes:"1.0,200,70.00,true,1000,2.5,etagenwohnung,zentralheizung,700",apiUserId:"47169",adSenseChannel:"Total c_Immobilien r_Bremen p_47169 d_VIP_Total d_VIP_Immobilien d_yield_test_vip",agofPageType:"3"});

                
                Belen.TrackingDispatcher.cleanLastEvent();
                

                Belen.TrackingDispatcher.init();
                

                // init common view
                Belen.Common.CommonView.init();

                // init page view
                window.eventCategory = "VIP";

    Belen.Common.CommonView.initArticles();
    Belen.Search.ViewAdView.init({
    
        bannerOpts: {
  "cat": "195",
  "tcat": "203",
  "city": "Bremen__Stadt__Burg_Grambke",
  "plz": "28719",
  "page": "ViewItemPage",
  "lsc":   [
    203,
    195
  ],
  "lsk":   [
    "ikea markus",
    "molger",
    "trysil",
    "monitor wandhalterung"
  ],
  "region": "Bremen",
  "hn": "ebay-kleinanzeigen.de",
  "pt": "VIP",
  "ct": "203",
  "hu": "",
  "li": "0",
  "ptg": "1-similar-items-experiment-similar_items_experiment_vip-default,browser-referer-alerting-a_default-group_referer_alerting-off,2-testbanner-default,vip-buyer-survey-off,hompage-modal-survey-off,24-yield-optimization-medium-c,25-testgrouptest-distribution_test-b,27-yield-optimization-small-a,29-testgrouptest-old-alg-distribution_old_test-b,36-grpc-b-grpc-on,4-feature-packages-feature_packages-b-default-off,42-tmx-profiling-tmx-on,5-treebay2-treebay2-c-default,6-bigger-card-experiment-bigger-card-experiment-default,74-react-messagebox-off,8-myads-on-react-myads_on_react-d-react,9-sticky-banner-vip-sticky_banner_vip-c-generic_messaging_vip,96-dfpbanner-test,js-errorlog-default,98-prebid-update-default,99-test-support-default",
  "kw": "!!_Gemuetliche_Altbauwohnung_mit_grossem_Balkon_in_zentraler_Lage...",
  "Verkaeufer": "gewerblich",
  "Angebotstyp": "Angebot",
  "adid": "921550031",
  "posterid": "37136318",
  "Preis": "500",
  "ExactPreis": "500",
  "yo_s": "A",
  "yo_m": "C",
  "abtest": "test",
  "tb2": "C",
  "Anzahl_Badezimmer": "1.0",
  "Nebenkosten__€_": "200",
  "Wohnflaeche__m²_": "160",
  "Einbaukueche": "true",
  "Kaution_/_Genoss._Anteile__€_": "1000",
  "Zimmer": "2.5",
  "Wohnungstyp": "etagenwohnung",
  "Heizungsart": "zentralheizung",
  "Warmmiete__€_": "700"
},
    
    contactPosterEnabled: true,
    userLoggedIn: false,
    loginUrl: '/m-einloggen.html',
    loginForContactPosterUrl: 'https://www.ebay-kleinanzeigen.de/m-einloggen.html?targetUrl=%2Fs-anzeige%2F-gemuetliche-altbauwohnung-mit-grossem-balkon-in-zentraler-lage-%2F921550031-203-34',
    msgboxUrl: '/m-nachrichten.html',
    adExpired:false,
    viewAdCounterUrl: 'https://www.ebay-kleinanzeigen.de/s-vac-inc-get.json?adId=921550031&userId=37136318',
    adId:'921550031',
    openFlagWindow: false,
    
    category: 'Mietwohnungen',
    
    isPVAP: false,
    messages: {
    },
    showSecureMsgOverlay: false,
    showDogsWarningOverlay: false,
    showPublicTransportationWarningOverlay: false,
    showPausedVeil: false,
    });

                // Tracking scripts and pixels
                

                Belen.Common.CommonView.initOverlayCookies();

                Belen.onWindowLoaded(function () {

                    
                    $('body').append('');
                    

                });
            });
        }

        
        if (typeof (AdsenseLoader) != 'undefined' && AdsenseLoader != null) {
            AdsenseLoader.loadAds(["ads",{"pubId":"ebay-kleinanzeigen-de-vip","query":"!! Gemütliche Altbauwohnung mit großem Balkon in zentraler Lage... Mietwohnungen","adIconSpacingBefore":8,"colorText":"#535353","colorDomainLink":"#979797","colorTitleLink":"#0165D2","colorAdSeparator":"#e5e5e5","colorBorder":"#ffffff","clickableBackgrounds":true,"adsLabel":false,"linkTarget":"_blank","adIconUrl":"https://afs.googleusercontent.com/partner-ebay-kleinanzeigen-de/nopic.png","adIconLocation":"ad-left","adIconHeight":72,"adIconWidth":142,"numRepeated":0,"adIconSpacingAfter":10,"colorAttribution":"#fff","fontSizeAttribution":"5px","titleBold":true,"rolloverLinkUnderline":true,"borderSelections":"bottom","lineHeightDomainLink":25,"lineHeightTitle":25,"lineHeightDescription":25,"adSafe":"high","hl":"de","gl":"de","fontSizeAnnotation":13,"fontSizeDescription":13,"fontSizeDomainLink":13,"fontSizeTitle":13,"fontFamily":"helvetica neue, arial","siteLinks":true,"channel":"Total c_Immobilien r_Bremen p_47169 d_VIP_Total d_VIP_Immobilien d_yield_test_vip"},{"width":"970px","container":"vap_adsense-middle","number":1,"sellerRatings":true,"siteLinks":true,"longerHeadlines":true,"detailedAttribution":true},{"width":"970px","container":"vap_adsense-bottom","number":4,"sellerRatings":true,"siteLinks":true,"longerHeadlines":true,"detailedAttribution":true}]);

        }
        

        // init early page view (before document ready)
        
            LazyLoad.js("https://script.ioam.de/iam.js", function(){
                var iam_data = {
                    "st": "ebaykanz",
                    "cp": "ebayK-3-203",
                    "sv": "i2"
                };
                if (typeof iom !== 'undefined') {
                    iom.c(iam_data, 1);
                }
            });
        

        var universalAnalyticsOpts = {
            account: "UA-24356365-9",
            domain: "ebay-kleinanzeigen.de",
            userId: "",
            dimensions: [{"index":1,"value":"VIP"}, {"index":2,"value":"195"}, {"index":3,"value":"203"}, {"index":6,"value":"1"}, {"index":7,"value":"13481"}, {"index":8,"value":"13541"}, {"index":9,"value":"34"}, {"index":10,"value":"203"}, {"index":11,"value":"Wohnung_mieten"}, {"index":12,"value":"34"}, {"index":13,"value":"28719"}, {"index":23,"value":"false"}, {"index":25,"value":"similar_items_experiment_vip-DEFAULT;bigger-card-experiment-DEFAULT;distribution_test-b;treebay2-c-default;feature_packages-b-default-off;myads_on_react-d-react;sticky_banner_vip-c-generic_messaging_vip;web_adv-a-default;"}, {"index":30,"value":"921550031"}, {"index":31,"value":"500.00"}, {"index":32,"value":"FIXED"}, {"index":33,"value":"20"}, {"index":34,"value":"OFFERED"}, {"index":38,"value":"3198"}, {"index":39,"value":"COMMERCIAL"}, {"index":45,"value":"28719"}, {"index":46,"value":"53.14833;8.71103"}, {"index":90,"value":"Immobilien"}, {"index":91,"value":"Wohnung_mieten"}, {"index":94,"value":"Bremen"}, {"index":95,"value":"Bremen (Stadt)"}, {"index":96,"value":"Burg-Grambke"}, {"index":97,"value":"28719"}, {"index":103,"value":"Trade"}, {"index":108,"value":"{\\n  \\"wohnung_mieten.badezimmer_d\\" : \\"1.0\\",\\n  \\"wohnung_mieten.nebenkosten_i\\" : \\"200\\",\\n  \\"wohnung_mieten.qm_d\\" : \\"70.00\\",\\n  \\"wohnung_mieten.built_in_kitchen_b\\" : \\"true\\",\\n  \\"wohnung_mieten.kaution_i\\" : \\"1000\\",\\n  \\"wohnung_mieten.zimmer_d\\" : \\"2.5\\",\\n  \\"wohnung_mieten.wohnungstyp_s\\" : \\"etagenwohnung\\",\\n  \\"wohnung_mieten.heizungsart_s\\" : \\"zentralheizung\\",\\n  \\"wohnung_mieten.warmmiete_i\\" : \\"700\\"\\n}"}, {"index":123,"value":"none-none-none-none-none"}, {"index":124,"value":"similar_items_experiment_vip-DEFAULT"}, {"index":125,"value":"distribution_test-b"}, {"index":127,"value":"feature_packages-b-default-off"}, {"index":128,"value":"treebay2-c-default"}, {"index":129,"value":"bigger-card-experiment-DEFAULT"}, {"index":131,"value":"myads_on_react-d-react"}, {"index":132,"value":"sticky_banner_vip-c-generic_messaging_vip"}, {"index":135,"value":"web_adv-a-default"}]
        };
        
        if (window.noBlocker === true) {
            universalAnalyticsOpts.dimensions.push({
                "index": 73, "value": "0"
            });

        } else {
            universalAnalyticsOpts.dimensions.push({
                "index": 73, "value": "1"
            });
        }

        Belen.UniversalAnalyticsLoader.init(universalAnalyticsOpts);

        

            Belen.Advertiser.Headerbidding.requestBids('vap', {
  "cat": "195",
  "tcat": "203",
  "city": "Bremen__Stadt__Burg_Grambke",
  "plz": "28719",
  "page": "ViewItemPage",
  "lsc":   [
    203,
    195
  ],
  "lsk":   [
    "ikea markus",
    "molger",
    "trysil",
    "monitor wandhalterung"
  ],
  "region": "Bremen",
  "hn": "ebay-kleinanzeigen.de",
  "pt": "VIP",
  "ct": "203",
  "hu": "",
  "li": "0",
  "ptg": "1-similar-items-experiment-similar_items_experiment_vip-default,browser-referer-alerting-a_default-group_referer_alerting-off,2-testbanner-default,vip-buyer-survey-off,hompage-modal-survey-off,24-yield-optimization-medium-c,25-testgrouptest-distribution_test-b,27-yield-optimization-small-a,29-testgrouptest-old-alg-distribution_old_test-b,36-grpc-b-grpc-on,4-feature-packages-feature_packages-b-default-off,42-tmx-profiling-tmx-on,5-treebay2-treebay2-c-default,6-bigger-card-experiment-bigger-card-experiment-default,74-react-messagebox-off,8-myads-on-react-myads_on_react-d-react,9-sticky-banner-vip-sticky_banner_vip-c-generic_messaging_vip,96-dfpbanner-test,js-errorlog-default,98-prebid-update-default,99-test-support-default",
  "kw": "!!_Gemuetliche_Altbauwohnung_mit_grossem_Balkon_in_zentraler_Lage...",
  "Verkaeufer": "gewerblich",
  "Angebotstyp": "Angebot",
  "adid": "921550031",
  "posterid": "37136318",
  "Preis": "500",
  "ExactPreis": "500",
  "yo_s": "A",
  "yo_m": "C",
  "abtest": "test",
  "tb2": "C",
  "Anzahl_Badezimmer": "1.0",
  "Nebenkosten__€_": "200",
  "Wohnflaeche__m²_": "160",
  "Einbaukueche": "true",
  "Kaution_/_Genoss._Anteile__€_": "1000",
  "Zimmer": "2.5",
  "Wohnungstyp": "etagenwohnung",
  "Heizungsart": "zentralheizung",
  "Warmmiete__€_": "700"
});

            // init all DFP pagelevel setting
            Belen.Advertiser.DFP.definePageSettings(
                {
  "cat": "195",
  "tcat": "203",
  "city": "Bremen__Stadt__Burg_Grambke",
  "plz": "28719",
  "page": "ViewItemPage",
  "lsc":   [
    203,
    195
  ],
  "lsk":   [
    "ikea markus",
    "molger",
    "trysil",
    "monitor wandhalterung"
  ],
  "region": "Bremen",
  "hn": "ebay-kleinanzeigen.de",
  "pt": "VIP",
  "ct": "203",
  "hu": "",
  "li": "0",
  "ptg": "1-similar-items-experiment-similar_items_experiment_vip-default,browser-referer-alerting-a_default-group_referer_alerting-off,2-testbanner-default,vip-buyer-survey-off,hompage-modal-survey-off,24-yield-optimization-medium-c,25-testgrouptest-distribution_test-b,27-yield-optimization-small-a,29-testgrouptest-old-alg-distribution_old_test-b,36-grpc-b-grpc-on,4-feature-packages-feature_packages-b-default-off,42-tmx-profiling-tmx-on,5-treebay2-treebay2-c-default,6-bigger-card-experiment-bigger-card-experiment-default,74-react-messagebox-off,8-myads-on-react-myads_on_react-d-react,9-sticky-banner-vip-sticky_banner_vip-c-generic_messaging_vip,96-dfpbanner-test,js-errorlog-default,98-prebid-update-default,99-test-support-default",
  "kw": "!!_Gemuetliche_Altbauwohnung_mit_grossem_Balkon_in_zentraler_Lage...",
  "Verkaeufer": "gewerblich",
  "Angebotstyp": "Angebot",
  "adid": "921550031",
  "posterid": "37136318",
  "Preis": "500",
  "ExactPreis": "500",
  "yo_s": "A",
  "yo_m": "C",
  "abtest": "test",
  "tb2": "C",
  "Anzahl_Badezimmer": "1.0",
  "Nebenkosten__€_": "200",
  "Wohnflaeche__m²_": "160",
  "Einbaukueche": "true",
  "Kaution_/_Genoss._Anteile__€_": "1000",
  "Zimmer": "2.5",
  "Wohnungstyp": "etagenwohnung",
  "Heizungsart": "zentralheizung",
  "Warmmiete__€_": "700"
}, true,
                '', '1-12-0'
            );

        


        // Facebook
        Belen.Global.fbAppId = '129026183788759';

        Belen.FacebookTracker.load({
            pixelid: "1418401095075716"
        });

        
        externalScripts.push("https://static.criteo.net/js/ld/ld.js");
        
        LazyLoad.js(window.belenScripts, handleDomReady);
        

        if (externalScripts != null && externalScripts.length > 0) {
            try {
                LazyLoad.js(externalScripts);
            } catch (e) {
                console.log(e);
            }
        }

    </script>

    </head>
<body
        id="vap" class="">
<header id="site-header">
    <section id="site-header-top" class="l-page-wrapper">
        <div id="site-logo">
            <div class="logo">
                <a data-gaevent="VIP,LogoClick"
                   href="/">
                    <img height="40" src="/static/img/common/logo/logo-ebayk-402x80.hsn0x4ev0qi.png"
                         alt=""></a><br>
                <span id="site-logo-claim" class="headline-small">Kostenlos. Einfach. Lokal.</span>
            </div>
        </div>
        <section id="site-signin" class="is-centered-button">
                <div class="login-overlay is-hidden">
                                <div class="login-overlay--content">
                                    <span class="headline-big">Hallo!</span>
                                    <p>Willkommen bei eBay Kleinanzeigen. Melde dich hier an,
                                        oder erstelle ein neues Konto, damit du:</p>
                                    <a title="Close (Esc)" type="button" class="j-overlay-close overlay-close"></a>
                                    <ul class="list">
                                        <li>
                                            <i class="icon icon-checkmark-green"></i>
                                            <span>Nachrichten senden und empfangen kannst</span>
                                        </li>
                                        <li>
                                            <i class="icon icon-checkmark-green"></i>
                                            <span>Eigene Anzeigen aufgeben kannst</span>
                                        </li>
                                        <li>
                                            <i class="icon icon-checkmark-green"></i>
                                            <span>Für dich interessante Anzeigen siehst</span>
                                        </li>
                                    </ul>
                                    <div class="login-overlay-buttons">
                                        <li><a class="button j-overlay-login" data-gaevent="VIP,LoginBegin"
                                               href="/m-einloggen.html?targetUrl=/anzeigen/s-detailansicht.html?adId=921550031">Einloggen</a>
                                        </li>
                                        <li><a class="button-secondary j-overlay-register" data-gaevent="VIP,UserRegistrationBegin"
                                               href="/m-benutzer-anmeldung.html">Registrieren</a>
                                        </li>
                                    </div>
                                </div>
                            </div>
                        <nav>
                            <ul class="linklist">
                                <li>
                                    <a class="button" data-gaevent="VIP,LoginBegin"
                                       href="/m-einloggen.html?targetUrl=/anzeigen/s-detailansicht.html?adId=921550031">
                                        <i class="button-icon icon-my-white"></i><span>Einloggen</span></a>
                                </li>
                                <li>oder</li>
                                <li><a class="text-medium text-bold" data-gaevent="VIP,UserRegistrationBegin"
                                       href="/m-benutzer-anmeldung.html">Registrieren</a>
                                </li>
                            </ul>
                        </nav>
                    </section>
        </section>

    <section id="site-searchbar" class="a-green bar">
        <div class="bar-inner l-page-wrapper">
            <div class="l-row">
                <div class="a-span-17 l-col">
                    <div id="site-search">
                                <form id="site-search-form" action="/s-suchanfrage.html" method="get"><div class="l-row">
                                        <div class="a-span-10 l-col">
                                            <div id="site-search-what" class="splitfield">

                                                <div class="splitfield-left">
                                                    <!-- SuggestionWidget  start -->
<div id="site-search-query-wrp" class="suggestionbox " data-ajaxurl="/s-suchbegriff-empfehlungen.json" data-suggest=''>


            <input type="text" id="site-search-query"
                   
                   class="splitfield-input is-not-clearable" name="keywords" tabindex="1" title=""
                   placeholder="Was suchst du?"
                   value=""
                    />

    </div>
</div>


                                                <div id="site-search-ctgry" class="multiselectbox splitfield-right"
                                                     data-ajaxurl="/s-kategorie-baum.html">
                                                    <input class="splitfield-dropdown splitfield-input splitfield-right-input"
                                                           tabindex="2" id="srch-ctgry-inpt" readonly="readonly"
                                                           type="text" value="Mietwohnungen "/>
                                                    <input type="hidden"
                                                           value="203"
                                                           name="categoryId" id="search-category-value"/>
                                                    <ul class="dropdown-list multiselectbox-list">
                                                        <li class="multiselectbox-option"><a class="multiselectbox-link"
                                                                                             data-val="0">Alle Kategorien</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="a-span-10 l-col">
                                            <div id="site-search-where" class="splitfield">
                                                <div class="splitfield-left">
                                                    <!-- SuggestionWidget  start -->
<div id="site-search-area-wrp" class="suggestionbox " data-ajaxurl="/s-ort-empfehlungen.json" data-suggest=''>


            <input type="text" id="site-search-area"
                   
                   class="splitfield-input is-not-clearable" name="locationStr" tabindex="3" title=""
                   placeholder="PLZ oder Ort"
                   value="Hohentor - Bremen (Stadt)"
                    />

    <input class="j-suggstnbx-key" type="hidden" value="13500" name="locationId" />
    </div>
</div>

                                                <div id="site-search-distance" class="a-short splitfield-right">
                                                    <div class="selectbox">
                                                        <input id="site-search-distance-inpt"
                                                               class="splitfield-input splitfield-dropdown splitfield-right-input"
                                                               tabindex="4" type="text" value="" autocomplete="off"
                                                               readonly="readonly"/>
                                                        <input id="site-search-distance-value" type="hidden" value=""
                                                               name="radius"/>
                                                        <ul id="site-search-distance-list"
                                                            class="dropdown-list selectbox-list">
                                                            <li class="selectbox-option is-selected" data-value="0">
    Ganzer Ort</li><li class="selectbox-option " data-value="5">
    + 5 km</li><li class="selectbox-option " data-value="10">
    + 10 km</li><li class="selectbox-option " data-value="20">
    + 20 km</li><li class="selectbox-option " data-value="30">
    + 30 km</li><li class="selectbox-option " data-value="50">
    + 50 km</li><li class="selectbox-option " data-value="100">
    + 100 km</li><li class="selectbox-option " data-value="150">
    + 150 km</li><li class="selectbox-option " data-value="200">
    + 200 km</li></ul>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="a-span-4 l-col">
                                            <button  id="site-search-submit" class="button"  type="submit" >
            <i class="button-icon  icon-magnifier-white"></i>
            <span>Finden</span>
            </button>
    </div>
                                    </div>

                                    <input type="hidden" name="sortingField" value="SORTING_DATE"
                                           id="search-sorting-field"/>
                                    <input type="hidden" id="search-adType-field" name="adType"
                                           value=""/>
                                    <input type="hidden" id="search-posterType-field" name="posterType"
                                           value=""/>
                                    <input type="hidden" id="search-pageNum" name="pageNum" value="1"/>
                                    <input type="hidden" id="search-action" name="action" value="find"/>
                                    <input type="hidden" name="maxPrice" value=""/>
                                    <input type="hidden" name="minPrice" value=""/>
                                    <div>
</div></form></div>
                        </div>
                <div class="a-span-7 l-col">
                    <nav id="site-mainnav" class="mainnav position-right-bottom">
                            <ul class="">
                                <li id="site-mainnav-postad"
                                    class="mainnav-item mainnav-postad ">
                                    <a data-gaevent="VIP,PostAdBegin"
                                       id="site-mainnav-postad-link" class="mainnav-item-inner"
                                       href="/p-anzeige-aufgeben.html">Anzeige aufgeben</a>
                                </li>
                                <li id="site-mainnav-my"
                                    class="mainnav-item mainnav-my "
                                    aria-haspopup="true">
                                    <a id="site-mainnav-my-link" class="mainnav-item-inner"
                                       href="/m-meine-anzeigen.html">Meins</a>

                                    <span id="msgbox-notification-badge" class="badge badge-menu is-hidden"></span>
                                </li>
                            </ul>
                        </nav>
                    </div>
            </div>
        </div>
    </section>

    <section id="site-header-sub" class="is-hidden">
        <nav id="site-subnav" class="mainnav-sub">
                <ul>
                    <li class="mainnav-sub-item">

                        <a id="site-subnav-msgbox" class="mainnav-sub-link"
                                    href="/m-nachrichten.html">
                            Nachrichten&nbsp;<span
                            id="sub-navi-num-new-conversations-fu" class="badge badge-submenu is-hidden"></span>
                        </a>


                    </li>
                    <li class="mainnav-sub-item">
                        <a id="site-subnav-myads" class="mainnav-sub-link"
                            href="/m-meine-anzeigen.html">Anzeigen</a>
                        </li>

                    <li class="mainnav-sub-item">
                        <a id="site-subnav-settings" class="mainnav-sub-link"
                           href="/m-einstellungen.html">
                            Einstellungen</a>
                    </li>
                </ul>
                <hr>
                <div class="mainnav-sub-header">Favoriten</div>
                <ul>
                    <li class="mainnav-sub-item">
                        <a id="site-subnav-watchlist" class="mainnav-sub-link"
                            href="/m-merkliste.html">
                            Merkliste</a>
                    </li>
                    <li class="mainnav-sub-item">
                        <a class="mainnav-sub-link" href="/m-meine-nutzer.html">
                            Nutzer</a>
                    </li>
                    <li class="mainnav-sub-item">
                        <a id="site-subnav-sase" class="mainnav-sub-link" href="/m-meine-suchen.html">
                            Suchaufträge</a>
                    </li>
                </ul>
            </nav>
        </section>
</header>
<noscript>
        <div class="l-page-wrapper container">
            <img src="/static/img/common/js-disabled.1aye6u7d9m1bi.png" alt="" class="margin-bottom-small"/>
        </div>
    </noscript>
    <!--[if lt IE 10 ]>
    <div class="l-page-wrapper container">
    <div class="l-container-row outcomebox-warning outcomebox-alt-background">
    <header><h1>Bitte installiere einen neuen Browser</h1></header>
    <div class="conditionalcomment-outcomeboxbody"><p>Du verwendest derzeit eine alte Version des Internet Explorers. eBay Kleinanzeigen funktioniert besser, wenn du einen <a class="lnk-action" href="https://www.mozilla.org/de/firefox/new/" target="_blank">aktuellen Browser installierst</a>.</p></div>
    </div>
    </div>
    <![endif]-->
<div id="site-content"
     class="l-page-wrapper l-container-row  ">
    <div id="banner-skyscraper-left" class="l-beside-page-left"
             data-reinsert-ad='{"zoneid":"767503","width":"160","height":"600","containerId":"crt-62e77656", "displayOnPages":["VIP"]}'>
            <div id="sky-atf-left-120x600" class="skyscraper-left-120"></div>
            <div id="sky-atf-left-160x600" class="skyscraper-left-160"></div>
            <div id="sky-atf-left-300x600" class="skyscraper-left-300"></div>
        </div>
    <script type="text/javascript">
            var pageContentEl = document.getElementById("site-content");
            pageContentEl.style.visibility = "hidden";

            window.setTimeout(function () {
                pageContentEl.style.visibility = "visible";
                if (Belen.Advertiser.Reinserter) {
                    Belen.Advertiser.Reinserter.triggerReinsert();
                }
            }, 1500);
        </script>
    <div id="vip-atf-billboard"
         class="l-container-row align-center"
         data-reinsert-ad='{"zoneid":"767505","width":"728","height":"90","containerId":"crt-1537f922", "displayOnPages":["VIP"]}'>
    </div>

    <div class="l-container-row">
        <div id="vap-brdcrmb" itemscope itemtype="http://data-vocabulary.org/Breadcrumb" class="breadcrump">
            <a class="breadcrump-link" itemprop="url" href="/stadt/bremen (stadt)/" title="Kleinanzeigen Bremen (Stadt)">
                <span itemprop="title">Kleinanzeigen Bremen (Stadt)</span>
            </a>
            <a class="breadcrump-link" itemprop="url" href="/s-immobilien/hohentor/c195l13500"><span itemprop="title">Immobilien</span></a>
                <a class="breadcrump-link" itemprop="url" href="/s-wohnung-mieten/hohentor/c203l13500"><span itemprop="title">Mietwohnungen</span></a>
            </div>
    </div>

    <section id="viewad-main" itemscope itemtype="http://data-vocabulary.org/Product" class="l-container-row">

                <meta itemprop="category" content="Mietwohnungen" />
                <header id="viewad-header" class="l-container-row">
                    <div class="a-middle-aligned l-row">
                        <div  class="a-span-20 l-col">
                            <div  class="articleheader">
                                <h1 id="viewad-title" class="articleheader--title" itemprop="name">!! Gemütliche Altbauwohnung mit großem Balkon in zentraler Lage...</h1>
                                <h2 class="articleheader--price" id="viewad-price">Preis: 500 €</h2>
                                </div>
                        </div>
                        <div class="a-span-4 l-col">
                            <div id="viewad-features" class="l-container align-right" >
                                </div>
                        </div>
                    </div>
                </header>

                <section  id="viewad-cntnt"  class="l-row a-double-margin l-container-row">
                    <section class="a-span-16 l-col">
    <article id="viewad-product" class="l-container-row contentbox">
        <div class="a-double-margin l-container-row">
                <div id="viewad-images" class="ad-gallery-horizontal l-container">
                    <div class="ad-gallery-horizontal--imagecontainer">
                        <div id="viewad-image" class="is-clickable ad-image-wrapper">
                            <div class="ad-gallery-lens"></div>
                            <div class="ad-image ad-image-on" data-inlinerendered="true" data-ix="0">
                                <img src="https://i.ebayimg.com/00/s/MzM3WDQ1MA==/z/UdUAAOSwIOtbbIQk/$_72.JPG"
                                     alt="!! Gemütliche Altbauwohnung mit großem Balkon in zentraler Lage... in Bremen (Stadt)"
                                     title="!! Gemütliche Altbauwohnung mit großem Balkon in zentraler Lage... in Bremen (Stadt)"/>
                            </div>
                        </div>
                    </div>
                    <div id="viewad-thumbnails" class="ad-nav">
                        <div class="ad-thumbs">
                            <ul id="viewad-thumbnail-list" class="list ad-thumb-list">
                            <li>
                                    <div class="imagebox-thumbnail">
                                        <img title=""
                                            data-imgsrc="https://i.ebayimg.com/00/s/MzM3WDQ1MA==/z/UdUAAOSwIOtbbIQk/$_72.JPG"
                                            src="https://i.ebayimg.com/00/s/MzM3WDQ1MA==/z/UdUAAOSwIOtbbIQk/$_6.JPG"
                                            
                                                itemprop="image" data-inlinepreload="true"
                                            
                                        />
                                    </div>
                                </li>
                            <li>
                                    <div class="imagebox-thumbnail">
                                        <img title=""
                                            data-imgsrc="https://i.ebayimg.com/00/s/MzM3WDQ0OQ==/z/QYsAAOSwyCNbbIQj/$_72.JPG"
                                            src="https://i.ebayimg.com/00/s/MzM3WDQ0OQ==/z/QYsAAOSwyCNbbIQj/$_6.JPG"
                                            
                                        />
                                    </div>
                                </li>
                            <li>
                                    <div class="imagebox-thumbnail">
                                        <img title=""
                                            data-imgsrc="https://i.ebayimg.com/00/s/MzM3WDQ0OQ==/z/9eYAAOSw53dbbIQl/$_72.JPG"
                                            src="https://i.ebayimg.com/00/s/MzM3WDQ0OQ==/z/9eYAAOSw53dbbIQl/$_6.JPG"
                                            
                                        />
                                    </div>
                                </li>
                            <li>
                                    <div class="imagebox-thumbnail">
                                        <img title=""
                                            data-imgsrc="https://i.ebayimg.com/00/s/NjAwWDQ1MA==/z/SIYAAOSwH4dbbIQk/$_72.JPG"
                                            src="https://i.ebayimg.com/00/s/NjAwWDQ1MA==/z/SIYAAOSwH4dbbIQk/$_6.JPG"
                                            
                                        />
                                    </div>
                                </li>
                            <li>
                                    <div class="imagebox-thumbnail">
                                        <img title=""
                                            data-imgsrc="https://i.ebayimg.com/00/s/NjAwWDQ1MA==/z/fwYAAOSwnn1bbIQk/$_72.JPG"
                                            src="https://i.ebayimg.com/00/s/NjAwWDQ1MA==/z/fwYAAOSwnn1bbIQk/$_6.JPG"
                                            
                                        />
                                    </div>
                                </li>
                            <li>
                                    <div class="imagebox-thumbnail">
                                        <img title=""
                                            data-imgsrc="https://i.ebayimg.com/00/s/NTk5WDQ1MA==/z/1JIAAOSwHp5bbIQk/$_72.JPG"
                                            src="https://i.ebayimg.com/00/s/NTk5WDQ1MA==/z/1JIAAOSwHp5bbIQk/$_6.JPG"
                                            
                                        />
                                    </div>
                                </li>
                            <li>
                                    <div class="imagebox-thumbnail">
                                        <img title=""
                                            data-imgsrc="https://i.ebayimg.com/00/s/NjAwWDQ1MA==/z/d3UAAOSwVVpbbIQj/$_72.JPG"
                                            src="https://i.ebayimg.com/00/s/NjAwWDQ1MA==/z/d3UAAOSwVVpbbIQj/$_6.JPG"
                                            
                                        />
                                    </div>
                                </li>
                            <li>
                                    <div class="imagebox-thumbnail">
                                        <img title=""
                                            data-imgsrc="https://i.ebayimg.com/00/s/MzM3WDQ0OQ==/z/ragAAOSwSelbbIQk/$_72.JPG"
                                            src="https://i.ebayimg.com/00/s/MzM3WDQ0OQ==/z/ragAAOSwSelbbIQk/$_6.JPG"
                                            
                                        />
                                    </div>
                                </li>
                            <li>
                                    <div class="imagebox-thumbnail">
                                        <img title=""
                                            data-imgsrc="https://i.ebayimg.com/00/s/NjAwWDQ1MA==/z/yhUAAOSwr85bbIQl/$_72.JPG"
                                            src="https://i.ebayimg.com/00/s/NjAwWDQ1MA==/z/yhUAAOSwr85bbIQl/$_6.JPG"
                                            
                                        />
                                    </div>
                                </li>
                            <li>
                                    <div class="imagebox-thumbnail">
                                        <img title=""
                                            data-imgsrc="https://i.ebayimg.com/00/s/NjAwWDQ1MA==/z/b0oAAOSwXSNbbIQk/$_72.JPG"
                                            src="https://i.ebayimg.com/00/s/NjAwWDQ1MA==/z/b0oAAOSwXSNbbIQk/$_6.JPG"
                                            
                                        />
                                    </div>
                                </li>
                            <li>
                                    <div class="imagebox-thumbnail">
                                        <img title=""
                                            data-imgsrc="https://i.ebayimg.com/00/s/MzM3WDQ0OQ==/z/QiwAAOSwcc9bbIQk/$_72.JPG"
                                            src="https://i.ebayimg.com/00/s/MzM3WDQ0OQ==/z/QiwAAOSwcc9bbIQk/$_6.JPG"
                                            
                                        />
                                    </div>
                                </li>
                            <li>
                                    <div class="imagebox-thumbnail">
                                        <img title=""
                                            data-imgsrc="https://i.ebayimg.com/00/s/NjAwWDQ1MA==/z/aeoAAOSw6tNbbIQk/$_72.JPG"
                                            src="https://i.ebayimg.com/00/s/NjAwWDQ1MA==/z/aeoAAOSw6tNbbIQk/$_6.JPG"
                                            
                                        />
                                    </div>
                                </li>
                            <li>
                                    <div class="imagebox-thumbnail">
                                        <img title=""
                                            data-imgsrc="https://i.ebayimg.com/00/s/NjAwWDQ1MA==/z/77YAAOSwt5dbbIQj/$_72.JPG"
                                            src="https://i.ebayimg.com/00/s/NjAwWDQ1MA==/z/77YAAOSwt5dbbIQj/$_6.JPG"
                                            
                                        />
                                    </div>
                                </li>
                            <li>
                                    <div class="imagebox-thumbnail">
                                        <img title=""
                                            data-imgsrc="https://i.ebayimg.com/00/s/NjAwWDQ1MA==/z/L3AAAOSwlrxbbIQl/$_72.JPG"
                                            src="https://i.ebayimg.com/00/s/NjAwWDQ1MA==/z/L3AAAOSwlrxbbIQl/$_6.JPG"
                                            
                                        />
                                    </div>
                                </li>
                            <li>
                                    <div class="imagebox-thumbnail">
                                        <img title=""
                                            data-imgsrc="https://i.ebayimg.com/00/s/MzM3WDQ0OQ==/z/zG8AAOSwgSJbbIQj/$_72.JPG"
                                            src="https://i.ebayimg.com/00/s/MzM3WDQ0OQ==/z/zG8AAOSwgSJbbIQj/$_6.JPG"
                                            
                                        />
                                    </div>
                                </li>
                            <li>
                                    <div class="imagebox-thumbnail">
                                        <img title=""
                                            data-imgsrc="https://i.ebayimg.com/00/s/NjAwWDQ1MA==/z/DT4AAOSwyCNbbIQk/$_72.JPG"
                                            src="https://i.ebayimg.com/00/s/NjAwWDQ1MA==/z/DT4AAOSwyCNbbIQk/$_6.JPG"
                                            
                                        />
                                    </div>
                                </li>
                            <li>
                                    <div class="imagebox-thumbnail">
                                        <img title=""
                                            data-imgsrc="https://i.ebayimg.com/00/s/NjAwWDQ1MA==/z/AHwAAOSw-6pbbIQk/$_72.JPG"
                                            src="https://i.ebayimg.com/00/s/NjAwWDQ1MA==/z/AHwAAOSw-6pbbIQk/$_6.JPG"
                                            
                                        />
                                    </div>
                                </li>
                            <li>
                                    <div class="imagebox-thumbnail">
                                        <img title=""
                                            data-imgsrc="https://i.ebayimg.com/00/s/NjAwWDQ1MA==/z/osMAAOSwuz1bbIQk/$_72.JPG"
                                            src="https://i.ebayimg.com/00/s/NjAwWDQ1MA==/z/osMAAOSwuz1bbIQk/$_6.JPG"
                                            
                                        />
                                    </div>
                                </li>
                            <li>
                                    <div class="imagebox-thumbnail">
                                        <img title=""
                                            data-imgsrc="https://i.ebayimg.com/00/s/MzM3WDQ0OQ==/z/uPIAAOSwLztbbIQk/$_72.JPG"
                                            src="https://i.ebayimg.com/00/s/MzM3WDQ0OQ==/z/uPIAAOSwLztbbIQk/$_6.JPG"
                                            
                                        />
                                    </div>
                                </li>
                            <li>
                                    <div class="imagebox-thumbnail">
                                        <img title=""
                                            data-imgsrc="https://i.ebayimg.com/00/s/NjAwWDQ1MA==/z/sjUAAOSwfrJbbIQk/$_72.JPG"
                                            src="https://i.ebayimg.com/00/s/NjAwWDQ1MA==/z/sjUAAOSwfrJbbIQk/$_6.JPG"
                                            
                                        />
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div id="viewad-lightbox" class="modal-dialog mfp-popup-huge mfp-hide">
                    <header>
                        <div class="banner-supersize box-centered" id="vip-gallery-728x90"></div>
                        </header>

                    <div class="ad-gallery modal-dialog-content">
                        <div class="ad-image-wrapper">
                            <div class="spinner-centered ad-loader"></div>
                        </div>
                        <div id="viewad-lightbox-thumbnails" class="ad-nav ">
                            <div class="ad-thumbs">
                                <ul id="viewad-lightbox-thumbnail-list" class="ad-thumb-list">
                                <li>
                                        <div class="imagebox-thumbnail">
                                            <img title=""
                                                 data-imgsrc="https://i.ebayimg.com/00/s/MzM3WDQ1MA==/z/UdUAAOSwIOtbbIQk/$_57.JPG"
                                                 src="https://i.ebayimg.com/00/s/MzM3WDQ1MA==/z/UdUAAOSwIOtbbIQk/$_6.JPG">
                                        </div>
                                    </li>
                                <li>
                                        <div class="imagebox-thumbnail">
                                            <img title=""
                                                 data-imgsrc="https://i.ebayimg.com/00/s/MzM3WDQ0OQ==/z/QYsAAOSwyCNbbIQj/$_57.JPG"
                                                 src="https://i.ebayimg.com/00/s/MzM3WDQ0OQ==/z/QYsAAOSwyCNbbIQj/$_6.JPG">
                                        </div>
                                    </li>
                                <li>
                                        <div class="imagebox-thumbnail">
                                            <img title=""
                                                 data-imgsrc="https://i.ebayimg.com/00/s/MzM3WDQ0OQ==/z/9eYAAOSw53dbbIQl/$_57.JPG"
                                                 src="https://i.ebayimg.com/00/s/MzM3WDQ0OQ==/z/9eYAAOSw53dbbIQl/$_6.JPG">
                                        </div>
                                    </li>
                                <li>
                                        <div class="imagebox-thumbnail">
                                            <img title=""
                                                 data-imgsrc="https://i.ebayimg.com/00/s/NjAwWDQ1MA==/z/SIYAAOSwH4dbbIQk/$_57.JPG"
                                                 src="https://i.ebayimg.com/00/s/NjAwWDQ1MA==/z/SIYAAOSwH4dbbIQk/$_6.JPG">
                                        </div>
                                    </li>
                                <li>
                                        <div class="imagebox-thumbnail">
                                            <img title=""
                                                 data-imgsrc="https://i.ebayimg.com/00/s/NjAwWDQ1MA==/z/fwYAAOSwnn1bbIQk/$_57.JPG"
                                                 src="https://i.ebayimg.com/00/s/NjAwWDQ1MA==/z/fwYAAOSwnn1bbIQk/$_6.JPG">
                                        </div>
                                    </li>
                                <li>
                                        <div class="imagebox-thumbnail">
                                            <img title=""
                                                 data-imgsrc="https://i.ebayimg.com/00/s/NTk5WDQ1MA==/z/1JIAAOSwHp5bbIQk/$_57.JPG"
                                                 src="https://i.ebayimg.com/00/s/NTk5WDQ1MA==/z/1JIAAOSwHp5bbIQk/$_6.JPG">
                                        </div>
                                    </li>
                                <li>
                                        <div class="imagebox-thumbnail">
                                            <img title=""
                                                 data-imgsrc="https://i.ebayimg.com/00/s/NjAwWDQ1MA==/z/d3UAAOSwVVpbbIQj/$_57.JPG"
                                                 src="https://i.ebayimg.com/00/s/NjAwWDQ1MA==/z/d3UAAOSwVVpbbIQj/$_6.JPG">
                                        </div>
                                    </li>
                                <li>
                                        <div class="imagebox-thumbnail">
                                            <img title=""
                                                 data-imgsrc="https://i.ebayimg.com/00/s/MzM3WDQ0OQ==/z/ragAAOSwSelbbIQk/$_57.JPG"
                                                 src="https://i.ebayimg.com/00/s/MzM3WDQ0OQ==/z/ragAAOSwSelbbIQk/$_6.JPG">
                                        </div>
                                    </li>
                                <li>
                                        <div class="imagebox-thumbnail">
                                            <img title=""
                                                 data-imgsrc="https://i.ebayimg.com/00/s/NjAwWDQ1MA==/z/yhUAAOSwr85bbIQl/$_57.JPG"
                                                 src="https://i.ebayimg.com/00/s/NjAwWDQ1MA==/z/yhUAAOSwr85bbIQl/$_6.JPG">
                                        </div>
                                    </li>
                                <li>
                                        <div class="imagebox-thumbnail">
                                            <img title=""
                                                 data-imgsrc="https://i.ebayimg.com/00/s/NjAwWDQ1MA==/z/b0oAAOSwXSNbbIQk/$_57.JPG"
                                                 src="https://i.ebayimg.com/00/s/NjAwWDQ1MA==/z/b0oAAOSwXSNbbIQk/$_6.JPG">
                                        </div>
                                    </li>
                                <li>
                                        <div class="imagebox-thumbnail">
                                            <img title=""
                                                 data-imgsrc="https://i.ebayimg.com/00/s/MzM3WDQ0OQ==/z/QiwAAOSwcc9bbIQk/$_57.JPG"
                                                 src="https://i.ebayimg.com/00/s/MzM3WDQ0OQ==/z/QiwAAOSwcc9bbIQk/$_6.JPG">
                                        </div>
                                    </li>
                                <li>
                                        <div class="imagebox-thumbnail">
                                            <img title=""
                                                 data-imgsrc="https://i.ebayimg.com/00/s/NjAwWDQ1MA==/z/aeoAAOSw6tNbbIQk/$_57.JPG"
                                                 src="https://i.ebayimg.com/00/s/NjAwWDQ1MA==/z/aeoAAOSw6tNbbIQk/$_6.JPG">
                                        </div>
                                    </li>
                                <li>
                                        <div class="imagebox-thumbnail">
                                            <img title=""
                                                 data-imgsrc="https://i.ebayimg.com/00/s/NjAwWDQ1MA==/z/77YAAOSwt5dbbIQj/$_57.JPG"
                                                 src="https://i.ebayimg.com/00/s/NjAwWDQ1MA==/z/77YAAOSwt5dbbIQj/$_6.JPG">
                                        </div>
                                    </li>
                                <li>
                                        <div class="imagebox-thumbnail">
                                            <img title=""
                                                 data-imgsrc="https://i.ebayimg.com/00/s/NjAwWDQ1MA==/z/L3AAAOSwlrxbbIQl/$_57.JPG"
                                                 src="https://i.ebayimg.com/00/s/NjAwWDQ1MA==/z/L3AAAOSwlrxbbIQl/$_6.JPG">
                                        </div>
                                    </li>
                                <li>
                                        <div class="imagebox-thumbnail">
                                            <img title=""
                                                 data-imgsrc="https://i.ebayimg.com/00/s/MzM3WDQ0OQ==/z/zG8AAOSwgSJbbIQj/$_57.JPG"
                                                 src="https://i.ebayimg.com/00/s/MzM3WDQ0OQ==/z/zG8AAOSwgSJbbIQj/$_6.JPG">
                                        </div>
                                    </li>
                                <li>
                                        <div class="imagebox-thumbnail">
                                            <img title=""
                                                 data-imgsrc="https://i.ebayimg.com/00/s/NjAwWDQ1MA==/z/DT4AAOSwyCNbbIQk/$_57.JPG"
                                                 src="https://i.ebayimg.com/00/s/NjAwWDQ1MA==/z/DT4AAOSwyCNbbIQk/$_6.JPG">
                                        </div>
                                    </li>
                                <li>
                                        <div class="imagebox-thumbnail">
                                            <img title=""
                                                 data-imgsrc="https://i.ebayimg.com/00/s/NjAwWDQ1MA==/z/AHwAAOSw-6pbbIQk/$_57.JPG"
                                                 src="https://i.ebayimg.com/00/s/NjAwWDQ1MA==/z/AHwAAOSw-6pbbIQk/$_6.JPG">
                                        </div>
                                    </li>
                                <li>
                                        <div class="imagebox-thumbnail">
                                            <img title=""
                                                 data-imgsrc="https://i.ebayimg.com/00/s/NjAwWDQ1MA==/z/osMAAOSwuz1bbIQk/$_57.JPG"
                                                 src="https://i.ebayimg.com/00/s/NjAwWDQ1MA==/z/osMAAOSwuz1bbIQk/$_6.JPG">
                                        </div>
                                    </li>
                                <li>
                                        <div class="imagebox-thumbnail">
                                            <img title=""
                                                 data-imgsrc="https://i.ebayimg.com/00/s/MzM3WDQ0OQ==/z/uPIAAOSwLztbbIQk/$_57.JPG"
                                                 src="https://i.ebayimg.com/00/s/MzM3WDQ0OQ==/z/uPIAAOSwLztbbIQk/$_6.JPG">
                                        </div>
                                    </li>
                                <li>
                                        <div class="imagebox-thumbnail">
                                            <img title=""
                                                 data-imgsrc="https://i.ebayimg.com/00/s/NjAwWDQ1MA==/z/sjUAAOSwfrJbbIQk/$_57.JPG"
                                                 src="https://i.ebayimg.com/00/s/NjAwWDQ1MA==/z/sjUAAOSwfrJbbIQk/$_6.JPG">
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <section id="viewad-details">
            <header class="splitheader-centered header-underlined">
                <h2 class="splitheader--title sectionheadline">Details</h2>
                <div class="splitheader--addon">
                    <div class="socialshare">

                        <span class="socialshare-label" id="viewad-share-label">Weiterempfehlen:</span>
                        <ul id="viewad-list-shares" class="socialshare-list">
                            <li id="viewad-share-mail"><i class="socialshare-icon-mail" title="email"></i></li>

                            <li id="viewad-share-facebook" class="facebook-share" data-fblink="https://www.ebay-kleinanzeigen.de/s-anzeige/-gemuetliche-altbauwohnung-mit-grossem-balkon-in-zentraler-lage-/921550031-203-34?utm_source=facebook&utm_medium=social&utm_campaign=socialbuttons&utm_content=desktop">
                                <i class="socialshare-icon-facebook" title="facebook"></i>
                            </li>
                            <li id="viewad-share-twitter" data-href="http://twitter.com/home?status=https%3A%2F%2Fwww.ebay-kleinanzeigen.de%2Fs-anzeige%2F-gemuetliche-altbauwohnung-mit-grossem-balkon-in-zentraler-lage-%2F921550031-203-34?utm_source=twitter&utm_medium=social&utm_campaign=socialbuttons&utm_content=desktop">
                                <i class="socialshare-icon-twitter" title="twitter"></i>
                            </li>
                            <li id="viewad-share-pinterest" data-image="https://i.ebayimg.com/00/s/MzM3WDQ1MA==/z/UdUAAOSwIOtbbIQk/$_20.JPG"
                                data-href="https://www.ebay-kleinanzeigen.de/s-anzeige/-gemuetliche-altbauwohnung-mit-grossem-balkon-in-zentraler-lage-/921550031-203-34?utm_source=facebook&utm_medium=social&utm_campaign=socialbuttons&utm_content=desktop">
                                <i class="socialshare-icon-pinterest" title="pinterest"></i>
                            </li>
                        </ul>
                    </div>
                </div>
            </header>
            <section class="l-container" itemprop="offerDetails" itemscope itemtype="http://data-vocabulary.org/Offer">
                <meta itemprop="price" content="500.00"/>
                    <meta itemprop="currency" content="EUR"/>
                <dl class="attributelist-striped">
                    <dt class="attributelist--key">Ort:</dt>
                    <dd class="attributelist--value" itemprop="seller" itemscope itemtype="http://data-vocabulary.org/Person">
                        <span itemprop="address" itemscope itemtype="http://data-vocabulary.org/Address">
                            <span id="street-address" itemprop="street-address">
                                    Burger Heerstraße 28</span>,
                            <span id="viewad-locality" itemprop="locality">
                                28719 Bremen (Stadt) - Burg-Grambke</span>
                        </span>
                    </dd>
                    <dt class="attributelist--key">Erstellungsdatum:</dt>
                        <dd class="attributelist--value">09.08.2018</dd>
                    <dt class="attributelist--key">Anzeigennummer:</dt>
                        <dd class="attributelist--value">921550031</dd>
                    <dt class="attributelist--key">Zimmer:</dt>
                            <dd class="attributelist--value">
                            <span >
                                2,5</span>
                            </dd>
                        <dt class="attributelist--key">Anzahl Badezimmer:</dt>
                            <dd class="attributelist--value">
                            <span >
                                1</span>
                            </dd>
                        <dt class="attributelist--key">Wohnfläche (m²):</dt>
                            <dd class="attributelist--value">
                            <span >
                                70</span>
                            </dd>
                        <dt class="attributelist--key">Nebenkosten (€):</dt>
                            <dd class="attributelist--value">
                            <span >
                                200</span>
                            </dd>
                        <dt class="attributelist--key">Warmmiete (€):</dt>
                            <dd class="attributelist--value">
                            <span >
                                700</span>
                            </dd>
                        <dt class="attributelist--key">Kaution / Genoss.-Anteile (€):</dt>
                            <dd class="attributelist--value">
                            <span >
                                1.000</span>
                            </dd>
                        <dt class="attributelist--key">Wohnungstyp:</dt>
                            <dd class="attributelist--value">
                            <span >
                                <a href="/s-wohnung-mieten/etagenwohnung/hohentor/c203l13500+wohnung_mieten.wohnungstyp_s:etagenwohnung">
                                            Etagenwohnung</a>
                                    </span>
                            </dd>
                        <dt class="attributelist--key">Heizungsart:</dt>
                            <dd class="attributelist--value">
                            <span >
                                Zentralheizung</span>
                            </dd>
                        <dt class="attributelist--key">Ausstattung:</dt>
                            <dd class="attributelist--value">
                                <a href="https://www.ebay-kleinanzeigen.de/s-wohnung-mieten/hohentor/einbauk%C3%BCche/k0c203l13500">
                                        Einbauküche</a></dd>
                        </dl>

                <div class="l-container align-right">
                        <div id="viewad-cntr" class="textcounter">
                            <span id="viewad-cntr-num"></span>
                        </div>
                    </div>
                </section>
        </section>

        <section id="viewad-description">
            <header>
                <h2 class="sectionheadline-underlined">Beschreibung</h2>
            </header>
            <section class="l-container">
                <p id="viewad-description-text" class="text-force-linebreak" itemprop="description">
                    !! Gemütliche Altbauwohnung mit großem Balkon in zentraler Lage !!<br /><br />Lagebeschreibung:<br />Dieses Wohnungsangebot befindet sich direkt an der Burger Heerstraße. Eine Vielzahl von Fachgeschäften sowie Dienstleistungsbetrieben liegen direkt vor der Haustür - ebenfalls eine Bushaltestelle. Der Burger Bahnhof ist in 5 Gehminuten erreicht, sodass man schnell in der Bremer City ist.<br />Für einen hohen Freizeitwert garantieren der nahegelegen Sportparksee, das Lesumufer, ein wunderschöner 18 Lochgolfplatz sowie das unter Naturschutz stehende Werderland.<br /><br />Objektbeschreibung:<br />Im 1.OG sowie im ausgebauten Dachgeschoss eines Zweifamilienhaus liegt diese ältere aber gepflegte 2,5 Zimmer Wohnung.<br />Im EG befindet sich seit Jahrzenten ein kleiner Frisörladen, so dass Sie Abends und das Wochenende das Haus &quot; für sich alleine&quot; haben.<br /><br />Über eine mit Teppichware ausgelegte Holztreppe gelangen Sie in das OG. Vom Flur aus liegt linke Hand das weiss geflieste Vollbad mit Fenster, WC sowie eine Badewanne. Rechte Seite befindet sich die große Küche mit Einbaumöbel ( Backofen, Ceranfeld, Spülmaschine und Kühlschrank). Die Küche bietet genügend Platz zum Sitzen. Auch der Waschmaschinenanschluss wurde in der Küche verlegt.<br />Von der Küche gelangen Sie auf den großen Süd-Westbalkon mit schönen Blick ins Grüne. Der Balkon ist teilüberdacht. Eine Markise schützt vor direktet Sonneneinstrahlung.<br />Das Wohnzimmer ist rechteckig geschnitten und mit Laminat ausgelegt.<br />Das Schlafzimmer ist derzeit von der Küche aus begehbar und liegt zur ruhigen Gartenseite.<br />Vom Flur aus führt eine weitere Treppe in das augebaute Dachgeschoss. Dieses kann als Abstellfläche ( diese Wohnung hat keinen Kelleranteil) genutzt werden oder aber als Hobbyraum oder Büro. Ein kleines Dachzimmer mit gemütlicher Schräge könnte als Schlafraum oder für Gäste dienen.<br /><br />Die Bodenbeläge varieren je nach Nutzung der Räume zwischen Teppich, Stäbchenparkett, Laminat oder PVC Belag.<br /><br />In den Nebenkosten von 200,- &#128; sind alle Verbrauchskosten ( Heizung, Wasser, Müll etc.) inklusive.<br /><br />Die Wohnung eignet sich ideal für ein berufstätiges Pärchen die die Annehmlichkeiten einer zentralen Lage bevorzugen.<br /><br />Gerne besichtige ich mit Ihnen- füllen Sie bitte dazu das Kontaktformular vollständig aus.<br /><br />Sonstiges:<br />Die Objektangaben wurden mit großer Sorgfalt zusammengestellt. Für die Richtigkeit der Verkäufer-, Anbieter- und Angebotsangaben können wir keine Haftung übernehmen. Irrtümer bleiben vorbehalten<br /><br />Wenn Ihr Interesse an diesem Immobilienangebot geweckt wurde können Sie mich gern kontaktieren um einen Besichtigungstermin zu vereinbaren.<br /><br />Sie möchten verkaufen oder vermieten? Sie benötigen eine Bewertung Ihrer Immobilie ? Gerne berate ich Sie - ich freue mich auf Ihren Anruf ! Büro: 0421- 602326 mobil 0172-5142185<br /><br />Daten:<br />Warmmiete: 700,00 €<br />Kaltmiete: 500,00 €<br />Nebenkosten: 200,00 €<br />Kaution&#x2F;Genossenschaftsanteile: 1.000,00 €<br />Zimmer: 2,5<br />Wohnfläche: ca. 70m²<br />Nutzfläche: ca. 15m²<br />Baujahr: 1913<br />Verfügbar ab: 01.November 2018<br /><br />Objektzustand: gepflegt<br /><br />Energie:<br />Energieausweis: Energieverbrauchsausweis<br />Energieverbrauch: 207.00 kWh(m²*a)<br />Energieverbrauch für Warmwasser enthalten<br /><br />Heizungsart: Zentralheizung<br />Wesentliche Energieträger: Öl<br /><br />Anbieter-Objekt-ID: SH571</p>
            </section>
        </section>

        <section id="viewad-imprint" class="modal-dialog mfp-popup-huge mfp-hide">
                <header>
                    <h2>Rechtliche Angaben</h2>
                </header>
                <section class="modal-dialog-content">
                    <p id="viewad-imprint-text">
                        Impressum<br />Silke Heimer<br />Am Burger See 10 a<br />28719 Bremen<br />Telefon: 0421 &#x2F; 60 23 26 und 0421 &#x2F; 84 74 40 25<br />Mobil: 0172 &#x2F; 51 42 185<br />Fax: 0421 &#x2F; 659 37 27<br />Kontakt per Mail: info@silke-heimer-immobilien.de<br />Homepage: www.silke-heimer-immobilien.de<br />Steuernummer: 60&#x2F;228&#x2F;31435<br />Aufsichtsbehörde nach § 34c GewO: Stadtamt der Stadt Bremen (Stresemannstr. 48, 28207 Bremen, Tel.: +49 (0) 421 &#x2F; 361-6908)<br />Diese Webseite beinhaltet externe Links. Ich bin nicht für den Inhalt der externen Seiten verantwortlich.</p>
                </section>
            </section>

            <section>
                <section class="l-container">
                    <p>
                        <a id="viewad-imprint-link"><span>Rechtliche Angaben</span></a>
                    </p>
                </section>
            </section>
        </article>
    <section class="contentbox" id="viewad-contact-bottom">
            <header id="viewad-contact-bottom-header" class="l-container-row">
                <h2 class="sectionheadline-underlined">
                    Nachricht schreiben</h2>
            </header>
            <div class="l-container">
                    <button  id="viewad-contact-button-login-bottom" class="viewad-contact-button-login button-iconized"  type="button" >
            <i class="button-icon  icon-mail"></i>
            <span>Nachricht schreiben</span>
            </button>
    </div>
            </section>
    </section>

<aside id="viewad-sidebar" class="a-span-8 l-col">
    <div id="viewad-contact-box" class="l-container-row contentbox">
    <div id="viewad-user-actions">
        <ul class="iconlist">
            <li><button data-gaevent="VIP,R2SEmailBegin" id="viewad-contact-button-login-top" class="button full-width viewad-contact-button-login"  type="button" >
            <i class="button-icon  icon-mail"></i>
            <span>Nachricht schreiben</span>
            </button>
    </li>
                <li id="viewad-action-watchlist"><a id="viewad-lnk-watchlist" class="button-secondary full-width"
                                                        data-ajaxurl="/m-merkliste-bearbeiten.html?csrf=e288d6fd-b9ed-4a8f-b817-4b2de9890c9b"
                                                        data-action="add"
                                                        data-add="Zur Merkliste hinzufügen"
                                                        data-remove="Von Merkliste entfernen"><i
                            class="button-icon icon-star-open-gray"></i><span>Zur Merkliste hinzufügen</span></a></li>
                </ul>
    </div>
</div>

<div id="viewad-profile-box" class="l-container-row contentbox">
        <div id="viewad-contact">
            <div class="l-container-row">
                <ul class="iconlist">
                    <li>
                            <i class="iconlist-icon-big">
                                <a href="/s-bestandsliste.html?userId=37136318"
                                   class="badge user-profile-badge">SH</a>
                            </i>
                        <span class="iconlist-text">
                            <span class="text-bold text-bigger text-force-linebreak">
                                <a href="/s-bestandsliste.html?userId=37136318">
                                    Silke Heimer - Silke Heimer</a>
                            </span>
                            <br>
                            <span class="text-light">
                                Gewerblicher Nutzer<br>
                                Aktiv seit 19.02.2016</span>
                        </span>
                        </li>
                    <li>
                            <i class="iconlist-icon-big icon-phone-circle"></i>
                        <span class="iconlist-text">
                            <span id="viewad-contact-phone" class="text-bold text-bigger">0421-602326</span>
                            </span>

                        </li>
                    </ul>
            </div>

            <hr class="a-horizontal and-grey separator"/>
            <ul class="flexlist">
                <li>
                    <a id="poster-other-ads-link" href="/s-bestandsliste.html?userId=37136318">
                            9 Anzeigen online
                        </a>
                    </li>
                <li class="right-aligned">
                    <a href="/m-einloggen.html?targetUrl=%2Fs-anzeige%2F-gemuetliche-altbauwohnung-mit-grossem-balkon-in-zentraler-lage-%2F921550031-203-34%3Fff%3Dtrue%26csrf%3De288d6fd-b9ed-4a8f-b817-4b2de9890c9b"data-gaevent="VIP,FollowUserBegin"id="follow-user-button"
           class="button button-toggle follow-user-button"
           title="Folgen"
        >
            <i class="button-icon button-icon icon-follow-user"></i>
            <span>Folgen</span>
            </a>
    </li>
            </ul>
        </div>
    </div>
<div id="viewad-sidebar-banner" class="align-center l-container-row"
         data-reinsert-ad='{"zoneid":"767504","width":"300","height":"250","containerId":"crt-c2087262", "displayOnPages":["VIP"]}'>
        <div id="vip-300x600"></div>
        <div id="vip-300x250"></div>
    </div>
<div id="viewad-action-box" class="l-container-row contentbox">
    <div id="viewad-watchlist-errmsg" class="modal-dialog mfp-popup-small mfp-hide">
        <header>Fehler</header>
        <div class="modal-dialog-content">
            <div class="outcomemessage-error"></div>
        </div>
    </div>

    <ul id="viewad-actions" class="flexlist">
        <li id="viewad-action-flag">
            <a id="viewad-action-flag-link" href="/m-einloggen.html?targetUrl=%2Fs-anzeige%2F-gemuetliche-altbauwohnung-mit-grossem-balkon-in-zentraler-lage-%2F921550031-203-34%3Ffl%3Dtrue%26csrf%3De288d6fd-b9ed-4a8f-b817-4b2de9890c9b" data-gaevent="VIP,ReportAdBegin"
               data-gaevent-direct="true">
                <i class="iconlist-icon icon-attention-blue"></i><span class="iconlist-text">Anzeige melden</span>
            </a>
        </li>
        <li id="viewad-action-prnt">
            <a data-printurl="/s-druckansicht.html?adId=921550031">
                <i class="iconlist-icon icon-print"></i>
                <span class=" iconlist-text">Anzeige drucken</span>
            </a>
        </li>
    </ul>
</div>
</aside>

</section>
            </section>
        <div id="vap_adsense-middle"></div>
<section id="vap-pstrads" class="l-container-row">
            <header class="l-container-row splitheader">
                <h2 class="headline-medium splitheader--title">Andere Anzeigen des Anbieters</h2>
                <div class="splitheader--addon">
                    <a href="/s-bestandsliste.html?userId=37136318">Alle Anzeigen dieses Anbieters</a></div>
                        </header>
            <div class="contentbox-unpadded">
            <ul  class="itemlist-separatedbefore ad-list ">
    <li class="ad-listitem    ">
            <article class="aditem" data-adid="921550031">
                <div class="aditem-image">
                    <div class="imagebox" data-gaevent="VIP,ViewUsersOtherAds" data-href="/s-anzeige/-gemuetliche-altbauwohnung-mit-grossem-balkon-in-zentraler-lage-/921550031-203-34">
                                        <img src="https://i.ebayimg.com/00/s/MzM3WDQ1MA==/z/UdUAAOSwIOtbbIQk/$_9.JPG"
                                             alt="!! Gemütliche Altbauwohnung mit großem Balkon in zentraler Lage... Bremen (Stadt) - Burg-Grambke Vorschau"/>
                                    </div>
                                </div>
                <div class="aditem-main">
                    <h2 class="text-module-begin"><a data-gaevent="VIP,ViewUsersOtherAds" href="/s-anzeige/-gemuetliche-altbauwohnung-mit-grossem-balkon-in-zentraler-lage-/921550031-203-34">!! Gemütliche Altbauwohnung mit großem Balkon in zentraler Lage...</a></h2>
                    <p>!! Gemütliche Altbauwohnung mit großem Balkon in zentraler Lage !!

Lagebeschreibung:
Dieses...</p>


                    <p class="text-module-end">
                        <span class="simpletag tag-small">2,5 Zimmer</span>
                        <span class="simpletag tag-small">70 m²</span>
                        </p>

                    </div>
                <div class="aditem-details">
                    <strong>500 €</strong>
                        <br>
                    28719<br>
                        Burg-&#8203Grambke<br>
                    </div>
                <div class="aditem-addon">
                    Gestern, 19:44</div>
            </article>
        </li>

    <li class="ad-listitem    ">
            <article class="aditem" data-adid="880367853">
                <div class="aditem-image">
                    <div class="imagebox" data-gaevent="VIP,ViewUsersOtherAds" data-href="/s-anzeige/-solides-reihenhaus-mit-garage-und-wintergarten-/880367853-208-9">
                                        <img src="https://i.ebayimg.com/00/s/MzM3WDQ1MA==/z/BvkAAOSwrU1bLAoV/$_9.JPG"
                                             alt="!! Solides Reihenhaus mit Garage und Wintergarten... Bremen - Blumenthal Vorschau"/>
                                    </div>
                                </div>
                <div class="aditem-main">
                    <h2 class="text-module-begin"><a data-gaevent="VIP,ViewUsersOtherAds" href="/s-anzeige/-solides-reihenhaus-mit-garage-und-wintergarten-/880367853-208-9">!! Solides Reihenhaus mit Garage und Wintergarten...</a></h2>
                    <p>!! Solides Reihenhaus mit Garage und Wintergarten sucht neuen Eigentümer !!

Lagebeschreibung:
Das...</p>


                    <p class="text-module-end">
                        <span class="simpletag tag-small">4 Zimmer</span>
                        <span class="simpletag tag-small">95 m²</span>
                        </p>

                    </div>
                <div class="aditem-details">
                    <strong> VB</strong>
                        <br>
                    28779<br>
                        Blumenthal<br>
                    </div>
                <div class="aditem-addon">
                    31.07.2018</div>
            </article>
        </li>

    </ul>
</div>
        </section>
    <section id="vap-smlrads" class="a-double-margin l-container-row">
            <header class="l-container-row splitheader">
                <h2 class="headline-medium splitheader--title">
                    Das könnte dich auch interessieren</h2>
                <div class="splitheader--addon">
                        <a id="vap-lnk-smlrads">Mehr ähnliche Anzeigen</a>
                    </div>
                </header>
            <div class=" contentbox-unpadded">
                <ul  class="itemlist-separatedbefore ad-list ">
    <li class="ad-listitem    ">
            <article class="aditem" data-adid="835672029">
                <div class="aditem-image">
                    <div class="imagebox" data-gaevent="VIP,RelatedItemClick" data-href="/s-anzeige/burg-grambke-1-zimmer-appartment-in-ruhiger-lage-mit-/835672029-196-34">
                                        <img src="https://i.ebayimg.com/00/s/NjgyWDEwMjQ=/z/HP4AAOSwrVRbUYx5/$_9.JPG"
                                             alt="Burg Grambke / 1-Zimmer-Appartment in ruhiger Lage mit... Bremen (Stadt) - Burg-Grambke Vorschau"/>
                                    </div>
                                </div>
                <div class="aditem-main">
                    <h2 class="text-module-begin"><a data-gaevent="VIP,RelatedItemClick" href="/s-anzeige/burg-grambke-1-zimmer-appartment-in-ruhiger-lage-mit-/835672029-196-34">Burg Grambke / 1-Zimmer-Appartment in ruhiger Lage mit...</a></h2>
                    <p>Burg Grambke / 1-Zimmer-Appartment in ruhiger Lage mit Blick ins...</p>


                    <p class="text-module-end">
                        <span class="simpletag tag-small">1 Zimmer</span>
                        <span class="simpletag tag-small">37 m²</span>
                        </p>

                    </div>
                <div class="aditem-details">
                    <strong>49.900 €</strong>
                        <br>
                    28719<br>
                        Burg-&#8203Grambke<br>
                    </div>
                <div class="aditem-addon">
                    22.07.2018</div>
            </article>
        </li>

    <li class="ad-listitem    ">
            <article class="aditem" data-adid="844839999">
                <div class="aditem-image">
                    <div class="imagebox" data-gaevent="VIP,RelatedItemClick" data-href="/s-anzeige/schicke-4-zimmer-wohnung-mit-wintergarten-und-balkon-in-/844839999-196-34">
                                        <img src="https://i.ebayimg.com/00/s/NjgyWDEwMjQ=/z/qe0AAOSwze9bau6Y/$_9.JPG"
                                             alt="Schicke 4-Zimmer-Wohnung mit Wintergarten und Balkon in... Bremen (Stadt) - Burg-Grambke Vorschau"/>
                                    </div>
                                </div>
                <div class="aditem-main">
                    <h2 class="text-module-begin"><a data-gaevent="VIP,RelatedItemClick" href="/s-anzeige/schicke-4-zimmer-wohnung-mit-wintergarten-und-balkon-in-/844839999-196-34">Schicke 4-Zimmer-Wohnung mit Wintergarten und Balkon in...</a></h2>
                    <p>Schicke 4-Zimmer-Wohnung mit Wintergarten und Balkon in zentraler Lage von...</p>


                    <p class="text-module-end">
                        <span class="simpletag tag-small">4 Zimmer</span>
                        <span class="simpletag tag-small">93,40 m²</span>
                        </p>

                    </div>
                <div class="aditem-details">
                    <strong>158.000 €</strong>
                        <br>
                    28719<br>
                        Burg-&#8203Grambke<br>
                    </div>
                <div class="aditem-addon">
                    05.08.2018</div>
            </article>
        </li>

    <li class="ad-listitem    ">
            <article class="aditem" data-adid="910273763">
                <div class="aditem-image">
                    <div class="imagebox" data-gaevent="VIP,RelatedItemClick" data-href="/s-anzeige/2-zimmer-altbau-wohnung-mit-balkon-im-viertel/910273763-203-13510">
                                        <img src="https://i.ebayimg.com/00/s/MTAyNFg1NDc=/z/hRkAAOSwNppbUgRi/$_9.JPG"
                                             alt="2 Zimmer Altbau Wohnung mit Balkon im Viertel Bremen (Stadt) - Fesenfeld Vorschau"/>
                                    </div>
                                </div>
                <div class="aditem-main">
                    <h2 class="text-module-begin"><a data-gaevent="VIP,RelatedItemClick" href="/s-anzeige/2-zimmer-altbau-wohnung-mit-balkon-im-viertel/910273763-203-13510">2 Zimmer Altbau Wohnung mit Balkon im Viertel</a></h2>
                    <p>Wunderschöne 2 Zimmer Altbauwohnung im Viertel zu vermieten. Sie bleibt teils möbliert, wenn...</p>


                    <p class="text-module-end">
                        <span class="simpletag tag-small">2 Zimmer</span>
                        <span class="simpletag tag-small">50 m²</span>
                        </p>

                    </div>
                <div class="aditem-details">
                    <strong>500 €</strong>
                        <br>
                    28203<br>
                        Fesenfeld<br>
                    </div>
                <div class="aditem-addon">
                    20.07.2018</div>
            </article>
        </li>

    <li class="ad-listitem     is-hidden ">
            <article class="aditem" data-adid="883938840">
                <div class="aditem-image">
                    <div class="imagebox" data-gaevent="VIP,RelatedItemClick" data-href="/s-anzeige/ansehen-frisch-modernisiertes-wohnglueck/883938840-203-34">
                                        <img src="https://i.ebayimg.com/00/s/NjAwWDgwMA==/z/IzoAAOSwaYVbLMLs/$_9.JPG"
                                             alt="Ansehen: frisch modernisiertes Wohnglück Bremen (Stadt) - Burg-Grambke Vorschau"/>
                                    </div>
                                </div>
                <div class="aditem-main">
                    <h2 class="text-module-begin"><a data-gaevent="VIP,RelatedItemClick" href="/s-anzeige/ansehen-frisch-modernisiertes-wohnglueck/883938840-203-34">Ansehen: frisch modernisiertes Wohnglück</a></h2>
                    <p>Ansehen: frisch modernisiertes Wohnglück

Lagebeschreibung:
Dieses Objekt befindet sich in einer...</p>


                    <p class="text-module-end">
                        <span class="simpletag tag-small">3 Zimmer</span>
                        <span class="simpletag tag-small">53,10 m²</span>
                        </p>

                    </div>
                <div class="aditem-details">
                    <strong>330 €</strong>
                        <br>
                    28719<br>
                        Burg-&#8203Grambke<br>
                    </div>
                <div class="aditem-addon">
                    05.08.2018</div>
            </article>
        </li>

    <li class="ad-listitem     is-hidden ">
            <article class="aditem" data-adid="921049822">
                <div class="aditem-image">
                    <div class="imagebox" data-gaevent="VIP,RelatedItemClick" data-href="/s-anzeige/gemuetliche-3-zkb-70-qm-in-rmh-in-findorff/921049822-203-38">
                                        <img src="https://i.ebayimg.com/00/s/NjgzWDEwMjQ=/z/hq8AAOSw4fZbaz7W/$_9.JPG"
                                             alt="Gemütliche 3 ZKB, 70 qm, in RMH in Findorff Bremen (Stadt) - Findorff Vorschau"/>
                                    </div>
                                </div>
                <div class="aditem-main">
                    <h2 class="text-module-begin"><a data-gaevent="VIP,RelatedItemClick"name="921049822" href="/s-anzeige/gemuetliche-3-zkb-70-qm-in-rmh-in-findorff/921049822-203-38">Gemütliche 3 ZKB, 70 qm, in RMH in Findorff</a></h2>
                    <p>Gemütliche Wohnung im 1. OG & DG eines Reihenhauses in Findorff, Tübinger Straße, sucht ruhige neue...</p>


                    <p class="text-module-end">
                        <span class="simpletag tag-small">3 Zimmer</span>
                        <span class="simpletag tag-small">70 m²</span>
                        </p>

                    </div>
                <div class="aditem-details">
                    <strong>590 €</strong>
                        <br>
                    28215<br>
                        Findorff<br>
                    </div>
                <div class="aditem-addon">
                    08.08.2018</div>
            </article>
        </li>

    <li class="ad-listitem     is-hidden ">
            <article class="aditem" data-adid="918000441">
                <div class="aditem-image">
                    <div class="imagebox" data-gaevent="VIP,RelatedItemClick" data-href="/s-anzeige/attraktive-3-zimmer-eigentumswohnung-mit-2-balkonen-in-grambke/918000441-196-34">
                                        <img src="https://i.ebayimg.com/00/s/MTAyNFgxMDI0/z/QtgAAOSwg2FbZFHJ/$_9.JPG"
                                             alt="Attraktive 3 Zimmer-Eigentumswohnung mit 2 Balkonen in Grambke Bremen (Stadt) - Burg-Grambke Vorschau"/>
                                    </div>
                                </div>
                <div class="aditem-main">
                    <h2 class="text-module-begin"><a data-gaevent="VIP,RelatedItemClick"name="918000441" href="/s-anzeige/attraktive-3-zimmer-eigentumswohnung-mit-2-balkonen-in-grambke/918000441-196-34">Attraktive 3 Zimmer-Eigentumswohnung mit 2 Balkonen in Grambke</a></h2>
                    <p>Zentrale, dennoch ruhige Wohnlage!
Die Bushaltestelle "Am Geestkamp" befindet sich praktisch vor...</p>


                    <p class="text-module-end">
                        <span class="simpletag tag-small">3 Zimmer</span>
                        <span class="simpletag tag-small">87 m²</span>
                        </p>

                    </div>
                <div class="aditem-details">
                    <strong>189.000 €</strong>
                        <br>
                    28719<br>
                        Burg-&#8203Grambke<br>
                    </div>
                <div class="aditem-addon">
                    03.08.2018</div>
            </article>
        </li>

    <li class="ad-listitem     is-hidden ">
            <article class="aditem" data-adid="920184813">
                <div class="aditem-image">
                    <div class="imagebox" data-gaevent="VIP,RelatedItemClick" data-href="/s-anzeige/preisguenstige-3-zimmer-wohnung/920184813-203-34">
                                        <img src="https://i.ebayimg.com/00/s/NjAwWDgwMA==/z/c7IAAOSwhO9bbXB5/$_9.JPG"
                                             alt="Preisgünstige 3-Zimmer-Wohnung Bremen (Stadt) - Burg-Grambke Vorschau"/>
                                    </div>
                                </div>
                <div class="aditem-main">
                    <h2 class="text-module-begin"><a data-gaevent="VIP,RelatedItemClick"name="920184813" href="/s-anzeige/preisguenstige-3-zimmer-wohnung/920184813-203-34">Preisgünstige 3-Zimmer-Wohnung</a></h2>
                    <p>Preisgünstige 3-Zimmer-Wohnung

Lagebeschreibung:
Unsere Wohnanlage befindet sich in einer ruhigen...</p>


                    <p class="text-module-end">
                        <span class="simpletag tag-small">3 Zimmer</span>
                        <span class="simpletag tag-small">80,89 m²</span>
                        </p>

                    </div>
                <div class="aditem-details">
                    <strong>389 €</strong>
                        <br>
                    28719<br>
                        Burg-&#8203Grambke<br>
                    </div>
                <div class="aditem-addon">
                    07.08.2018</div>
            </article>
        </li>

    <li class="ad-listitem     is-hidden ">
            <article class="aditem" data-adid="917703660">
                <div class="aditem-image">
                    <div data-gaevent="VIP,RelatedItemClick" data-href="/s-anzeige/nachmieter-gesucht-2-zi-whn-wartburgstr-28217/917703660-203-25" class="imagebox is-nopic"></div>
                        </div>
                <div class="aditem-main">
                    <h2 class="text-module-begin"><a data-gaevent="VIP,RelatedItemClick"name="917703660" href="/s-anzeige/nachmieter-gesucht-2-zi-whn-wartburgstr-28217/917703660-203-25">Nachmieter gesucht 2.Zi. Whn. Wartburgstr. 28217</a></h2>
                    <p>Wegen beruflichen Gründen muss die bequeme und günstige Wohnung abgegeben werden. Es handelt sich...</p>


                    <p class="text-module-end">
                        <span class="simpletag tag-small">2 Zimmer</span>
                        <span class="simpletag tag-small">60 m²</span>
                        </p>

                    </div>
                <div class="aditem-details">
                    <strong> VB</strong>
                        <br>
                    28217<br>
                        Walle<br>
                    </div>
                <div class="aditem-addon">
                    02.08.2018</div>
            </article>
        </li>

    <li class="ad-listitem     is-hidden ">
            <article class="aditem" data-adid="916046288">
                <div class="aditem-image">
                    <div class="imagebox" data-gaevent="VIP,RelatedItemClick" data-href="/s-anzeige/vermiete-etagenwohnung-in-bremen-arsten/916046288-203-18882">
                                        <img src="https://i.ebayimg.com/00/s/NzY4WDEwMjQ=/z/0vkAAOSw0BdbX0tL/$_9.JPG"
                                             alt="Vermiete Etagenwohnung in Bremen arsten Bremen (Stadt) - Arsten Vorschau"/>
                                    </div>
                                </div>
                <div class="aditem-main">
                    <h2 class="text-module-begin"><a data-gaevent="VIP,RelatedItemClick"name="916046288" href="/s-anzeige/vermiete-etagenwohnung-in-bremen-arsten/916046288-203-18882">Vermiete Etagenwohnung in Bremen arsten</a></h2>
                    <p>Etagenwohnung in ruhigem Wohngebiet. Die Immobilie ist in einem gepflegten Zustand und wurde 2002...</p>


                    <p class="text-module-end">
                        <span class="simpletag tag-small">2 Zimmer</span>
                        <span class="simpletag tag-small">56 m²</span>
                        </p>

                    </div>
                <div class="aditem-details">
                    <strong>727 €</strong>
                        <br>
                    28279<br>
                        Arsten<br>
                    </div>
                <div class="aditem-addon">
                    30.07.2018</div>
            </article>
        </li>

    <li class="ad-listitem     is-hidden ">
            <article class="aditem" data-adid="835731805">
                <div class="aditem-image">
                    <div class="imagebox" data-gaevent="VIP,RelatedItemClick" data-href="/s-anzeige/burg-grambke-kapitalanlage-1-zimmer-appartment-in-ruhiger-/835731805-208-34">
                                        <img src="https://i.ebayimg.com/00/s/NjgyWDEwMjQ=/z/sLMAAOSwMbRbZbpZ/$_9.JPG"
                                             alt="Burg Grambke / Kapitalanlage: 1-Zimmer-Appartment in ruhiger... Bremen (Stadt) - Burg-Grambke Vorschau"/>
                                    </div>
                                </div>
                <div class="aditem-main">
                    <h2 class="text-module-begin"><a data-gaevent="VIP,RelatedItemClick"name="835731805" href="/s-anzeige/burg-grambke-kapitalanlage-1-zimmer-appartment-in-ruhiger-/835731805-208-34">Burg Grambke / Kapitalanlage: 1-Zimmer-Appartment in ruhiger...</a></h2>
                    <p>Burg Grambke / Kapitalanlage: 1-Zimmer-Appartment in ruhiger Lage mit Blick ins...</p>


                    <p class="text-module-end">
                        <span class="simpletag tag-small">1 Zimmer</span>
                        <span class="simpletag tag-small">37 m²</span>
                        </p>

                    </div>
                <div class="aditem-details">
                    <strong>49.900 €</strong>
                        <br>
                    28719<br>
                        Burg-&#8203Grambke<br>
                    </div>
                <div class="aditem-addon">
                    22.07.2018</div>
            </article>
        </li>

    </ul>
</div>

        </section>
    <div id="vap_adsense-bottom"></div>
<!-- cp box -->
        <form id="viewad-contact-form" class="viewad-contact-poster mfp-popup-large mfp-hide modal-dialog" action="/s-anbieter-kontaktieren.json" method="post"><input id="adId" name="adId" type="hidden" value="921550031"/><header><h2>Nachricht schreiben</h2></header>
            <section class="modal-dialog-content">

                <div id="viewad-contact-top-success" class="ajaxform-success l-container-row" style="display: none;">
                    <div class="outcomebox-success">
                                <header class="ajaxform-success-msg"></header>
                                    <section class="outcomebox--body">
                                        <h2>Immobilien-Anfrage</h2>
<p>
    Diese Unterhaltung wird nicht in deiner Nachrichten-Box erscheinen.</p></section>
                            </div>
                        </div>

                <div id="viewad-contact-top-error" class="outcomebox-error l-container-row" style="display:none;">
                    <header>Fehler</header>
                    <section class="outcomebox--body">
                        <p class="ajaxform-error"></p>
                    </section>
                </div>

                <fieldset>
                    <div id="viewad-contact-top-mandatoryhint" class="align-right l-container-row">
                        Pflichtfelder sind mit Sternchen * gekennzeichnet
                    </div>

                    <div class="formgroup">
                        <label for="viewad-contact-message" class="formgroup-label-mandatory">Nachricht</label>
                        <div class="formgroup-input">
                            <textarea id="viewad-contact-message" class="viewad-contact-message" name="message" cols="10" rows="5"></textarea>
                            <div class="ajaxform-fielderror formerror is-hidden" data-fieldname="message"></div>
                        </div>
                    </div>
                    <div class="formgroup">
                            <label class="formgroup-label-mandatory" for="viewad-contact-contactFirstName">Vorname</label>
                            <div class="formgroup-input">
                                <input class="formcontrol" type="text" name="contactFirstName" id="viewad-contact-contactFirstName" value="" data-name=""/>
                                <div class="ajaxform-fielderror formerror is-hidden" data-fieldname="contactFirstName"></div>
                            </div>
                        </div>
                        <div class="formgroup">
                            <label class="formgroup-label-mandatory" for="viewad-contact-contactName">Nachname</label>
                            <div class="formgroup-input">
                                <input class="formcontrol" type="text" name="contactName" id="viewad-contact-contactLastName" value="" data-name=""/>
                                <div class="ajaxform-fielderror formerror is-hidden" data-fieldname="contactName"></div>
                            </div>
                        </div>
                        <div class="formgroup">
                        <label class="formgroup-label" for="viewad-contact-phonenumber">Telefonnummer</label>
                        <div class="formgroup-input">
                            <input class="formcontrol" type="text" name="phoneNumber" id="viewad-contact-phonenumber" value="" data-phone="" />
                            <div class="ajaxform-fielderror formerror is-hidden" data-fieldname="phoneNumber"></div>
                        </div>
                    </div>
                    <div class="formgroup">
                            <label class="formgroup-label" for="viewad-contact-street">Straße und Hausnummer</label>
                            <div class="formgroup-input">
                                <input class="formcontrol" type="text" name="street" id="viewad-contact-street" value="" data-name=""/>
                                <div class="ajaxform-fielderror formerror is-hidden" data-fieldname="street"></div>
                            </div>
                        </div>
                        <div class="formgroup">
                            <label class="formgroup-label" for="viewad-contact-zipCode">Postleitzahl</label>
                            <div class="formgroup-input">
                                <input class="formcontrol" type="text" name="zipCode" id="viewad-contact-zipCode" value="" data-name=""/>
                                <div class="ajaxform-fielderror formerror is-hidden" data-fieldname="zipCode"></div>
                            </div>
                        </div>
                        <div class="formgroup">
                            <label class="formgroup-label" for="viewad-contact-city">Ort</label>
                            <div class="formgroup-input">
                                <input class="formcontrol" type="text" name="city" id="viewad-contact-city" value="" data-name=""/>
                                <div class="ajaxform-fielderror formerror is-hidden" data-fieldname="city"></div>
                            </div>
                        </div>
                    <p class="text-small">
                        Wir behalten uns vor, über unseren Server gesendete Mitteilungen zum Schutz vor betrügerischen oder verdächtigen Aktivitäten oder Spam zu überprüfen. Mit Absenden dieser Nachricht erklärst du dich hiermit und mit unseren <a href="https://themen.ebay-kleinanzeigen.de/nutzungsbedingungen/" target="_blank">Nutzungsbedingungen</a> und unserer <a href="https://themen.ebay-kleinanzeigen.de/datenschutzerklaerung/" target="_blank">Datenschutzerklärung</a> einverstanden.</p>
                    <div class="modal-dialog--buttons">
                        <div class="ajaxform-spinner"></div>
                        <button  id="viewad-contact-cancel" class="magnific-close button-secondary"  type="button" >
            <span>Abbrechen</span>
            </button>
    <button data-gaevent="VIP,R2SEmailAttempt" data-gaevent-direct="true" id="viewad-contact-submit" class="button"  type="submit" >
            <span>Nachricht senden</span>
            </button>
    </div>
                </fieldset>
            </section>

        <div>
<input type="hidden" name="_csrf" value="e288d6fd-b9ed-4a8f-b817-4b2de9890c9b" />
</div></form><form id="mfp-frm-sendfrnd" class="modal-dialog mfp-popup-medium mfp-hide" action="/s-freund-senden.json" method="post"><input type="hidden" name="adId" value="921550031" id="mfp-sendToFriend-adId"/>
    <header>
        <h2>Weiterempfehlen</h2>
    </header>

    <div class="modal-dialog-content">

        <div id="viewad-sendtofriend-success" class="ajaxform-success l-container-row" style="display: none;">
            <div class="outcomemessage-success ajaxform-success-msg"></div>
        </div>

    <div id="viewad-sendtofriend-error" class="outcomebox-error l-container-row" style="display:none;">
            <header>Fehler</header>
            <section class="outcomebox--body">
                    <p class="ajaxform-error"></p>
            </section>
        </div>

        <fieldset>
            <div class="l-container-row align-right">
                Pflichtfelder sind mit Sternchen * gekennzeichnet!
            </div>

            <div class="formgroup">
                <label class="formgroup-label-mandatory" for="mfp-sendToFriend-from">Von</label>
                <div class="formgroup-input">
                    <input class="formcontrol" type="email" name="from" id="mfp-sendToFriend-from" value="" placeholder="Deine E-Mail-Adresse"/>
                        <div class="ajaxform-fielderror formerror" data-fieldname="from"></div>
                </div>
            </div>

            <div class="formgroup">
                <label class="formgroup-label-mandatory" for="mfp-sendToFriend-to" >An</label>
                <div class="formgroup-input">
                    <input class="formcontrol" type="email" name="to" id="mfp-sendToFriend-to"  placeholder="E-Mail-Adresse des Freundes"/>
                    <div class="ajaxform-fielderror formerror" data-fieldname="to"></div>
                </div>
            </div>

            <div class="formgroup">
                <label class="formgroup-label" for="mfp-sendToFriend-message">Deine Nachricht</label>
                <div class="formgroup-input">
                    <textarea  cols="10" id="mfp-sendToFriend-message" rows="5" name="message"></textarea>
                </div>
            </div>

            <div class="formgroup">
                    <label class="formgroup-label"></label>
                    <div class="formgroup-input">
                        <div id="send-to-friend-captcha" class="g-recaptcha" data-sitekey="6LcZlE0UAAAAAFQKM6e6WA2XynMyr6WFd5z1l1Nr"></div>
                        <div class="ajaxform-fielderror formerror" data-fieldname="recaptchaResponse"></div>
                    </div>
                </div>
            <div class="modal-dialog--buttons">
                <div class="ajaxform-spinner"></div>
                <button  class="button-secondary magnific-close"  type="button" >
            <span>Abbrechen</span>
            </button>
    <button  id="mfp-sendToFriend-submit" class="button"  type="submit" >
            <span>Nachricht senden</span>
            </button>
    </div>
        </fieldset>
    </div>
<div>
<input type="hidden" name="_csrf" value="e288d6fd-b9ed-4a8f-b817-4b2de9890c9b" />
</div></form><form id="viewad-flag-form" class="modal-dialog mfp-popup-small mfp-hide" action="/s-anzeige-melden.json" method="post"><header><h2>Anzeige melden</h2></header>

        <div class="modal-dialog-content">

            <div id="viewad-flagad-success" class="ajaxform-success l-container-row" style="display: none;">
                <div class=" outcomebox-success">
                    <header></header>
                    <section class="outcomebox--body">
                        <p class="ajaxform-success-msg"></p>
                    </section>
                </div>
            </div>

            <div id="viewad-flagad-error" class="outcomebox-error l-container-row" style="display:none;">
                <header></header>
                <section class="outcomebox--body">
                    <p class="ajaxform-error"></p>
                </section>
            </div>

            <fieldset>
            <input type="hidden" id="viewad-flag-adId" value="921550031" name="adId"/>

            <div class="l-container-row">
                <select id="vap-flag-reason" class="width-full" name="reason">
                    <option value="">Bitte wählen</option>
                    <option value="SPAM">Spam</option>
                    <option value="COMMERCIAL">Gewerblich</option>
                    <option value="PRICE">Unrealistischer Preis</option>
                    <option value="OTHER">Andere Gr&uuml;nde</option>
                    <option value="DECEPTION">Betrug</option>
                    <option value="WRONG_CATEGORY">Falsche Kategorie</option>
                    <option value="PROHIBITED">Verbotener Artikel</option>
                    </select>
            </div>

            <div class="l-container-row">
                <textarea class="textfield" id="vap-flag-comment"  name="comment" rows="5" placeholder="Bitte gib einen Grund an"></textarea>
            </div>
                <div class="l-container-row">
                    Bitte beachte auch unsere <a target="_blank" href="https://themen.ebay-kleinanzeigen.de/nutzungsbedingungen/ ">Nutzungsbedingungen</a>.
                </div>
            <div class="modal-dialog--buttons">
                <div class="ajaxform-spinner"></div>
                <button  id="viewad-flag-cancel" class="button-secondary magnific-close"  type="button" >
            <span>Abbrechen</span>
            </button>
    <button  id="viewad-flag-submit" class="button"  type="submit" >
            <span>Melden</span>
            </button>
    </div>
        </fieldset>
        </div>
    <div>
<input type="hidden" name="_csrf" value="e288d6fd-b9ed-4a8f-b817-4b2de9890c9b" />
</div></form></div>

<footer id="site-footer" class="l-page-wrapper align-center">
    <nav id="site-footer-nav">
        <ul class="flexlist is-top-aligned l-container-row">
            <li class="flexlist--column l-col">
                <ul class="list">
                    <li class="flexlist--header">eBay Kleinanzeigen</li>
                    <li>
                        <a href="https://themen.ebay-kleinanzeigen.de/ueber-uns/"
                           data-gaevent="VIP,FooterClick,AboutUs" data-gaevent-direct="true">
                            Über uns</a>
                    </li>
                    <li>
                        <a href="https://careers.ebayinc.com/join-our-team/start-your-search/find-jobs-by-location/detail/germany"
                           data-gaevent="VIP,FooterClick,Careers" data-gaevent-direct="true">
                            Karriere</a>
                    </li>
                    <li>
                        <a href="https://themen.ebay-kleinanzeigen.de/presse/"
                           data-gaevent="VIP,FooterClick,Press" data-gaevent-direct="true">
                            Presse</a>
                    </li>
                    <li>
                        <a href="http://blog.ebay-kleinanzeigen.de"
                           data-gaevent="VIP,FooterClick,Blog" data-gaevent-direct="true">
                            eBay Kleinanzeigen Blog</a>
                    </li>
                    <li>
                        <a href="https://themen.ebay-kleinanzeigen.de/mobile-apps/"
                           data-gaevent="VIP,FooterClick,MobileApps" data-gaevent-direct="true">
                            Mobile&nbsp;Apps</a>
                    </li>
                </ul>
            <li>
            <li class="flexlist--column l-col">
                <ul class="list">
                    <li class="flexlist--header">Informationen</li>
                    <li>
                        <a href="https://themen.ebay-kleinanzeigen.de/hilfe/"
                           data-gaevent="VIP,FooterClick,Help" data-gaevent-direct="true">
                            Hilfe</a>
                    </li>
                    <li>
                        <a href="/sicherheitshinweise.html"
                           data-gaevent="VIP,FooterClick,Security">
                            Tipps für deine Sicherheit</a>
                    </li>
                    <li>
                        <a href="https://themen.ebay-kleinanzeigen.de/datenschutzerklaerung/"
                           data-gaevent="VIP,FooterClick,PrivacyPolicy" data-gaevent-direct="true">
                            Datenschutzerklärung</a>
                    </li>
                    <li>
                        <a href="https://themen.ebay-kleinanzeigen.de/nutzungsbedingungen/"
                           data-gaevent="VIP,FooterClick,Terms" data-gaevent-direct="true">
                            Nutzungsbedingungen</a>
                    </li>
                    <li>
                        <a href="/impressum.html"
                           data-gaevent="VIP,FooterClick,Imprint">
                            Impressum</a>
                    </li>
                </ul>
            <li>
            <li class="flexlist--column l-col">
                <ul class="list">
                    <li class="flexlist--header">Für Unternehmen</li>
                    <li>
                        <a href="https://themen.ebay-kleinanzeigen.de/immobilienmakler/"
                           data-gaevent="VIP,FooterClick,RealEstate" data-gaevent-direct="true">
                            Immobilienmakler</a>
                    </li>
                    <li>
                        <a href="https://themen.ebay-kleinanzeigen.de/erfolgreich-anzeigen-schalten/"
                           data-gaevent="VIP,FooterClick,AdHelp" data-gaevent-direct="true">
                            Erfolgreich inserieren</a>
                    </li>
                    <li>
                        <a href="https://themen.ebay-kleinanzeigen.de/pro-fuer-unternehmen/"
                           data-gaevent="VIP,FooterClick,ProInformation" data-gaevent-direct="true">
                            PRO für&nbsp;Unternehmen</a>
                    </li>
                </ul>
            <li>
            <li class="flexlist--column l-col">
                <ul class="list">
                    <li class="flexlist--header">Social Media</li>
                    <li>
                        <a href="https://www.facebook.com/eBayKleinanzeigen/" target="_blank"
                           data-gaevent="VIP,FooterClick,Facebook" data-gaevent-direct="true">
                            Facebook</a>
                    </li>
                    <li>
                        <a href="https://www.youtube.com/user/eBayKleinanzeigen" target="_blank"
                           data-gaevent="VIP,FooterClick,YouTube" data-gaevent-direct="true">
                            YouTube</a>
                    </li>
                    <li>
                        <a href="https://www.instagram.com/ebay_kleinanzeigen/" target="_blank"
                           data-gaevent="VIP,FooterClick,Instagram" data-gaevent-direct="true">
                            Instagram</a>
                    </li>
                    <li>
                        <a href="https://twitter.com/eBay_KA" target="_blank"
                           data-gaevent="VIP,FooterClick,Twitter" data-gaevent-direct="true">
                            Twitter</a>
                    </li>
                    <li>
                        <a href="https://www.pinterest.de/eBay_KA/" target="_blank"
                           data-gaevent="VIP,FooterClick,Pinterest" data-gaevent-direct="true">
                            Pinterest</a>
                    </li>
                </ul>
            <li>
            <li class="flexlist--column l-col">
                <ul class="list">
                    <li class="flexlist--header">Allgemein</li>
                    <li><a href="/s-beliebte-angebote.html"
                           data-gaevent="VIP,FooterClick,PopularAds">Beliebte&nbsp;Angebote</a></li>
                    <li>
                                        <a href="/s-beliebte-suchen.html?catId=203"
                                           data-gaevent="VIP,FooterClick,PopularCategories">
                                            Beliebte Suchen für Mietwohnungen</a></li>
                                <li>
                                <a href="/s-katalog-orte.html"
                                   data-gaevent="VIP,FooterClick,BrowseLocations">
                                    Anzeigen&nbsp;Übersicht</a>
                            </li>
                        <li>
                        <a href="/s-unternehmensseiten-verzeichnis.html"
                           data-gaevent="VIP,FooterClick,LandingPageStoresOverview">
                            Übersicht&nbsp;der&nbsp;Unternehmensseiten</a>
                    </li>
                    <li>
                        <a href="https://seiten.ebay-kleinanzeigen.de/autobewertung"
                           data-gaevent="VIP,FooterClick,CarRating" data-gaevent-direct="true">
                            Autobewertung</a>
                    </li>
                </ul>
            <li>
        </ul>
    </nav>

    <p class="text-light">
        <small>Copyright © 2005-2018 eBay Kleinanzeigen GmbH. Alle Rechte vorbehalten. Ausgewiesene Marken gehören ihren jeweiligen Eigentümern.</small>
    </p>

    <nav id="site-footer-tbs" class="tabnav-tiny tinytabs">
            <ul>
                <li data-panel="site-footer-tbs-brwseloc"><a id="site-footer-tablink-browseloc">Weitere Kategorien</a>
                </li>
                <li data-panel="site-footer-tbs-partner"><a id="site-footer-tablink-partner">Partner</a></li>
            </ul>

            <div id="site-footer-tbs-popsrch" class="tinytabs-panel" style="display: block;">
                </div>

            <div id="site-footer-tbs-brwseloc" class="tinytabs-panel is-hidden">
                <ul class="linklist-tiny">
                        <li><a href="/s-wohnung-mieten/dachgeschosswohnung/burg-grambke/c203l13541+wohnung_mieten.wohnungstyp_s:dachgeschosswohnung">Dachgeschosswohnung in Burg-Grambke</a></li>
                        <li><a href="/s-wohnung-mieten/erdgeschosswohnung/burg-grambke/c203l13541+wohnung_mieten.wohnungstyp_s:erdgeschosswohnung">Erdgeschosswohnung in Burg-Grambke</a></li>
                        <li><a href="/s-wohnung-mieten/etagenwohnung/burg-grambke/c203l13541+wohnung_mieten.wohnungstyp_s:etagenwohnung">Etagenwohnung in Burg-Grambke</a></li>
                        <li><a href="/s-wohnung-mieten/hochparterre/burg-grambke/c203l13541+wohnung_mieten.wohnungstyp_s:hochparterre">Hochparterre in Burg-Grambke</a></li>
                        <li><a href="/s-wohnung-mieten/loft/burg-grambke/c203l13541+wohnung_mieten.wohnungstyp_s:loft">Loft in Burg-Grambke</a></li>
                        <li><a href="/s-wohnung-mieten/maisonette/burg-grambke/c203l13541+wohnung_mieten.wohnungstyp_s:maisonette">Maisonette in Burg-Grambke</a></li>
                        <li><a href="/s-wohnung-mieten/penthouse/burg-grambke/c203l13541+wohnung_mieten.wohnungstyp_s:penthouse">Penthouse in Burg-Grambke</a></li>
                        <li><a href="/s-wohnung-mieten/souterrain/burg-grambke/c203l13541+wohnung_mieten.wohnungstyp_s:souterrain">Souterrain in Burg-Grambke</a></li>
                        <li><a href="/s-wohnung-mieten/terrassenwohnung/burg-grambke/c203l13541+wohnung_mieten.wohnungstyp_s:terrassenwohnung">Terrassenwohnung in Burg-Grambke</a></li>
                        <li><a href="/s-wohnung-mieten/andere/burg-grambke/c203l13541+wohnung_mieten.wohnungstyp_s:andere">Sonstige in Burg-Grambke</a></li>
                        <li><a href="/s-auf-zeit-wg/burg-grambke/c199l13541">Auf Zeit &amp; WG in Burg-Grambke</a></li>
                        <li><a href="/s-wohnung-kaufen/burg-grambke/c196l13541">Eigentumswohnungen in Burg-Grambke</a></li>
                        <li><a href="/s-ferienwohnung-ferienhaus/burg-grambke/c275l13541">Ferien- &amp; Auslandsimmobilien in Burg-Grambke</a></li>
                        <li><a href="/s-garage-lagerraum/burg-grambke/c197l13541">Garagen &amp; Stellplätze in Burg-Grambke</a></li>
                        <li><a href="/s-gewerbeimmobilien/burg-grambke/c277l13541">Gewerbeimmobilien in Burg-Grambke</a></li>
                        <li><a href="/s-grundstuecke-garten/burg-grambke/c207l13541">Grundstücke &amp; Gärten in Burg-Grambke</a></li>
                        <li><a href="/s-haus-kaufen/burg-grambke/c208l13541">Häuser zum Kauf in Burg-Grambke</a></li>
                        <li><a href="/s-haus-mieten/burg-grambke/c205l13541">Häuser zur Miete in Burg-Grambke</a></li>
                        <li><a href="/s-wohnung-mieten/burg-grambke/c203l13541">Mietwohnungen in Burg-Grambke</a></li>
                        <li><a href="/s-umzug-transport/burg-grambke/c238l13541">Umzug &amp; Transport in Burg-Grambke</a></li>
                        <li><a href="/s-immobilien/sonstiges/burg-grambke/c198l13541">Weitere in Burg-Grambke</a></li>
                        <li><a href="/s-katalog-orte.html?locationId=13541">Anzeigen in Burg-Grambke</a></li>
                        </ul>
                </div>

            <div id="site-footer-tbs-partner" class="tinytabs-panel is-hidden">
                <ul class="linklist-tiny">
                    <li><a href="http://www.ebay.de" target="_blank">eBay</a></li>
                    <li><a href="http://www.mobile.de" target="_blank">mobile.de</a></li>
                    <li><a href="http://www.brands4friends.de/" target="_blank">brands4friends</a></li>
                    <li><a href="http://www.stubhub.de" target="_blank">StubHub</a></li>
                    <li><a href="/partner.html">Weitere eBay Angebote</a>
                    </li>
                </ul>
            </div>
        </nav>
    </footer>
<advertiser:remarketing/>
<noscript>
    <img height="1" width="1" style="display:none" alt=""
         src="https://www.facebook.com/tr?id=1418401095075716&amp;ev=PageView&amp;noscript=1"/>
</noscript>
</body>
</html>
`;
