const Apartment = require('./../../models/apartment');
const sourceCode = require('./../../service/sourceCodeService');
const cheerio = require("cheerio");
const logger = require('./../../service/logger');
const parser = require('./../../service/parser');
const map = require('lodash/map');
const includes = require('lodash/includes');
const replace = require('lodash/replace');
const geocoder = require('./../../service/geocoder');

function exposeCrawler () {
    
    let apt, $;
    
    function getTitle() {
        return $('h1').text();
    }
    
    function getDescription() {
        return $('#viewad-description-text').text();
    }
    
    function getPriceTotal() {
        let price = 0;
        
        $('.attributelist-striped dt').each(function (index, element) {
            if (includes($(element).text(), 'Warmmiete')) {
                price = parser.matchPrice($(element).next().text().trim() + '€');
                
                return false;
            }
        });
        
        return price;
    }
    
    function getPriceCold() {
        return apt.price.total - apt.price.utilities;
    }
    
    function getPriceUtilities() {
        let utilities = 0;
        let heating = 0;
        
        $('.attributelist-striped dt').each(function (index, element) {
            if (includes($(element).text(), 'Nebenkosten')) {
                utilities = parser.matchPrice($(element).next().text().trim() + '€');
            }
            if (includes($(element).text(), 'Heizkosten')) {
                heating = parser.matchPrice($(element).next().text().trim() + '€');
            }
        });
        
        return utilities + heating;
    }
    
    function getRooms() {
        let rooms = 0;
        
        $('.attributelist-striped dt').each(function (index, element) {
            if (includes($(element).text(), 'Zimmer')) {
                rooms = parseInt($(element).next().text().trim());
                
                return false;
            }
        });
        
        return rooms;
    }
    
    function getSize() {
        let size = 0;
        
        $('.attributelist-striped dt').each(function (index, element) {
            if (includes($(element).text(), 'Wohnfläche')) {
                size = parseInt($(element).next().text().trim());
                
                return false;
            }
        });
        
        return size;
    }
    
    function getImages() {
        let imgId = '$_10';
        
        return map($('#viewad-thumbnail-list img'), function(el) {
            return {
                url: replace($(el).attr('src'), '$_6', imgId),
                caption: $(el).attr('alt')
            }
        });
    }
    
    function getAddress() {
        let address = '';
        
        $('.attributelist-striped dt').each(function (index, element) {
            if (includes($(element).text(), 'Ort')) {
                address = parser.removeMultipleWhitespaces($(element).next().text().trim().replace(/\n/g,'').replace(/\t/g,''));
                
                return false;
            }
        });
        
        return address;
    }
    
    function getFeatures() {
        let features = [];
        
        $('.attributelist-striped dt').each(function (index, element) {
            if (includes($(element).text(), 'Ausstattung')) {
                $(element).next().find('a').each(function (index, el) {
                    features.push($(el).text().trim());
                });
                
                return false;
            }
        });
        
        return features;
    }
    
    function getFreeFrom() {
        let month = false;
        let year = false;
        
        $('.attributelist-striped dt').each(function (index, element) {
            if (includes($(element).text(), 'ab Monat')) {
                month = $(element).next().text().trim();
            }
            if (includes($(element).text(), 'ab Jahr')) {
                year = $(element).next().text().trim();
            }
        });
        
        if (month && year) {
            return month + '.' + year;
        }
        
        return null;
    }
    
    function getPetsAllowed() {
        return null;
    }
    
    function getBuildYear() {
        return null;
    }
    
    function isExpired() {
        return $('#srchrslt-adexpired').length;
    }
    
    function start(providerId, cityId, url, callback) {
        
        if (!providerId || !cityId || !url) {
            logger.warn('Cannot start crawler without proper information!');
            
            return false;
        }
        
        apt = new Apartment();
        
        sourceCode.fetch(url, providerId, 'expose', function(html) {
            if (html) {
                
                $ = cheerio.load(html);

                if (isExpired(url)) {
                    logger.log('EXPIRED: ' + url);

                    callback();

                    return false;
                }

                apt.providerId = providerId;
                apt.cityId = cityId;
                apt.link = url;
                apt.title = getTitle();
                apt.description = getDescription();
                apt.price.total = getPriceTotal();
                apt.price.utilities = getPriceUtilities();
                apt.price.cold = getPriceCold();
                apt.rooms = getRooms();
                apt.size = getSize();
                apt.images = getImages();
                apt.location.address = getAddress();
                apt.features = getFeatures();
                apt.freeFrom = getFreeFrom();
                apt.petsAllowed = getPetsAllowed();
                apt.buildYear = getBuildYear();

                geocoder.parse(apt.location.address, function (location) {
                    apt.location = location;
                    
                    apt.save(function (err) {
                        if (err) {
                            logger.error(err);
                        }
    
                        console.log(apt);
                        
                        callback();
                    });
                });
            }
            else {
                callback();
            }
        });
    }
    
    return {
        start: start
    }
}

module.exports = exposeCrawler();
