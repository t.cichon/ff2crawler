const Apartment = require('./../../models/apartment');
const sourceCode = require('./../../service/sourceCodeService');
const cheerio = require("cheerio");
const logger = require('./../../service/logger');
const parser = require('./../../service/parser');
const map = require('lodash/map');
const includes = require('lodash/includes');
const extend = require('lodash/extend');
const geocoder = require('./../../service/geocoder');

function exposeCrawler () {
    
    let apt, $;
    
    function getTitle() {
        return $('h1').text().trim();
    }
    
    function getDescription() {
        let text = '';
        
        $('.section_label').each(function (index, element) {
            if (includes($(element).text(), 'Ausstattung')) {
                text += ' ' +  $(element).next().text().trim();
            }
            if (includes($(element).text(), 'Sonstiges')) {
                text += ' ' +  $(element).next().text().trim();
            }
            if (includes($(element).text(), 'Lage')) {
                text += ' ' +  $(element).next().text().trim();
            }
        });
        
        return text.substring(1);
    }
    
    function getPriceCold() {
        let price = null;
        
        $('.datatable:not(.energytable) .datalabel').each(function (index, element) {
            if (includes($(element).text(), 'Kaltmiete')) {
                
                price = parser.matchPrice($(element).next().text().trim());
                
                return false;
            }
        });
        
        return price;
    }
    
    function getPriceUtilities() {
        let utilities = null;
        let heating = null;
        
        $('.datatable:not(.energytable) .datalabel').each(function (index, element) {
            if (includes($(element).text(), 'Nebenkosten')) {
                
                utilities = parser.matchPrice($(element).next().text().trim());
            }
            
            if (includes($(element).clone().children().remove().end().text(), 'Heizkosten')) {
                
                heating = parser.matchPrice($(element).next().text().trim());
            }
        });
        
        return utilities + heating;
    }
    
    function getPriceTotal() {
        return apt.price.cold + apt.price.utilities;
    }
    
    function getRooms() {
        return parseInt($('.hardfact.rooms').text()) || 1;
    }
    
    function getSize() {
        let size = null;
        
        $('.hardfactlabel').each(function (index, element) {
            if (includes($(element).text(), 'Wohnfl')) {
                
                size = parseInt($(element).parent().text().trim());
                
                return false;
            }
        });
        
        return size;
    }
    
    function getImages() {
        let images = [];

        try {
            let IwAG = {};
            let script = $('script[src^="//media-static.immowelt.org/_ts/iwag/expose/expose.bundle.pack.js"]').next().get()[0].children[0].data;
            
            IwAG.$ = $;
            IwAG.$.extend = extend;
            
            eval(script);
            
            images = map(IwAG.Vars.exposeConfig.imageData, function(el) {
    
                return {
                    url: el.srcImageStage,
                    caption: parser.toRegularCase(el.caption)
                }
            });
            
        }
        catch(e) {
            console.log(e);
        }
        
        return images;
    }
    
    function getAddress() {
        let address = '';

        try {
            let IwAG = {};
            let script = $('script[src^="//media-static.immowelt.org/_ts/iwag/expose/expose.bundle.pack.js"]').next().get()[0].children[0].data;
            
            IwAG.$ = $;
            IwAG.$.extend = extend;
            
            eval(script);
            
            address =  IwAG.Vars.MapOptions.street ?
                IwAG.Vars.MapOptions.street + ', ' + IwAG.Vars.MapOptions.address :
                IwAG.Vars.MapOptions.address
        }
        catch(e) {
            console.log(e);
        }
        
        return address;
    }
    
    function getFeatures() {
        return $('.merkmale').text().trim().split(',');
    }
    
    function getFreeFrom() {
        let freeFrom = null;
        
        $('.section_content strong').each(function (index, element) {
            if (includes($(element).text(), 'frei ab ')) {
                
                freeFrom = $(element).text().trim().replace(/frei ab /g,'');
                
                return false;
            }
        });
        
        return freeFrom;
    }
    
    function getPetsAllowed() {
        return null;
    }
    
    function getBuildYear() {
        let buildYear = null;
        
        $('.section_content p').each(function (index, element) {
            if (includes($(element).text(), 'Baujahr ')) {
                
                buildYear = $(element).text().trim().replace(/Baujahr /g,'');
                
                return false;
            }
        });
        
        return buildYear;
    }
    
    function isExpired() {
        return $('h2').text() === 'Objekt nicht mehr verfügbar.';
    }
    
    function start(providerId, cityId, url, callback) {
        
        if (!providerId || !cityId || !url) {
            logger.warn('Cannot start crawler without proper information!');
            
            return false;
        }
        
        apt = new Apartment();
        
        sourceCode.fetch(url, providerId, 'expose', function(html) {
            if (html) {
                $ = cheerio.load(html);
                
                // if (isExpired(url)) {
                //     logger.log('EXPIRED: ' + url);
                //
                //     callback();
                //
                //     return false;
                // }
                
                apt.providerId = providerId;
                apt.cityId = cityId;
                apt.link = url;
                apt.title = getTitle();
                apt.description = getDescription();
                apt.price.cold = getPriceCold();
                apt.price.utilities = getPriceUtilities();
                apt.price.total = getPriceTotal();
                apt.rooms = getRooms();
                apt.size = getSize();
                apt.images = getImages();
                apt.location.address = getAddress();
                apt.features = getFeatures();
                apt.freeFrom = getFreeFrom();
                apt.petsAllowed = getPetsAllowed();
                apt.buildYear = getBuildYear();
                
                geocoder.parse(apt.location.address, function (location) {
                    apt.location = location;
                    
                    apt.save(function (err) {
                        if (err) {
                            logger.error(err);
                        }
    
                        console.log(apt);
                        
                        callback();
                    });
                });
            
            }
            else {
                callback();
            }
        });
    }
    
    return {
        start: start
    }
}

module.exports = exposeCrawler();
