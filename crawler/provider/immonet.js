const Apartment = require('./../../models/apartment');
const sourceCode = require('./../../service/sourceCodeService');
const cheerio = require("cheerio");
const logger = require('./../../service/logger');
const parser = require('./../../service/parser');
const map = require('lodash/map');
const geocoder = require('./../../service/geocoder');

function exposeCrawler () {
    
    let apt, $;
    
    function getTitle() {
        return $('h1').text().trim();
    }
    
    function getDescription() {
        let text = '';
        
        let features = $('#ausstattung');
        let description = $('#objectDescription');
        let location = $('#locationDescription');
        let other = $('#otherDescription');
        
        if (features) {
            text += features.text().trim();
        }
        if (description) {
            text += ' ' + description.text().trim();
        }
        if (location) {
            text += ' ' + location.text().trim();
        }
        if (other) {
            text += ' ' + other.text().trim();
        }
        
        return text;
    }
    
    function getPriceCold() {
        return parser.matchPrice($('#priceid_2').text().trim());
    }
    
    function getPriceUtilities() {
        let utilities = parser.matchPrice($('#priceid_20').text().trim());
        let heating = parser.matchPrice($('#priceid_5').text().trim());
        
        return utilities + heating;
    }
    
    function getPriceTotal() {
        return apt.price.cold + apt.price.utilities;
    }
    
    function getRooms() {
        return parseInt($('#equipmentid_1').text()) || 1;
    }
    
    function getSize() {
        return parseInt($('#areaid_1').text());
    }
    
    function getImages() {
        return map($('.fotorama div'), function(el) {
            return {
                url: $(el).data('img'),
                caption: $(el).data('caption')
            }
        });
    }
    
    function getAddress() {
        return $('.mini-map-icon-svg')
            .next()
            .clone()
            .children()
            .remove()
            .end()
            .text()
            .trim()
            .replace(/\n/g,' ')
            .replace(/\t/g,'')
            .replace(/\s\s+/g, ', ');
    }
    
    function getFeatures() {
        return map($('#panelFeatures ul.list-unstyled span.block'), function(el) {
            return $(el).text().trim();
        });
    }
    
    function getFreeFrom() {
        return $('#deliveryValue').text().trim() || null;
    }
    
    function getPetsAllowed() {
        return null;
    }
    
    function getBuildYear() {
        return parseInt($('#yearbuild').text().trim()) || null;
    }
    
    function isExpired() {
        return $('h2').text() === 'Objekt nicht mehr verfügbar.';
    }
    
    function start(providerId, cityId, url, callback) {
        
        if (!providerId || !cityId || !url) {
            logger.warn('Cannot start crawler without proper information!');
            
            return false;
        }
        
        apt = new Apartment();
        
        sourceCode.fetch(url, providerId, 'expose', function(html) {
            if (html) {
                $ = cheerio.load(html);
                
                if (isExpired(url)) {
                    logger.log('EXPIRED: ' + url);

                    callback();

                    return false;
                }
                
                apt.providerId = providerId;
                apt.cityId = cityId;
                apt.link = url;
                apt.title = getTitle();
                apt.description = getDescription();
                apt.price.cold = getPriceCold();
                apt.price.utilities = getPriceUtilities();
                apt.price.total = getPriceTotal();
                apt.rooms = getRooms();
                apt.size = getSize();
                apt.images = getImages();
                apt.location.address = getAddress();
                apt.features = getFeatures();
                apt.freeFrom = getFreeFrom();
                apt.petsAllowed = getPetsAllowed();
                apt.buildYear = getBuildYear();
                
                geocoder.parse(apt.location.address, function (location) {
                    apt.location = location;
                    
                    apt.save(function (err) {
                        if (err) {
                            logger.error(err);
                        }
    
                        console.log(apt);
                        
                        callback();
                    });
                });
            }
            else {
                callback();
            }
        });
    }
    
    return {
        start: start
    }
}

module.exports = exposeCrawler();
