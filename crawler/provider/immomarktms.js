const Apartment = require('./../../models/apartment');
const sourceCode = require('./../../service/sourceCodeService');
const cheerio = require("cheerio");
const logger = require('./../../service/logger');
const parser = require('./../../service/parser');
const map = require('lodash/map');
const includes = require('lodash/includes');
const geocoder = require('./../../service/geocoder');

function exposeCrawler () {
    
    let apt, $;
    
    function getTitle() {
        return $('h1').text().trim();
    }
    
    function getDescription() {
        let text = '';
        
        $('#edc-description p').each(function (index, element) {
            text += ' ' + $(element).text().trim();
        });
        
        return text.substring(1);
    }
    
    function getPriceCold() {
        let priceCold = null;
        
        $('#ecc-data div[class^="col-"]').each(function (index, element) {
            if (includes($(element).text(), 'kalt­miete')) {
                priceCold = parser.matchPrice($(element).next().text());
                
                return false;
            }
        });
        
        return priceCold;
    }
    
    function getPriceUtilities() {
        let utilities = null;
        let heating = null;
        
        $('#ecc-data div[class^="col-"]').each(function (index, element) {
            if (includes($(element).text(), 'Betriebs- / Neben­kos­ten')) {
                utilities = parser.matchPrice($(element).next().text());
            }
            if (includes($(element).text(), 'Heiz­kosten')) {
                heating = parser.matchPrice($(element).next().text());
            }
        });
        
        return utilities + heating;
    }
    
    function getPriceTotal() {
        return apt.price.cold + apt.price.utilities;
    }
    
    function getRooms() {
        let rooms = null;
        
        $('#edc-data div[class^="col-"]').each(function (index, element) {
            if (includes($(element).text(), 'Zimmer')) {
                rooms = parseInt($(element).next().text());
                
                return false;
            }
        });
        
        return rooms;
    }
    
    function getSize() {
        let size = null;
        
        $('#edc-data div[class^="col-"]').each(function (index, element) {
            if (includes($(element).text(), 'Wohnfläche')) {
                size = parseInt($(element).next().text());
                
                return false;
            }
        });
        
        return size;
    }
    
    function getImages() {
        return map($('.fotorama a'), function(el) {
            return {
                url: $(el).data('full'),
                caption: $(el).data('caption')
            }
        });
    }
    
    function getAddress() {
        return $('.eps-item-location')
            .text()
            .trim();
    }
    
    function getFeatures() {
        return map($('#edc-equipment .row > div:first-of-type'), function(el) {
            return $(el).text().trim();
        });
    }
    
    function getFreeFrom() {
        let freeFrom = null;
        
        $('#edc-data div[class^="col-"]').each(function (index, element) {
            if (includes($(element).text(), 'verfügbar ab')) {
                freeFrom = $(element).next().text().trim();
                
                return false;
            }
        });
        
        return freeFrom;
    }
    
    function getPetsAllowed() {
        return null;
    }
    
    function getBuildYear() {
        let buildYear = null;
        
        $('#edc-data div[class^="col-"]').each(function (index, element) {
            if (includes($(element).text(), 'Baujahr')) {
                buildYear = parseInt($(element).next().text().trim());
                
                return false;
            }
        });
        
        return buildYear;
    }
    
    function isExpired() {
        return $('h2').text() === 'Objekt nicht mehr verfügbar.';
    }
    
    function start(providerId, cityId, url, callback) {
        
        if (!providerId || !cityId || !url) {
            logger.warn('Cannot start crawler without proper information!');
            
            return false;
        }
        
        apt = new Apartment();
        
        sourceCode.fetch(url, providerId, 'expose', function(html) {
            if (html) {
                $ = cheerio.load(html);
                
                // if (isExpired(url)) {
                //     logger.log('EXPIRED: ' + url);
                //
                //     callback();
                //
                //     return false;
                // }
                //
                apt.providerId = providerId;
                apt.cityId = cityId;
                apt.link = url.split('?')[0]; // remove all unnecessary params
                apt.title = getTitle();
                apt.description = getDescription();
                apt.price.cold = getPriceCold();
                apt.price.utilities = getPriceUtilities();
                apt.price.total = getPriceTotal();
                apt.rooms = getRooms();
                apt.size = getSize();
                apt.images = getImages();
                apt.location.address = getAddress();
                apt.features = getFeatures();
                apt.freeFrom = getFreeFrom();
                apt.petsAllowed = getPetsAllowed();
                apt.buildYear = getBuildYear();
                
                geocoder.parse(apt.location.address, function (location) {
                    apt.location = location;
                    
                    apt.save(function (err) {
                        if (err) {
                            logger.error(err);
                        }
    
                        console.log(apt);
                        
                        callback();
                    });
                });
            }
            else {
                callback();
            }
        });
    }
    
    return {
        start: start
    }
}

module.exports = exposeCrawler();
