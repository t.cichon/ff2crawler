const Apartment = require('./../../models/apartment');
const sourceCode = require('./../../service/sourceCodeService');
const cheerio = require("cheerio");
const logger = require('./../../service/logger');
const parser = require('./../../service/parser');
const map = require('lodash/map');
const geocoder = require('./../../service/geocoder');

function exposeCrawler () {
    
    let apt, $;
    
    function getTitle() {
        return $('h1').text();
    }
    
    function getDescription() {
        return $('.is24qa-objektbeschreibung').text();
    }
    
    function getPriceTotal() {
        return parser.matchPrice($('.is24qa-gesamtmiete').text());
    }
    
    function getPriceCold() {
        return parser.matchPrice($('.is24qa-kaltmiete').text());
    }
    
    function getPriceUtilities() {
        return apt.price.total - apt.price.cold;
    }
    
    function getRooms() {
        return parseInt($('.is24qa-zimmer').text()) || 1;
    }
    
    function getSize() {
        return parseInt($('.is24qa-flaeche.is24-value').text());
    }
    
    function getImages() {
        return map($('#fullscreenGallery .sp-slides .sp-slide img'), function(el) {
            return {
                url: $(el).data('src'),
                caption: $(el).data('caption')
            }
        });
    }
    
    function getAddress() {
        let zip = $('.address-with-map-link .zip-region-and-country');
        
        return zip.prev().hasClass('block') ?
            zip.prev().text() + " " + zip.text() :
            zip.text();
    }
    
    function getFeatures() {
        return map($('.boolean-listing span'), function(el) {
            return $(el).text();
        });
    }
    
    function getFreeFrom() {
        return $('.is24qa-bezugsfrei-ab').text() || null;
    }
    
    function getPetsAllowed() {
        return $('.is24qa-haustiere').text() || null;
    }
    
    function getBuildYear() {
        return parseInt($('.is24qa-baujahr').text()) || null;
    }
    
    function isExpired() {
        return $('.status-warning').length;
    }
    
    function start(providerId, cityId, url, callback) {
        
        if (!providerId || !cityId || !url) {
            logger.warn('Cannot start crawler without proper information!');
            
            return false;
        }
        
        apt = new Apartment();
        
        sourceCode.fetch(url, providerId, 'expose', function(html) {
            if (html) {
                $ = cheerio.load(html);
                
                if (isExpired(url)) {
                    logger.log('EXPIRED: ' + url);
                    
                    callback();
                    
                    return false;
                }
                
                apt.providerId = providerId;
                apt.cityId = cityId;
                apt.link = url;
                apt.title = getTitle();
                apt.description = getDescription();
                apt.price.total = getPriceTotal();
                apt.price.cold = getPriceCold();
                apt.price.utilities = getPriceUtilities();
                apt.rooms = getRooms();
                apt.size = getSize();
                apt.images = getImages();
                apt.location.address = getAddress();
                apt.features = getFeatures();
                apt.freeFrom = getFreeFrom();
                apt.petsAllowed = getPetsAllowed();
                apt.buildYear = getBuildYear();
                
                geocoder.parse(apt.location.address, function (location) {
                    apt.location = location;
                    
                    apt.save(function (err) {
                        if (err) {
                            logger.error(err);
                        }
    
                        console.log(apt);
                        
                        callback();
                    });
                });
            }
            else {
                callback();
            }
        });
    }
    
    return {
        start: start
    }
}

module.exports = exposeCrawler();
