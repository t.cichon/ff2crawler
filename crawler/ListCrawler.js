const GlobalEvents = require('../events/eventEmitter');
const request = require('request');
const needle = require('needle');
const random_ua = require('random-ua');
const cheerio = require("cheerio");
const format = require("vrep").format;
const find = require("lodash/find");
const findIndex = require("lodash/findIndex");
const emitter = require('emitter').EventEmitter;
const logger = require('./../service/logger');
const EVENTS = require('../events/interface');

// models
const Job = require('../models/job');
const Provider = require('./../models/provider');

require('dotenv').config();

let ListCrawler = {
    run: function (providerId, cityId) {
        
        let providerDocument = null;
        let baseUrl = null;
        let crawlUrl = null;
        let selector = null;
        
        Provider.findOne({ _id: providerId }).exec(function (err, provider) {
            
            if (!err && provider && provider.isActive) {
                let city = find(provider.city, { _id: cityId });
                
                if (city && city.isActive) {
                    providerDocument = provider;
                    
                    baseUrl = provider.baseUrl;
                    crawlUrl = city.url;
                    selector = provider.selector;
                    
                    crawl();
                }
                else if (!city.isActive) {
                    logger.log('Skipping ' +cityId + ':' + providerId + ' ...');
                
                    callDoneEvent(true);
                }
            }
            else if (provider && !provider.isActive) {
                logger.log('Skipping ' + providerId + ' - ' + cityId + ' ...');
                
                callDoneEvent(true);
            }
            else {
                callDoneEvent(true);
            }
        });
        
        function crawl() {
            
            logger.log(format("Run crawl for: {providerId} - {cityId}", {
                providerId: providerId,
                cityId: cityId
            }));
            
            let requestOptions = {
                url: crawlUrl,
                // headers: {
                //     'User-Agent': random_ua.generate()
                // },
                follow_max: 0,
                compressed: true,
                timeout: 10000,
                open_timeout: 10000 // needle timeout
            };
            
            request(requestOptions, function (error, response, html) {
                !error && response.statusCode === 200 ?
                    parseHtml(html) :
                    tryStreamCrawl();
            });
            
            /*
             * optional crawling method via streaming
             */
            function tryStreamCrawl() {
                let stream = needle.get(requestOptions.url, requestOptions);
                let html = '';
                
                // read stream
                stream.on('readable', function () {
                    while (data = this.read()) {
                        html += data.toString();
                    }
                });
                
                stream.on('end', function () {
                    parseHtml(html);
                });
                
                stream.on('done', function(err, resp) {
                    if (err) {
                        console.log(err);
                        
                        callDoneEvent();
                    }
                });
            }
            
            /*
             * parse html, find links and write them synchronous into db
             */
            function parseHtml(html) {
                
                let $ = cheerio.load(html);
                let links = $(selector);
                let linkCount = links.get().length;
                let index = 0;
                let findings = 0;
                
                processMatch();
                
                function processMatch() {
                    
                    let job = {
                        providerId: providerId,
                        cityId: cityId,
                        link: format('{baseUrl}{scrapeUrl}', {
                            baseUrl: baseUrl,
                            scrapeUrl: $(links[index]).attr('href')
                        })
                    };
                    
                    if (index === linkCount) {
                        processStatistics(findings);
                        
                        callDoneEvent();
                    }
                    else {
                        
                        Job.findOne({ link: job.link }).exec(function (err, entry) {
                            index++;
                            
                            if (!entry) {
                                
                                createJob(job, function () {
                                    findings++;
                                    processMatch();
                                });
                            }
                            else  {
                                processMatch();
                            }
                        });
                    }
                }
            }
            
            function createJob(data, callback) {
                
                Job.create(data, function(err) {
                    
                    if (err) {
                        console.log(err);
                    }
                    else {
                        logger.success("created new job...!");
                        
                        ListCrawler.emit(EVENTS.NEW.JOB);
                        GlobalEvents.emit(EVENTS.NEW.JOB, data);
                    }
                    
                    callback.call();
                });
            }
            
            function processStatistics(findings) {
                
                let index = findIndex(providerDocument.city, { _id: cityId });
                
                providerDocument.city[index].lastCrawl = new Date();
                providerDocument.city[index].apartmentsFound += findings;
                
                providerDocument.save(function (err) {
                    if (err) console.log(err);
                });
            }
        }
        
        function callDoneEvent(instant) {
            if (instant) {
                ListCrawler.emit('done');
            }
            else {
                setTimeout(function() {
                    
                    logger.log("-- done");
                    ListCrawler.emit('done');
                }, 2000);
            }
        }
        
    }
};

emitter.extend(ListCrawler);

module.exports = {
    ListCrawler: ListCrawler
};
