# ff2crawler

This project provides the base for the [ff2](https://gitlab.com/t.cichon/ff2) project and scrapes and delivers the data necessary for the frontend.
There are some steps you need to do before running the scraper for the first time.

1. Duplicate `.env.example` and name it `.env`. Change the variables inside the environment file if you like. By default the mongo database is called `flatflow`, but you can always change the name prior to calling the import. If you decide to rename it, go to the `package.json` file and change line 6 accordingly for the next step. 
2. Install the package `mongoimport` globally with `npm install -g mongoimport`. 
3. Run `npm run import` for creating the `providers` collection and import provider settings for the crawler.
4. install npm dependencies with `npm install`.
5. Make sure your database instance is running or start it with `mongod`.
6. Run server and start crawling by calling `npm run start`.

## API Routes
### Get all apartments
You will find a list of all scraped apartments by going to `http://localhost:3003/api/apartments`. This will render a JSON response with all found apartments, sorted by actuality.

#### Filtering
You can add a list of parameters to the URL for filtering your apartments. All results are by default paginated.

- `page`: Defines the current page of the list. Default: `1`.
- `limit`: Defines the count of results per page. Default: `20`.
- `cityId`: By default either the german cities `muenster` or `bremen`. You will get all apartments of all cities if this param is not provided.
- `providerId`: Either `ebayKleinanzeigen`, `immonet`, `immobilienscout24`, `immowelt` or `immomarktms`. You are free to add as many new providers as you like.
- `minRooms` and `maxRooms`: Both parameters must be set for filtering rooms.
- `minPrice` and `maxPrice`: Both parameters must be set for filtering prices.
- `minSize` and `maxSize`: Both parameters must be set for filtering apartment sizes.
- `features`: A list of features for the apartment. List must be comma separated. Right now all features are in German.
- `days`: Shows apartments not older than the provided value in days. Default: `7` days.

Example: `http://localhost:3003/api/apartments?page=1&limit=5&cityId=muenster&days=3&features=balkon,terrasse`

### Get a single Apartment
You only need one parameter to get all information about a single apartment.

Example: `http://localhost:3003/api/apartment?id?{id}`

### Get a list of cities and providers
Lists all `providerId`s and `cityId`s.

Example: `http://localhost:3003/api/cities`

## Important security notice
It is up to you to secure your database! By default all MongoDBs are open and accessible by everyone just by connecting to `your-ip:27017` without any credentials. Go to [DigitalOcean's tutorial](https://www.digitalocean.com/community/tutorials/how-to-install-and-secure-mongodb-on-ubuntu-16-04) if you don't know how to do that. If everything is set up just add your username and password to your `.env` file and restart your server.
