require('dotenv').config();

const mongoose = require('mongoose');
const format = require("vrep").format;
const logger = require('./service/logger');
const GlobalEvents = require('./events/eventEmitter');
const EVENTS = require('./events/interface');

let connectionString = format('mongodb://{credentials}{host}:{port}/{database}', {
    credentials: !!process.env.DB_USER && !!process.env.DB_PASS ?
        `${process.env.DB_USER}:${process.env.DB_PASS}@` :
        '',
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    database: process.env.DB_NAME,
});

function connect() {

    mongoose.connect(connectionString);
    
    mongoose.connection.on(EVENTS.DB.CONNECTED, function () {
        logger.success("Mongoose connected...! ");
        
        GlobalEvents.emit(EVENTS.DB.CONNECTED);
        
    });
    
    mongoose.connection.on(EVENTS.DB.ERROR, function(err) {
        logger.error("Mongoose default connection has occured "+err+" error");
    });

    mongoose.connection.on(EVENTS.DB.DISCONNECTED, function(){
        logger.log("Mongoose default connection is disconnected");
    });

    process.on('SIGINT', function(){
        mongoose.connection.close(function(){
            logger.warn("Mongoose default connection is disconnected due to application termination");
            process.exit(0)
        });
    });
    
}

connect();

module.exports = {
    connectionString: connectionString
};
