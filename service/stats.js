const GlobalEmitter = require('./../events/eventEmitter');
const Statistics = require('./../models/statistics');
const Coordinator = require('./../Coordinator').Coordinator;
const EVENTS = require('./../events/interface');

function stats () {
    let stats = null;
    
    GlobalEmitter.on(EVENTS.DB.CONNECTED, function () {
        
        Statistics.findOne({ _id: 'statistics' }).exec(function (err, statistic) {
            
            if (!statistic) {
                stats = new Statistics();
                stats._id = 'statistics';
                
                stats.save();
            }
            else {
                stats = statistic;
            }
        });
        
    });
    
    function saveNextRun() {
        if (stats) {
            
            // console.log(Coordinator.nextRunMoment);
            
            // stats.nextCrawl = Coordinator.nextRunMoment;
            // stats.save();
        }
    }
    
    return {
        saveNextRun: saveNextRun
    }
}

module.exports = stats();
