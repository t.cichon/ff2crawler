
function parser() {
    
    function matchPrice(priceString) {
        let price = null;
        let regex = /([0-9]{1}\D?[0-9]{0,5}(?:.[0-9]{0,2}).{0,2}?)\s?(€|euro|eur|EUR|,-|kalt|warm|KM)/;
        let match = priceString.trim().match(regex);
        
        if (match !== null) {
            
            if (typeof match[1] !== 'undefined') {
                price = match[1];
                
                // remove decimal points for like 231,12 or 432.43
                let secondLastChar = price[price.length - 2];
                let thirdLastChar  = price[price.length - 3];
                
                if (secondLastChar === '.' || secondLastChar === ',') {
                    price = price.slice(0, -2);
                }
                else if (thirdLastChar === '.' || thirdLastChar === ',') {
                    price = price.slice(0, -3);
                }
                
                // remove thousand period like 1.321€
                if (price.indexOf('.') !== -1) {
                    price = price.replace('.', '');
                }
                
                price = parseInt(price);
            }
        }
        
        return price;
    }
    
    function removeMultipleWhitespaces(string) {
        return string.replace(/ +(?= )/g,'');
    }
    
    function toRegularCase(string) {
        return string.charAt(0).toUpperCase() + string.substr(1);
    }
    
    return {
        matchPrice: matchPrice,
        toRegularCase: toRegularCase,
        removeMultipleWhitespaces: removeMultipleWhitespaces
    }
    
}

module.exports = parser();
