const chalk = require('chalk');

let normal = chalk.bold.white;
let warning = chalk.bold.yellow;
let error = chalk.bold.magenta;
let success = chalk.bold.cyan;
let info = chalk.bgGreen;

module.exports = {
    log: function (message) {
        console.log(normal(message));
    },
    info: function (message) {
        console.log(info(message));
    },
    warn: function (message) {
        console.log(warning(message));
    },
    error: function (message) {
        console.log(error(message));
    },
    success: function (message) {
        console.log(success(message));
    },
};
