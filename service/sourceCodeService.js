const request = require('request');
const needle = require('needle');
const format = require("vrep").format;

require('dotenv').config();

function sourceCodeService () {

    function fetch(url, providerId, type, callback) {
        
        // FOR DEV PURPOSES ONLY!!!
        if (process.env.DEV_MODE === 'true' && providerId && type) {
            console.log('*** RETURNING DEV HTML !!! ***');
            
            let path = format('./../dummy/{providerId}_{type}', {
                providerId: providerId,
                type: type
            });
            
            callback(require(path).data);
            
            return false;
        }
        
        let requestOptions = {
            url: url,
            follow_max: 0,
            compressed: true,
            timeout: 10000,
            open_timeout: 10000 // needle timeout
        };
     
        request(requestOptions, function (error, response, html) {
            !error && response.statusCode === 200 ?
                callback(html) :
                tryStreamCrawl();
        });
    
        /*
         * optional crawling method via streaming
         */
        function tryStreamCrawl() {
            let stream = needle.get(requestOptions.url, requestOptions);
            let html = '';
        
            // read stream
            stream.on('readable', function () {
                while (data = this.read()) {
                    html += data.toString();
                }
            });
        
            // stream.on('end', function () {
            //     parseHtml(html);
            // });
        
            stream.on('done', function (err, html) {
                if (err) {
                    console.log(err);

                    callback(null);
                }
                else {
                    callback(html);
                }
            });
        }
        
    }
    
    return {
        fetch: fetch
    }

}

module.exports = sourceCodeService();
