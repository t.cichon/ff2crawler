let axios = require("axios");

function geocoder() {
    
    function parse(address, callback) {
        let location = {
            address: address,
            city: null,
            street: null,
            houseNumber: null,
            postcode: null,
            suburb: null,
            neighbourhood: null,
            lat: null,
            lng: null
        };
        
        let filter = {
            params: {
                q: address,
                language: 'de',
                pretty: 1,
                key: process.env.OPENCAGEDATA_KEY
            }
        };
        
        axios.get('https://api.opencagedata.com/geocode/v1/json', filter)
            .then(function (response) {
                let data = response.data;
                
                if (data && data.status.message === "OK" && data.results.length) {
                    
                    location.city = data.results[0].components.city || null;
                    location.street = data.results[0].components.road || null;
                    location.houseNumber = data.results[0].components.house_number || null;
                    location.postcode = data.results[0].components.postcode || null;
                    location.suburb = data.results[0].components.suburb || null;
                    location.neighbourhood = data.results[0].components.neighbourhood || null;
                    location.lat =  data.results[0].geometry.lat;
                    location.lng =  data.results[0].geometry.lng;
                }
                
                callback(location);
            })
            .catch(function (e) {
                console.log(e);
                
                callback(location);
            });
    }
    
    return {
        parse: parse,
    }
    
}

module.exports = geocoder();
