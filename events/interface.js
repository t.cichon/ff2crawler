module.exports = {
    NEW: {
        JOB: 'newJob'
    },
    DB: {
        CONNECTED: 'connected',
        ERROR: 'error',
        DISCONNECTED: 'disconnected'
    }
};
