module.exports = function(app) {
    
    const settings = {
        apiPrefix: '/api'
    };

    // Controllers
    // -------------------------------
    let controller = {
        index: require('./controllers/index'),
        api: {
            apartments: require('./controllers/api/apartments'),
            apartment: require('./controllers/api/apartment'),
            cities: require('./controllers/api/cities'),
        }
    };

    // Default Routes
    // -------------------------------
    app.get('/', controller.index.get);

    // API Routes
    // -------------------------------
    app.route(settings.apiPrefix + '/apartments')
        .get(controller.api.apartments.get);
    
    app.route(settings.apiPrefix + '/apartment')
        .get(controller.api.apartment.get);
    
    app.route(settings.apiPrefix + '/cities')
        .get(controller.api.cities.get);
};
