const random = require('lodash/random');
const ListCrawler = require("./crawler/ListCrawler").ListCrawler;
const emitter = require('emitter').EventEmitter;
const each = require('lodash/each');
const logger = require('./service/logger');
const moment = require("moment");
const format = require("vrep").format;
const stats = require('./service/stats');

// models
const Provider = require('./models/provider');
const Job = require('./models/job');

const crawler = {
    immobilienscout24: require('./crawler/provider/Immobilienscout24'),
    ebayKleinanzeigen: require('./crawler/provider/ebayKleinanzeigen'),
    immonet: require('./crawler/provider/immonet'),
    immowelt: require('./crawler/provider/immowelt'),
    immomarktms: require('./crawler/provider/immomarktms'),
};

require('dotenv').config();

let C = {
    
    timer: null,
    websites: [],
    nextRunMoment: 0,
    jobQueueRunning: false,
    sleepTime: {
        start: '00:00',
        end: '07:00'
    },
    
    start: function () {
        
        if (process.env.DEV_MODE === 'true') {
            logger.warn('*** DEV MODE: Coordinator NOT started ***');
            
            return false;
        }
        
        if (C.isSleepyTime()) {
            logger.warn('It\'s sleepy time...');
            C.scheduleNextRun();
            
            return false;
        }
        
        C.getWebsites(function () {
            
            logger.success('*** Coordinator started ***');
            
            C.processWebsite();
            
        });
    },
    
    isSleepyTime: function () {
        let startTime = moment(C.sleepTime.start, "HH:mm");
        let endTime = moment(C.sleepTime.end, "HH:mm");
        
        return moment().isBetween(startTime, endTime);
    },
    
    scheduleNextRun: function () {
    
        let nextRunMomentInMs = random(process.env.COORDINATOR_CRAWL_START, process.env.COORDINATOR_CRAWL_END);
        C.nextRunMoment = moment().add(nextRunMomentInMs, 'ms');
        C.logTime();
        
        stats.saveNextRun();
        
        C.timer = setTimeout(function () {
            
            C.start();
        }, nextRunMomentInMs);
    },
    
    logTime: function () {
        
        logger.info(format("--- Next run in {minutes} at {time} ---", {
            minutes: moment(C.nextRunMoment).fromNow(),
            time: moment(C.nextRunMoment).format('HH:mm:ss')
        }));
    },
    
    stop: function () {
        
        clearTimeout(C.timer);
        
        logger.success('*** Coordinator stopped ***');
    },
    
    getWebsites: function (callback) {
        
        Provider.find({}).exec(function (err, provider) {
            let websites = [];
            
            if (!err && provider) {
                
                each(provider, function (prv) {
                    
                    if (prv) {
                        
                        each(prv.city, function (city) {
                            websites.push({
                                providerId: prv._id,
                                cityId: city._id
                            });
                        });
                    }
                });
            }
            
            C.websites = websites;
            
            callback();
        });
    },
    
    processWebsite: function () {
        
        let website = C.websites[0];
        
        if (website) {
            
            ListCrawler.run(website.providerId, website.cityId);
            
            ListCrawler.once('done', function() {
                C.websites.shift();
                
                // scrape next provider
                C.processJobs();
            });
        }
        else {
            
            C.scheduleNextRun();
        }
    },
    
    done: function () {
        
        C.emit('done');
    },
    
    processJobs: function () {
        
        Job.findOne({ processed: false }).sort({ createdAt: 1 }).exec(function(err, job) {
            
            if (job && crawler[job.providerId]) {
                
                crawler[job.providerId].start(job.providerId, job.cityId, job.link, function () {

                    job.processed = true;

                    // update processed job
                    job.save(function (err) {
                        if (err) {
                            logger.error(err);
                        }

                        setTimeout(function() {

                            C.processJobs();
                        }, random(5000, 20000));
                    });
                });

            }
            else {
                C.processWebsite();
            }
        });
    }
};

emitter.extend(C);

module.exports = {
    Coordinator: C,
};
