const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const sassMiddleware = require('node-sass-middleware');
const EVENTS = require('./events/interface');
const GlobalEmitter = require('./events/eventEmitter');
const Coordinator = require('./Coordinator').Coordinator;

require('dotenv').config();

let app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(cookieParser());
app.use(sassMiddleware({
    src: path.join(__dirname, 'public'),
    dest: path.join(__dirname, 'public'),
    indentedSyntax: false, // true = .sass and false = .scss
    sourceMap: true
}));
app.use(express.static(path.join(__dirname, 'public')));

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});


// Routes
// -------------------------------
require('./routes')(app);

// error handler
// -------------------------------
require('./app/errorHandling')(app);

// db connection
// -------------------------------
require('./db');

GlobalEmitter.on(EVENTS.DB.CONNECTED, function () {
    
    Coordinator.start();
    
});

module.exports = app;
