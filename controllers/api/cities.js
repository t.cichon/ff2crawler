const messages = require("../../api/messages");
const mergeWith = require("lodash/mergeWith");
const each = require("lodash/each");

const Provider = require('./../../models/provider');

exports.get = function (req, res) {
    
    Provider.find({}).exec(function (err, provider) {
        let cities = [];
        
        if (!err && provider) {
            
            each(provider, function(prv) {
                
                if (prv) {
                    
                    each(prv.city, function (city) {
                        cities.push({
                            providerId: prv._id,
                            cityId: city._id
                        });
                    });
                }
            });
        }
        
        if (cities.length) {
            res.json(mergeWith(messages.api.OK, {
                cities: cities
            }));
        }
        else {
            res.json(messages.api.ERROR);
        }

    });
};
