const messages = require("../../api/messages");
const Apartment = require('./../../models/apartment');

exports.get = function (req, res) {
    
    // single apartment
    if (req.query.id) {
        Apartment.find({_id: req.query.id}).exec(function (err, apartment) {
            if (!err && apartment && apartment.length) {
                res.json(apartment[0]);
            }
            else {
                res.json(messages.api.ERROR);
            }
        });
    }
    else {
        res.json(messages.api.ERROR);
    }
     
};
