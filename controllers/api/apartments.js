const messages = require("../../api/messages");
const Apartment = require('./../../models/apartment');

exports.get = function (req, res) {
    let query = {};
    let options = {
        page: req.query.page || 1,
        limit: (req.query.limit ? parseInt(req.query.limit) : 20), // TODO prevent calling limit with like 99999999
        sort: {
            createdAt: -1
        },
    };
    
    // cityId
    if (req.query.cityId) {
        query.cityId = req.query.cityId;
    }
    // providerId
    if (req.query.providerId) {
        query.providerId = req.query.providerId;
    }
    // room filter
    if (req.query.minRooms && req.query.maxRooms) {
        query.rooms = {
            $gte: parseInt(req.query.minRooms),
            $lte: parseInt(req.query.maxRooms)
        }
    }
    // price filter
    if (req.query.minPrice && req.query.maxPrice) {
        query["price.cold"] = {
            $gte: parseInt(req.query.minPrice),
            $lte: parseInt(req.query.maxPrice)
        }
    }
    // size filter
    if (req.query.minSize && req.query.maxSize) {
        query.size = {
            $gte: parseInt(req.query.minSize),
            $lte: parseInt(req.query.maxSize)
        }
    }
    // feature filter
    if (req.query.features) {
        let feats = req.query.features.split(',');
        let regexFeats = [];
        
        for (let i=0; i<feats.length; i++) {
            regexFeats.push(new RegExp(feats[i], 'i'));
        }
        
        query.features = {
            $all: regexFeats
        }
    }
    // number of days to look back
    let daysToLookBack = req.query.days ? parseInt(req.query.days) : 7;
    query.createdAt = { $gte: new Date((new Date().getTime() - (daysToLookBack * 24 * 60 * 60 * 1000))) };
    
    Apartment.paginate(query, options, function (err, apartments) {
        if (err) {
            console.log(err);
            res.json(messages.api.ERROR);
        }
        
        res.json(apartments);

    });
    
};
