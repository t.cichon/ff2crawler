/* global module */

const mongoose = require('mongoose');
const modelName = 'statistics';
const logger = require('./../service/logger');

require('dotenv').config();

let Settings = mongoose.model(modelName, new mongoose.Schema({
    _id: String,
    nextCrawl: Number,
    
}, {
    timestamps: false,
    toJSON: {
        virtuals: true
    }
}));

module.exports = Settings;
