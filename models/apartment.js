/* global module */

const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');
const id = require('shortid');
const modelName = 'apartment';
const logger = require('./../service/logger');

require('dotenv').config();

let Apartment = mongoose.model(modelName, new mongoose.Schema({
    _id: {
        type: String,
        default: id.generate
    },
    providerId: String,
    cityId: String,
    link: String,
    title: String,
    description: String,
    price: {
        total: Number,
        cold: Number,
        utilities: Number
    },
    rooms: {
        type: Number,
        default: null
    },
    size: {
        type: Number,
        default: null
    },
    images: [{
        url: String,
        caption: String
    }],
    location: {
        address: {
            type: String,
            default: null
        },
        city: {
            type: String,
            default: null
        },
        street: {
            type: String,
            default: null
        },
        houseNumber: {
            type: String,
            default: null
        },
        postcode: {
            type: String,
            default: null
        },
        suburb: {
            type: String,
            default: null
        },
        neighbourhood: {
            type: String,
            default: null
        },
        lat: {
            type: String,
            default: null
        },
        lng: {
            type: String,
            default: null
        }
    },
    features: Array,
    freeFrom: {
        type: String,
        default: null
    },
    petsAllowed: {
        type: String,
        default: null
    },
    buildYear: {
        type: Number,
        default: null
    },
}, {
    timestamps: true,
    toJSON: {
        virtuals: true
    }
}).plugin(mongoosePaginate));

module.exports = Apartment;
