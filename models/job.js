/* global module */

let mongoose = require('mongoose');
var mongoosePaginate = require('mongoose-paginate');
let id = require('shortid');
let modelName = 'job';

let schemaSettings = {
    timestamps: true
};

let Job = mongoose.model(modelName, new mongoose.Schema({
    _id: {
        type: String,
        'default': id.generate
    },
    providerId: String,
    cityId: String,
    link: String,
    processed: {
        type: Boolean,
        'default': false
    }
}, schemaSettings).plugin(mongoosePaginate));

module.exports = Job;
