/* global module */

let mongoose = require('mongoose');
let id = require('shortid');
let modelName = 'provider';

let Provider = mongoose.model(modelName, new mongoose.Schema({
    _id: {
        type: String,
        'default': id.generate
    },
    name: String,
    isActive: Boolean,
    selector: String,
    baseUrl: String,
    city: [{
        _id: String,
        name: String,
        url: String,
        isActive: Boolean,
        crawlInterval: Number,
        lastCrawl: Date,
        apartmentsFound: Number
    }]
    
}, {
    timestamps: false,
    toJSON: {
        virtuals: true
    }
}));

module.exports = Provider;
