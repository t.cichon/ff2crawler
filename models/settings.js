/* global module */

const mongoose = require('mongoose');
const modelName = 'settings';

require('dotenv').config();

let Settings = mongoose.model(modelName, new mongoose.Schema({
    _id: modelName,
    
}, {
    timestamps: false,
    toJSON: {
        virtuals: true
    }
}));

module.exports = Settings;
